import 'package:final_project_ui/data/girl.dart';
import 'package:final_project_ui/themes/colors.dart';
import 'package:flutter/material.dart';

class ArgsChatScreen {
  final String name;
  final String imageUrl;

  ArgsChatScreen(this.name, this.imageUrl);
}

class ChatScreen extends StatefulWidget {
  static const String routName = 'chat-screen';

  final String name;
  final String imageUrl;

  const ChatScreen({
    Key key,
    @required this.name,
    @required this.imageUrl,
  }) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  void _setting() {}

  void _uploadImage() {}

  void _sendText() {}

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBody(size),
      appBar: getAppBar(),
    );
  }

  getBody(Size size) {
    return Container(
      color: MyAppTheme.secondary,
      child: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: girls.length,
              itemBuilder: (context, index) {
                return Container();
              },
            ),
          ),
          Container(
            height: 80,
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.image,
                    color: MyAppTheme.primary,
                  ),
                  highlightColor: MyAppTheme.primary,
                  splashColor: MyAppTheme.primary,
                  onPressed: _uploadImage,
                ),
                Expanded(
                  child: Container(
                    height: 36,
                    child: TextField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          contentPadding: EdgeInsets.only(
                            left: 20,
                            right: 10,
                          ),
                          hintText: 'Aa ...'),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send, color: MyAppTheme.primary,),
                  onPressed: _sendText,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  getAppBar() {
    return AppBar(
      backgroundColor: MyAppTheme.secondary,
      iconTheme: IconThemeData(color: MyAppTheme.primary),
      title: Row(
        children: [
          CircleAvatar(
            backgroundImage: AssetImage(widget.imageUrl),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            widget.name,
            style: TextStyle(
              color: MyAppTheme.primary,
            ),
          ),
        ],
      ),
      elevation: 0,
      actions: [
        IconButton(
          icon: Icon(Icons.settings),
          onPressed: _setting,
        ),
      ],
    );
  }
}
