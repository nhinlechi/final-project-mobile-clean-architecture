import 'package:final_project_ui/themes/colors.dart';
import 'package:flutter/material.dart';

class RegisterWithEmailTab extends StatefulWidget {
  @override
  _RegisterWithEmailTabState createState() => _RegisterWithEmailTabState();
}

class _RegisterWithEmailTabState extends State<RegisterWithEmailTab> {
  String _email;
  String _password;

  void _registerNewAccount() async {

  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: 'Email',
              contentPadding: EdgeInsets.all(15),
              labelText: 'Email',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (value) {
              _email = value;
            },
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'Password',
              contentPadding: EdgeInsets.all(15),
              labelText: 'Password',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (value) {
              _password = value;
            },
          ),
          Expanded(child: Container()),
          Container(
            width: size.width,
            height: 50,
            child: ElevatedButton(
              onPressed: _registerNewAccount,
              child: Text('Continue'),
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
              ),
            ),
          ),
          SizedBox(height: 20,),
        ],
      ),
    );
  }
}
