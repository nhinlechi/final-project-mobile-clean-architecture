import 'package:final_project_ui/providers/auth_provider.dart';
import 'package:final_project_ui/screens/register_screens/register_screen.dart';
import 'package:final_project_ui/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class LoginWithEmailTab extends StatefulWidget {
  @override
  _LoginWithEmailTabState createState() => _LoginWithEmailTabState();
}

class _LoginWithEmailTabState extends State<LoginWithEmailTab> {
  String _email;
  String _password;

  void _submit() async {
    try {
      await Provider.of<AuthProvider>(context, listen: false).login(
        username: _email,
        password: _password,
      );
      Navigator.push(
        context,
        PageTransition(
          child: RegisterScreen(),
          type: PageTransitionType.rightToLeft,
        ),
      );
    } on Exception catch (e) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text(e.toString()),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: 'Email',
              contentPadding: EdgeInsets.all(15),
              labelText: 'Email',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (value) {
              _email = value;
            },
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'Password',
              contentPadding: EdgeInsets.all(15),
              labelText: 'Password',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (value) {
              _password = value;
            },
          ),
          Align(
            alignment: Alignment.centerRight,
            child: TextButton(
              onPressed: () {},
              child: Text(
                'Forgot password?',
                style: TextStyle(
                  color: MyAppTheme.primary,
                  fontWeight: FontWeight.bold,
                ),
              ),
              style: TextButton.styleFrom(
                primary: MyAppTheme.primary,
                padding: EdgeInsets.only(
                  left: 20,
                ),
              ),
            ),
          ),
          Expanded(child: Container()),
          Container(
            width: size.width,
            height: 50,
            child: ElevatedButton(
              onPressed: _submit,
              child: Text('Continue'),
              style: ElevatedButton.styleFrom(
                primary: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
