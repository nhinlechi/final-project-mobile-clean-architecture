import 'package:final_project_ui/themes/text_style.dart';
import 'package:final_project_ui/widgets/my_app_bar.dart';

import '../themes/colors.dart';
import 'package:flutter/material.dart';

class StrangerFilterScreen extends StatefulWidget {
  static const routeName = 'stranger-filter-screen';

  @override
  _StrangerFilterScreenState createState() => _StrangerFilterScreenState();
}

class _StrangerFilterScreenState extends State<StrangerFilterScreen> {
  List<bool> _genderSelected = [true, false, false];
  final List<String> _genderTexts = ['Men', 'Women', 'Others'];
  double _minAge = 18;
  double _maxAge = 80;
  double _maxDistance = 10; // Km
  List<bool> _relationshipSelected = [true, false, false];
  final List<String> _relationshipTexts = ['In Relation', 'Alone', 'Divorce'];

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: MyAppBar(
        title: Text(
          'Filters',
          style: MyTextStyle.ts_AppBar_title,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 20, top: 20, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'I\'m interested in',
              style: MyTextStyle.ts_filter_title,
            ),
            SizedBox(
              height: 10,
            ),
            ToggleButtons(
              direction: Axis.horizontal,
              children: List.generate(_genderSelected.length, (index) {
                return Container(
                  width: (size.width - 40) / _genderSelected.length,
                  alignment: Alignment.center,
                  child: Text(_genderTexts[index]),
                );
              }),
              borderRadius: BorderRadius.circular(10),
              fillColor: MyAppTheme.primary,
              textStyle: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
              selectedColor: Colors.white,
              isSelected: _genderSelected,
              onPressed: (index) {
                for (int i = 0; i < _genderSelected.length; i++) {
                  _genderSelected[i] = false;
                }
                setState(() {
                  _genderSelected[index] = !_genderSelected[index];
                });
              },
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Age range',
                  style: MyTextStyle.ts_filter_title,
                ),
                Text(
                  '${_minAge.toInt()} - ${_maxAge.toInt()}',
                  style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RangeSlider(
              values: RangeValues(_minAge, _maxAge),
              min: 18,
              max: 80,
              onChanged: (value) {
                setState(() {
                  _minAge = value.start;
                  _maxAge = value.end;
                });
              },
              activeColor: MyAppTheme.primary,
            ),
            SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Distance (km)',
                  style: MyTextStyle.ts_filter_title,
                ),
                Text(
                  '0 - ${_maxDistance.toInt()}',
                  style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
                ),
              ],
            ),
            Slider(
              value: _maxDistance,
              onChanged: (value) {
                setState(() {
                  _maxDistance = value;
                });
              },
              min: 1,
              max: 10,
              divisions: 9,
              label: '${_maxDistance.toInt()} km',
              activeColor: MyAppTheme.primary,
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              'Relationship',
              style: MyTextStyle.ts_filter_title,
            ),
            SizedBox(
              height: 10,
            ),
            ToggleButtons(
              direction: Axis.horizontal,
              children: List.generate(_relationshipSelected.length, (index) {
                return Container(
                  padding: EdgeInsets.symmetric(horizontal: 5),
                  width: (size.width - 40) / _relationshipSelected.length,
                  alignment: Alignment.center,
                  child: Text(_relationshipTexts[index]),
                );
              }),
              borderRadius: BorderRadius.circular(10),
              fillColor: MyAppTheme.primary,
              textStyle: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
              selectedColor: Colors.white,
              isSelected: _relationshipSelected,
              onPressed: (index) {
                for (int i = 0; i < _relationshipSelected.length; i++) {
                  _relationshipSelected[i] = false;
                }
                setState(() {
                  _relationshipSelected[index] = !_relationshipSelected[index];
                });
              },
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
      bottomSheet: BottomAppBar(
        child: Container(
          padding: EdgeInsets.all(20),
          height: 80,
          width: size.width,
          child: ElevatedButton(
            child: Text(
              'Save',
            ),
            style: ButtonStyle(
              shape: MaterialStateProperty.resolveWith<OutlinedBorder>(
                (states) {
                  return RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  );
                },
              ),
              backgroundColor: MaterialStateProperty.resolveWith(
                  (states) => MyAppTheme.primary),
            ),
            onPressed: () {},
          ),
        ),
      ),
    );
  }
}
