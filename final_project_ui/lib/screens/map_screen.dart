import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../themes/colors.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _mapController = Completer();
  LocationPermission permission;
  LatLng _myPosition;

  Future<dynamic> showCustomDialog() async {
    return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Enable location services'),
        content: Text('Enable location service to use this feature'),
        backgroundColor: MyAppTheme.secondary,
        actions: [
          ElevatedButton(
            child: Text('OK'),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(MyAppTheme.primary),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled = false;

    // Test location service are enabled
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await showCustomDialog();
      return Future.error('Location services are enabled');
    }

    // Test permission
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    // Get current location
    var pos = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _myPosition = LatLng(pos.latitude, pos.longitude);
    });
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(pos.latitude, pos.longitude),
          zoom: 20,
        ),
      ),
    );
    print(pos);
    return pos;
  }

  Future<LatLng> _requestLastLocation() async {
    bool serviceEnabled = false;
    LocationPermission permission;

    // Test location service are enabled
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await showCustomDialog();
      return Future.error('Location services are enabled');
    }

    // Test permission
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    final pos = await Geolocator.getLastKnownPosition();
    return LatLng(pos.latitude, pos.longitude);
  }

  final _initialLatLng = LatLng(10.8231, 106.6297); // Ho Chi Minh City Lat Long

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          getMapView(),
          getConfigLocation(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'find-people-button',
        child: Icon(Icons.person_search),
        backgroundColor: MyAppTheme.primary,
        onPressed: () {},
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  getMapView() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: _initialLatLng,
        zoom: 13,
      ),
      onMapCreated: (controller) {
        _mapController.complete(controller);
      },
      markers: <Marker>{
        if (_myPosition != null)
          Marker(
            markerId: MarkerId('m1'),
            position: _myPosition,
            icon: BitmapDescriptor.defaultMarkerWithHue(
                BitmapDescriptor.hueOrange),
          ),
      },
      trafficEnabled: true,
      mapToolbarEnabled: true,
    );
  }

  getConfigLocation() {
    return SafeArea(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: MyAppTheme.secondary,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(30),
                            ),
                          ),
                          hintText: 'Your Location',
                          contentPadding: EdgeInsets.symmetric(
                            horizontal: 20,
                            vertical: 5,
                          ),
                          fillColor: MyAppTheme.secondary,
                          focusColor: MyAppTheme.primary,
                          filled: true,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      onTap: _determinePosition,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: MyAppTheme.primary,
                        ),
                        child: Icon(Icons.my_location_rounded),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Expanded(child: Container()),
        ],
      ),
    );
  }
}
