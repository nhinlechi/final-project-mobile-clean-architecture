import 'dart:async';

import '../models/matching_info.dart';
import '../providers/matching_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';

import 'package:provider/provider.dart';

class CountdownScreen extends StatefulWidget {
  final int timeRemaining;

  const CountdownScreen({Key key, @required this.timeRemaining})
      : super(key: key);

  @override
  _CountdownScreenState createState() => _CountdownScreenState();
}

class _CountdownScreenState extends State<CountdownScreen>
    with TickerProviderStateMixin {
  AnimationController _animController;
  bool _isLoading = false;

  String get timerString {
    Duration duration = _animController.duration * _animController.value;
    return '${duration.inMinutes}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  void _acceptMatching() async {
    setState(() {
      _isLoading = true;
    });
    final matchingProvider =
        Provider.of<MatchingProvider>(context, listen: false);
    final matchId = matchingProvider.matchId;
    await matchingProvider.confirmMatching(matchId, true);
    //Navigator.of(context).pop(true);
  }

  void _cancelMatching() async {
    final matchingProvider =
        Provider.of<MatchingProvider>(context, listen: false);
    final matchId = matchingProvider.matchId;
    print(matchId);
    await matchingProvider.confirmMatching(matchId, false);
    Navigator.of(context).pop(false);
  }

  @override
  void initState() {
    super.initState();
    _animController = AnimationController(
      vsync: this,
      duration: Duration(seconds: widget.timeRemaining),
    );
    _animController.reverse(from: 1.0);
    _animController.addStatusListener((status) {
      print(status);
      if (status == AnimationStatus.dismissed) {
        Navigator.of(context).pop(false);
      }
    });
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: getBody(),
    );
  }

  getBody() {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            child: Align(
              alignment: FractionalOffset.center,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: AnimatedBuilder(
                        animation: _animController,
                        builder: (context, child) {
                          return CustomPaint(
                            painter: TimerPainter(
                              animation: _animController,
                              backgroundColor: Colors.grey.withOpacity(0.2),
                              progressColor: Colors.orangeAccent,
                            ),
                          );
                        },
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.center,
                      child: FutureBuilder(
                        future: Provider.of<MatchingProvider>(context,
                                listen: false)
                            .getMatchingInfo(),
                        builder: (context, matchingInfoSnapshot) {
                          if (matchingInfoSnapshot.connectionState ==
                              ConnectionState.done)
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      (matchingInfoSnapshot.data
                                              as MatchingInfo)
                                          .avatar),
                                  radius: 50,
                                ),
                                Text(
                                  (matchingInfoSnapshot.data as MatchingInfo)
                                      .name,
                                  style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.orangeAccent,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                AnimatedBuilder(
                                  animation: _animController,
                                  builder: (context, child) {
                                    return Text(
                                      timerString,
                                      style: TextStyle(
                                        fontSize: 60,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.orangeAccent,
                                      ),
                                    );
                                  },
                                )
                              ],
                            );
                          return Container(
                            child: Text('Loading'),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : getConfirmButton(),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  getConfirmButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        FloatingActionButton(
          onPressed: _cancelMatching,
          backgroundColor: Colors.red,
          heroTag: 'floating-button-1',
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
        FloatingActionButton(
          onPressed: _acceptMatching,
          backgroundColor: Colors.orangeAccent,
          heroTag: 'floating-button-2',
          child: Icon(
            Icons.done_outline,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class TimerPainter extends CustomPainter {
  final Animation<double> animation;
  final Color backgroundColor;
  final Color progressColor;

  TimerPainter(
      {@required this.animation,
      @required this.backgroundColor,
      @required this.progressColor})
      : super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    // Draw circle
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 10.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);

    // Draw progress bar
    paint.color = progressColor;
    final progress = (1.0 - animation.value) * 2.0 * pi;
    canvas.drawArc(Offset.zero & size, pi * 3 / 2, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        progressColor != old.progressColor ||
        backgroundColor != old.backgroundColor;
  }
}
