import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ArgsPlaceMapScreen {
  final double latitude;
  final double longitude;

  ArgsPlaceMapScreen(this.latitude, this.longitude);
}

class PlaceMapScreen extends StatelessWidget {
  static const routeName = 'place-map-screen';

  final double latitude;
  final double longitude;

  final Completer<GoogleMapController> _mapController = Completer();

  PlaceMapScreen({Key key,@required this.latitude,@required this.longitude}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(this.latitude, this.longitude),
          zoom: 13,
        ),
        onMapCreated: (controller) {
          _mapController.complete(controller);
        },
        markers: <Marker>{
          Marker(
            markerId: MarkerId('m1'),
            position: LatLng(this.latitude, this.longitude),
            icon:
                BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
          ),
        },
        mapToolbarEnabled: true,
      ),
    );
  }
}
