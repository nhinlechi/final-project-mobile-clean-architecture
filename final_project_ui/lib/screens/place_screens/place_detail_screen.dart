import 'package:final_project_ui/data/girl.dart';
import 'package:final_project_ui/screens/place_screens/place_map_screen.dart';
import 'package:final_project_ui/themes/colors.dart';
import 'package:final_project_ui/themes/text_style.dart';
import 'package:final_project_ui/widgets/circle_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

// This useful for pass argument (data) to this screen;
class ArgsPlaceDetailScreen {
  final String id;
  final String imageUrl;

  ArgsPlaceDetailScreen(this.id, this.imageUrl);
}

class PlaceDetailScreen extends StatelessWidget {
  static const routeName = 'place-detail-screen';

  final String id;
  final String imageUrl;

  const PlaceDetailScreen({Key key, @required this.id, @required this.imageUrl})
      : super(key: key);

  void _goToMap(BuildContext context) {
    Navigator.of(context).pushNamed(
      PlaceMapScreen.routeName,
      arguments: ArgsPlaceMapScreen(
        10.8231,
        106.6297,
      ),
    );
  }

  void _reviewPlace() {}

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final double placeImageHeight = size.height * 2 / 5;
    final double sizedBoxHeight = placeImageHeight - 30;
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final double backButtonContainerHeight = 30;
    return Scaffold(
      body: Container(
        height: size.height,
        child: Stack(
          children: [
            Hero(
              tag: this.id,
              child: Container(
                height: placeImageHeight,
                width: size.width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(this.imageUrl), fit: BoxFit.cover)),
              ),
            ),
            SingleChildScrollView(
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: statusBarHeight,
                      ),
                      Container(
                        height: backButtonContainerHeight,
                        alignment: Alignment.centerLeft,
                        child: CircleButton(
                          icon: Icons.arrow_back,
                          iconColor: MyAppTheme.secondary,
                          backgroundColor:
                              MyAppTheme.secondary.withOpacity(0.4),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(
                        height: sizedBoxHeight -
                            statusBarHeight -
                            backButtonContainerHeight,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(20)),
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 40,
                            bottom: 20,
                            right: 20,
                            left: 20,
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  LimitedBox(
                                    maxWidth: size.width * 3 / 5,
                                    child: Text(
                                      'San Fu Lou 6 – Cantonese Kitchen - Lê Thánh Tôn asdfasdfsadfasdfasd',
                                      style: TextStyle(
                                        fontSize: MyTextStyle.title_4_fontsize,
                                      ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        '4.3',
                                        style: TextStyle(
                                          fontSize:
                                              MyTextStyle.title_4_fontsize,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Icon(
                                        Icons.star,
                                        color: MyAppTheme.primary,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Reviews',
                                style: TextStyle(
                                  fontSize: MyTextStyle.title_4_fontsize,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Divider(),
                              Column(
                                children: List.generate(girls.length, (index) {
                                  return ListTile(
                                    contentPadding: EdgeInsets.only(bottom: 10),
                                    leading: CircleAvatar(
                                      backgroundImage:
                                          AssetImage(girls[index]['img']),
                                    ),
                                    title: Text(
                                      girls[index]['name'],
                                      style: TextStyle(
                                          fontSize:
                                              MyTextStyle.title_5_fontsize,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: List.generate(
                                            5,
                                            (index) => Icon(
                                              Icons.star,
                                              size: 14,
                                              color: MyAppTheme.primary,
                                            ),
                                          ),
                                        ),
                                        Text(girls[index]['text']),
                                      ],
                                    ),
                                    trailing: Icon(Icons.more_vert),
                                  );
                                }),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    top: sizedBoxHeight - 25,
                    right: 20,
                    child: Row(
                      children: [
                        CircleButton(
                          onTap: _reviewPlace,
                          icon: Icons.edit,
                          iconColor: MyAppTheme.primary,
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CircleButton(
                          onTap: () => _goToMap(context),
                          icon: Icons.location_on,
                          iconColor: Colors.white,
                          backgroundColor: MyAppTheme.primary,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
