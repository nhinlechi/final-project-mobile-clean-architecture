import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

import '../../themes/colors.dart';

class RegisterGenderPage extends StatefulWidget {
  @override
  _RegisterGenderPageState createState() => _RegisterGenderPageState();
}

class _RegisterGenderPageState extends State<RegisterGenderPage> {
  //
  List<bool> _genderSelected = [true, false, false];
  final List<String> _genderTexts = ['Men', 'Women', 'Others'];

  //
  List<bool> _relationshipSelected = [true, false, false];
  final List<String> _relationshipTexts = ['In Relation', 'Alone', 'Divorce'];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'You are...?',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 10,
          ),
          ToggleButtons(
            direction: Axis.horizontal,
            children: List.generate(_genderSelected.length, (index) {
              return Container(
                width: (size.width - 50) / _genderSelected.length,
                alignment: Alignment.center,
                child: Text(_genderTexts[index]),
              );
            }),
            borderRadius: BorderRadius.circular(10),
            fillColor: MyAppTheme.primary,
            textStyle: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
            selectedColor: Colors.white,
            isSelected: _genderSelected,
            onPressed: (index) {
              for (int i = 0; i < _genderSelected.length; i++) {
                _genderSelected[i] = false;
              }
              setState(() {
                _genderSelected[index] = !_genderSelected[index];
              });
            },
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'And your relationship?',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 10,
          ),
          ToggleButtons(
            direction: Axis.horizontal,
            children: List.generate(_relationshipSelected.length, (index) {
              return Container(
                padding: EdgeInsets.symmetric(horizontal: 5),
                width: (size.width - 50) / _relationshipSelected.length,
                alignment: Alignment.center,
                child: Text(_relationshipTexts[index]),
              );
            }),
            borderRadius: BorderRadius.circular(10),
            fillColor: MyAppTheme.primary,
            textStyle: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
            selectedColor: Colors.white,
            isSelected: _relationshipSelected,
            onPressed: (index) {
              for (int i = 0; i < _relationshipSelected.length; i++) {
                _relationshipSelected[i] = false;
              }
              setState(() {
                _relationshipSelected[index] = !_relationshipSelected[index];
              });
            },
          ),
        ],
      ),
    );
  }
}
