import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

class RegisterFavoritesPage extends StatefulWidget {
  @override
  _RegisterFavoritesPageState createState() => _RegisterFavoritesPageState();
}

class _RegisterFavoritesPageState extends State<RegisterFavoritesPage> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'You Like what...?',
            style: MyTextStyle.ts_register_title,
          ),
          Wrap(
            spacing: 10,
            children: [
              ActionChip(
                label: Text(
                  'Play Guitar',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
              ActionChip(
                label: Text(
                  'Pet',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
              ActionChip(
                label: Text(
                  'Music',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
              ActionChip(
                label: Text(
                  'Soccer',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
              ActionChip(
                label: Text(
                  'Three some',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
              ActionChip(
                label: Text(
                  'Play Video Game',
                  style: MyTextStyle.ts_chip,
                ),
                padding: EdgeInsets.all(10),
                onPressed: () {},
              ),
            ],
          )
        ],
      ),
    );
  }
}
