import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../pages/root_page.dart';
import '../../themes/colors.dart';
import 'register_birth_date_page.dart';
import 'register_favorites_page.dart';
import 'register_gender_page.dart';
import 'register_name_page.dart';
import 'register_photo_page.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  CarouselController _buttonCarouselController = CarouselController();
  final int _maxPages = 5;
  int _pageIndex = 0;

  void _onPressBack() async {
    if (_pageIndex > 0) {
      setState(() {
        _pageIndex--;
      });
      await _buttonCarouselController.animateToPage(
        _pageIndex,
        duration: Duration(milliseconds: 300),
      );
    }
  }

  void _onPressNext() async {
    if (_pageIndex < _maxPages - 1) {
      setState(() {
        _pageIndex++;
      });
      await _buttonCarouselController.animateToPage(
        _pageIndex,
        duration: Duration(milliseconds: 300),
      );
    } else {
      _loginSuccess();
    }
  }

  void _loginSuccess() async {
    Navigator.of(context).pushReplacement(PageTransition(
        child: RootPage(), type: PageTransitionType.rightToLeft));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: PageView(
          physics: new NeverScrollableScrollPhysics(),
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(20),
                    child: Row(
                      children: List.generate(_maxPages, (index) {
                        return Container(
                          margin: EdgeInsets.only(right: 16),
                          height: 10,
                          width: 10,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: index <= _pageIndex
                                ? MyAppTheme.primary
                                : MyAppTheme.grey,
                          ),
                        );
                      }),
                    ),
                  ),
                  CarouselSlider(
                    items: [
                      RegisterNamePage(),
                      RegisterBirthDatePage(),
                      RegisterGenderPage(),
                      RegisterPhotoPage(),
                      RegisterFavoritesPage(),
                    ],
                    carouselController: _buttonCarouselController,
                    options: CarouselOptions(
                      autoPlay: false,
                      enlargeCenterPage: true,
                      viewportFraction: 0.9,
                      aspectRatio: size.width / (size.height - 100),
                      // TODO: Refactor this code line to flexible height
                      initialPage: 0,
                      scrollPhysics: NeverScrollableScrollPhysics(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Padding(
        padding: EdgeInsets.only(left: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              heroTag: 'fab-back',
              child: Icon(Icons.arrow_back_ios_rounded, color: Colors.white,),
              backgroundColor: MyAppTheme.primary,
              onPressed: _onPressBack,
            ),
            FloatingActionButton(
              heroTag: 'fab-next',
              child: Icon(Icons.arrow_forward_ios_rounded, color: Colors.white,),
              backgroundColor: MyAppTheme.primary,
              onPressed: _onPressNext,
            )
          ],
        ),
      ),
    );
  }
}
