import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

import '../../themes/colors.dart';

class RegisterPhotoPage extends StatefulWidget {
  @override
  _RegisterPhotoPageState createState() => _RegisterPhotoPageState();
}

class _RegisterPhotoPageState extends State<RegisterPhotoPage> {
  void _onTapAddProfilePhoto() async {}

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'We are already your fan!',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: _onTapAddProfilePhoto,
            child: Container(
              width: size.width / 2,
              height: size.width / 2 * 4 / 3,
              decoration: BoxDecoration(
                color: MyAppTheme.grey,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FloatingActionButton(
                    onPressed: () {},
                    child: Icon(Icons.add, color: Colors.white,),
                    backgroundColor: MyAppTheme.primary,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Add your profile photo',
                    style: TextStyle(
                      color: Colors.black.withOpacity(0.5),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
