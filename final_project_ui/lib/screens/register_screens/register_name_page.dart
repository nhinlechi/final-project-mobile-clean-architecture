import 'package:final_project_ui/themes/colors.dart';
import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

class RegisterNamePage extends StatefulWidget {
  @override
  _RegisterNamePageState createState() => _RegisterNamePageState();
}

class _RegisterNamePageState extends State<RegisterNamePage> {
  String _firstName;
  String _lastName;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Hey there!',
            style: MyTextStyle.ts_register_title,
          ),
          Text(
            'What\'s your name?',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'First Name',
              contentPadding: EdgeInsets.all(15),
              labelText: 'First Name',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: 'Last Name',
              labelText: 'Last Name',
              contentPadding: EdgeInsets.all(15),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
