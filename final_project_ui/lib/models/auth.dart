class Auth {
  final String token;
  final String firstName;

  Auth({this.token, this.firstName});

  factory Auth.fromJson(Map<String, dynamic> json) {
    return Auth(
      token: json['data']['token'],
      firstName: json['data']['first_name'],
    );
  }
}
