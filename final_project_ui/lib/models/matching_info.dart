class MatchingInfo {
  final String name;
  final String avatar;
  final String matchId;

  MatchingInfo({this.name, this.avatar, this.matchId});

  factory MatchingInfo.fromJson(Map<String, dynamic> json) {
    return MatchingInfo(
      name: json['data']['strangerinfor']['firstname'],
      avatar: json['data']['strangerinfor']['avatar'],
      matchId: json['data']['matched']['_id'],
    );
  }
}
