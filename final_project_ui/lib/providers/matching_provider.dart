import 'package:flutter/cupertino.dart';

import '../models/matching_info.dart';
import '../network_module/HttpClient.dart';
import '../network_module/api_path.dart';

class MatchingProvider with ChangeNotifier {
  String _matchId;
  bool _isRequestingMatch = false;

  get matchId {
    return _matchId;
  }

  get isRequestingMatch {
    return _isRequestingMatch;
  }

  void startRequestMatch() {
    _isRequestingMatch = true;
    notifyListeners();
  }

  void cancelRequestMatch() {
    _isRequestingMatch = false;
    notifyListeners();
  }

  Future<void> findPeople(double lat, double long) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.matching);
      final response = await HttpClient.instance.postData(apiPath, {
        'lat': lat,
        'long': long,
      });
    } on Exception catch (e) {
      print(e);
    }
  }

  Future<void> sendFirebaseToken(String firebaseToken) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.send_firebase_token);
      final response = await HttpClient.instance.postData(apiPath, {
        'tokenid': firebaseToken,
      });
    } on Exception catch (e) {
      print('Send Firebase Token error: ' + e.toString());
    }
  }

  Future<MatchingInfo> getMatchingInfo() async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.get_matching_info);
      final response = await HttpClient.instance.fetchData(apiPath);
      final matchingInfo = MatchingInfo.fromJson(response);
      this._matchId = matchingInfo.matchId;
      return matchingInfo;
    } on Exception catch (e) {
      print('get Matching Info error: ' + e.toString());
    }
  }

  Future<void> confirmMatching(String matchId, bool isAccepted) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.confirm_matching);
      final response = await HttpClient.instance.updateData(apiPath, {
        'matchid': matchId,
        'status': isAccepted,
      });
    } on Exception catch (e) {
      print('Confirm matching error: ' + e.toString());
    }
  }


}
