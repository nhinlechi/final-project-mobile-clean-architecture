import '../models/auth.dart';
import '../network_module/api_exceptions.dart';
import '../network_module/api_response.dart';
import '../repositories/auth_repository.dart';
import 'package:flutter/foundation.dart';

import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  Auth _auth;
  AuthRepository _authRepos;

  AuthProvider() {
    _auth = Auth(token: null, firstName: null);
    _authRepos = AuthRepository();
  }

  String get token {
    return _auth.token;
  }

  String get firstName {
    return _auth.firstName;
  }

  Future<ApiResponse> login({String username, String password}) async {
    try {
      _auth = await _authRepos.login(username: username, password: password);
      saveToken(_auth.token);
      notifyListeners();
      return ApiResponse.completed(_auth);
    } on AppException catch (e) {
      return ApiResponse.error(e.toString());
    } on Exception catch (e) {
      throw e;
    }
  }

  void logout() {
    /* _authRepos.logout();*/
    _auth = Auth(firstName: null, token: null);
    notifyListeners();
  }

  Future<void> saveToken(String token) async {
    if (token == null) return;
    final prefs = await SharedPreferences.getInstance();
    // set value
    prefs.setString('token', token);
  }
}
