import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final Function onTap;
  final IconData icon;
  final Color iconColor;
  final Color backgroundColor;

  const CircleButton({
    Key key,
    @required this.onTap,
    @required this.icon,
    @required this.iconColor,
    @required this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: this.backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.4),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, 2),
            ),
          ],
        ),
        child: Icon(
          this.icon,
          color: iconColor,
        ),
      ),
    );
  }
}
