import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../themes/colors.dart';
import '../themes/text_style.dart';

class SearchBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(right: 10),
      child: Container(
        height: 40,
        width: size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: MyAppTheme.accent,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Icon(
                Feather.search,
                size: 18,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
            Text(
              "Search",
              style: TextStyle(
                fontSize: MyTextStyle.title_5_fontsize,
                color: Colors.black.withOpacity(0.5),
              ),
            )
          ],
        ),
      ),
    );
  }
}
