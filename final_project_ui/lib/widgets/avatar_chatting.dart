import 'package:flutter/material.dart';

import '../themes/colors.dart';

class AvatarChatting extends StatelessWidget {
  final String imageUri;
  final bool isActive;

  const AvatarChatting(
      {Key key, @required this.imageUri, this.isActive = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      child: Stack(
        children: [
          CircleAvatar(
            backgroundImage: AssetImage(imageUri),
            radius: 35,
          ),
          if (isActive)
            Positioned(
              right: 0,
              bottom: 0,
              child: Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: MyAppTheme.primary,
                  border: Border.all(width: 2, color: MyAppTheme.secondary),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
