import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

class PlaceItem extends StatelessWidget {
  final String id;
  final String imageUrl;
  final String placeName;

  const PlaceItem({
    Key key,
    @required this.id,
    @required this.imageUrl,
    @required this.placeName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 14),
      child: Container(
        width: 120,
        height: 190,
        child: Column(
          children: [
            Hero(
              tag: this.id,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  height: 140,
                  width: 120,
                  child: Image.asset(
                    imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              this.placeName,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
              maxLines: 2,
              style: TextStyle(fontSize: MyTextStyle.paragraph_fontsize),
            ),
          ],
        ),
      ),
    );
  }
}
