import '../themes/colors.dart';
import '../themes/text_style.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final List<Widget> actions;
  final Widget title;

  const MyAppBar({Key key, this.actions, @required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: IconThemeData(color: MyAppTheme.primary),
      title: this.title,
      actions: this.actions,
      backgroundColor: MyAppTheme.secondary,
      elevation: 0,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(55);
}
