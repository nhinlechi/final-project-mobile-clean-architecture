import 'package:final_project_ui/screens/chat_screens/chat_screen.dart';
import 'package:final_project_ui/themes/colors.dart';
import 'package:page_transition/page_transition.dart';

import '../themes/text_style.dart';
import 'avatar_chatting.dart';
import 'package:flutter/material.dart';

class ChatItem extends StatelessWidget {
  final String chatId;
  final String imageUrl;
  final String name;
  final String newestText;
  final bool isActive;

  const ChatItem(
      {Key key,
      @required this.chatId,
      @required this.imageUrl,
      @required this.name,
      @required this.newestText,
      this.isActive = false})
      : super(key: key);

  void _openChatRoom(BuildContext context, String name, String imageUrl) async {
    Navigator.of(context).pushNamed(
      ChatScreen.routName,
      arguments: ArgsChatScreen(
        name,
        imageUrl,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: InkWell(
        onTap: () => _openChatRoom(context, this.name, this.imageUrl),
        splashColor: MyAppTheme.primary,
        child: Row(
          children: [
            AvatarChatting(
              imageUri: imageUrl,
              isActive: isActive,
            ),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.name,
                  style: TextStyle(
                      fontSize: MyTextStyle.title_5_fontsize,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  this.newestText,
                  style: TextStyle(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
