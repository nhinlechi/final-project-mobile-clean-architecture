import 'package:flutter/material.dart';

class MyAppTheme {
  static final Color primary = Colors.orangeAccent;
  static final Color accentColor = Colors.orangeAccent.withOpacity(0.5);
  static final Color secondary = Colors.white;
  static final Color accent = Colors.orangeAccent.withOpacity(0.2);
  static final Color grey = Colors.grey.withOpacity(0.5);
  static final Color black_grey = Colors.black.withOpacity(0.5);
}

