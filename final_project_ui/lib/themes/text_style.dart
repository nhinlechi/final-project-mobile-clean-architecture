import 'package:flutter/material.dart';

import 'colors.dart';

class MyTextStyle {
  static final double title_1_fontsize = 36;
  static final double title_2_fontsize = 28;
  static final double title_3_fontsize = 24;
  static final double title_4_fontsize = 18;
  static final double title_5_fontsize = 16;
  static final double paragraph_fontsize = 14;

  static final TextStyle ts_register_title = TextStyle(
    fontSize: MyTextStyle.title_1_fontsize,
    fontWeight: FontWeight.normal,
    color: Colors.black.withOpacity(0.5),
  );

  static final TextStyle ts_chip = TextStyle(
    fontSize: MyTextStyle.title_5_fontsize,
    color: Colors.black.withOpacity(0.5),
  );

  static final TextStyle ts_AppBar_title = TextStyle(
    fontSize: MyTextStyle.title_3_fontsize,
    color: MyAppTheme.primary,
  );

  static final TextStyle ts_filter_title = TextStyle(fontSize: MyTextStyle.title_4_fontsize, color: MyAppTheme.black_grey);
}
