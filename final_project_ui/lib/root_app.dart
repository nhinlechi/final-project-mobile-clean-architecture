import 'package:final_project_ui/pages/find_people_page.dart';
import 'package:final_project_ui/screens/chat_screens/chat_screen.dart';
import 'package:final_project_ui/screens/login_screens/login_with_email_screen.dart';
import 'package:final_project_ui/screens/place_screens/place_detail_screen.dart';
import 'package:final_project_ui/screens/place_screens/place_map_screen.dart';
import 'package:final_project_ui/screens/stranger_filter_screen.dart';
import 'package:final_project_ui/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'screens/login_screens/login_screen.dart';

class RootApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoginScreen(),
      theme: ThemeData(
        primaryColor: Colors.orangeAccent,
        backgroundColor: Colors.white,
        accentColor: MyAppTheme.accentColor,
      ),
      debugShowCheckedModeBanner: false,
      onGenerateRoute: routes,
    );
  }

  Route<dynamic> routes(RouteSettings settings) {
    switch (settings.name) {
      case LoginWithEmailScreen.routeName:
        return PageTransition(
          child: LoginWithEmailScreen(),
          type: PageTransitionType.bottomToTop,
        );
      case PlaceDetailScreen.routeName:
        {
          final ArgsPlaceDetailScreen args = settings.arguments;
          return PageTransition(
            child: PlaceDetailScreen(
              id: args.id,
              imageUrl: args.imageUrl,
            ),
            type: PageTransitionType.bottomToTop,
          );
        }
      case PlaceMapScreen.routeName:
        {
          final ArgsPlaceMapScreen args = settings.arguments;
          return PageTransition(
            child: PlaceMapScreen(
              latitude: args.latitude,
              longitude: args.longitude,
            ),
            type: PageTransitionType.rightToLeft,
          );
        }
      case FindPeoplePage.routeName:
        return PageTransition(
          child: FindPeoplePage(),
          type: PageTransitionType.fade,
          duration: Duration(milliseconds: 600),
        );
      case ChatScreen.routName:
        {
          final ArgsChatScreen args = settings.arguments;
          return PageTransition(
            child: ChatScreen(
              name: args.name,
              imageUrl: args.imageUrl,
            ),
            type: PageTransitionType.rightToLeft,
          );
        }
      case StrangerFilterScreen.routeName:
        return PageTransition(
          child: StrangerFilterScreen(),
          type: PageTransitionType.topToBottom,
        );
    }
  }
}
