import 'package:flutter/material.dart';

import '../data/girl.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBody(size),
    );
  }

  getBody(Size size) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          backgroundColor: MyAppTheme.primary,
          floating: true,
          pinned: true,
          snap: false,
          expandedHeight: 600,
          elevation: 0,
          actions: [
            IconButton(
              icon: Icon(Icons.add_a_photo),
              color: MyAppTheme.secondary,
              onPressed: () {},
            )
          ],
          flexibleSpace: FlexibleSpaceBar(
            background: Stack(
              children: [
                Container(
                  width: size.width,
                  child: Image.asset(
                    girls[0]['img'],
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        MyAppTheme.secondary,
                        Colors.transparent,
                        Colors.transparent,
                        Colors.transparent,
                      ],
                    ),
                  ),
                ),
              ],
            ),
            title: Text(
              'Alan Walker',
              textAlign: TextAlign.center,
            ),
            centerTitle: true,
            titlePadding: EdgeInsets.only(bottom: 15),
          ),
        ),
        SliverPadding(
          padding: EdgeInsets.only(left: 10, right: 10),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  "Thông tin cơ bản",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: MyTextStyle.title_4_fontsize),
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {},
                )
              ]),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.account_box,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("Lê Chí Nhin"),
                subtitle: Text("Họ tên"),
              ),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.attribution_outlined,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("Nam"),
                subtitle: Text("Giới tính"),
              ),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.cake,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("1 tháng 1, 1999"),
                subtitle: Text("Ngày sinh"),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  "Thông tin liên hệ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: MyTextStyle.title_4_fontsize),
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {},
                )
              ]),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.contact_phone,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("077 555 3993"),
                subtitle: Text("Điện thoại"),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  "Tình trạng mối quan hệ",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: MyTextStyle.title_4_fontsize),
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {},
                )
              ]),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.wc_outlined,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("Độc thân"),
                subtitle: Text('Hôn nhân'),
              ),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                Text(
                  "Khác",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: MyTextStyle.title_4_fontsize),
                ),
                IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {},
                )
              ]),
              ListTile(
                leading: CircleAvatar(
                  child: Icon(
                    Icons.more_horiz,
                    color: Colors.white,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
                title: Text("Không"),
                subtitle: Text("Tôn giáo"),
              ),
            ]),
          ),
        ),
      ],
    );
  }
}
