import 'package:final_project_ui/widgets/circle_button.dart';

import '../providers/matching_provider.dart';
import '../screens/stranger_filter_screen.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/my_app_bar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class FindPeoplePage extends StatefulWidget {
  static const String routeName = 'find-people-page';

  @override
  _FindPeoplePageState createState() => _FindPeoplePageState();
}

class _FindPeoplePageState extends State<FindPeoplePage> {
  var _firebaseMessaging = FirebaseMessaging.instance;

  void _startFindPeople(BuildContext ctx) async {
    final firebaseToken = await _firebaseMessaging.getToken();
    print('current firebase token: ' + firebaseToken);

    final matchingProvider =
        Provider.of<MatchingProvider>(context, listen: false);

    print('begin request math -------------------------------------');
    matchingProvider.startRequestMatch();
    print('send firebase token ------------------------------------');
    await matchingProvider.sendFirebaseToken(firebaseToken);
    print('begin find people --------------------------------------');
    await matchingProvider.findPeople(0.0, 0.0);

    Navigator.of(context).pop(true);
  }

  void _strangerFilter(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(StrangerFilterScreen.routeName);
  }

  void _cancelRequestMatching() {
    Provider.of<MatchingProvider>(context, listen: false).cancelRequestMatch();
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: Text(
          '',
          style: MyTextStyle.ts_AppBar_title,
        ),
        actions: [
          IconButton(
            icon: Icon(Feather.filter),
            color: MyAppTheme.primary,
            onPressed: () => _strangerFilter(context),
          )
        ],
      ),
      body: getBody(context),
    );
  }

  getBody(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
          child: Hero(
            tag: 'find-people-button',
            child: GestureDetector(
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: MyAppTheme.primary,
                ),
                child: Consumer<MatchingProvider>(
                  builder: (context, value, child) {
                    return value.isRequestingMatch
                        ? Center(
                            child: CircularProgressIndicator(
                              backgroundColor: MyAppTheme.secondary,
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  MyAppTheme.grey),
                            ),
                          )
                        : Icon(
                            Icons.person_search,
                            color: MyAppTheme.secondary,
                            size: 40,
                          );
                  },
                ),
              ),
              onTap: () {
                _startFindPeople(context);
              },
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          'Find friend near you now!',
          style: TextStyle(fontSize: MyTextStyle.paragraph_fontsize),
        ),
        SizedBox(
          height: 50,
        ),
        Consumer<MatchingProvider>(
          builder: (context, value, child) => value.isRequestingMatch
              ? CircleButton(
                  onTap: _cancelRequestMatching,
                  icon: Icons.clear,
                  iconColor: MyAppTheme.secondary,
                  backgroundColor: MyAppTheme.grey,
                )
              : Container(),
        ),
      ],
    );
  }
}
