import 'package:final_project_ui/screens/place_screens/place_detail_screen.dart';
import 'package:final_project_ui/widgets/place_item.dart';

import '../data/girl.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/my_app_bar.dart';
import '../widgets/search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DiscoverPage extends StatefulWidget {
  @override
  _DiscoverPageState createState() => _DiscoverPageState();
}

class _DiscoverPageState extends State<DiscoverPage> {
  void _goToPlace(String placeId, String imageUrl) async {
    Navigator.of(context).pushNamed(
      PlaceDetailScreen.routeName,
      arguments: ArgsPlaceDetailScreen(
        placeId,
        imageUrl,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: getBody(),
    );
  }

  getAppBar() {
    return MyAppBar(
      title: Text(
        'Discoveries',
        style: MyTextStyle.ts_AppBar_title,
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.filter_alt_outlined),
          iconSize: 26,
          color: MyAppTheme.primary,
          onPressed: () {},
        ),
      ],
    );
  }

  getBody() {
    return Container(
      color: MyAppTheme.secondary,
      child: Padding(
        padding: EdgeInsets.only(left: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10,
            ),
            SearchBar(),
            SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Top Place',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      girls.length,
                      (index) {
                        return InkWell(
                          onTap: () => _goToPlace(
                              'place-${index.toString()}', girls[index]['img']),
                          child: PlaceItem(
                            id: 'place-${index.toString()}',
                            imageUrl: girls[index]['img'],
                            placeName: 'San Fu Lou 6 – Cantonese Kitchen',
                          ),
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Top Group',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      girls.length,
                      (index) {
                        return Padding(
                          padding: EdgeInsets.only(right: 14),
                          child: Container(
                            width: 120,
                            child: Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(10),
                                  child: Container(
                                    height: 140,
                                    width: 120,
                                    child: Image.asset(
                                      girls[index]['img'],
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  'Guitar group',
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  style: TextStyle(
                                      fontSize: MyTextStyle.paragraph_fontsize),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
