import 'package:final_project_ui/providers/matching_provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../screens/countdown_screen.dart';
import '../themes/colors.dart';
import 'chat_page.dart';
import 'discover_page.dart';
import 'find_people_page.dart';
import 'notify_page.dart';
import 'profile_page.dart';

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  final int _chatPageIndex = 3;
  int _pageIndex = 0;

  List<Widget> _pages = [
    DiscoverPage(),
    NotifyPage(),
    Container(),
    ChatPage(),
    ProfilePage(),
  ];

  @override
  void initState() {
    super.initState();

    // TODO: Refactor this handler
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {
        print('Notify Body: ${message.notification.body}');
        print('Notify Title: ${message.notification.title}');
        final notifyTitle = message.notification.title;
        final notifyBody = message.notification.body;

        // Start countdown
        if (notifyTitle == 'request match') {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => CountdownScreen(timeRemaining: 30),
            ),
          );
        } else if (notifyTitle == 'cofirm match') {
          if (notifyBody != '1') {
            Provider.of<MatchingProvider>(context, listen: false).cancelRequestMatch();
            setState(() {
              _pageIndex = _chatPageIndex;
            });
            Navigator.of(context).pop(true);
          }
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: getBottomNavbar(),
      body: getBody(),
      floatingActionButton: FloatingActionButton(
        child: Consumer<MatchingProvider>(
          builder: (context, value, child) => value.isRequestingMatch
              ? CircularProgressIndicator(
                  backgroundColor: MyAppTheme.secondary,
                  valueColor: AlwaysStoppedAnimation<Color>(MyAppTheme.grey),
                )
              : Icon(
                  Icons.person_search,
                  color: Colors.white,
                ),
        ),
        backgroundColor: MyAppTheme.primary,
        heroTag: 'find-people-button',
        onPressed: () {
          Navigator.of(context).pushNamed(FindPeoplePage.routeName);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  getBottomNavbar() {
    List<Widget> navIcons = [
      Icon(Icons.home),
      Icon(Icons.notifications),
      null,
      Icon(Icons.chat),
      Icon(Icons.person),
    ];

    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(
          _pages.length,
          (index) {
            return index != 2
                ? IconButton(
                    icon: navIcons[index],
                    iconSize: 26,
                    color: _pageIndex == index
                        ? MyAppTheme.primary
                        : MyAppTheme.grey,
                    onPressed: () {
                      setState(
                        () {
                          _pageIndex = index;
                        },
                      );
                    },
                  )
                : SizedBox(
                    width: 50,
                  );
          },
        ),
      ),
    );
  }

  getBody() {
    return IndexedStack(
      index: _pageIndex,
      children: _pages,
    );
  }
}
