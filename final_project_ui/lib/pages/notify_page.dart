import 'package:final_project_ui/themes/text_style.dart';

import '../data/girl.dart';
import '../themes/colors.dart';
import '../widgets/my_app_bar.dart';
import 'package:flutter/material.dart';

class NotifyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: Text('Notifies', style: MyTextStyle.ts_AppBar_title,),
      ),
      body: getBody(),
    );
  }

  getBody() {
    return Container(
      color: MyAppTheme.secondary,
      child: ListView.builder(
        itemCount: girls.length,
        itemBuilder: (context, index) {
          return ListTile(
            leading: CircleAvatar(
              backgroundColor: MyAppTheme.primary,
              child: Icon(
                Icons.notifications_active,
                color: MyAppTheme.secondary,
              ),
            ),
            title: Text(girls[index]['text']),
            trailing: girls[index]['online']
                ? Container(
                    height: 10,
                    width: 10,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: MyAppTheme.primary,
                    ),
                  )
                : null,
          );
        },
      ),
    );
  }
}
