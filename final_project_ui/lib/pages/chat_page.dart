import 'package:final_project_ui/themes/text_style.dart';
import 'package:flutter/material.dart';

import '../data/girl.dart';
import '../themes/colors.dart';
import '../widgets/avatar_chatting.dart';
import '../widgets/chat_item.dart';
import '../widgets/my_app_bar.dart';
import '../widgets/search_bar.dart';

class ChatPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
        appBar: getAppBar(),
        body: getBody(context),
      ),
    );
  }

  getAppBar() {
    return MyAppBar(
      title: Text('Chats', style: MyTextStyle.ts_AppBar_title,),
      actions: [
        IconButton(
          icon: Icon(Icons.edit),
          color: MyAppTheme.primary,
          iconSize: 26,
          onPressed: () {},
        ),
        SizedBox(
          width: 10,
        ),
      ],
    );
  }

  getBody(BuildContext context) {
    return Container(
      color: MyAppTheme.secondary,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 10),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              SearchBar(),
              SizedBox(
                height: 15,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: List.generate(
                    girls.length,
                    (index) {
                      return Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            AvatarChatting(
                              imageUri: girls[index]['img'],
                              isActive: girls[index]['online'],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: 70,
                              height: 30,
                              child: Text(
                                girls[index]['name'],
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Column(
                children: List.generate(
                  girls.length,
                  (index) {
                    return ChatItem(
                      chatId: 'chat-$index',
                      imageUrl: girls[index]['img'],
                      name: girls[index]['name'],
                      isActive: girls[index]['online'],
                      newestText: girls[index]['text'],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
