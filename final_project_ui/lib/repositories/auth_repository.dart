import '../models/auth.dart';
import '../network_module/HttpClient.dart';
import '../network_module/api_path.dart';

class AuthRepository {
  Future<Auth> login({String username, String password}) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.login);
      final response = await HttpClient.instance.postData(
        apiPath,
        {
          "username": username,
          "password": password,
        },
      );
      return Auth.fromJson(response);
    } on Exception catch (e) {
      throw e;
    }
  }
}
