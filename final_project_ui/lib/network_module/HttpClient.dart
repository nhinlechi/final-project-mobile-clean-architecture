import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'api_base.dart';
import 'api_exceptions.dart';

class HttpClient {
  static final HttpClient _singleton = HttpClient();

  static HttpClient get instance => _singleton;

  Future<dynamic> fetchData(String apiPath,
      {Map<String, String> params}) async {
    var responseJson;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth-token": token
    };
    try {
      final response = await http.get(
        Uri.https(
          APIBase.baseURL,
          '/api/v1$apiPath',
          params,
        ),
        headers: header,
      );
      responseJson = _returnResponse(response);
      print('response body ${responseJson.toString()}');
      return responseJson;
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

  String queryParameters(Map<String, String> params) {
    if (params != null) {
      final jsonString = Uri(queryParameters: params);
      return '?${jsonString.query}';
    }
    return '';
  }

  Future<dynamic> postData(String apiPath, dynamic body) async {
    var responseJson;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'auth-token': token
    };
    try {
      final response = await http.post(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      print('response body ${responseJson.toString()}');
      return responseJson;
    } on Exception catch (e) {
      throw e;
    }
  }

  Future<dynamic> updateData(String apiPath, dynamic body) async {
    var responseJson;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'auth-token': token
    };
    try {
      final response = await http.patch(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      print('response body ${responseJson.toString()}');
      return responseJson;
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
      case 201:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(json.decode(response.body)['message']);
      case 401:
      case 403:
      case 409:
        throw UnauthorisedException(json.decode(response.body)['message']);
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
