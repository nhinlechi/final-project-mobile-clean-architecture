enum APIPath {
  fetch_user,
  login,
  login_facebook,
  login_google,
  check_register,
  register,
  matching,
  get_matching_info,
  send_firebase_token,
  confirm_matching,
}

class APIPathHelper{
  static String getValue(APIPath path){
    switch(path){
      case APIPath.fetch_user:
        return '/user';
      case APIPath.login:
        return '/auth/login';
      case APIPath.login_facebook:
        return '/auth/loginfacebook';
      case APIPath.login_google:
        return '/auth/logingoogle';
      case APIPath.check_register:
        return '/auth/checkregister';
      case APIPath.register:
        return '/auth/register';
      case APIPath.matching:
        return '/request-match';
      case APIPath.get_matching_info:
        return '/match';
      case APIPath.send_firebase_token:
        return '/firebase';
      case APIPath.confirm_matching:
        return '/match/confirm';
      default:
        return '';
    }
  }

}