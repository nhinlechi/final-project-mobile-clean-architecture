import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class Failure extends Equatable {
  final String message;

  Failure(this.message);

  List<Object> get props => [message];
}

class APIFailure extends Failure {
  APIFailure({@required String message}) : super(message);
}

class CacheFailure extends Failure {
  CacheFailure({String message = "Something broken"}) : super(message);
}

class NetworkFailure extends Failure {
  NetworkFailure({String message = "Waiting for network"}) : super(message);
}
