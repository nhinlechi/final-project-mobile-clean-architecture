class APIException implements Exception {
  final status;
  final message;

  APIException([this.status, this.message]);

  String toString() {
    return '$status : $message';
  }
}

class CacheException implements Exception {}

class NetworkException implements Exception {
  static const String NO_INTERNET = 'No Internet';
  static const String WAITING_NETWORK = 'Waiting network';

  @override
  String toString() {
    return WAITING_NETWORK;
  }
}
