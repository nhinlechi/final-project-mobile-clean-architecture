import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../feature/data/models/user/user_login.dart';

const CACHED_USER_LOGIN = 'CACHED_USER_LOGIN';

class Token {
  final SharedPreferences sharedPreferences;

  Token(this.sharedPreferences);

  String getToken() {
    final jsonString = sharedPreferences.getString(CACHED_USER_LOGIN);

    if (jsonString != null) {
      return UserLogin.fromJson(json.decode(jsonString)).token;
    }
    return null;
  }
}
