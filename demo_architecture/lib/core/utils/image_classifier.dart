import 'package:mime/mime.dart';

class ImageClassifier {
  static String classify(String path) {
    return lookupMimeType(path);
  }
}
