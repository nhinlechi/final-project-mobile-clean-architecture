final mockPlaces = [
  {
    "address": "84A Bùi Thị Xuân, Phường Phạm Ngũ Lão, Quận 1, Thành phố Hồ Chí Minh, Việt Nam",
    "photos": [
      "https://lh3.googleusercontent.com/p/AF1QipNNl5XRNlUs5p3M7pW1rknk_r0NP6e8wKKKQugo=s1600-w570-h570"
    ],
    "rating": 5,
    "user_ratings_total": 2,
    "_id": "604c89b7fa976f49008e0953",
    "location": {
      "lat": 10.7708453,
      "lng": 106.6894196
    },
    "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png",
    "name": "Nhà Hàng Sky",
    "type": "restaurant",
    "__v": 0
  },
  {
    "address": "số 11C Trần Văn Cẩn, Hẻm 93 Lũy Bán Bích, Tân Thới Hoà, Tân Phú, Thành phố Hồ Chí Minh, Việt Nam",
    "photos": [
      "https://lh3.googleusercontent.com/p/AF1QipMEIYvd_0EPWAQVs-tF5dYR8PJG2dHY9_yYnbAn=s1600-w3024-h4032"
    ],
    "rating": 5,
    "user_ratings_total": 1,
    "_id": "604c89b7fa976f49008e0957",
    "location": {
      "lat": 10.761885,
      "lng": 106.631649
    },
    "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png",
    "name": "Trà sữa ăn vặt Siu Ù",
    "type": "restaurant",
    "__v": 0
  },
  {
    "address": "180 Nguyễn Văn Trỗi, Phường 8, Phú Nhuận, Thành phố Hồ Chí Minh, Việt Nam",
    "photos": [
      "https://lh3.googleusercontent.com/p/AF1QipPfr1it3Q_uD-r_5oBhscGUZWgPcOd5Yup1Yj-c=s1600-w570-h570"
    ],
    "rating": 5,
    "user_ratings_total": 1,
    "_id": "604c89b7fa976f49008e0980",
    "location": {
      "lat": 10.7994101,
      "lng": 106.6694028
    },
    "icon": "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png",
    "name": "Nhà Hàng Giấc Mơ Nhỏ",
    "type": "restaurant",
    "__v": 0
  },
];