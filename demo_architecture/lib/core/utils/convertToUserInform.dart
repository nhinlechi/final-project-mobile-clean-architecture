import 'package:intl/intl.dart';

import '../../feature/data/models/user/user_inform.dart';
import '../../feature/domain/entities/user/user_inform.dart';

class ConvertUserInform {
  static UserInform convert(UserInformModel user) {
    var format = new DateFormat.yMd();
    String userId = user.id;
    String firstName = user.firstname[0].toUpperCase() + user.firstname.substring(1);
    String lastName = user.lastname[0].toUpperCase() + user.lastname.substring(1);
    String hoten = (firstName + " " + lastName) ?? "Không rõ";
    String gioitinh;
    switch (user.gender) {
      case 1:
        gioitinh = "Nam";
        break;
      case 2:
        gioitinh = "Nữ";
        break;
      default:
        gioitinh = "Khác";
        break;
    }
    DateTime today = DateTime.now();
    DateTime dateofbirth = new DateTime.fromMillisecondsSinceEpoch(user.dateofbirth);
    String age = (today.year - dateofbirth.year).toString();
    String birthDay = format.format(dateofbirth);
    String phone = user.phone ?? "Không có";
    String rela;
    switch (user.relationship) {
      case 1:
        rela = "Hẹn hò";
        break;
      case 2:
        rela = "Độc thân";
        break;
      case 3:
        rela = "Đã ly hôn";
        break;
      default:
        rela = "Khác";
        break;
    }
    List<String> avt = user.avatar ??
        [
          "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMREBUQEhISEBIVFQ8PDw8VFRUPDw8PFRUWFhURFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0fHR0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tNS0tLS0tLf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAQIDBAUGBwj/xABAEAACAQIDBgMECAQEBwEAAAAAAQIDEQQSIQUTMUFRYRRxgQYVIpEHMkJSobHR8GKSwfEjU7LhM0OCorPC0iT/xAAaAQADAQEBAQAAAAAAAAAAAAAAAQIDBAUG/8QAJxEAAgIBBAICAgIDAAAAAAAAAAECERIDEyExQVEEFCJhMnEVI8H/2gAMAwEAAhEDEQA/AOzqUUMSsazw3YfDZl9b2OaNnY5oyYxZNCmy/PCqPDUVcLWKJy9FSKJoEjpiqmFiJKdkPWUjVBj/AA0ug7JolhCJJGnG5XVFiqm0FiovTpaaEU4PzCjWa7k06nYdk0UJ36EeZmjvOqEvB8hMpP8ARm3FRpqnDsMdCPIhplqaKCHI0YYWLHPCRDFhuIzU2OUzQ8PFCuhBhjIWcTOzD1MveHiRzwiCpDyiVJJDHBFiVEFRM2my1JFfdCOmWXTEykOJSkVcgmRlvKG7fQhxKzKe7E3ZdcOwKPYjEeZS3Q3dF9Q7D40ewtux7lGY6YmR9DVqU42G04kvS57Bapl2fQDVydkBO1L2G6T5URyop8yRxEjTZ6lnAMjhkiKouxcSHZEMaZThFPihZQUeRYlT6CpdUIdlXfroP8QuhM6S6EVWKtwABY1Y8xZSiVd23yJadHqLIdEmaPQjcn0J4QJFAOxcIrwaa1QyVBsubsVQ7joLKSwrF3Ei7GI4WIZlBUprmKsPLqXgHiGZR8Mw8Ky8AYIWbKO5kh2cuNEcqSFjXQ8r7IozCbQ7dDXAnkpUM3ZJGmhJCbxIXCHyyeNNC5Ctv2O8QwU4E4SJsgiUSvOs2Rtky1YropQfku2Q2WhUUmK5XJ3kPbH2i2JkiuZExGzB6q9GmP7LGdAVQJ3/ANBgi6hyGocmehZy0KOTGi3HYUOEC4ILCgEsKAWAjihjiSNCZRDREovqSRYZBYxsA2OsADZRuOyRZVEhiroJUUIqJOTKpEm8HJnif0g+39fxMsPh5ToQpSs2rKrUqR5uX2Y/w8eN+i8+xW16tSWadSc5au8pSlK746tlq2aLRbPq4SUj569j/pBxGEqxVScq9D6s6U5N5U3rODd2mvl+a9/wtaFSEasJKcJxjOE1qpRaummJsmWm49jnUfQTNJ8iUCefZNr0QtSE3LLACxDJlfcsTcssgLBBmyruGKsOyyAttD3GVvDh4csgG1EWbK/hxfDk4C24hmyDw5E8OXBCJaUWNajKnhmBbAjYiPcZWTHDRyN0wFTHXGCjsQ64XGihYUOuFxAHkFDrhcaAZCodcUYKGQUOuFxooZBQtwuIKgyEfJ23MY62Iq1ZaSqVa1WS6Oc5St6Xt6FDMWdr/wDHq6Zf8St8P3fjl8PpwKVyrPSolVQ+k/orquWyMM23L4asbvklVmlHySsvQ+aUfRn0OVpy2VTzpJRnWhSskr01K99P4nNehLfJnrr/AF3+1/07YUULis4LECwoBYAgABiAWwAMAyi2ECwwDQAsAWAAFgABAFAVAVEhyI1McpnOpG9ElgsR5xcw8hUSAR5gzDyFRJcUizBmDIKJbhcjUgzBkGJJcW5FmC4ZCxJbhciuAZBRLcLkZVx+0qOHjnrVYUo8nOSjm7RT4vshp2FHi/0t+x0cPWWJoqWSvKpUndpqOIc3KcV0i1JNL+GR5z4Op938Y/qdPt/azxWJqYio5Jzk8uaLtCnf4aavwSVkUndcHddVY7I6Krlm+7JKijs3A2q03VX+GpwdVKzk6aksyS62vzPoT2Y9qtmuEMNh5LDxjdU6M4ukldtu0n8Lbbb43bZ4K8/2ZX7PQbKrW4ON12CWlH9kyk5qmz6SxPtVgqbyzxeHjLmt5FtednoXtn7SpYiG8o1IVYXcc0GpLMuK8z5cVaT+zb0PYfoTlLw+Ii1ZKrTkul3Cz/0ox1IYq0Q9NJWelqQtyMDDIiiW4lxlxbhkKh9wuMFHkFDhRtwuPIVDgEuFx5gKAlwuPMBQEAM0IzkxyZjLbcSSO2YnEpI7npS9GuBkPbK6MPfS6MrMW1I2LhcyffMejEe2F0YZoNqRsXC5j+9+we9+wZoWzI2QMeG2L8h/vXsGaDZkaoGV707C+8+wZoNmRqpi3Mr3l2D3mGaFtSKntt7RLA4Z1FZ1Zvd0Ivhns25vslr8lzPDcbtSdepKpVm6lSXGctW107LstDrfpf2jvK1CnyhTnP1nKz/8aPPHI9L49KF+yHHwy5Kp6laVNrWOnb7LGqoPUzobsnohdbWz0Y9VmuY+aUlZ6lSpQa+q9Oj/AFIdotUyfxttLs9w+iXF0vB7mOVVE99Us/iqKdkqj1vply9FZHhMHJ8Y+r1O7+jfaqoYq10pTpypRutJWtLKnyfwcOdu1nz/ACE3By9FpJ/iu2e6XFuc179l0XzElt6XRHl/Yj6K+pM6a4Zjl17Qy6LsQvbcr39LEv5S9FL4eodcpIW5x9HbTjzv5knv+XYn7a8oH8Kfg63MFzkYbane91boTe+pdUL7sfQn8KZ1Nwuc0tqT6oX3nPqg+6vRP1JnSXC5znvefYT31LsJ/OgvAfTmdJcDmvfUuiAn/IQD6eocwq6RIsQjA8S+bFWKO/FHouJ0PiBHiUjBeKfUheMb5lqBm4nSeMHeKOZWM7jnimGCFR0fjkRrGa9jAjjh/jRbZR0Cxr6AsazIoYu3ETFV7tWdh7aJ80bTxEkriLEy6mKsRO3HQHXn1DAdGxLEy6jPEN8yjLF5opc0OhiLEYD6OK9v6l8Ur/5VNLyvJ/m2ctOZu+2+Iz4uX8MKcF8s3/uczVZ6EOII4pK5MsQnfW9kSxkuTTKtCGnV9P0J0zSLIkkT3C5EmPWpZmO8tDW9mPixVOMmo/FGUXbjKLzW7OyevPhzMPNb9DY9lJXxdLs537f4ctTPV/hL+i9Nfkv7PTJVhjrMgdRC37nivSR7KdEm8GqtrYauzM+VTLJtsyekaR5NKFa5IpmZg+LdzSpxTMZ6RTaRIpj6dS5RxFfI7DcJW4mEtApK1ZrxqNcxfEspxm+osqgtlrozpF3fjd7fgUXVQirrkQ9NseJbdTzArPEgZ/XXsdfo5BydyWLZWWJd7sfWxeZ8LH06ijnbZaSfUbLQqrEMjqYrkawSMNSMjQo030bXUJz5EUdoyjG2hVVRzd7mjivBkpPyaNFK9rlhQV7XMf4s3PzNdRhlvm1t1G4GW7+yOs7StcbGZmyxLz5b6dS7FR+8TKCRpDVfkt79ri9AqYhPgyvUy243K0sQrWtYnFGilZpxXO5LGvysYMce1pctYPEuT1IaRXJyvtRL/wDTPvkf/bEw6zNX2knfE1Hw1irdUopJ+qs/Ux6rN30cfllijwXLhZkt2uPzIqEla31b8OaY7M49+zWha6IZIn6i5rDI1Y84uPeLuvkTON1dPMuq/QpckPgVzTWv83K/Rmv7K4WcsVGUdFDM6jvZZJRlFed20YKk0+F1zXJnQeyKtOc4y4RUHTf1lmknm7r4LadeWl41ZfgzTRjc0dxNK9uBDiK6Tsig6rb1ZI6q6XOHE9WixHFNIrVZXd2NrVou1tBZPgyGjWPsmwr1LiqNGU6jTH1KrT0ZDiVVsuV5XevEfh/hKEMSk72uWKeKUn2Mnp2W7SovLE2B4lNWsVZV4lXEVyXpEw5fRoSrJOwniFbTiYMMXad/Qs4rFU4v6yJel6NGq7L8q3cDBntKnf6wBsMdr2VI1dSeErleVJ34aF3BYe56i6OCfDCnbmPrUYcSCVL43HjqNxtCUUuJcVzZhN+CCriVw5ItYGouJmWLFGuoaM3RzT/JUascRxI69X4Svg6qlf1sJUl8DKbtGUdJRZXhxZYpVtClSqu/Ade2r0XF9EjFo67L06vJDKzv0v5pM5jGbVlJuzajwSWmncqKuarTj5Zg/kNdI6yNCT5P5NlnC1MvE4xV2Tw2pUX/ADJ9LZm1bpYT0V4Y18p+UW/aaa3ra5xi/Xh/QwZzvy1LeMxLqO8nd9eGnJFPJr5NakyiyM022XVG0VdXVl5ock+TbXSydgVbTVefMMy4rR/maUQKpPtH04iJuLuvVdQcuo1sBEznm1Wj5x/Q6T2Ub3U00rKcbP7V3HVPtorf9RydvQ6H2WrNby750n52z/qZ6vMTbQ41EdK7c9BVUiNq1otJfMz8bXjHhoc3J6KryWpJvgOipLmZNHaGR9S5DaKlqQ0aLU8IsTk3oRTm48SlLGNSvfiQYrGtktF7mPZowxS5k1TGxjC61Zzu+chm/fAmiZa1l2W055r8ug6rtWTM2UxkpjqzPeot18bcoVqrfMbNjGysTOWs5djs7Aj3qAMCc2dHgsak9XJt342t8lwZJ7xWqfDXg3B26HPb3l/sOhJ27X1JUmejqxiuWjbw+0VF/CnxslJ5peer4Dsbj5TWXMu+jV/J3ujEpzs7qyd+7JI4i+ujf4LUcdR2c01HvyabpaL+7Ia9lJKUbpp2s1mv5FWriZWWV2a4x4p36XQzEYtysm7Wtf8AtxOpzyj+PZwpYyt8lyUclpKXwvXpbz5ElDFSqRtFX6u2n9zL3qSvq9X0as0uF0yR4/LzyK6la9r9la11cnOapFRcHbaL8YOLTcmtdY2TzdtURbWrrdS0eqcU1ayb06kFXFZ1ePxJWvZadeJQ2jibwtZJXuraad+/AvJ5VRMpRUOezLv++4Zhshty2zkolUhcxEmOTGpBQ+4ub+jGoRsdhRcb/fMi/f7Q2Fbk/n0Huz7/AJj7BcCwzcrP1/oTLP0RVlBrgNdSS5iugasuSzc7I3NgPJHXhUd4vrk0/NvR9upzEZ9WaOExDUVa9ottebtf/SidR2jTReM7Z0uPxeQw62JzPVjMVjHPiVd4c7R06mrk+Oi1vRyrPkylmDOLAhSL6xHUhlV1uQKoJmFgys2yyqwmYr3ZWq4iSk1cFp2JzNByEzFelXbim+OvJdWLn/eo8AysklUsQud9AnJDvhtZLW103Z/kN8DjXkNz3FBPs/x/QCOS84+iTK2+voSxi1+7Ebk0Ni1f8jLs9nWgTqnfS/V8dUlrqNVr63fDVcgva/N6ftCwhL6zjpa+vTrboPo87UZHOvrlXBPi3p56ENLEPV2Ttp/Tj0G1pWej/fqQvV266vlqbR4XB581yX8O24PTW+ZSV3l01btrlK6w0601GF5NLVvhFcE2+S/ESnLqnZfD+upYpbRlFZYtpLpp6mmlBN8smU3FUjRwOypwi05cdVl+re3MgxmxZyt8SVr8iBbVn95/Md70l99/M6VDTTswepNqiB7An95fJ/qNewZ/eXy/3JntSX3n82MltKX3n8xtQFch09lyaV1G6urpZb9ODKtTZU1wXzZN7xfX8WJ499fxYsYBcipLAVV9hv5EbwlT7kvkaHj31Elj2Jxj7GnL0Zzw8/uT/lYvh5v7Ev5WX/HB44WMfY7foorCVPuSHx2fUf2fm0WvHPqDx3cKj7C5DaGxqj6I28JseOVKUn3asrv1RieNfUTxr6j/AABNnSe4qX35/OP/AMirYNH/ADKn80f0OZeM7irGB+A8pHTR2Bh/vz/miKtg0Pvy/mj+pzHjQ8b3D8AykbO1dlQpwzwk3ZpTTcXo9E1bvZeqMhOw14u6avx/uMTMZpXwbQk2uR6mVcR9b0RLLQir8V5EpFSJsN9X1Y99CHD8H5lmFufoJ8AhZxVsvN8yN+d1wT4MbOovQiqSJUWOWraJd75/gBDdCDxMczSnoRsAOZH1Ldrkfa0c2nGyX7RLPFSccrbaVrLoAAjh1oqjPqLXv+ZEp9AA6Inkag6L4rW9+vLuJKGv4igaQ7M5cISNIHTADSiBN2JuwAKFYbsXdAAUgbB0hN2ABQ7B0xHTAAoLFyBkQAFILF3QbrgADpCsN2g3aABUgsXdoFSQoDoLHRgkJcAIl2aQfBLdysvJd7eZBio2a8tAAzXDN5fwsdg7t2XYkqyuvy8wAH2Qv4FeUdRAAaZlJdDbCgBRFs//2Q==",
        ];
    return new UserInform(userId, hoten, phone, gioitinh, birthDay, phone, rela, avt,
        lastName, firstName, dateofbirth, user.favorites, age);
  }
}
