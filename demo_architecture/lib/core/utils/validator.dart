class Validator{
  static String validatorEmail(String value){
    if (value.isEmpty ||
        !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(value)) {
      return 'Xin nhập đúng Email';
    }
    return null;
  }
  static String validatorPassword(String value){
    if(value.length <6 || !RegExp(r"^[a-zA-Z0-9]{6,30}$").hasMatch(value))
      return "Password phải từ 6 ký tự trở lên";
    return null;
  }
}