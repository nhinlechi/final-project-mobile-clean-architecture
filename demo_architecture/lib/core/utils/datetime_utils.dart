class DateTimeUtils {
  static String differentDayText(int timeInMilliseconds) {
    final diff =
        DateTime.fromMillisecondsSinceEpoch(timeInMilliseconds).difference(
      DateTime.now(),
    );
    final diffDays = diff.inDays;
    final diffHours = diff.inHours;
    final diffMinutes = diff.inMinutes;
    String diffText;
    if (diffDays > 0) {
      diffText = '$diffDays days left';
    }
    if (diffDays == 0) {
      if (diffHours > 0) {
        diffText = '$diffHours hours left';
      }
      if (diffHours < 0) {
        diffText = '${diffHours.abs()} hours ago';
      }
      if (diffHours == 0) {
        if (diffMinutes > 0) {
          diffText = '$diffMinutes minutes left';
        } else {
          diffText = '${diffMinutes.abs()} minutes ago';
        }
      }
    }
    if (diffDays < 0) {
      diffText = '${diffDays.abs()} days ago';
    }
    return diffText;
  }
}
