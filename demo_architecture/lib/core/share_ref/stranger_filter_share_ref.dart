import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StrangerFilterShareRef {
  static const String MY_LATITUDE = "my_latitude";
  static const String MY_LONGITUDE = "my_longitude";
  static const String STRANGER_GENDER = "stranger_gender";
  static const String STRANGER_MIN_AGE = "stranger_min_age";
  static const String STRANGER_MAX_AGE = "stranger_max_age";
  static const String STRANGER_MAX_DISTANCE = "stranger_max_distance";
  static const String STRANGER_RELATIONSHIP = "stranger_relationship";

  final SharedPreferences sharedPreferences;

  StrangerFilterShareRef({@required this.sharedPreferences});

  double get latitude => sharedPreferences.getDouble(MY_LATITUDE);

  double get longitude => sharedPreferences.getDouble(MY_LONGITUDE);

  int get gender => sharedPreferences.getInt(STRANGER_GENDER);

  int get minAge => sharedPreferences.getInt(STRANGER_MIN_AGE);

  int get maxAge => sharedPreferences.getInt(STRANGER_MAX_AGE);

  int get maxDistance => sharedPreferences.get(STRANGER_MAX_DISTANCE);

  int get relationship => sharedPreferences.getInt(STRANGER_RELATIONSHIP);

  void saveFilter({
    double lat,
    double long,
    int gender,
    int minAge,
    int maxAge,
    int maxDistance,
    int relationship,
  }) async {
    await sharedPreferences.setDouble(MY_LATITUDE, lat);
    await sharedPreferences.setDouble(MY_LONGITUDE, long);
    //
    await sharedPreferences.setInt(STRANGER_GENDER, gender);
    await sharedPreferences.setInt(STRANGER_MIN_AGE, minAge);
    await sharedPreferences.setInt(STRANGER_MAX_AGE, maxAge);
    await sharedPreferences.setInt(STRANGER_MAX_DISTANCE, maxDistance);
    await sharedPreferences.setInt(STRANGER_RELATIONSHIP, relationship);
  }

  void resetValue() async {
    // Lat long init with Ho Chi Minh city
    await sharedPreferences.setDouble(MY_LATITUDE, 0.0);
    await sharedPreferences.setDouble(MY_LONGITUDE, 0.0);
    //
    await sharedPreferences.setInt(STRANGER_GENDER, 0);
    await sharedPreferences.setInt(STRANGER_MIN_AGE, 18);
    await sharedPreferences.setInt(STRANGER_MAX_AGE, 80);
    await sharedPreferences.setInt(STRANGER_MAX_DISTANCE, 10);
    await sharedPreferences.setInt(STRANGER_RELATIONSHIP, 0);
  }

  void init() {
    if (!sharedPreferences.containsKey(STRANGER_GENDER)) {
      resetValue();
    }
  }
}
