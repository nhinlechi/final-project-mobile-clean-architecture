import 'package:flutter/foundation.dart';

class APIBase {
  static const String HEROKU_SERVER_URL = 'togetherapis.herokuapp.com';

  static const String HTTPS_SERVER_URL = 'api.togetherapplication.online';

  static String get baseURL {
    if (kReleaseMode) {
      return HTTPS_SERVER_URL;
    } else {
      return HTTPS_SERVER_URL;
    }
  }

  static String get chatSocketUrl => 'https://$HTTPS_SERVER_URL/chat';

  static String get videoCallSocketUrl => 'https://$HTTPS_SERVER_URL/videocall';

  static String get chatGroupEventSocketUrl => 'https://$HTTPS_SERVER_URL/chatevent';
}
