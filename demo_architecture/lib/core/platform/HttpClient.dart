import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import '../error/exceptions.dart';
import '../utils/image_classifier.dart';
import '../../feature/data/models/utils/signed_url.dart';

import '../utils/get_token.dart';
import 'package:http/http.dart' as http;

import 'api_base.dart';
import 'api_path.dart';

class HttpClient {
  final Token token;

  HttpClient({this.token});

  Future<Map<String, dynamic>> fetchData(
      {String apiPath, Map<String, dynamic> queryParameters}) async {
    var responseJson;
    String _token = token.getToken() ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      "auth-token": _token
    };
    try {
      final response = await http.get(
        Uri.https(
          APIBase.baseURL,
          '/api/v1$apiPath',
          queryParameters,
        ),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }

  Future<Map<String, dynamic>> fetchDataWithNoToken(
      {String apiPath, Map<String, String> params}) async {
    var responseJson;
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };
    try {
      final response = await http.get(
        Uri.https(
          APIBase.baseURL,
          '/api/v1$apiPath',
          params,
        ),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  String queryParameters(Map<String, String> params) {
    if (params != null) {
      final jsonString = Uri(queryParameters: params);
      return '?${jsonString.query}';
    }
    return '';
  }

  Future<Map<String, dynamic>> postData(
      {String apiPath, Map<String, dynamic> body}) async {
    try {
      var responseJson;
      String _token = token.getToken() ?? "";
      var header = {
        HttpHeaders.contentTypeHeader: 'application/json',
        'auth-token': _token
      };

      final response = await http.post(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  Future<Map<String, dynamic>> postDataNoHeader(
      {String apiPath, Map<String, dynamic> body}) async {
    try {
      var responseJson;
      var header = {
        HttpHeaders.contentTypeHeader: 'application/json',
      };
      log("postDataNoHeader: " + json.encode(body));

      final response = await http.post(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateData(
      {String apiPath, dynamic body}) async {
    var responseJson;
    String _token = token.getToken() ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'auth-token': _token
    };
    try {
      final response = await http.patch(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  Future<Map<String, dynamic>> put({String apiPath, dynamic body}) async {
    var responseJson;
    String _token = token.getToken() ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'auth-token': _token
    };
    try {
      final response = await http.put(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  Future<Map<String, dynamic>> delete({String apiPath, dynamic body}) async {
    var responseJson;
    String _token = token.getToken() ?? "";
    var header = {
      HttpHeaders.contentTypeHeader: 'application/json',
      'auth-token': _token
    };
    try {
      final response = await http.delete(
        Uri.https(APIBase.baseURL, '/api/v1$apiPath'),
        body: json.encode(body),
        headers: header,
      );
      responseJson = _returnResponse(response);
      //log('${responseJson.toString()}', name: 'response');
      return responseJson;
    } catch (e) {
      throw e;
    }
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
      case 201:
        var responseJson = json.decode(response.body);
        return responseJson;
      default:
        throw APIException(
          response.statusCode,
          json.decode(response.body)['message'],
        );
    }
  }

  Future<String> uploadImage(File imageFile) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.upload_image);
      final type = ImageClassifier.classify(imageFile.path);
      final json = await fetchData(
        apiPath: apiPath,
        queryParameters: {
          'type': type,
        },
      );
      final signedUrl = SignedUrlModel.fromJson(json);
      final res = await http.put(
        Uri.parse(signedUrl.uploadUrl),
        headers: {
          'Content-Type': type,
        },
        body: imageFile.readAsBytesSync(),
      );
      log(res.statusCode.toString(), name: 'upload image to cloud storage');
      return signedUrl.accessUrl;
    } catch (e) {
      throw e;
    }
  }
}
