enum APIPath {
  fetch_user,
  login,
  login_facebook,
  login_google,
  check_register,
  register,
  update_user,
  match_now,
  match_later,
  get_matching_info,
  send_firebase_token,
  confirm_matching,
  get_places,
  get_comment_place,
  cancel_request_match,
  comment_place,
  get_list_group,
  get_user_by_id,
  get_list_favorite,
  add_information_extension,
  get_my_list_favorite,
  upload_image,
  get_tag,
  get_group_of_user,
  request_join_group,
  get_chat,
  get_notifications,
  leave_group,
  send_place,
  confirm_place,
  get_group_by_id,
  get_my_match,
  confirm_request_join_group,
  get_recommend_place,
  create_new_group_event,
  get_list_group_event,
  update_group_event,
  delete_group_event,
  join_group_event,
  get_group_event_messages,
  match_meeting_history,
}

class APIPathHelper {
  static String getValue(APIPath path) {
    switch (path) {
      case APIPath.fetch_user:
        return '/user';
      case APIPath.login:
        return '/auth/login';
      case APIPath.login_facebook:
        return '/auth/loginfacebook';
      case APIPath.login_google:
        return '/auth/logingoogle';
      case APIPath.check_register:
        return '/auth/checkregister';
      case APIPath.register:
        return '/auth/register';
      case APIPath.update_user:
        return '/user/update';
      case APIPath.match_now:
        return '/match/matchnow';
      case APIPath.get_matching_info:
        return '/match';
      case APIPath.send_firebase_token:
        return '/firebase';
      case APIPath.confirm_matching:
        return '/match/confirmmatch';
      case APIPath.get_places:
        return '/place';
      case APIPath.get_comment_place:
        return '/place/getcommentsplacebyid';
      case APIPath.cancel_request_match:
        return '/match/cancelrequest';
      case APIPath.comment_place:
        return '/place/comment';
      case APIPath.login_google:
        return '/auth/logingoogle';
      case APIPath.get_list_group:
        return '/group';
      case APIPath.get_user_by_id:
        return '/user';
      case APIPath.get_list_favorite:
        return '/favorite';
      case APIPath.add_information_extension:
        return '/user/addinformationextension';
      case APIPath.get_my_list_favorite:
        return '/favorite/myfavorites';
      case APIPath.upload_image:
        return '/storage';
      case APIPath.get_tag:
        return '/tag';
      case APIPath.get_group_of_user:
        return '/group/getgroupofuser';
      case APIPath.request_join_group:
        return '/group/requestjoin';
      case APIPath.get_chat:
        return '/match/chat';
      case APIPath.get_notifications:
        return '/notification';
      case APIPath.leave_group:
        return '/group/leave';
      case APIPath.send_place:
        return '/match/sendPlace';
      case APIPath.confirm_place:
        return '/match/confirmPlace';
      case APIPath.get_group_by_id:
        return '/group/getgroupbyid';
      case APIPath.get_my_match:
        return '/match';
      case APIPath.match_later:
        return '/match/matchlater';
      case APIPath.confirm_request_join_group:
        return '/group/confirmjoin';
      case APIPath.match_meeting_history:
        return '/match/meetinghistory';
      default:
        return '';
    }
  }

  static String getValueWithParam(APIPath path, String param) {
    switch (path) {
      case APIPath.get_recommend_place:
        return '/match/recommendplace/$param';
      default:
        return '';
    }
  }
}

class GroupAPIPathHelper {
  static String getValue(APIPath path, String groupId) {
    switch (path) {
      case APIPath.create_new_group_event:
      case APIPath.delete_group_event:
      case APIPath.get_list_group_event:
      case APIPath.update_group_event:
        return '/group/$groupId/event';
      case APIPath.join_group_event:
        return '/group/$groupId/event/join';
      case APIPath.get_group_event_messages:
        return '/group/$groupId/event/chat';
      default:
        return '';
    }
  }
}
