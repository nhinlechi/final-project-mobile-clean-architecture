class SocketEvent {
  static final String connect = 'connect';
  static final String disconnect = 'disconnect';
  static final String clientJoinChat = 'client_join_chat';
  static final String serverJoinChat = 'server_join_chat';
  static final String clientLeaveChat = 'client_leave_chat';
  static final String serverLeaveChat = 'server_leave_chat';
  static final String clientSentChat = 'client_sent_chat';
  static final String serverSentChat = 'server_sent_chat';
  static final String clientStartTyping = 'client_start_typing';
  static final String clientEndTyping = 'client_end_typing';
  static final String serverStartTyping = 'server_start_typing';
  static final String serverEndTyping = 'server_end_typing';
}