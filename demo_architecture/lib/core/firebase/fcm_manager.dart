import 'dart:convert';
import 'dart:developer';

import 'package:demo_architecture/feature/data/datasources/remotesources/matching/matching_remote_data.dart';
import 'package:demo_architecture/feature/data/models/chat/match_room_model.dart';
import 'package:demo_architecture/feature/domain/usecase/notifications/get_notification_uc.dart';
import '../../feature/data/models/notification/notification_model.dart';
import '../../feature/presentation/bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import '../../feature/presentation/bloc/match_blocs/firebase_bloc.dart';
import '../../feature/presentation/bloc/match_blocs/matching_bloc/matching_bloc.dart';
import '../../feature/presentation/bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import '../../feature/presentation/repository_providers/match_repository_provider.dart';
import '../../feature/presentation/repository_providers/user_repository_provider.dart';
import '../../feature/presentation/screens/call_video_screens/incoming_call_screen.dart';
import '../../feature/presentation/screens/find_people_screens/countdown_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

typedef void NotifyCallback();

class FCMManager {
  static const String REQUEST_MATCH_NOW = '1';
  static const String REQUEST_MATCH_LATER = '2';
  static const String CONFIRM_MATCH = '3';
  static const String REQUEST_JOIN_GROUP = '4';
  static const String JOIN_GROUP_SUCCESS = '5';
  static const String COMING_MEETING = '6';
  static const String NEW_CHAT = '7';
  static const String REQUEST_PLACE_IN_MATCH = '8';
  static const String SOMEONE_CALL = '9';

  NotifyCallback onMatchSuccess;
  NotifyCallback onMatchDenied;

  final BuildContext context;

  FCMManager({
    @required this.context,
  });

  void init() {
    _updateFirebaseToken();
    _handleMessages();
  }

  void _updateFirebaseToken() async {
    // Update firebase token
    final firebaseToken = await FirebaseMessaging.instance.getToken();
    final firebaseBloc = context.read<FirebaseBloc>();
    firebaseBloc.add(UpdateFirebaseTokenEvent(firebaseToken));
    log('update firebase token', name: 'update firebase token');
  }

  void _handleMessages() {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      if (message.notification != null) {
        final notifyTitle = message.notification.title;
        final notificationCode = message.notification.body;
        final dataString = message.data;
        final data = json.decode(dataString['stringdata']);

        log(notifyTitle, name: 'notification');
        log(notificationCode, name: 'notification');
        log(dataString.toString(), name: 'notification');
        log(data.toString(), name: 'notification data');

        switch (notificationCode) {
          case SOMEONE_CALL:
            {
              final callerData = data['caller'];
              final callerId = callerData['_id'];
              final fullName =
                  '${callerData['firstname']} ${callerData['lastname']}';
              final avatar = callerData['avatar'][0];
              Navigator.of(context).pushNamed(
                IncomingCallScreen.routeName,
                arguments: InComingCallScreenArgs(
                  fullName: fullName,
                  callerId: callerId,
                  avatar: avatar,
                ),
              );
              return;
            }
          case REQUEST_MATCH_NOW:
            {
              final currentUserInfo =
                  context.read<UserRepositoryProvider>().userInform;
              // Get data matching from notify
              final notificationModel =
                  NotificationModel.fromNotifyData(notificationCode, data);
              final notificationData =
                  notificationModel.data as RequestMatchData;
              // Cache
              final repos = context.read<MatchRepositoryProvider>();
              repos.currentMatchingInfo = notificationModel.data;
              final currentTimeStamp = DateTime.now().millisecondsSinceEpoch;
              final timeOut =
                  (notificationData.timeout - currentTimeStamp) / 1000;

              Navigator.of(context).pushNamed(
                CountdownScreen.routeName,
                arguments: CountdownScreenArgs(
                  fcmManager: this,
                  notificationId: notificationModel.id,
                  otherInfo:
                      currentUserInfo.userId == notificationData.users[0].userId
                          ? notificationData.users[1]
                          : notificationData.users[0],
                  timeRemaining: timeOut.toInt(),
                ),
              );
              return;
            }
          case CONFIRM_MATCH:
            {
              if (data['result'] == '1') {
                // 1 -> send matching success event to matching bloc
                BlocProvider.of<MatchingBloc>(context).add(
                  MatchingSuccessEvent(
                    matchRoomId: data['matchId'],
                  ),
                );
                // 2 -> Navigate to chat screen (callback to root route/root page)
                onMatchSuccess?.call();
              } else if (data['result'] == '-1') {
                // call back to another screen (countdown screen do it jobs)
                onMatchDenied?.call();
              }
              return;
            }
          case REQUEST_PLACE_IN_MATCH:
            {
              // Convert data
              final notificationModel =
                  NotificationModel.fromNotifyData(notificationCode, data);
              final notificationData =
                  notificationModel.data as RequestPlaceData;
              // Add Event to confirm place bloc
              context.read<ConfirmPlaceBloc>().add(
                    ReceiveRequestPlaceEvent(
                      data: RequestPlaceEntity(
                        matchId: notificationData.matchId,
                        notificationId: notificationModel.id,
                        place: notificationData.place,
                        senderId: notificationData.senderId,
                        datingTime: notificationData.datingTime,
                      ),
                    ),
                  );
              return;
            }
          case REQUEST_MATCH_LATER:
            {
              final currentUserInfo =
                  context.read<UserRepositoryProvider>().userInform;
              // Get data matching from notify
              final notificationModel =
                  NotificationModel.fromNotifyData(notificationCode, data);
              final notificationData =
                  notificationModel.data as RequestMatchData;
              // Cache
              final repos = context.read<MatchRepositoryProvider>();
              repos.currentMatchingInfo = notificationModel.data;
              final currentTimeStamp = DateTime.now().millisecondsSinceEpoch;
              final timeOut =
                  (notificationData.timeout - currentTimeStamp) / 1000;

              Navigator.of(context).pushNamed(
                CountdownScreen.routeName,
                arguments: CountdownScreenArgs(
                  mode: MatchMode.Later,
                  fcmManager: this,
                  notificationId: notificationModel.id,
                  otherInfo:
                      currentUserInfo.userId == notificationData.users[0].userId
                          ? notificationData.users[1]
                          : notificationData.users[0],
                  timeRemaining: timeOut.toInt(),
                ),
              );
              return;
            }
        }

        // Refresh Notify Page
        context.read<GetNotificationsBloc>().add(
              FetchNotificationsEvent(
                params: GetNotificationParams(
                  rowPerPage: 100,
                  page: 1,
                ),
              ),
            );
      }
    });
  }
}
