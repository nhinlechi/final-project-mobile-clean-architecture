import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/matching/request_match_later_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/user/auto_login_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/user/logout_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_dates_bloc/get_dates_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_recommend_place_bloc/get_recommend_place_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/upload_message_image_bloc/upload_message_image_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/confirm_request_join_group_bloc/confirm_request_join_group_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/create_new_group_event_bloc/create_new_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_group_event_messages_bloc/group_event_messages_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/group_scroll_bloc/group_scroll_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/join_group_event_bloc/join_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/update_group_event_bloc/update_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/place_blocs/place_scroll_bloc/place_scroll_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/platform/HttpClient.dart';
import 'core/platform/network_infor.dart';
import 'core/share_ref/stranger_filter_share_ref.dart';
import 'core/utils/get_token.dart';
import 'core/utils/string_utils.dart';
import 'feature/data/datasources/localsources/autheticate_local_data.dart';
import 'feature/data/datasources/localsources/group/group_local_source.dart';
import 'feature/data/datasources/localsources/login_local_data.dart';
import 'feature/data/datasources/localsources/user_inform_local_sources.dart';
import 'feature/data/datasources/remotesources/authentication/authenticate_remote_data.dart';
import 'feature/data/datasources/remotesources/authentication/login_remote_data.dart';
import 'feature/data/datasources/remotesources/chat/chat_remote_data.dart';
import 'feature/data/datasources/remotesources/favorites/favorite_remote_data.dart';
import 'feature/data/datasources/remotesources/group/group_remote_data.dart';
import 'feature/data/datasources/remotesources/matching/matching_remote_data.dart';
import 'feature/data/datasources/remotesources/notification/notification_remote_data.dart';
import 'feature/data/datasources/remotesources/place/place_remote_source.dart';
import 'feature/data/datasources/remotesources/user/update_basic_remote_data.dart';
import 'feature/data/datasources/remotesources/user/update_phone_remote_data.dart';
import 'feature/data/datasources/remotesources/user/update_relationship_remote_data.dart';
import 'feature/data/datasources/remotesources/user/user_inform_remote_data.dart';
import 'feature/data/repositories/authentication/authenticate_repository_impl.dart';
import 'feature/data/repositories/authentication/login_repo_impl.dart';
import 'feature/data/repositories/chat/get_chat_repos_impl.dart';
import 'feature/data/repositories/favorite/favorite_repo_impl.dart';
import 'feature/data/repositories/group/group_repo_impl.dart';
import 'feature/data/repositories/matching/matching_repos.dart';
import 'feature/data/repositories/notification/notification_repos_impl.dart';
import 'feature/data/repositories/places/place_repository_impl.dart';
import 'feature/data/repositories/user/update_basic_repo_impl.dart';
import 'feature/data/repositories/user/update_phone_repo_impl.dart';
import 'feature/data/repositories/user/update_rela_repo_impl.dart';
import 'feature/data/repositories/user/user_inform.repository_impl.dart';
import 'feature/domain/repositories/chat/get_chat_repos.dart';
import 'feature/domain/repositories/favorite/favorite_repository.dart';
import 'feature/domain/repositories/group/group_repository.dart';
import 'feature/domain/repositories/matching/matching_repos.dart';
import 'feature/domain/repositories/notification/notification_repos.dart';
import 'feature/domain/repositories/place/place_repository.dart';
import 'feature/domain/repositories/user/login_repo.dart';
import 'feature/domain/repositories/user/update_basic_user_inform_repo.dart';
import 'feature/domain/repositories/user/update_phone_user_inform_repo.dart';
import 'feature/domain/repositories/user/update_rela_user_inform_repo.dart';
import 'feature/domain/repositories/user/user_inform_repository.dart';
import 'feature/domain/usecase/chats/get_chat_messages_uc.dart';
import 'feature/domain/usecase/favorites/get_favorite_uc.dart';
import 'feature/domain/usecase/favorites/get_my_favorite_uc.dart';
import 'feature/domain/usecase/group/add_new_group.dart';
import 'feature/domain/usecase/group/get_group_by_id.dart';
import 'feature/domain/usecase/group/get_list_group.dart';
import 'feature/domain/usecase/group/get_list_group_of_user_uc.dart';
import 'feature/domain/usecase/group/get_list_tag_of_group_uc.dart';
import 'feature/domain/usecase/group/leave_group.dart';
import 'feature/domain/usecase/group/request_join_group_uc.dart';
import 'feature/domain/usecase/group/update_group.dart';
import 'feature/domain/usecase/matching/cancel_request_match_uc.dart';
import 'feature/domain/usecase/matching/confirm_matching_uc.dart';
import 'feature/domain/usecase/matching/confirm_place_uc.dart';
import 'feature/domain/usecase/matching/get_my_match_uc.dart';
import 'feature/domain/usecase/matching/request_match_uc.dart';
import 'feature/domain/usecase/matching/send_place_uc.dart';
import 'feature/domain/usecase/matching/update_firebase_uc.dart';
import 'feature/domain/usecase/notifications/get_notification_uc.dart';
import 'feature/domain/usecase/places/comment_place_uc.dart';
import 'feature/domain/usecase/places/get_comment_place_uc.dart';
import 'feature/domain/usecase/places/get_places_uc.dart';
import 'feature/domain/usecase/user/get_user_inform.dart';
import 'feature/domain/usecase/user/get_user_inform_by_id.dart';
import 'feature/domain/usecase/user/login_fb_uc.dart';
import 'feature/domain/usecase/user/login_gg_uc.dart';
import 'feature/domain/usecase/user/login_uc.dart';
import 'feature/domain/usecase/user/register_social_usecase.dart';
import 'feature/domain/usecase/user/register_uc.dart';
import 'feature/domain/usecase/user/update_basic_inform_uc.dart';
import 'feature/domain/usecase/user/update_phone_inform_uc.dart';
import 'feature/domain/usecase/user/update_rela_inform_uc.dart';
import 'feature/domain/usecase/user/update_user_avatar_uc.dart';
import 'feature/domain/usecase/user/update_user_inform_uc.dart';
import 'feature/presentation/bloc/authentication_blocs/loginv2/authenticate_bloc.dart';
import 'feature/presentation/bloc/authentication_blocs/register/register_bloc.dart';
import 'feature/presentation/bloc/chat_blocs/get_chat_bloc.dart';
import 'feature/presentation/bloc/chat_blocs/get_my_match_bloc/get_my_match_bloc.dart';
import 'feature/presentation/bloc/favorite_bloc/get_list_favorite/get_list_favorite_bloc.dart';
import 'feature/presentation/bloc/favorite_bloc/get_my_list_favorites/get_my_list_favorites_bloc.dart';
import 'feature/presentation/bloc/group_blocs/add_group_bloc/add_group_bloc.dart';
import 'feature/presentation/bloc/group_blocs/get_group_by_id_bloc/get_group_by_id_bloc.dart';
import 'feature/presentation/bloc/group_blocs/get_list_group_of_user_bloc/get_list_group_of_user_bloc.dart';
import 'feature/presentation/bloc/group_blocs/get_list_tag_group_bloc/get_list_tag_group_bloc.dart';
import 'feature/presentation/bloc/group_blocs/group_bloc.dart';
import 'feature/presentation/bloc/group_blocs/leave_group_bloc/leave_group_bloc.dart';
import 'feature/presentation/bloc/group_blocs/request_join_group_bloc/request_join_group_bloc.dart';
import 'feature/presentation/bloc/group_blocs/update_group_bloc/update_group_bloc.dart';
import 'feature/presentation/bloc/match_blocs/confirm_match_bloc/confirm_match_bloc.dart';
import 'feature/presentation/bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import 'feature/presentation/bloc/match_blocs/firebase_bloc.dart';
import 'feature/presentation/bloc/match_blocs/matching_bloc/matching_bloc.dart';
import 'feature/presentation/bloc/match_blocs/request_match_bloc/request_match_bloc.dart';
import 'feature/presentation/bloc/match_blocs/send_place_bloc/send_place_bloc.dart';
import 'feature/presentation/bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import 'feature/presentation/bloc/place_blocs/comment_place/comment_in_place_bloc.dart';
import 'feature/presentation/bloc/place_blocs/get_comment_place/comment_place_bloc.dart';
import 'feature/presentation/bloc/place_blocs/get_places_bloc/get_places_bloc.dart';
import 'feature/presentation/bloc/place_blocs/get_top_place/top_place_bloc.dart';
import 'feature/presentation/bloc/user_blocs/get_user_by_id_bloc/get_user_by_id_bloc.dart';
import 'feature/presentation/bloc/user_blocs/update_avatar/update_user_avatar_bloc.dart';
import 'feature/presentation/bloc/user_blocs/update_basic/update_basic_bloc.dart';
import 'feature/presentation/bloc/user_blocs/update_phone/update_phone_bloc.dart';
import 'feature/presentation/bloc/user_blocs/update_rela/update_rela_bloc.dart';
import 'feature/presentation/bloc/user_blocs/update_user_bloc/update_user_bloc.dart';
import 'feature/presentation/bloc/user_blocs/user_inform/user_inform_bloc.dart';
import 'feature/presentation/repository_providers/group_repository_provider.dart';
import 'feature/presentation/repository_providers/match_repository_provider.dart';
import 'feature/presentation/repository_providers/place_comments_repository_provider.dart';
import 'feature/presentation/repository_providers/user_repository_provider.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Number Trivia
  // Bloc

  sl.registerFactory(() => TopPlaceBloc(useCase: sl()));
  sl.registerFactory(
    () => AuthenticateBloc(
      loginUseCase: sl(),
      loginFacebookUseCase: sl(),
      loginGoogleUseCase: sl(),
      userRepositoryProvider: sl(),
      logoutUC: sl(),
      autoLoginUC: sl(),
    ),
  );
  sl.registerFactory(
      () => UserInformBloc(getUserInform: sl(), userReposProvider: sl()));
  sl.registerFactory(() => UpdatePhoneBloc(updateInform: sl()));
  sl.registerFactory(() => UpdateBasicBloc(updateInform: sl()));
  sl.registerFactory(() => UpdateRelaBloc(updateInform: sl()));
  sl.registerFactory(
      () => GetCommentPlaceBloc(usecase: sl(), placeCommentsRP: sl()));
  sl.registerFactory(() => ConfirmMatchBloc(confirmMatchingUC: sl()));
  sl.registerFactory(() => RequestMatchBloc(
        requestMatchUC: sl(),
        cancelRequestMatchUC: sl(),
        requestMatchLaterUC: sl(),
      ));
  sl.registerFactory(() => GroupBloc(useCase: sl(), groupReposProvider: sl()));
  sl.registerFactory(() => MatchingBloc(matchRepositoryProvider: sl()));
  sl.registerFactory(() => CommentInPlaceBloc(usecase: sl()));
  sl.registerFactory(() => GetPlacesBloc(getPlaceUseCase: sl()));
  sl.registerFactory(() => RegisterBloc(useCase: sl(), socialUseCase: sl()));
  sl.registerFactory(() => GetUserByIdBloc(useCase: sl()));
  sl.registerFactory(() => GetListFavoriteBloc(useCase: sl()));
  sl.registerFactory(() => GetMyListFavoritesBloc(useCase: sl()));
  sl.registerFactory(() => AddGroupBloc(useCase: sl()));
  sl.registerFactory(() => UpdateUserAvatarBloc(updateUserAvatarUC: sl()));
  sl.registerFactory(() => GetListTagGroupBloc(useCase: sl()));
  sl.registerFactory(() => GetListGroupOfUserBloc(useCase: sl()));
  sl.registerFactory(() => RequestJoinGroupBloc(useCase: sl()));
  sl.registerFactory(() => GetChatBloc(useCase: sl()));
  sl.registerFactory(() => GetNotificationsBloc(useCase: sl()));
  sl.registerFactory(() => FirebaseBloc(useCase: sl()));
  sl.registerFactory(() => UpdateUserBloc(useCase: sl()));
  sl.registerFactory(() => LeaveGroupBloc(useCase: sl()));
  sl.registerFactory(() => UpdateGroupBloc(useCase: sl()));
  sl.registerFactory(() => SendPlaceBloc(useCase: sl()));
  sl.registerFactory(() => GetGroupByIdBloc(useCase: sl()));
  sl.registerFactory(() => GetMyMatchBloc(useCase: sl(), provider: sl()));
  sl.registerFactory(() => ConfirmPlaceBloc(useCase: sl()));
  sl.registerFactory(() => PlaceScrollBloc());
  sl.registerFactory(() => GroupScrollBloc());
  sl.registerFactory(() => CreateNewGroupEventBloc(useCase: sl()));
  sl.registerFactory(() => GetListGroupEventBloc(useCase: sl()));
  sl.registerFactory(() => ConfirmRequestJoinGroupBloc(useCase: sl()));
  sl.registerFactory(() => UploadMessageImageBloc(useCase: sl()));
  sl.registerFactory(() => JoinGroupEventBloc(useCase: sl()));
  sl.registerFactory(() => UpdateGroupEventBloc(useCase: sl()));
  sl.registerFactory(() => GetRecommendPlaceBloc(useCase: sl()));
  sl.registerFactory(() => GroupEventMessagesBloc(useCase: sl()));
  sl.registerFactory(() => GetDatesBloc(useCase: sl()));

  // Repository Provider
  sl.registerLazySingleton(() => UserRepositoryProvider());
  sl.registerLazySingleton(() => MatchRepositoryProvider());
  sl.registerLazySingleton(() => PlaceCommentsRP());
  sl.registerLazySingleton(() => GroupRepositoryProvider());

  // Use cases
  sl.registerLazySingleton(() => GetUserInform(sl()));
  sl.registerLazySingleton(() => UpdatePhoneInform(sl()));
  sl.registerLazySingleton(() => UpdateRelaInform(sl()));
  sl.registerLazySingleton(() => UpdateBasicInform(sl()));
  sl.registerLazySingleton(() => LoginUseCase(sl()));
  sl.registerLazySingleton(() => RequestMatchNowUC(repos: sl()));
  sl.registerLazySingleton(() => ConfirmMatchingUC(repos: sl()));
  sl.registerLazySingleton(() => GetPlaceUseCase(sl()));
  sl.registerLazySingleton(() => GetCommentPlaceUsecase(sl()));
  sl.registerLazySingleton(() => CancelRequestMatchUC(repos: sl()));
  sl.registerLazySingleton(() => CommentPlaceUseCase(sl()));
  sl.registerLazySingleton(() => LoginGoogleUseCase(sl()));
  sl.registerLazySingleton(() => LoginFacebookUseCase(sl()));
  sl.registerLazySingleton(() => RegisterUseCase(sl()));
  sl.registerLazySingleton(() => GetListGroupUC(sl()));
  sl.registerLazySingleton(() => GetUserInformByIdUC(sl()));
  sl.registerLazySingleton(() => GetFavoriteUsecase(sl()));
  sl.registerLazySingleton(() => RegisterSocialUseCase(sl()));
  sl.registerLazySingleton(() => GetMyFavoriteListUseCase(sl()));
  sl.registerLazySingleton(() => AddNewGroupUseCase(sl()));
  sl.registerLazySingleton(() => UpdateUserAvatarUC(repos: sl()));
  sl.registerLazySingleton(() => GetListTagOfGroupUseCase(repository: sl()));
  sl.registerLazySingleton(
      () => GetListGroupOfUserUseCase(groupRepository: sl()));
  sl.registerLazySingleton(() => RequestJoinGroupUseCase(sl()));
  sl.registerLazySingleton(() => GetChatUC(repos: sl()));
  sl.registerLazySingleton(() => GetNotificationUC(repos: sl()));
  sl.registerLazySingleton(() => UpdateUserInformUseCase(sl()));
  sl.registerLazySingleton(() => UpdateFirebaseTokenUC(repos: sl()));
  sl.registerLazySingleton(() => LeaveGroupUseCase(sl()));
  sl.registerLazySingleton(() => UpdateGroupUseCase(sl()));
  sl.registerLazySingleton(() => SendPlaceUC(repos: sl()));
  sl.registerLazySingleton(() => GetGroupByIdUseCase(sl()));
  sl.registerLazySingleton(() => GetMyMatchUC(repository: sl()));
  sl.registerLazySingleton(() => ConfirmPlaceUC(repos: sl()));
  sl.registerLazySingleton(() => RequestMatchLaterUC(repository: sl()));
  sl.registerLazySingleton(() => CreateNewGroupEventUC(repos: sl()));
  sl.registerLazySingleton(() => GetListGroupEventUC(repos: sl()));
  sl.registerLazySingleton(() => ConfirmRequestJoinGroupUC(repos: sl()));
  sl.registerLazySingleton(() => UploadMessageImageUC(repos: sl()));
  sl.registerLazySingleton(() => JoinGroupEventUC(repos: sl()));
  sl.registerLazySingleton(() => UpdateGroupEventUC(repos: sl()));
  sl.registerLazySingleton(() => GetRecommendPlaceUC(repos: sl()));
  sl.registerLazySingleton(() => GetGroupEventMessagesUC(repos: sl()));
  sl.registerLazySingleton(() => GetDatesUC(repos: sl()));
  sl.registerLazySingleton(() => LogoutUC(repos: sl()));
  sl.registerLazySingleton(() => AutoLoginUC(repos: sl()));

  // Repository

  sl.registerLazySingleton<PlaceRepository>(
    () => PlaceRepositoryImpl(sl(), sl()),
  );

  sl.registerLazySingleton<UserInformRepository>(
    () => UserInformRepoImpl(
        userInformLocalData: sl(),
        userInformRemoteData: sl(),
        networkInfo: sl()),
  );
  sl.registerLazySingleton<AuthenRepoImpl>(
    () => AuthenRepoImpl(sl(), sl(), sl()),
  );

  sl.registerLazySingleton<UpdatePhoneUserInformRepo>(
    () => UpdatePhoneRepoImpl(sl(), sl(), sl()),
  );

  sl.registerLazySingleton<LoginRepository>(
    () => LoginRepositoryImpl(sl(), sl(), sl(), sl()),
  );

  sl.registerLazySingleton<UpdateRelaUserInformRepo>(
    () => UpdateRelaRepoImpl(sl(), sl(), sl()),
  );

  sl.registerLazySingleton<UpdateBasicUserInformRepo>(
    () => UpdateBasicRepoImpl(sl(), sl(), sl()),
  );

  sl.registerLazySingleton<MatchingRepository>(
    () => MatchingRepositoryImpl(remoteData: sl(), networkInfo: sl()),
  );

  sl.registerLazySingleton<GroupRepository>(
    () => GroupRepositoryImplement(sl(), sl(), sl()),
  );

  sl.registerLazySingleton<FavoriteRepository>(
    () => FavoriteReposiotyImpl(sl(), sl()),
  );

  sl.registerLazySingleton<ChatRepository>(
    () => ChatRepositoryImpl(networkInfo: sl(), remoteData: sl()),
  );

  sl.registerLazySingleton<NotificationRepos>(
    () => NotificationReposImpl(networkInfo: sl(), remoteData: sl()),
  );

  // Data sources
  sl.registerLazySingleton<UserInformRemoteData>(
    () => UserInformRemoteDataSourceImpl(client: sl()),
  );

  sl.registerLazySingleton<LoginRemoteData>(
    () => LoginRemoteDataImpl(client: sl()),
  );

  sl.registerLazySingleton<LoginLocalData>(
    () => LoginLocalDataImpl(sl()),
  );

  sl.registerLazySingleton<UpdatePhoneUserRemoteData>(
    () => UpdatePhoneInformRemoteDataSourceImpl(client: sl()),
  );
  sl.registerLazySingleton<UpdateRelaUserRemoteData>(
    () => UpdateRelaInformRemoteDataSourceImpl(client: sl()),
  );

  sl.registerLazySingleton<UpdateBasicUserRemoteData>(
    () => UpdateBasicInformRemoteDataSourceImpl(client: sl()),
  );

  sl.registerLazySingleton<UserInformLocalData>(
    () => UserInformLocalDataImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<AuthenticateRemoteData>(
    () => AuthenticateRemoteDataImpl(client: sl()),
  );

  sl.registerLazySingleton<AuthentcateLocalData>(
    () => AuthentcateLocalDataImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<MatchingRemoteData>(
    () => MatchingRemoteDataImpl(client: sl()),
  );

  sl.registerLazySingleton<PlaceRemoteDataSource>(
    () => PlaceRemoteDataSourceImpl(sl()),
  );

  sl.registerLazySingleton<GroupRemoteData>(
    () => GroupRemoteDataImpl(sl()),
  );

  sl.registerLazySingleton<GroupLocalSource>(
    () => GroupLocalSourceImpl(sharedPreferences: sl()),
  );

  sl.registerLazySingleton<FavoriteRemoteData>(
    () => FavoriteRemoteDataImpl(sl()),
  );

  sl.registerLazySingleton<ChatRemoteData>(
    () => ChatRemoteDataImpl(client: sl()),
  );

  sl.registerLazySingleton<NotificationRemoteData>(
    () => NotificationRemoteDataImpl(client: sl()),
  );

  //! Core
  sl.registerLazySingleton(() => InputConverter());
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<Token>(() => Token(sl()));
  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => DataConnectionChecker());
  sl.registerLazySingleton(() => HttpClient(token: sl()));

  // Share ref
  sl.registerLazySingleton(
      () => StrangerFilterShareRef(sharedPreferences: sl()));
  sl<StrangerFilterShareRef>().init();
}
