import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/utils/simple_bloc_observer.dart';
import 'feature/presentation/pages/app.dart';
import 'injection_container.dart' as di;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print("Handling a background message: ${message.notification}");
}

void main() async {
  // Bloc observer
  Bloc.observer = SimpleBlocObserver();

  // Widget Binding
  WidgetsFlutterBinding.ensureInitialized();

  // Firebase Services
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  // Dependency Injection
  await di.init();

  /// Run App
  runApp(App());
}
