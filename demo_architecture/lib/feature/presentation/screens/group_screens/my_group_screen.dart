import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_of_user_bloc/get_list_group_of_user_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/group_screens/widgets/joined_in_group_tab_view.dart';
import 'package:demo_architecture/feature/presentation/screens/group_screens/widgets/my_group_tab_view.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/add_group_screens/add_group_screen.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

import '../../../../injection_container.dart';
import '../../themes/colors.dart';

class MyGroupScreen extends StatefulWidget {
  static const routeName = 'my-group-screen';

  @override
  _MyGroupScreenState createState() => _MyGroupScreenState();
}

class _MyGroupScreenState extends State<MyGroupScreen> {
  GetListGroupOfUserBloc _groupOfUserBloc;

  void _openAddGroupScreen(BuildContext context) async {
    final result =
        await Navigator.of(context).pushNamed(AddGroupScreen.routeName);
    if (result != null && result) {
      _groupOfUserBloc.add(
        GetListGroupOfUserEventSubmitted(NoParams()),
      );
    }
  }

  @override
  void initState() {
    super.initState();

    // Initial Bloc
    _groupOfUserBloc = sl<GetListGroupOfUserBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBody(size),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _openAddGroupScreen(context),
        backgroundColor: MyAppTheme.primary,
        child: Icon(
          Icons.group_add,
          color: MyAppTheme.secondary,
        ),
      ),
    );
  }

  Widget getBody(Size size) => Column(
        children: [
          const SizedBox(height: 50),
          getHeader(size),
          const SizedBox(height: 16),
          Expanded(
            child: getBodyContent(size),
          ),
        ],
      );

  Widget getHeader(Size size) => Padding(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
        child: Text(
          'My Groups',
          style: TextStyle(
            fontSize: MyTextStyle.title_2_fontsize,
            color: MyAppTheme.black_grey,
          ),
        ),
      );

  Widget getBodyContent(Size size) => DefaultTabController(
        length: 2,
        child: Column(
          children: [
            TabBar(
              labelPadding: const EdgeInsets.all(10),
              indicatorColor: MyAppTheme.primary,
              labelColor: MyAppTheme.primary,
              unselectedLabelColor: MyAppTheme.black_grey,
              tabs: [
                Text(
                  'Of Me',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_5_fontsize,
                  ),
                ),
                Text(
                  'Joined in',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_5_fontsize,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            Expanded(
              child: TabBarView(
                children: [
                  MyGroupTabView(groupOfUserBloc: _groupOfUserBloc),
                  JoinedGroupTabView(groupOfUserBloc: _groupOfUserBloc),
                ],
              ),
            ),
          ],
        ),
      );
}
