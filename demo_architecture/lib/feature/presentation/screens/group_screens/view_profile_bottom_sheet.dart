import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../domain/entities/user/user_inform.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';

class ViewProfileBottomSheet extends StatelessWidget {
  final UserInform host;

  const ViewProfileBottomSheet({Key key, this.host}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 16,
            ),
            Container(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CircleAvatar(
                    backgroundImage: NetworkImage(host.avt[0]),
                    radius: 80,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    host.hoten,
                    style: GoogleFonts.arimaMadurai(
                        fontSize: MyTextStyle.title_2_fontsize),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Wrap(
                spacing: 8,
                runSpacing: 8,
                children: List.generate(
                  host.favorites.length,
                  (index) => SizedBox(
                    height: 32,
                    child: Chip(
                      label: Text(host.favorites[index].name),
                      labelStyle: TextStyle(color: MyAppTheme.secondary),
                      backgroundColor: MyAppTheme.black_grey,
                    ),
                  ),
                )),
            SizedBox(
              height: 16,
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: CircleAvatar(
                child: Icon(
                  Icons.phone,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
              ),
              title: Text(host.phone),
              subtitle: Text("Phone"),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: CircleAvatar(
                child: Icon(
                  FontAwesome.transgender,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
              ),
              title: Text(host.gioitinh),
              subtitle: Text("Gender"),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: CircleAvatar(
                child: Icon(
                  Icons.cake,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
              ),
              title: Text(host.birthDay),
              subtitle: Text("Birthday"),
            ),
            ListTile(
              contentPadding: EdgeInsets.all(0),
              leading: CircleAvatar(
                child: Icon(
                  Icons.family_restroom,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
              ),
              title: Text(host.rela),
              subtitle: Text("Relationship status"),
            ),
          ],
        ),
      ),
    );
  }
}
