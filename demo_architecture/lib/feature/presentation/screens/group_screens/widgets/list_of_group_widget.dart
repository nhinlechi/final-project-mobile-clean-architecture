import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/group_detail_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_item.dart';
import 'package:flutter/material.dart';

class ListOfGroupWidget extends StatelessWidget {
  final List<GroupEntity> listGroup;

  const ListOfGroupWidget({Key key, @required this.listGroup}) : super(key: key);

  void _goToGroup(BuildContext context, String imageUrl, GroupEntity group) {
    Navigator.of(context).pushNamed(
      GroupDetailScreen.routeName,
      arguments: ArgsGroupDetailScreen(
        imageUrl: imageUrl,
        heroTag: 'groupItem ${group.id}',
        group: group,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 0.9,
      ),
      itemCount: listGroup.length,
      itemBuilder: (context, index) => GroupItem(
        heroTag: 'groupItem ${listGroup[index].id}',
        imageUrl: listGroup[index].avatar,
        nameGroup: listGroup[index].name,
        isPrivate: listGroup[index].isprivate,
        onTap: () => _goToGroup(
          context,
          listGroup[index].avatar,
          listGroup[index],
        ),
      ),
    );
  }
}
