import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_of_user_bloc/get_list_group_of_user_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/group_screens/widgets/list_of_group_widget.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyGroupTabView extends StatelessWidget {
  final GetListGroupOfUserBloc groupOfUserBloc;

  const MyGroupTabView({
    Key key,
    @required this.groupOfUserBloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final auth = context.read<UserRepositoryProvider>().auth;
    return BlocBuilder<GetListGroupOfUserBloc, GetListGroupOfUserState>(
      bloc: groupOfUserBloc,
      builder: (context, state) {
        // Get My Groups
        if (state is GetListGroupOfUserInitial) {
          groupOfUserBloc.add(
            GetListGroupOfUserEventSubmitted(NoParams()),
          );
        }
        // Loading State
        if (state is GetListGroupOfUserLoading) {
          return CircularLoading();
        }
        // Get My Group Success
        if (state is GetListGroupOfUserLoaded) {
          final myGroups =
              state.listGroupEntity.groups.where((g) => g.host == auth.userId).toList();
          return ListOfGroupWidget(listGroup: myGroups);
        }
        // Other state
        return Container();
      },
    );
  }
}
