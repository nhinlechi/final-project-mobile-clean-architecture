import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../data_prototype/girl.dart';

class AvatarSlider extends StatefulWidget {
  const AvatarSlider({Key key}) : super(key: key);

  @override
  _AvatarSliderState createState() => _AvatarSliderState();
}

class _AvatarSliderState extends State<AvatarSlider> {
  final List<String> avatarsGirls = [
    girls[0]['img'],
    girls[1]['img'],
    girls[2]['img'],
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocBuilder<UserInformBloc, UserInformState>(
      builder: (context, state) {
        List<String> avatars;
        bool isNetwork;
        if (state is Loaded) {
          isNetwork = true;
          avatars = state.userInform.avt;
        } else {
          avatars = avatarsGirls;
          isNetwork = false;
        }
        return CarouselSlider.builder(
            itemCount: avatars.length,
            itemBuilder: (context, index, realIndex) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  width: size.width,
                  child: isNetwork
                      ? Image.network(
                          avatars[index],
                          fit: BoxFit.cover,
                        )
                      : Image.asset(
                          avatars[index],
                          fit: BoxFit.cover,
                        ),
                ),
              );
            },
            options: CarouselOptions(
              height: size.height / 2.1,
              enlargeCenterPage: true,
              autoPlay: true,
            ));
      },
    );
  }
}
