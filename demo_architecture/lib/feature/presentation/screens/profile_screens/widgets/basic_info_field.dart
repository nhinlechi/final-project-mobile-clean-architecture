import 'package:flutter/material.dart';

import '../../../themes/colors.dart';

class BasicInfoField extends StatelessWidget {
  final String title;
  final String subtitle;
  final IconData iconData;

  const BasicInfoField({
    Key key,
    @required this.title,
    @required this.subtitle,
    @required this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      contentPadding: EdgeInsets.all(0),
      leading: CircleAvatar(
        child: Icon(
          this.iconData,
          color: MyAppTheme.secondary,
        ),
        backgroundColor: MyAppTheme.black_grey,
      ),
      title: Text(
        this.title,
        style: TextStyle(),
      ),
      subtitle: Text(this.subtitle),
    );
  }
}
