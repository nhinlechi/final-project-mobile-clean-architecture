import 'dart:io';

import 'package:flutter/material.dart';

import '../../../themes/colors.dart';

enum ImageType {
  none,
  local,
  network,
}

class EditAvatarContainer extends StatelessWidget {
  final String avatarUri;
  final ImageType type;
  final Function() addAvatarCallback;
  final Function(String imageUri) deleteAvatarCallback;

  const EditAvatarContainer({
    Key key,
    @required this.avatarUri,
    @required this.type,
    this.addAvatarCallback,
    this.deleteAvatarCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width / 3 - 3 * 8;
    return GestureDetector(
      onTap: () => addAvatarCallback?.call(),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: width * 13 / 10,
            width: width,
            decoration: BoxDecoration(
              color: MyAppTheme.grey,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: MyAppTheme.grey,
                  offset: Offset(0, 2),
                  blurRadius: 10,
                )
              ],
              image: this.avatarUri != null
                  ? DecorationImage(
                      image: type == ImageType.network
                          ? NetworkImage(this.avatarUri)
                          : FileImage(File(this.avatarUri)),
                      fit: BoxFit.cover,
                    )
                  : null,
            ),
          ),
          Positioned(
            bottom: -7,
            right: -7,
            child: PhysicalModel(
              color: MyAppTheme.grey,
              borderRadius: BorderRadius.circular(30),
              elevation: 4,
              child: InkWell(
                onTap: () {
                  if (deleteAvatarCallback != null) {
                    deleteAvatarCallback(this.avatarUri);
                  } else {
                    addAvatarCallback();
                  }
                },
                child: CircleAvatar(
                  radius: 14,
                  backgroundColor: avatarUri == null
                      ? MyAppTheme.primary
                      : MyAppTheme.black_grey,
                  child: Icon(
                    avatarUri == null ? Icons.add : Icons.clear_rounded,
                    color: MyAppTheme.secondary,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
