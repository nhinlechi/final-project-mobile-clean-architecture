import 'package:flutter/material.dart';

import '../../../themes/colors.dart';

class InfoField extends StatelessWidget {
  final String text;
  final IconData iconData;

  const InfoField({
    Key key,
    @required this.text,
    @required this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: Row(
        children: [
          Icon(
            iconData,
            color: MyAppTheme.black_grey,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            text,
            style: TextStyle(
              fontWeight: FontWeight.normal,
              color: MyAppTheme.black_grey,
            ),
          ),
        ],
      ),
    );
  }
}
