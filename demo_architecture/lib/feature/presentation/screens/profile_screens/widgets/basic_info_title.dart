import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../../../injection_container.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/get_user_inform.dart';
import '../../../../domain/usecase/user/update_user_inform_uc.dart';
import '../../../bloc/user_blocs/drop_down_rela/dropdown_rela_bloc.dart';
import '../../../bloc/user_blocs/update_user_bloc/update_user_bloc.dart';
import '../../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';

enum EditInforType {
  edt_basic,
  edt_phone,
  edt_rela,
}

class BasicInfoTitle extends StatelessWidget {
  final String title;
  final EditInforType editInforType;
  final UserInform userInform;
  BasicInfoTitle({
    Key key,
    @required this.title,
    @required this.userInform,
    this.editInforType,
  }) : super(key: key);

  final List<String> genderList = ["Nam", "Nữ", "Khác"];
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> showEditBasicDialog(
      BuildContext context, String title, UserInform user) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(create: (_) => sl<UpdateUserBloc>()),
            ],
            child: BlocBuilder<UpdateUserBloc, UpdateUserState>(
                builder: (context, state) {
              String ngaysinh = user.birth.microsecondsSinceEpoch.toString();
              String gioitinh = (genderList
                          .indexWhere((element) => element == user.gioitinh) +
                      1)
                  .toString();
              final TextEditingController _textEditingLastNameController =
                  TextEditingController(text: user.lastname);
              final TextEditingController _textEditingFirstNameController =
                  TextEditingController(text: user.firstname);
              return AlertDialog(
                content: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          controller: _textEditingFirstNameController,
                          validator: (value) {
                            return value.isNotEmpty ? null : "Xin nhập họ";
                          },
                          decoration: InputDecoration(labelText: "Họ"),
                        ),
                        TextFormField(
                          controller: _textEditingLastNameController,
                          validator: (value) {
                            return value.isNotEmpty ? null : "Xin nhập tên";
                          },
                          decoration: InputDecoration(labelText: "Tên"),
                        ),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Giới tính"),
                              BlocProvider(
                                create: (_) => DropdownRelaBloc(),
                                child: BlocBuilder<DropdownRelaBloc,
                                        DropdownRelaState>(
                                    builder: (context, state) {
                                  if (state is DropdownChange)
                                    return DropdownButton<String>(
                                      value: state.onValueChange,
                                      //elevation: 5,
                                      style: TextStyle(color: Colors.black),
                                      items: genderList
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      hint: Text(
                                        user.gioitinh,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      onChanged: (String value) {
                                        gioitinh = (genderList.indexWhere(
                                                    (element) =>
                                                        element == value) +
                                                1)
                                            .toString();
                                        context
                                            .read<DropdownRelaBloc>()
                                            .add(ChangedValueDropdown(value));
                                      },
                                    );
                                  else
                                    return DropdownButton<String>(
                                      value: user.gioitinh,
                                      //elevation: 5,
                                      style: TextStyle(color: Colors.black),
                                      items: genderList
                                          .map<DropdownMenuItem<String>>(
                                              (String value) {
                                        return DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(value),
                                        );
                                      }).toList(),
                                      hint: Text(
                                        user.rela,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      onChanged: (String value) {
                                        gioitinh = (genderList.indexWhere(
                                                    (element) =>
                                                        element == value) +
                                                1)
                                            .toString();
                                        context
                                            .read<DropdownRelaBloc>()
                                            .add(ChangedValueDropdown(value));
                                      },
                                    );
                                }),
                              ),
                            ]),
                        InputDatePickerFormField(
                          initialDate: user.birth,
                          firstDate: DateTime(0),
                          lastDate: DateTime(2021),
                          fieldLabelText: "Ngày sinh",
                          errorFormatText: "Sai định dạng (02/02/2000)",
                          selectableDayPredicate: (val) {
                            ngaysinh = val.millisecondsSinceEpoch.toString();
                            return true;
                          },
                        ),
                      ],
                    )),
                title: Text(title),
                actions: <Widget>[
                  BlocBuilder<UpdateUserBloc, UpdateUserState>(
                      builder: (context, state) {
                    if (state is UpdateUserLoaded) {
                      Navigator.of(context).pop();
                      context.read<UserInformBloc>().add(GetUserInformEvent(
                          GetUserInformParams(["favorites"])));
                    }
                    if (state is UpdateUserLoading)
                      return const CircularProgressIndicator();
                    else
                      return ElevatedButton(
                        child: Text('OK'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                MyAppTheme.primary)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            // Do something like updating SharedPreferences or User Settings etc.
                            context.read<UpdateUserBloc>().add(
                                UpdateUserEventSubmit(UpdateUserInformParams(
                                    dateofbirth: int.parse(ngaysinh),
                                    firstname:
                                        _textEditingFirstNameController.text,
                                    lastname:
                                        _textEditingLastNameController.text,
                                    gender: int.parse(gioitinh))));
                          }
                        },
                      );
                  })
                ],
              );
            }),
          );
        });
  }

  Future<void> showEditPhoneDialog(
      BuildContext context, String title, UserInform user) async {
    return await showDialog(
        context: context,
        builder: (context) {
          final TextEditingController _textEditingPhoneController =
              TextEditingController(text: user.phone);
          return MultiBlocProvider(
            providers: [
              BlocProvider(create: (_) => sl<UpdateUserBloc>()),
            ],
            child: BlocBuilder<UpdateUserBloc, UpdateUserState>(
                builder: (context, state) {
              return AlertDialog(
                content: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.phone,
                          controller: _textEditingPhoneController,
                          validator: (value) {
                            return (value.length == 10 && value[0] == '0')
                                ? null
                                : "Xin nhập đúng số điện thoại";
                          },
                          decoration:
                              InputDecoration(labelText: "Số điện thoại"),
                        ),
                      ],
                    )),
                title: Text(title),
                actions: <Widget>[
                  BlocBuilder<UpdateUserBloc, UpdateUserState>(
                      builder: (context, state) {
                    if (state is UpdateUserLoaded) {
                      Navigator.of(context).pop();
                      context.read<UserInformBloc>().add(GetUserInformEvent(
                          GetUserInformParams(["favorites"])));
                    }
                    if (state is UpdateUserLoading)
                      return const CircularProgressIndicator();
                    else
                      return ElevatedButton(
                        child: Text('OK'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                MyAppTheme.primary)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            // Do something like updating SharedPreferences or User Settings etc.
                            context.read<UpdateUserBloc>().add(
                                UpdateUserEventSubmit(UpdateUserInformParams(
                                    phone: _textEditingPhoneController.text)));
                          }
                        },
                      );
                  })
                ],
              );
            }),
          );
        });
  }

  int relationship;

  Future<void> showEditRelationShipDialog(
      BuildContext context, String title, UserInform user) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(create: (_) => sl<UpdateUserBloc>()),
            ],
            child: BlocBuilder<UpdateUserBloc, UpdateUserState>(
                builder: (context, state) {
              return AlertDialog(
                content: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        BlocProvider(
                          create: (_) => DropdownRelaBloc(),
                          child:
                              BlocBuilder<DropdownRelaBloc, DropdownRelaState>(
                                  builder: (context, state) {
                            if (state is DropdownChange)
                              return DropdownButton<String>(
                                value: state.onValueChange,
                                //elevation: 5,
                                style: TextStyle(color: Colors.black),
                                items: <String>[
                                  'Độc thân',
                                  'Hẹn hò',
                                  'Đã ly hôn'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                hint: Text(
                                  user.rela,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                                onChanged: (String value) {
                                  final List<String> items = [
                                    'Hẹn hò',
                                    'Độc thân',
                                    'Đã ly hôn'
                                  ];
                                  relationship = items.indexWhere(
                                          (element) => element == value) +
                                      1;
                                  context
                                      .read<DropdownRelaBloc>()
                                      .add(ChangedValueDropdown(value));
                                },
                              );
                            else
                              return DropdownButton<String>(
                                value: user.rela,
                                //elevation: 5,
                                style: TextStyle(color: Colors.black),
                                items: <String>[
                                  'Độc thân',
                                  'Hẹn hò',
                                  'Đã ly hôn'
                                ].map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                hint: Text(
                                  user.rela,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                                onChanged: (String value) {
                                  final List<String> items = [
                                    'Hẹn hò',
                                    'Độc thân',
                                    'Đã ly hôn'
                                  ];
                                  relationship = items.indexWhere(
                                          (element) => element == value) +
                                      1;
                                  context
                                      .read<DropdownRelaBloc>()
                                      .add(ChangedValueDropdown(value));
                                },
                              );
                          }),
                        ),
                      ],
                    )),
                title: Text(title),
                actions: <Widget>[
                  BlocBuilder<UpdateUserBloc, UpdateUserState>(
                      builder: (context, state) {
                    if (state is UpdateUserLoaded) {
                      Navigator.of(context).pop();
                      context.read<UserInformBloc>().add(GetUserInformEvent(
                          GetUserInformParams(["favorites"])));
                    }
                    if (state is UpdateUserLoading)
                      return const CircularProgressIndicator();
                    else
                      return ElevatedButton(
                        child: Text('OK'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                MyAppTheme.primary)),
                        onPressed: () {
                          // Do something like updating SharedPreferences or User Settings etc.
                          context.read<UpdateUserBloc>().add(
                              UpdateUserEventSubmit(UpdateUserInformParams(
                                  relationship: relationship)));
                        },
                      );
                  })
                ],
              );
            }),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          this.title,
          style: TextStyle(
            fontSize: MyTextStyle.title_4_fontsize,
            fontWeight: FontWeight.bold,
            color: MyAppTheme.black_grey,
          ),
        ),
        IconButton(
            icon: Icon(
              Feather.edit,
              size: 24,
              color: MyAppTheme.primary,
            ),
            onPressed: () async {
              switch (this.editInforType) {
                case EditInforType.edt_basic:
                  await showEditBasicDialog(
                      context, "Chỉnh sửa thông tin cơ bản", userInform);
                  break;
                case EditInforType.edt_phone:
                  await showEditPhoneDialog(
                      context, "Chỉnh sửa số điện thoại", userInform);
                  break;
                case EditInforType.edt_rela:
                  await showEditRelationShipDialog(
                      context, "Chỉnh sửa mối quan hệ", userInform);
                  break;
              }
            })
      ],
    );
  }
}
