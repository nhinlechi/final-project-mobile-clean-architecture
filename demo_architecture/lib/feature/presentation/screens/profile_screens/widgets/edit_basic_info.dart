import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../../domain/entities/user/user_inform.dart';
import '../../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import 'basic_info_field.dart';
import 'basic_info_title.dart';

class EditBasicInfo extends StatefulWidget {
  final UserInform userInform;

  const EditBasicInfo({Key key, this.userInform}) : super(key: key);

  @override
  _EditBasicInfoState createState() => _EditBasicInfoState();
}

class _EditBasicInfoState extends State<EditBasicInfo> {
  UserInform user;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    user = widget.userInform;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserInformBloc, UserInformState>(
      builder: (context, state) {
        if (state is Loaded) {
          user = state.userInform;
        }
        return Column(
          children: [
            BasicInfoTitle(
              title: 'Basic Information',
              editInforType: EditInforType.edt_basic,
              userInform: user,
            ),
            BasicInfoField(
              title: user.hoten,
              iconData: Icons.person,
              subtitle: 'Name',
            ),
            BasicInfoField(
              title: user.gioitinh,
              iconData: FontAwesome.transgender,
              subtitle: 'Gender',
            ),
            BasicInfoField(
              title: user.birthDay,
              iconData: Icons.cake,
              subtitle: 'Birth Date',
            ),
            BasicInfoTitle(
              title: 'Contact Information',
              editInforType: EditInforType.edt_phone,
              userInform: user,
            ),
            BasicInfoField(
              title: user.sdt,
              iconData: Icons.phone,
              subtitle: 'Phone',
            ),
            BasicInfoTitle(
              title: 'Relationship Information',
              editInforType: EditInforType.edt_rela,
              userInform: user,
            ),
            BasicInfoField(
              title: user.rela,
              iconData: Icons.family_restroom,
              subtitle: 'Relationship',
            ),
          ],
        );
      },
    );
  }
}
