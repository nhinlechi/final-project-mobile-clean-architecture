import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/favorite/favorite_model.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../../themes/colors.dart';
import '../edit_favorite_screen.dart';

class EditFavorites extends StatefulWidget {
  final List<FavoriteModel> userFavorites;
  final UserInform userInform;

  const EditFavorites({
    Key key,
    this.userFavorites,
    this.userInform,
  }) : super(key: key);
  @override
  _EditFavoritesState createState() => _EditFavoritesState();
}

class _EditFavoritesState extends State<EditFavorites> {
  void _onEditFavorites() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => EditFavoriteScreen(
          userInform: widget.userInform,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserInformBloc, UserInformState>(
      builder: (context, state) {
        List<FavoriteModel> listFavorite;
        if (state is Loaded) {
          listFavorite = state.userInform.favorites;
        } else
          listFavorite = widget.userFavorites;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
                spacing: 8,
                runSpacing: 8,
                children: List.generate(
                  listFavorite.length,
                  (index) => SizedBox(
                    height: 32,
                    child: Chip(
                      label: Text(listFavorite[index].name),
                      labelStyle: TextStyle(color: MyAppTheme.secondary),
                      backgroundColor: MyAppTheme.black_grey,
                    ),
                  ),
                )),
            SizedBox(
              height: 8,
            ),
            SizedBox(
              height: 32,
              child: ActionChip(
                label: Text('Edit ...'),
                labelStyle: TextStyle(color: MyAppTheme.secondary),
                backgroundColor: MyAppTheme.primary,
                elevation: 4,
                onPressed: _onEditFavorites,
              ),
            ),
          ],
        );
      },
    );
  }
}
