import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../../injection_container.dart';
import '../../../../domain/usecase/user/get_user_inform.dart';
import '../../../../domain/usecase/user/update_user_avatar_uc.dart';
import '../../../bloc/user_blocs/update_avatar/update_user_avatar_bloc.dart';
import '../../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../../repository_providers/user_repository_provider.dart';
import '../../../themes/colors.dart';
import '../../../widgets/generals/circular_loading.dart';
import 'edit_avatar_container.dart';

class EditAvatar extends StatefulWidget {
  @override
  _EditAvatarState createState() => _EditAvatarState();
}

class _EditAvatarState extends State<EditAvatar> {
  final ImagePicker _imagePicker = ImagePicker();

  UpdateUserAvatarBloc _updateUserAvatarBloc;
  List<String> avatarUrls;
  List<File> imageFiles = [];
  bool editMode = false;

  Future<File> _pickImageFromGallery() async {
    final pickedFile = await _imagePicker.getImage(
      source: ImageSource.gallery,
      maxHeight: 1000,
      maxWidth: 1000,
    );
    return File(pickedFile.path);
  }

  void addAvatarHandler() async {
    // Switch View Mode to Edit Mode
    editMode = true;
    _updateUserAvatarBloc.add(EditUserAvatarEvent());
    //
    final imageFile = await _pickImageFromGallery();
    setState(() {
      imageFiles.add(imageFile);
    });
  }

  void deleteAvatarHandler(String imageUri) {
    // Switch View Mode to Edit Mode
    editMode = true;
    _updateUserAvatarBloc.add(EditUserAvatarEvent());
    //
    setState(() {
      avatarUrls.remove(imageUri);
      imageFiles.removeWhere((imageFile) => imageFile.path == imageUri);
    });
  }

  void saveAvatarChange() {
    if (avatarUrls.length + imageFiles.length < 1) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('At least 1 images!')),
      );
      return;
    }
    _updateUserAvatarBloc.add(
      StartUpdateUserAvatarEvent(
        params: UpdateUserAvatarParams(
          imageFiles: this.imageFiles,
          oldAvatarUrls: this.avatarUrls,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    // Get all avatar
    final userInfo = context.read<UserRepositoryProvider>().userInform;
    avatarUrls = [...userInfo.avt];

    // Get Bloc
    _updateUserAvatarBloc = sl<UpdateUserAvatarBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Wrap(
          spacing: 20,
          runSpacing: 24,
          children: List.generate(
            6,
            (index) {
              if (index < avatarUrls.length) {
                return EditAvatarContainer(
                  type: ImageType.network,
                  avatarUri: avatarUrls[index],
                  deleteAvatarCallback: deleteAvatarHandler,
                );
              }

              if (index < avatarUrls.length + imageFiles.length) {
                return EditAvatarContainer(
                  type: ImageType.local,
                  avatarUri: imageFiles[index - avatarUrls.length].path,
                  deleteAvatarCallback: deleteAvatarHandler,
                );
              }

              return EditAvatarContainer(
                type: ImageType.none,
                avatarUri: null,
                addAvatarCallback: addAvatarHandler,
              );
            },
          ),
        ),
        if (editMode)
          Column(
            children: [
              BlocBuilder<UpdateUserAvatarBloc, UpdateUserAvatarState>(
                bloc: _updateUserAvatarBloc,
                builder: (context, state) {
                  if (state is UpdateUserAvatarLoadingState) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircularLoading(),
                    );
                  }
                  if (state is UpdateUserAvatarSuccessState) {
                    context.read<UserInformBloc>().add(
                        GetUserInformEvent(GetUserInformParams(["favorites"])));
                    return Container();
                  }
                  return Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: ElevatedButton(
                      onPressed: saveAvatarChange,
                      child: Text(
                        'Save Avatar Changes',
                        style: TextStyle(
                          color: MyAppTheme.secondary,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
      ],
    );
  }
}
