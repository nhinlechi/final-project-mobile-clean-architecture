import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/usecase/user/get_user_inform.dart';
import '../../../domain/usecase/user/update_user_inform_uc.dart';
import '../../bloc/favorite_bloc/get_list_favorite/get_list_favorite_bloc.dart';
import '../../bloc/user_blocs/update_user_bloc/update_user_bloc.dart';
import '../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';

class EditFavoriteScreen extends StatefulWidget {
  final UserInform userInform;

  const EditFavoriteScreen({Key key, this.userInform}) : super(key: key);

  @override
  _EditFavoriteScreenState createState() => _EditFavoriteScreenState();
}

class _EditFavoriteScreenState extends State<EditFavoriteScreen> {
  UpdateUserBloc _updateUserBloc;
  GetListFavoriteBloc getListFavoriteBloc;
  List<String> _idFilters = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getListFavoriteBloc = sl<GetListFavoriteBloc>();
    _updateUserBloc = sl<UpdateUserBloc>();
    getListFavoriteBloc.add(GetFavoriteRegister());
    widget.userInform.favorites.forEach((element) {
      _idFilters.add(element.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocListener<UpdateUserBloc, UpdateUserState>(
        bloc: _updateUserBloc,
        listener: (context, state) {
          if (state is UpdateUserLoaded) {
            context
                .read<UserInformBloc>()
                .add(GetUserInformEvent(GetUserInformParams(['favorites'])));
            Navigator.pop(context);
          }
        },
        child: Scaffold(
          body: Container(
            width: size.width,
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40),
                Text(
                  'You Like what...?',
                  style: MyTextStyle.ts_register_title,
                ),
                SizedBox(height: 20),
                Expanded(
                  child: SingleChildScrollView(
                    child: BlocBuilder<GetListFavoriteBloc,
                            GetListFavoriteState>(
                        bloc: getListFavoriteBloc,
                        builder: (context, state) {
                          if (state is GetListFavoriteLoaded)
                            return Wrap(
                              spacing: 10,
                              children: List.generate(
                                (state.listFavorite.length < 30)
                                    ? state.listFavorite.length
                                    : 30,
                                (index) => FilterChip(
                                  label: Text(
                                    state.listFavorite[index].name,
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: MyTextStyle.title_5_fontsize,
                                    color: MyAppTheme.secondary,
                                  ),
                                  backgroundColor: MyAppTheme.black_grey,
                                  selectedColor: MyAppTheme.primary,
                                  checkmarkColor: MyAppTheme.secondary,
                                  selected: _idFilters
                                      .contains(state.listFavorite[index].id),
                                  onSelected: (value) {
                                    setState(() {
                                      if (value &&
                                          !_idFilters.contains(
                                              state.listFavorite[index].id)) {
                                        _idFilters
                                            .add(state.listFavorite[index].id);
                                      } else {
                                        _idFilters.removeWhere((id) =>
                                            id == state.listFavorite[index].id);
                                      }
                                    });
                                  },
                                ),
                              ),
                            );
                          return Container();
                        }),
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
                /*ElevatedButton(
                  onPressed: () {},
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    primary: MyAppTheme.primary,
                  ),
                  child: Text(
                    'See more...',
                    style: TextStyle(
                      color: MyAppTheme.secondary,
                    ),
                  ),
                ),*/
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              _updateUserBloc.add(UpdateUserEventSubmit(
                  UpdateUserInformParams(favorites: _idFilters)));
            },
            child: Icon(
              Icons.check,
              size: 36,
              color: MyAppTheme.secondary,
            ),
            backgroundColor: MyAppTheme.primary,
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerFloat,
        ));
  }
}
