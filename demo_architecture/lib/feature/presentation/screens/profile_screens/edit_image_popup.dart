import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../themes/colors.dart';

class GetImagePopup extends StatefulWidget {
  final Function(File imageFile) callback;

  const GetImagePopup({Key key, this.callback}) : super(key: key);

  @override
  _GetImagePopupState createState() => _GetImagePopupState();
}

class _GetImagePopupState extends State<GetImagePopup> {
  PickedFile _imageFile;

  final ImagePicker _imagePicker = ImagePicker();

  void _pickImageFromGallery() async {
    final pickedFile = await _imagePicker.getImage(
      source: ImageSource.gallery,
      maxHeight: 500,
      maxWidth: 500,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }

  void _pickImageFromCamera() async {
    final pickedFile = await _imagePicker.getImage(source: ImageSource.camera);
    setState(() {
      _imageFile = pickedFile;
    });
  }

  void _completeEditImage() async {
    if (_imageFile == null) return;

    if (widget.callback != null) {
      widget.callback(File(_imageFile.path));
    }

    Navigator.of(context).pop(File(_imageFile.path));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height,
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: size.height * 1 / 6),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: MyAppTheme.secondary,
      ),
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: MyAppTheme.grey,
                borderRadius: BorderRadius.circular(20),
                border: Border.all(
                  width: 1,
                  color: MyAppTheme.grey,
                ),
                image: _imageFile != null
                    ? DecorationImage(
                        image: FileImage(File(_imageFile.path)),
                        fit: BoxFit.cover,
                      )
                    : null,
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: ElevatedButton(
                      onPressed: _pickImageFromGallery,
                      child: Icon(
                        Icons.photo,
                        color: MyAppTheme.secondary,
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        primary: MyAppTheme.black_grey,
                        padding: EdgeInsets.symmetric(horizontal: 0),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  SizedBox(
                    height: 40,
                    width: 40,
                    child: ElevatedButton(
                      onPressed: _pickImageFromCamera,
                      child: Icon(
                        Icons.add_a_photo,
                        color: MyAppTheme.secondary,
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: CircleBorder(),
                        primary: MyAppTheme.black_grey,
                        padding: EdgeInsets.symmetric(horizontal: 0),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 48,
                width: 48,
                child: ElevatedButton(
                  onPressed: _completeEditImage,
                  child: Icon(
                    Icons.check,
                    color: MyAppTheme.secondary,
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    primary: MyAppTheme.primary,
                    padding: EdgeInsets.symmetric(horizontal: 0),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
