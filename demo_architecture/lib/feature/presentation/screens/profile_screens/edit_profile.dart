import 'package:demo_architecture/feature/presentation/bloc/authentication_blocs/loginv2/authenticate_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/login_screens/login_screen.dart';
import 'package:demo_architecture/feature/presentation/widgets/dialogs.dart';
import 'package:flutter/material.dart';

import '../../../domain/entities/user/user_inform.dart';
import '../../themes/colors.dart';
import '../../widgets/generals/my_app_bar.dart';
import 'widgets/edit_avatar.dart';
import 'widgets/edit_basic_info.dart';
import 'widgets/edit_favorites.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditProfileScreen extends StatefulWidget {
  final UserInform userInform;

  const EditProfileScreen({
    Key key,
    this.userInform,
  }) : super(key: key);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  void _logout() async {
    final confirm = await showCustomConfirmDialog(
      context,
      'Logout',
      'Do you wanna logout? We will miss you!',
    );
    if (confirm != null && confirm) {
      context.read<AuthenticateBloc>().add(LogoutEvent());
      Navigator.of(context).pushNamedAndRemoveUntil(
        LoginScreen.routeName,
        (route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBody(size),
      appBar: getAppBar(),
    );
  }

  getBody(Size size) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
          width: size.width,
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              EditAvatar(),
              SizedBox(height: 32),
              EditFavorites(
                userFavorites: widget.userInform.favorites,
                userInform: widget.userInform,
              ),
              SizedBox(height: 32),
              EditBasicInfo(
                userInform: widget.userInform,
              ),
              getLogoutButton(),
            ],
          ),
        ),
      ),
    );
  }

  getAppBar() {
    return MyAppBar(
      title: Text(''),
    );
  }

  getLogoutButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 16),
      child: InkWell(
        onTap: _logout,
        child: ListTile(
          contentPadding: EdgeInsets.all(0),
          leading: CircleAvatar(
            child: Icon(
              Icons.logout,
              color: MyAppTheme.secondary,
            ),
            backgroundColor: MyAppTheme.primary,
          ),
          title: Text(
            'Log Out',
            style: TextStyle(),
          ),
          subtitle: Text('Choose another account?'),
        ),
      ),
    );
  }
}
