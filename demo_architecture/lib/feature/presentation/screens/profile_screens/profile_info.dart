import 'package:demo_architecture/feature/presentation/screens/group_screens/my_group_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../domain/entities/user/user_inform.dart';
import '../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import '../../widgets/generals/circle_button.dart';
import '../../widgets/generals/circular_loading.dart';
import 'edit_profile.dart';
import 'widgets/info_field.dart';

class ProfileInfo extends StatefulWidget {
  const ProfileInfo({Key key}) : super(key: key);

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  void _onEditProfile(BuildContext context, UserInform userInform) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => EditProfileScreen(
          userInform: userInform,
        ),
      ),
    );
  }

  void _openMyGroupScreen(BuildContext context) {
    Navigator.of(context).pushNamed(MyGroupScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final rightPadding = 16.0;
    final buttonSize = 50.0; // TODO: Refactor this hard code (hard size)
    return BlocBuilder<UserInformBloc, UserInformState>(
        builder: (context, state) {
      if (state is Loaded) {
        UserInform user = state.userInform;
        return Stack(
          children: [
            Column(
              children: [
                SizedBox(
                  height: size.height / 2.1 + 16,
                ),
                Expanded(
                  child: Container(
                      width: size.width,
                      padding: EdgeInsets.fromLTRB(16, 20, 16, 0),
                      decoration: BoxDecoration(
                        color: MyAppTheme.secondary,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(28),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.6),
                            offset: Offset(0, -8),
                            blurRadius: 16,
                          )
                        ],
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${user.hoten}, ${user.age}',
                              style: TextStyle(
                                fontSize: MyTextStyle.title_3_fontsize,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InfoField(
                              text: user.rela,
                              iconData: Icons.family_restroom,
                            ),
                            InfoField(
                              text: user.gioitinh,
                              iconData: FontAwesome.transgender,
                            ),
                            InfoField(
                              text: user.sdt,
                              iconData: Icons.contact_phone,
                            ),
                            InfoField(
                              text: 'Lives in Ho Chi Minh City',
                              iconData: Icons.location_on,
                            ),
                            InfoField(
                              text: user.birthDay,
                              iconData: Icons.cake,
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Wrap(
                              spacing: 8,
                              runSpacing: 8,
                              children: List.generate(
                                user.favorites.length,
                                (index) => SizedBox(
                                  height: 32,
                                  child: Chip(
                                    label: Text(user.favorites[index].name),
                                    labelStyle:
                                        TextStyle(color: MyAppTheme.secondary),
                                    backgroundColor: MyAppTheme.black_grey,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 32,
                            ),
                            SizedBox(height: 16),
                          ],
                        ),
                      )),
                ),
              ],
            ),
            Positioned(
              top: size.height / 2.1 - 8,
              right: rightPadding + buttonSize + 8,
              child: CircleButton(
                iconColor: MyAppTheme.primary,
                backgroundColor: MyAppTheme.secondary,
                onTap: () => _openMyGroupScreen(context),
                icon: FontAwesome.group,
              ),
            ),
            Positioned(
              top: size.height / 2.1 - 8,
              right: rightPadding,
              child: CircleButton(
                iconColor: MyAppTheme.secondary,
                backgroundColor: MyAppTheme.primary,
                onTap: () => _onEditProfile(context, user),
                icon: Feather.edit_2,
              ),
            ),
          ],
        );
      }
      return CircularLoading();
    });
  }
}
