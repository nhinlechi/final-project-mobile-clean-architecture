import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../../../themes/colors.dart';

class NotificationItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final IconData subIconData;
  final String avatarURL;
  final Function() deleteCallback;
  final Function() onTapCallback;

  const NotificationItem({
    Key key,
    @required this.title,
    @required this.subtitle,
    @required this.subIconData,
    this.avatarURL,
    this.deleteCallback,
    this.onTapCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTapCallback?.call(),
      child: Slidable(
        actionPane: SlidableDrawerActionPane(),
        actionExtentRatio: 0.25,
        child: ListTile(
          leading: avatarURL != null
              ? CircleAvatar(
                  backgroundColor: MyAppTheme.black_grey,
                  backgroundImage: NetworkImage(avatarURL),
                  radius: 24,
                )
              : CircleAvatar(
                  backgroundColor: MyAppTheme.black_grey,
                  child: Icon(
                    Icons.notifications_active,
                    color: MyAppTheme.secondary,
                  ),
                  radius: 24,
                ),
          title: Text(
            this.title,
            maxLines: 1,
          ),
          subtitle: Row(
            children: [
              Icon(
                this.subIconData,
                color: MyAppTheme.black_grey,
                size: 20,
              ),
              SizedBox(
                width: 8,
              ),
              Text(this.subtitle),
            ],
          ),
          trailing: Container(
            height: 10,
            width: 10,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: MyAppTheme.primary,
            ),
          ),
        ),
        secondaryActions: [
          IconSlideAction(
            color: Colors.red,
            onTap: deleteCallback,
            icon: FontAwesome5.trash_alt,
          ),
        ],
      ),
    );
  }
}
