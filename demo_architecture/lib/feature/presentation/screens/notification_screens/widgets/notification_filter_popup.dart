import 'package:demo_architecture/feature/data/models/notification/notification_model.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class NotificationFilterPopup extends StatelessWidget {
  final int codeFilter;

  NotificationFilterPopup({
    Key key,
    @required this.codeFilter,
  }) : super(key: key);

  final filters = [
    {
      'icon': Icons.circle,
      'text': 'ALL',
      'code': -1,
    },
    {
      'icon': Icons.person_add_rounded,
      'text': 'Request Match',
      'code': NotificationModel.REQUEST_MATCH_NOW,
    },
    {
      'icon': Icons.check_circle,
      'text': 'Match Success',
      'code': NotificationModel.CONFIRM_MATCH,
    },
    {
      'icon': Icons.group_add,
      'text': 'Request Join Group',
      'code': NotificationModel.REQUEST_JOIN_GROUP,
    },
    {
      'icon': Icons.group,
      'text': 'Join Group Success',
      'code': NotificationModel.JOIN_GROUP_SUCCESS,
    },
    {
      'icon': Icons.place,
      'text': 'Request Dating Place',
      'code': NotificationModel.REQUEST_PLACE,
    },
  ];

  void _filterHandler(BuildContext context, int code) {
    Navigator.of(context).pop(code);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height/2,
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Feather.filter,
                  color: MyAppTheme.black_grey,
                  size: 20,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  'Notification Filter',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    color: MyAppTheme.black_grey,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: List.generate(
                    filters.length,
                    (index) => getFilterItem(context, index),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getFilterItem(BuildContext context, int index) => InkWell(
        onTap: () => _filterHandler(context, filters[index]['code']),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (index > 0)
                Icon(
                  filters[index]['icon'],
                  size: 20,
                  color: filters[index]['code'] == this.codeFilter
                      ? MyAppTheme.primary
                      : MyAppTheme.black,
                ),
              const SizedBox(width: 8),
              Text(
                filters[index]['text'],
                style: TextStyle(
                  fontSize: MyTextStyle.title_5_fontsize,
                  color: filters[index]['code'] == this.codeFilter
                      ? MyAppTheme.primary
                      : MyAppTheme.black,
                ),
              ),
            ],
          ),
        ),
      );
}
