import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_by_id.dart';
import 'package:demo_architecture/feature/domain/usecase/notifications/get_notification_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/user/get_user_inform_by_id.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/confirm_request_join_group_bloc/confirm_request_join_group_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_group_by_id_bloc/get_group_by_id_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/user_blocs/get_user_by_id_bloc/get_user_by_id_bloc.dart';
import 'package:demo_architecture/feature/presentation/data_prototype/girl.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/my_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../../injection_container.dart';

class ArgsConfirmRequestJoinGroupScreen {
  final String notificationId;
  final String groupId;
  final String senderId;
  final String reasonWantToJoinGroup;

  ArgsConfirmRequestJoinGroupScreen({
    @required this.notificationId,
    @required this.groupId,
    @required this.senderId,
    @required this.reasonWantToJoinGroup,
  });
}

class ConfirmRequestJoinGroupScreen extends StatefulWidget {
  static const String routeName = 'confirm-request-join-group-screen';

  final ArgsConfirmRequestJoinGroupScreen args;

  const ConfirmRequestJoinGroupScreen({Key key, @required this.args}) : super(key: key);

  @override
  _ConfirmRequestJoinGroupScreenState createState() =>
      _ConfirmRequestJoinGroupScreenState();
}

class _ConfirmRequestJoinGroupScreenState extends State<ConfirmRequestJoinGroupScreen> {
  GetGroupByIdBloc _getGroupByIdBloc;
  GetUserByIdBloc _getUserByIdBloc;
  ConfirmRequestJoinGroupBloc _confirmRequestJoinGroupBloc;

  void _acceptRequestJoinGroup() {
    _confirmRequestJoinGroupBloc.add(
      ConfirmRequestJoinGroup(
        params: ConfirmRequestJoinGroupParams(
          status: true,
          notificationId: widget.args.notificationId,
        ),
      ),
    );
  }

  void _denyRequestJoinGroup() {
    _confirmRequestJoinGroupBloc.add(
      ConfirmRequestJoinGroup(
        params: ConfirmRequestJoinGroupParams(
          status: false,
          notificationId: widget.args.notificationId,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // Initial Bloc
    _getGroupByIdBloc = sl<GetGroupByIdBloc>();
    _getUserByIdBloc = sl<GetUserByIdBloc>();
    _confirmRequestJoinGroupBloc = sl<ConfirmRequestJoinGroupBloc>();

    // Send Get Detail Event
    _getUserByIdBloc.add(
      GetUserEvent(
        GetUserInformByIdParams(
          widget.args.senderId,
          ["favorites"],
        ),
      ),
    );
    _getGroupByIdBloc.add(
      GetGroupByIdEventSubmit(
        GetGroupByIdParams(
          widget.args.groupId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: MyAppBar(
        title: Text(''),
      ),
      body: getBody(size),
      floatingActionButton: getFloatingActionButtons(size),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  getBody(Size size) {
    return BlocBuilder<GetGroupByIdBloc, GetGroupByIdState>(
      bloc: _getGroupByIdBloc,
      builder: (context, groupState) {
        if (groupState is GetGroupByIdLoaded) {
          return BlocBuilder<GetUserByIdBloc, GetUserByIdState>(
            bloc: _getUserByIdBloc,
            builder: (context, userState) {
              if (userState is GetUserByIdLoaded) {
                return SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: size.height / 6),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            foregroundImage: NetworkImage(userState.userInform.avt[0]),
                            radius: 60,
                          ),
                          const SizedBox(width: 8),
                          Icon(
                            Feather.arrow_right_circle,
                            color: MyAppTheme.black_grey,
                            size: 28,
                          ),
                          const SizedBox(width: 8),
                          CircleAvatar(
                            foregroundImage: NetworkImage(groupState.groupEntity.avatar),
                            radius: 60,
                          ),
                        ],
                      ),
                      const SizedBox(height: 32),
                      Text(
                        '${userState.userInform.firstname} want to join into your group!',
                        style: TextStyle(
                          fontSize: MyTextStyle.title_4_fontsize,
                        ),
                      ),
                      const SizedBox(height: 16),
                      Container(
                        width: size.width * 3 / 5,
                        alignment: Alignment.center,
                        child: Text(
                          widget.args.reasonWantToJoinGroup,
                          style: TextStyle(
                            fontSize: MyTextStyle.paragraph_fontsize,
                          ),
                          maxLines: 4,
                        ),
                      ),
                    ],
                  ),
                );
              }
              return CircularLoading();
            },
          );
        }
        return CircularLoading();
      },
    );
  }

  Widget getFloatingActionButtons(Size size) =>
      BlocBuilder<ConfirmRequestJoinGroupBloc, ConfirmRequestJoinGroupState>(
        bloc: _confirmRequestJoinGroupBloc,
        builder: (context, state) {
          if (state is ConfirmRequestJoinGroupSuccessState) {
            // Reload notifications
            context.read<GetNotificationsBloc>().add(
                  FetchNotificationsEvent(
                    params: GetNotificationParams(page: 1, rowPerPage: 10),
                  ),
                );
            Navigator.of(context).pop();
          }
          if (state is ConfirmRequestJoinGroupLoadingState) {
            return CircularLoading();
          }
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FloatingActionButton(
                  onPressed: _denyRequestJoinGroup,
                  child: Icon(
                    Icons.clear,
                    color: MyAppTheme.secondary,
                  ),
                  backgroundColor: MyAppTheme.black_grey,
                ),
                FloatingActionButton(
                  onPressed: _acceptRequestJoinGroup,
                  child: Icon(
                    Icons.check,
                    color: MyAppTheme.secondary,
                  ),
                  backgroundColor: MyAppTheme.primary,
                ),
              ],
            ),
          );
        },
      );
}
