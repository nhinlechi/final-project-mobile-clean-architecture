import 'dart:developer';

import 'package:demo_architecture/feature/presentation/screens/find_people_screens/widgets/pick_location_filter.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../core/share_ref/stranger_filter_share_ref.dart';
import '../../../../injection_container.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import '../../widgets/generals/my_app_bar.dart';

class StrangerFilterScreen extends StatefulWidget {
  static const routeName = 'stranger-filter-screen';

  @override
  _StrangerFilterScreenState createState() => _StrangerFilterScreenState();
}

class _StrangerFilterScreenState extends State<StrangerFilterScreen> {
  List<bool> _genderSelected = [false, false, false, false];
  final List<String> _genderTexts = ['All', 'Men', 'Women', 'Others'];

  List<bool> _relationshipSelected = [false, false, false];
  final List<String> _relationshipTexts = ['In Relation', 'Alone', 'Divorce'];

  LatLng _myLocation;
  int _gender;
  int _minAge;
  int _maxAge;
  int _maxDistance;
  int _relationship;
  StrangerFilterShareRef shareRef;

  bool enabledRelationFilter;

  void _saveStrangerFilter() {
    shareRef.saveFilter(
      lat: _myLocation.latitude,
      long: _myLocation.longitude,
      gender: _gender,
      maxAge: _maxAge,
      maxDistance: _maxDistance,
      minAge: _minAge,
      relationship: _relationship,
    );
    Navigator.of(context).pop();
  }

  void _pickLocationHandler(LatLng location) {
    setState(() {
      this._myLocation = location;
    });
  }

  void _initStrangerFilter() {
    shareRef = sl<StrangerFilterShareRef>();

    _myLocation = LatLng(shareRef.latitude, shareRef.longitude);

    _minAge = shareRef.minAge;
    _maxAge = shareRef.maxAge;
    _maxDistance = shareRef.maxDistance;
    _gender = shareRef.gender;
    _relationship = shareRef.relationship;

    _genderSelected[_gender] = true;
    if (_relationship > 0) {
      _relationshipSelected[_relationship - 1] = true;
    }
    enabledRelationFilter = _relationship == 0 ? false : true;
  }

  @override
  void initState() {
    super.initState();
    // Get data from share references
    _initStrangerFilter();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: MyAppBar(
        title: Text(
          'Filters',
          style: MyTextStyle.ts_AppBar_title,
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 16, top: 20, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PickLocationFilter(
                pickLocationCallback: _pickLocationHandler,
                currentLocation: _myLocation,
              ),
              const SizedBox(height: 10),
              Text(
                'I\'m interested in',
                style: MyTextStyle.ts_filter_title,
              ),
              const SizedBox(height: 10),
              ToggleButtons(
                direction: Axis.horizontal,
                children: List.generate(_genderSelected.length, (index) {
                  return Container(
                    width: (size.width - 40) / _genderSelected.length,
                    alignment: Alignment.center,
                    child: Text(_genderTexts[index]),
                  );
                }),
                borderRadius: BorderRadius.circular(10),
                fillColor: MyAppTheme.primary,
                textStyle: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                selectedColor: Colors.white,
                isSelected: _genderSelected,
                onPressed: (index) {
                  for (int i = 0; i < _genderSelected.length; i++) {
                    _genderSelected[i] = false;
                  }
                  setState(() {
                    _genderSelected[index] = !_genderSelected[index];
                    _gender = index;
                  });
                },
              ),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Age range',
                    style: MyTextStyle.ts_filter_title,
                  ),
                  Text(
                    '$_minAge - $_maxAge',
                    style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              RangeSlider(
                values: RangeValues(_minAge * 1.0, _maxAge * 1.0),
                min: 18,
                max: 80,
                onChanged: (value) {
                  setState(() {
                    _minAge = value.start.toInt();
                    _maxAge = value.end.toInt();
                  });
                },
                activeColor: MyAppTheme.primary,
                labels: RangeLabels('$_minAge', '$_maxAge'),
              ),
              const SizedBox(height: 30),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Distance (km)',
                    style: MyTextStyle.ts_filter_title,
                  ),
                  Text(
                    '0 - $_maxDistance',
                    style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
                  ),
                ],
              ),
              Slider(
                value: _maxDistance * 1.0,
                onChanged: (value) {
                  setState(() {
                    _maxDistance = value.toInt();
                  });
                },
                min: 1,
                max: 10,
                divisions: 9,
                label: '$_maxDistance km',
                activeColor: MyAppTheme.primary,
              ),
              const SizedBox(height: 30),
              SwitchListTile(
                value: enabledRelationFilter,
                contentPadding: const EdgeInsets.all(0),
                title: Text(
                  'Relationship',
                  style: MyTextStyle.ts_filter_title,
                ),
                activeColor: MyAppTheme.primary,
                onChanged: (value) {
                  setState(() {
                    enabledRelationFilter = !enabledRelationFilter;
                    for (int i = 0; i < _relationshipSelected.length; i++) {
                      _relationshipSelected[i] = false;
                    }
                    _relationship = enabledRelationFilter ? 1 : 0;
                    _relationshipSelected[0] = true;
                  });
                },
              ),
              const SizedBox(height: 10),
              if (enabledRelationFilter)
                ToggleButtons(
                  direction: Axis.horizontal,
                  children: List.generate(_relationshipSelected.length, (index) {
                    return Container(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      width: (size.width - 40) / _relationshipSelected.length,
                      alignment: Alignment.center,
                      child: Text(_relationshipTexts[index]),
                    );
                  }),
                  borderRadius: BorderRadius.circular(10),
                  fillColor: MyAppTheme.primary,
                  textStyle: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                  selectedColor: Colors.white,
                  isSelected: _relationshipSelected,
                  onPressed: (index) {
                    for (int i = 0; i < _relationshipSelected.length; i++) {
                      _relationshipSelected[i] = false;
                    }
                    setState(() {
                      _relationshipSelected[index] = !_relationshipSelected[index];
                      _relationship = index + 1;
                    });
                  },
                ),
              const SizedBox(height: 100),
            ],
          ),
        ),
      ),
      bottomSheet: BottomAppBar(
        child: Container(
          padding: EdgeInsets.only(left: 20, bottom: 20, top: 14, right: 20),
          height: 80,
          width: size.width,
          child: ElevatedButton(
            child: Text(
              'Save',
              style: TextStyle(
                color: MyAppTheme.secondary,
                fontSize: MyTextStyle.title_5_fontsize,
              ),
            ),
            onPressed: _saveStrangerFilter,
          ),
        ),
      ),
    );
  }
}
