import 'dart:math';

import 'package:bot_toast/bot_toast.dart';
import 'package:demo_architecture/feature/data/datasources/remotesources/matching/matching_remote_data.dart';
import 'package:demo_architecture/feature/presentation/pages/home_page.dart';
import 'package:demo_architecture/feature/presentation/screens/group_screens/view_profile_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../../core/firebase/fcm_manager.dart';
import '../../../../injection_container.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/usecase/matching/confirm_matching_uc.dart';
import '../../bloc/match_blocs/confirm_match_bloc/confirm_match_bloc.dart';
import '../../bloc/match_blocs/matching_bloc/matching_bloc.dart';

class CountdownScreenArgs {
  final MatchMode mode;
  final int timeRemaining;
  final String notificationId;
  final UserInform otherInfo;
  final FCMManager fcmManager;

  CountdownScreenArgs({
    this.mode = MatchMode.Now,
    @required this.timeRemaining,
    @required this.notificationId,
    @required this.otherInfo,
    this.fcmManager,
  });
}

class CountdownScreen extends StatefulWidget {
  static const String routeName = 'countdown-screen';

  final CountdownScreenArgs args;

  const CountdownScreen({
    Key key,
    @required this.args,
  }) : super(key: key);

  @override
  _CountdownScreenState createState() => _CountdownScreenState();
}

class _CountdownScreenState extends State<CountdownScreen>
    with TickerProviderStateMixin {
  AnimationController _animController;
  ConfirmMatchBloc _confirmMatchBloc;
  MatchingBloc _matchingBloc;

  String get timerString {
    Duration duration = _animController.duration * _animController.value;
    if (duration.inDays > 0) {
      return '${duration.inDays}days left';
    }
    return '${duration.inHours > 0 ? '${duration.inHours}:' : ''}${duration.inMinutes % 60}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  void _acceptMatching() async {
    _confirmMatchBloc.add(
      ConfirmMatchingEvent(
        params: ConfirmMatchingParams(
          isAccepted: true,
          notificationId: widget.args.notificationId,
        ),
      ),
    );
    if (this.widget.args.mode == MatchMode.Later) {
      Navigator.of(context).pop();
    }
  }

  void _cancelMatching() async {
    _confirmMatchBloc.add(
      ConfirmMatchingEvent(
        params: ConfirmMatchingParams(
          isAccepted: false,
          notificationId: widget.args.notificationId,
        ),
      ),
    );
    Navigator.of(context).pop();
  }

  void _showOtherUserInfoDetail() {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) {
        return ViewProfileBottomSheet(host: widget.args.otherInfo);
      },
    );
  }

  @override
  void initState() {
    super.initState();

    // Bloc Instance
    _confirmMatchBloc = sl<ConfirmMatchBloc>();
    _matchingBloc = BlocProvider.of<MatchingBloc>(context);
    // Initial Animation Controller
    _animController = AnimationController(
      vsync: this,
      duration: Duration(seconds: widget.args.timeRemaining),
    );
    _animController.reverse(from: 1.0);
    _animController.addStatusListener((status) {
      print(status);
      if (status == AnimationStatus.dismissed) {
        Navigator.of(context).pop(false);
      }
    });
    // Register callback
    widget.args.fcmManager?.onMatchDenied = _closeCountdownScreen;
  }

  _closeCountdownScreen() {
    BotToast.cleanAll();
    BotToast.showText(
      text: 'This match was deleted. Sorry!',
      textStyle: TextStyle(
        color: Colors.white,
      ),
      duration: Duration(seconds: 3),
    );
    Future.delayed(Duration(seconds: 1), () {
      Navigator.of(context).pop(false);
    });
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: BlocListener(
        bloc: _matchingBloc,
        listener: (ctx, state) {
          if (state is MatchSuccessState) {
            Navigator.of(context).popUntil(
              ModalRoute.withName(HomePage.routeName),
            );
          }
        },
        child: Scaffold(
          body: getBody(),
        ),
      ),
    );
  }

  getBody() {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Expanded(
            child: Align(
              alignment: FractionalOffset.center,
              child: AspectRatio(
                aspectRatio: 1.0,
                child: Stack(
                  children: [
                    Positioned.fill(
                      child: AnimatedBuilder(
                        animation: _animController,
                        builder: (context, child) {
                          return CustomPaint(
                            painter: TimerPainter(
                              animation: _animController,
                              backgroundColor: Colors.grey.withOpacity(0.2),
                              progressColor: Colors.orangeAccent,
                            ),
                          );
                        },
                      ),
                    ),
                    Align(
                      alignment: FractionalOffset.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                            onTap: _showOtherUserInfoDetail,
                            child: CircleAvatar(
                              backgroundImage:
                                  NetworkImage(widget.args.otherInfo.avt[0]),
                              radius: 50,
                            ),
                          ),
                          Text(
                            '${widget.args.otherInfo.firstname} ${widget.args.otherInfo.lastname}',
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.orangeAccent,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          AnimatedBuilder(
                            animation: _animController,
                            builder: (context, child) {
                              return Text(
                                timerString,
                                style: TextStyle(
                                  fontSize: 48,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.orangeAccent,
                                ),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          BlocBuilder(
            bloc: _confirmMatchBloc,
            builder: (context, state) {
              if (state is ConfirmMatchingLoadingState ||
                  state is ConfirmMatchingSuccessState) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              if (state is ConfirmMatchingFailureState) {
                _closeCountdownScreen();
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
              return getConfirmButton();
            },
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }

  getConfirmButton() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        FloatingActionButton(
          onPressed: _cancelMatching,
          backgroundColor: Colors.red,
          heroTag: 'floating-button-1',
          child: Icon(
            Icons.clear,
            color: Colors.white,
          ),
        ),
        FloatingActionButton(
          onPressed: _acceptMatching,
          backgroundColor: Colors.orangeAccent,
          heroTag: 'floating-button-2',
          child: Icon(
            Icons.done_outline,
            color: Colors.white,
          ),
        ),
      ],
    );
  }
}

class TimerPainter extends CustomPainter {
  final Animation<double> animation;
  final Color backgroundColor;
  final Color progressColor;

  TimerPainter(
      {@required this.animation,
      @required this.backgroundColor,
      @required this.progressColor})
      : super(repaint: animation);

  @override
  void paint(Canvas canvas, Size size) {
    // Draw circle
    Paint paint = Paint()
      ..color = backgroundColor
      ..strokeWidth = 10.0
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;

    canvas.drawCircle(size.center(Offset.zero), size.width / 2.0, paint);

    // Draw progress bar
    paint.color = progressColor;
    final progress = (1.0 - animation.value) * 2.0 * pi;
    canvas.drawArc(Offset.zero & size, pi * 3 / 2, -progress, false, paint);
  }

  @override
  bool shouldRepaint(TimerPainter old) {
    return animation.value != old.animation.value ||
        progressColor != old.progressColor ||
        backgroundColor != old.backgroundColor;
  }
}
