import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../themes/colors.dart';

class MapScreen extends StatefulWidget {
  final LatLng previousLocation;

  const MapScreen({Key key, @required this.previousLocation}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  Completer<GoogleMapController> _mapController = Completer();
  LocationPermission permission;
  LatLng _myLocation;
  LatLng _initialLatLng;

  Future<dynamic> showCustomDialog() async {
    return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Enable location services'),
        content: Text('Enable location service to use this feature'),
        backgroundColor: MyAppTheme.secondary,
        actions: [
          ElevatedButton(
            child: Text('OK'),
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(MyAppTheme.primary),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }

  Future<Position> _getCurrentPosition() async {
    bool serviceEnabled = false;

    // Test location service are enabled
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      await showCustomDialog();
      return Future.error('Location services are enabled');
    }

    // Test permission
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    // Get current location
    var pos = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _myLocation = LatLng(pos.latitude, pos.longitude);
    });
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(pos.latitude, pos.longitude),
          zoom: 20,
        ),
      ),
    );
    print(pos);
    return pos;
  }

  void _pickLocationHandler(LatLng location) {
    setState(() {
      _myLocation = location;
    });
  }

  void _saveLocation() {
    Navigator.of(context).pop(_myLocation);
  }

  @override
  void initState() {
    super.initState();

    // Initial Lat Long
    if (widget.previousLocation.latitude != 0 && widget.previousLocation.longitude != 0) {
      _initialLatLng = widget.previousLocation;
    } else {
      _initialLatLng = LatLng(10.8231, 106.6297); // Ho Chi Minh City Lat Long
    }
    _myLocation = _initialLatLng;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          getMapView(),
          getConfigLocation(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'find-people-button',
        child: Icon(
          Icons.check,
          color: MyAppTheme.secondary,
        ),
        backgroundColor: MyAppTheme.primary,
        onPressed: _saveLocation,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  getMapView() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: _initialLatLng,
        zoom: 13,
      ),
      onMapCreated: (controller) {
        _mapController.complete(controller);
      },
      markers: <Marker>{
        Marker(
          markerId: MarkerId('m1'),
          position: _myLocation,
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        ),
      },
      onTap: _pickLocationHandler,
      trafficEnabled: false,
      mapToolbarEnabled: false,
      zoomControlsEnabled: false,
    );
  }

  getConfigLocation() {
    return SafeArea(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(child: Container()),
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: ElevatedButton(
              onPressed: _getCurrentPosition,
              style: ElevatedButton.styleFrom(
                shape: CircleBorder(),
                padding: const EdgeInsets.all(10),
              ),
              child: Icon(
                Icons.my_location_rounded,
                color: MyAppTheme.secondary,
              ),
            ),
          ),
          const SizedBox(height: 16),
        ],
      ),
    );
  }
}
