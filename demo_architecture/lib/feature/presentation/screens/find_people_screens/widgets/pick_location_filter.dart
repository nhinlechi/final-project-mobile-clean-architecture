import 'dart:developer';

import 'package:demo_architecture/feature/presentation/screens/find_people_screens/map_screen.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';

class PickLocationFilter extends StatelessWidget {
  final LatLng currentLocation;
  final Function(LatLng latLng) pickLocationCallback;

  const PickLocationFilter({
    Key key,
    @required this.pickLocationCallback,
    @required this.currentLocation,
  }) : super(key: key);

  void _pickLocation(BuildContext context) async {
    final rs = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => MapScreen(
          previousLocation: currentLocation,
        ),
      ),
    );
    if (rs is LatLng) {
      pickLocationCallback?.call(rs);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        ElevatedButton(
          onPressed: () => _pickLocation(context),
          style: ElevatedButton.styleFrom(
            primary: MyAppTheme.black_grey,
          ),
          child: Row(
            children: [
              Icon(
                Icons.location_on_rounded,
                color: MyAppTheme.secondary,
              ),
              const SizedBox(width: 8),
              Text(
                'Pick your location',
                style: TextStyle(
                  color: MyAppTheme.secondary,
                  fontSize: MyTextStyle.title_5_fontsize,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
