import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_recommend_place_bloc/get_recommend_place_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/search_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/widgets/place_filter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../injection_container.dart';
import '../../../domain/entities/place/place_entity.dart';
import '../../../domain/usecase/places/get_places_uc.dart';
import '../../bloc/place_blocs/get_places_bloc/get_places_bloc.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import '../../widgets/generals/circular_loading.dart';
import '../../widgets/generals/search_bar_widget.dart';
import 'date_picker_popup.dart';
import 'widgets/place_picker_toggle.dart';

class RecommendPlacePopup extends StatefulWidget {
  final String matchId;

  const RecommendPlacePopup({Key key, @required this.matchId})
      : super(key: key);

  @override
  _RecommendPlacePopupState createState() => _RecommendPlacePopupState();
}

class _RecommendPlacePopupState extends State<RecommendPlacePopup> {
  GetPlacesBloc _getPlacesBloc;
  GetRecommendPlaceBloc _getRecommendPlaceBloc;

  final heroTagSearchBar = 'search-place-bar';

  // filter
  String categoryType;
  String sortByType;
  String sortType = '-1';

  // search text
  final _textController = TextEditingController();
  GetPlacesParams _params = GetPlacesParams(rowsPerPage: 16, page: 1);

  // Place Picked Id
  PlaceEntity _placePicked;

  Future<void> _openSearchScreen() async {
    final text = await Navigator.of(context).pushNamed(
      SearchScreen.routeName,
      arguments: ArgsSearchScreen(
        searchText: _textController.text,
        heroTag: heroTagSearchBar,
      ),
    );
    setState(() {
      this._textController.text = text;
    });
    // Http Request get place
    _getPlaces();
  }

  void _filterCallback(String filter, FilterType type) {
    setState(() {
      switch (type) {
        case FilterType.categories:
          categoryType = filter;
          break;
        case FilterType.sortBy:
          sortByType = filter;
          break;
        case FilterType.sort:
          sortType = filter;
          break;
      }
    });
    _getPlaces();
  }

  void _getPlaces() {
    _params = GetPlacesParams(
      searchText: _textController.text,
      page: 1,
      rowsPerPage: 16,
      type: categoryType,
      sortBy: sortByType,
      sort: int.tryParse(sortType),
    );

    _getPlacesBloc.add(GetPlaces(params: _params));
  }

  void _pickPlaceCallback(PlaceEntity place) {
    _placePicked = place;
  }

  void _openDatePicker() async {
    if (_placePicked != null) {
      final result = await Navigator.of(context).push(PageTransition(
        child: DatePickerPopup(
          matchId: widget.matchId,
          place: _placePicked,
        ),
        type: PageTransitionType.rightToLeftWithFade,
        duration: Duration(milliseconds: 350),
        reverseDuration: Duration(milliseconds: 500),
      ));

      if (result) Navigator.of(context).pop();
    }
  }

  @override
  void initState() {
    super.initState();
    // Init get place bloc
    _getPlacesBloc = sl<GetPlacesBloc>();
    _getRecommendPlaceBloc = sl<GetRecommendPlaceBloc>();
    _getPlacesBloc.add(GetPlaces(
        params: GetPlacesParams(
      page: 1,
      rowsPerPage: 5,
    )));
    print(widget.matchId);
    _getRecommendPlaceBloc.add(
      SubmitGetRecommendPlace(
        params: GetRecommendPlaceParams(matchId: widget.matchId),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.only(top: 32),
        child: DefaultTabController(
          length: 2,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  'Recommend Place Near You',
                  style: MyTextStyle.ts_register_title,
                ),
              ),
              TabBar(
                labelStyle: TextStyle(
                  fontSize: MyTextStyle.title_5_fontsize,
                ),
                tabs: [
                  Tab(text: "Near you"),
                  Tab(text: "Other"),
                ],
              ),
              Expanded(
                child: TabBarView(
                  children: [
                    getRecommendPlaceTabView(),
                    getPickPlaceTabView(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: MyAppTheme.primary,
        child: Icon(
          Icons.arrow_forward_ios,
          color: MyAppTheme.secondary,
        ),
        onPressed: _openDatePicker,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }

  getRecommendPlaceTabView() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 16),
        child: BlocBuilder<GetRecommendPlaceBloc, GetRecommendPlaceState>(
          bloc: _getRecommendPlaceBloc,
          builder: (context, state) {
            if (state is GetRecommendPlaceSuccess) {
              return PlacePickerToggle(
                places: state.places,
                pickPlaceCallback: _pickPlaceCallback,
              );
            }
            return CircularLoading();
          },
        ),
      ),
    );
  }

  getPickPlaceTabView() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SearchBarWidget(
            heroTag: 'pick-place-search',
            onTap: _openSearchScreen,
            readOnly: true,
            controller: _textController,
          ),
          ClipRRect(
            child: PlaceFilter(
              filterCallback: _filterCallback,
            ),
            borderRadius: BorderRadius.circular(30),
          ),
          SizedBox(
            height: 32,
          ),
          BlocBuilder<GetPlacesBloc, GetPlacesState>(
            bloc: _getPlacesBloc,
            builder: (context, state) {
              if (state is GetPlacesSuccess) {
                return PlacePickerToggle(
                  places: state.placesEntity.places,
                  pickPlaceCallback: _pickPlaceCallback,
                );
              }
              return CircularLoading();
            },
          ),
          SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}
