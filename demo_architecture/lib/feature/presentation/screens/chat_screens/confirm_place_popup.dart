import 'package:demo_architecture/feature/domain/usecase/notifications/get_notification_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_time_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';
import '../../../domain/usecase/matching/confirm_place_uc.dart';
import '../../bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import '../../repository_providers/user_repository_provider.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import '../../widgets/generals/circular_loading.dart';
import 'widgets/dating_event_place_widget.dart';

class ConfirmPlacePopup extends StatefulWidget {
  final RequestPlaceEntity data;

  const ConfirmPlacePopup({Key key, @required this.data}) : super(key: key);

  @override
  _ConfirmPlacePopupState createState() => _ConfirmPlacePopupState();
}

class _ConfirmPlacePopupState extends State<ConfirmPlacePopup> {
  ConfirmPlaceBloc _confirmPlaceBloc;

  @override
  void initState() {
    super.initState();
    _confirmPlaceBloc = sl<ConfirmPlaceBloc>();
  }

  void _acceptRequestPlace(BuildContext context) {
    _confirmPlaceBloc.add(
      ConfirmDatingEvent(
        data: widget.data,
        params: ConfirmPlaceParams(
          notificationId: widget.data.notificationId,
          isAccepted: true,
        ),
      ),
    );
  }

  void _rejectRequestPlace(BuildContext context) {
    _confirmPlaceBloc.add(
      RejectDatingEvent(
        data: widget.data,
        params: ConfirmPlaceParams(
          notificationId: widget.data.notificationId,
          isAccepted: false,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final auth = context.read<UserRepositoryProvider>().auth;
    final matchRooms = context.read<MatchRepositoryProvider>().matchRooms;
    final currentRoom =
        matchRooms.firstWhere((r) => r.matchId == widget.data.matchId);
    final otherUserInfo = currentRoom.participants
        .firstWhere((p) => p.userId == widget.data.senderId);
    //
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              auth.userId == widget.data.senderId
                  ? 'You sent this dating place'
                  : '${otherUserInfo.firstname} created a date.',
              style: MyTextStyle.ts_title_Popup,
            ),
            Text(
              auth.userId == widget.data.senderId
                  ? 'Waiting for your friend reply!'
                  : 'Do you agree to dating?',
              style: MyTextStyle.ts_title_Popup,
            ),
            const SizedBox(
              height: 32,
            ),
            DatingEventTimeWidget(
              label: 'Dating Time',
              datingTime: widget.data.datingTime,
            ),
            const SizedBox(
              height: 16,
            ),
            DatingEventPlaceWidget(
              label: 'Dating Place',
              place: widget.data.place,
            ),
          ],
        ),
      ),
      floatingActionButton: BlocBuilder<ConfirmPlaceBloc, ConfirmPlaceState>(
        bloc: _confirmPlaceBloc,
        builder: (context, state) {
          // Reload notify page
          if (state is ConfirmPlaceSuccess || state is RejectPlaceSuccess) {
            context.read<GetNotificationsBloc>().add(
                  FetchNotificationsEvent(
                    params: GetNotificationParams(
                      page: 1,
                      rowPerPage: 100,
                    ),
                  ),
                );
          }
          // Loading State
          if (state is ConfirmPlaceLoading) {
            return CircularLoading();
          }
          // Success State
          if (state is ConfirmPlaceSuccess) {
            return Container();
          }
          // Reject Place
          if (state is RejectPlaceSuccess) {
            Navigator.of(context).pop();
          }
          // Sender not show confirm button
          final auth = context.read<UserRepositoryProvider>().auth;
          if (widget.data.senderId == auth.userId) return Container();
          // Confirm Button
          return getFloatingActionButtons(context, size);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget getFloatingActionButtons(BuildContext context, Size size) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FloatingActionButton(
              onPressed: () => _rejectRequestPlace(context),
              child: Icon(
                Icons.clear,
                color: MyAppTheme.secondary,
              ),
              backgroundColor: MyAppTheme.black_grey,
            ),
            FloatingActionButton(
              onPressed: () => _acceptRequestPlace(context),
              child: Icon(
                Icons.check,
                color: MyAppTheme.secondary,
              ),
              backgroundColor: MyAppTheme.primary,
            ),
          ],
        ),
      );
}
