import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/entities/place/place_entity.dart';
import '../../../domain/usecase/matching/send_place_uc.dart';
import '../../bloc/match_blocs/send_place_bloc/send_place_bloc.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import 'widgets/dating_event_place_widget.dart';

class DatePickerPopup extends StatefulWidget {
  final PlaceEntity place;
  final String matchId;

  const DatePickerPopup({
    Key key,
    @required this.place,
    @required this.matchId,
  }) : super(key: key);

  @override
  _DatePickerPopupState createState() => _DatePickerPopupState();
}

class _DatePickerPopupState extends State<DatePickerPopup> {
  final _dateController = TextEditingController();
  final _timeController = TextEditingController();
  DateTime date;
  TimeOfDay time;
  SendPlaceBloc _sendPlaceBloc;

  void _pickDate() async {
    final date = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        Duration(
          days: 30,
        ),
      ),
    );
    if (date != null) {
      this.date = date;
      _dateController.text = date.toString().split(' ')[0];
    }
  }

  void _pickTime() async {
    final time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(
        DateTime.now().add(
          Duration(hours: 1),
        ),
      ),
    );
    if (time != null) {
      this.time = time;
      _timeController.text = '${time.format(context)}';
    }
  }

  void _today() {
    final now = DateTime.now();
    this.date = now.subtract(Duration(hours: now.hour, minutes: now.minute));
    _dateController.text = date.toString().split(' ')[0];
  }

  void _sendPlaceAndDatePicked() {
    final diffTime = date
        .add(Duration(
            hours: time.period == DayPeriod.pm && time.hourOfPeriod == 0
                ? 24
                : time.hour,
            minutes: time.minute))
        .difference(DateTime.now())
        .inMinutes;
    if (diffTime < 30) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('At least at 30 minutes later')));
      return;
    }

    if (date != null) {
      // Get dating time
      final datingTime = date.millisecondsSinceEpoch +
          Duration(hours: time.hour, minutes: time.minute).inMilliseconds;
      // Send Place
      _sendPlaceBloc.add(
        SendDatingPlaceEvent(
          params: SendPlaceParams(
            placeId: widget.place.id,
            matchId: widget.matchId,
            datingTime: datingTime,
          ),
        ),
      );
      // Popup route
      Navigator.of(context).pop(true);
    } else {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Fill date and time, please!')));
    }
  }

  @override
  void initState() {
    super.initState();

    // Get send place bloc
    _sendPlaceBloc = context.read<SendPlaceBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: getBody(size),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.send,
          color: MyAppTheme.secondary,
        ),
        onPressed: _sendPlaceAndDatePicked,
        backgroundColor: MyAppTheme.primary,
      ),
    );
  }

  Widget getBody(Size size) => Padding(
        padding: EdgeInsets.fromLTRB(16, size.height / 12, 16, 0),
        child: Column(
          children: [
            Text(
              'Choose day to meeting',
              style: MyTextStyle.ts_register_title,
            ),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              height: 40,
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _dateController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 0,
                          horizontal: 16,
                        ),
                        prefixIcon: Icon(Icons.date_range),
                      ),
                      readOnly: true,
                      onTap: _pickDate,
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  ElevatedButton(
                    onPressed: _today,
                    child: Text(
                      'Today',
                      style: TextStyle(
                        color: MyAppTheme.secondary,
                        fontSize: MyTextStyle.title_5_fontsize,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 16,
            ),
            SizedBox(
              height: 40,
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      controller: _timeController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 0,
                          horizontal: 16,
                        ),
                        prefixIcon: Icon(Icons.access_time),
                      ),
                      readOnly: true,
                      onTap: _pickTime,
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  ElevatedButton(
                    onPressed: _pickTime,
                    child: Text(
                      'Pick Time',
                      style: TextStyle(
                        color: MyAppTheme.secondary,
                        fontSize: MyTextStyle.title_5_fontsize,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 32,
            ),
            DatingEventPlaceWidget(
              label: 'Dating Place',
              place: widget.place,
            ),
          ],
        ),
      );
}
