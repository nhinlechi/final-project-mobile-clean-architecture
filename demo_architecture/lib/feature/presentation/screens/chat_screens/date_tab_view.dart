import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_dates_bloc/get_dates_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/date_detail_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/date_filter_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/date_item.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/search_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class DateTabView extends StatefulWidget {
  const DateTabView({Key key}) : super(key: key);

  @override
  _DateTabViewState createState() => _DateTabViewState();
}

class _DateTabViewState extends State<DateTabView> {
  AuthenticateToken auth;
  var statusFilter = StatusFilterType.All;
  var sortFilter = SortFilterType.None;

  @override
  void initState() {
    super.initState();
    //
    auth = context.read<UserRepositoryProvider>().auth;
  }

  void _filterHandler(dynamic filter) {
    if (filter is StatusFilterType) {
      setState(() {
        statusFilter = filter;
      });
    } else if (filter is SortFilterType) {
      setState(() {
        sortFilter = filter;
      });
    }
  }

  void _openDateDetail(DateModel date) {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) => DateDetailScreen(dateModel: date),
    );
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        context.read<GetDatesBloc>().add(
              FetchDatesEvent(
                params: GetDatesParams(),
              ),
            );
        Future.delayed(Duration(seconds: 2));
      },
      child: Column(
        children: [
          const SizedBox(height: 24),
          DateFilterWidget(
            filterCallback: _filterHandler,
          ),
          getListDates(),
        ],
      ),
    );
  }

  getListDates() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 16),
        child: BlocBuilder<GetDatesBloc, GetDatesState>(
          builder: (context, state) {
            // Initial
            if (state is GetDatesInitial) {
              context.read<GetDatesBloc>().add(
                    FetchDatesEvent(
                      params: GetDatesParams(),
                    ),
                  );
            }
            // Loading
            if (state is GetDatesLoading) {
              return CircularLoading();
            }
            // Success
            if (state is GetDatesSuccess) {
              var dates = [...state.dates];
              // Status Filter
              if (statusFilter == StatusFilterType.Upcoming)
                dates = dates
                    .where((date) =>
                        date.datingTime > DateTime.now().millisecondsSinceEpoch)
                    .toList();
              else if (statusFilter == StatusFilterType.History)
                dates = dates
                    .where((date) =>
                        date.datingTime < DateTime.now().millisecondsSinceEpoch)
                    .toList();
              // Sort Filter
              if (sortFilter == SortFilterType.Inc) {
                dates.sort((a, b) => b.datingTime - a.datingTime);
              } else if (sortFilter == SortFilterType.Dec) {
                dates.sort((a, b) => a.datingTime - b.datingTime);
              }
              //
              return ListView.builder(
                  itemCount: dates.length,
                  itemBuilder: (context, index) {
                    final date = dates[index];
                    final otherUserInfo = date.participants
                        .firstWhere((e) => e.id != auth.userId);
                    return DateItem(
                      placePhotoUrl: date.place.photo[0],
                      friendName: otherUserInfo.firstName,
                      datingTime: date.datingTime,
                      onTap: () => _openDateDetail(date),
                    );
                  });
            }
            return Container();
          },
        ),
      ),
    );
  }
}
