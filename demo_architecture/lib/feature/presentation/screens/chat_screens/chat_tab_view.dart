import 'dart:developer';

import 'package:demo_architecture/core/platform/api_base.dart';
import 'package:demo_architecture/core/utils/get_token.dart';
import 'package:demo_architecture/core/utils/string_utils.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_my_match_bloc/get_my_match_bloc.dart';
import 'package:demo_architecture/feature/presentation/data_prototype/girl.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/current_match.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/search_screen.dart';
import 'package:demo_architecture/feature/presentation/widgets/chat_item.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/search_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../../injection_container.dart';
import 'chat_screen.dart';

class ChatTabView extends StatefulWidget {
  const ChatTabView({Key key}) : super(key: key);

  @override
  _ChatTabViewState createState() => _ChatTabViewState();
}

class _ChatTabViewState extends State<ChatTabView>
    with AutomaticKeepAliveClientMixin {
  Socket _socket;
  AuthenticateToken auth;
  final _searchTextController = TextEditingController();

  void _connectSocketIO() {
    // Get accessToken
    final token = sl<Token>().getToken();

    // Connect to server socket
    final socketOption = OptionBuilder()
        .setTransports(['websocket'])
        .setQuery({'token': token})
        .disableAutoConnect()
        .build();
    _socket = io(APIBase.chatSocketUrl, socketOption);
    _socket.connect();

    // On Connect
    _socket.onConnect((data) {
      log('Socket connected', name: 'SocketIO');
    });
  }

  void _disconnectSocketIO() {
    _socket.onDisconnect((data) {
      log('Disconnected', name: 'socketIO');
    });
    _socket.disconnect();
    _socket.dispose();
  }

  void _openChatRoom(ArgsChatScreen args) {
    Navigator.of(context).pushNamed(
      ChatScreen.routName,
      arguments: args,
    );
  }

  Future<void> _openSearchScreen() async {
    final text = await Navigator.of(context).pushNamed(
      SearchScreen.routeName,
      arguments: ArgsSearchScreen(
        searchText: _searchTextController.text,
        heroTag: 'match-search-bar',
      ),
    );
    setState(() {
      this._searchTextController.text = text;
    });
  }

  @override
  void initState() {
    super.initState();
    // Create And Connect socket
    _connectSocketIO();
    // Get user info
    auth = context.read<UserRepositoryProvider>().auth;
    // Save socket to repos
    context.read<MatchRepositoryProvider>().chatSocket = _socket;
  }

  @override
  void dispose() {
    _disconnectSocketIO();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return getBody(context);
  }

  getBody(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        context.read<GetMyMatchBloc>().add(GetAllMyMatchEvent());
        return Future.delayed(Duration(seconds: 1));
      },
      child: Column(
        children: [
          SearchBarWidget(
            heroTag: 'search-chat-bar',
            readOnly: true,
            onTap: _openSearchScreen,
            controller: _searchTextController,
          ),
          CurrentMatchWidget(
            openChatRoomCallback: _openChatRoom,
            socket: _socket,
          ),
          Expanded(
            child: BlocBuilder<GetMyMatchBloc, GetMyMatchState>(
              bloc: context.read<GetMyMatchBloc>()..add(GetAllMyMatchEvent()),
              builder: (context, state) {
                if (state is GetMyMatchLoading || state is GetMyMatchInitial) {
                  return CircularLoading();
                }
                if (state is GetMyMatchSuccess) {
                  final matchRooms = [...state.matchRooms];
                  return ListView.builder(
                    itemCount: matchRooms.length,
                    itemBuilder: (context, index) {
                      final room = state.matchRooms[index];
                      final otherUserInfo =
                          room.participants[0].userId == auth.userId
                              ? room.participants[1]
                              : room.participants[0];
                      if (StringUtils.isContains(
                        otherUserInfo.firstname + otherUserInfo.lastname,
                        _searchTextController.text,
                      ))
                        return ChatItem(
                          chatId: matchRooms[index].matchId,
                          imageUrl: otherUserInfo.avt[0],
                          isDefaultImage: true,
                          name:
                              '${otherUserInfo.firstname} ${otherUserInfo.lastname}',
                          newestText: getNewestMessage(
                              matchRooms[index].lastMessage.messages.last),
                          onTap: () {
                            _openChatRoom(
                              ArgsChatScreen(
                                socket: _socket,
                                otherUserInfo: otherUserInfo,
                                chatRoomId: room.matchId,
                              ),
                            );
                          },
                        );
                      return Container();
                    },
                  );
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  getNewestMessage(MessageData newestMessage) {
    if (newestMessage.type == MessageData.TEXT) {
      return newestMessage.message;
    }
    return newestMessage.type;
  }
}
