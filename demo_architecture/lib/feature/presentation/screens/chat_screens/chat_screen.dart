import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/upload_message_image_bloc/upload_message_image_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../../core/socket/socket_event.dart';
import '../../../../injection_container.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../themes/colors.dart';
import '../call_video_screens/call_video_screen.dart';
import 'widgets/messages.dart';
import 'widgets/new_message.dart';

class ArgsChatScreen {
  final UserInform otherUserInfo;
  final String chatRoomId;
  final Socket socket;

  ArgsChatScreen({
    @required this.chatRoomId,
    @required this.otherUserInfo,
    @required this.socket,
  });
}

class ChatScreen extends StatelessWidget {
  static const String routName = 'chat-screen';

  final ArgsChatScreen args;

  ChatScreen({
    Key key,
    @required this.args,
  }) : super(key: key);

  final GlobalKey<MessagesWidgetState> _messagesKey = GlobalKey();

  void _setting() {}

  void _callVideo(BuildContext context) {
    Navigator.of(context).pushNamed(
      CallVideoScreen.routeName,
      arguments: ArgsCallVideoScreen(
        isAnswer: false,
        receiverId: args.otherUserInfo.userId,
      ),
    );
  }

  void _addMessageHandler(String message) {
    _messagesKey.currentState.addMessage(MessageData.TEXT, message);
  }

  void _addMessageImageHandler(String imageUrl) {
    _messagesKey.currentState.addMessage(MessageData.IMAGE, imageUrl);
  }

  void _leaveRoom(String chatRoomId) {
    this.args.socket.emitWithAck(
      SocketEvent.clientLeaveChat,
      {
        'roomId': chatRoomId,
      },
      ack: (result) {
        print('ack $result');
        if (result != null) {
          print(result);
        } else {
          print("Leave room $chatRoomId succeed");
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        _leaveRoom(this.args.chatRoomId);
        return true;
      },
      child: BlocProvider(
        create: (context) => sl<UploadMessageImageBloc>(),
        child: Scaffold(
          body: getBody(size),
          appBar: getAppBar(context),
        ),
      ),
    );
  }

  getBody(Size size) {
    return Container(
      color: MyAppTheme.secondary,
      child: Column(
        children: [
          Expanded(
            child: MessagesWidget(
              key: _messagesKey,
              socket: this.args.socket,
              chatRoomId: this.args.chatRoomId,
              otherUserInfo: this.args.otherUserInfo,
            ),
          ),
          NewMessageWidget(
            socket: this.args.socket,
            addMessageCallback: _addMessageHandler,
            addMessageImageCallback: _addMessageImageHandler,
            chatRoomId: this.args.chatRoomId,
          ),
        ],
      ),
    );
  }

  getAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: MyAppTheme.secondary,
      iconTheme: IconThemeData(color: MyAppTheme.primary),
      title: Row(
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(this.args.otherUserInfo.avt[0]),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            this.args.otherUserInfo.firstname,
            style: TextStyle(
              color: MyAppTheme.primary,
            ),
          ),
        ],
      ),
      elevation: 0,
      actions: [
        IconButton(
          icon: Icon(Icons.videocam),
          onPressed: () => _callVideo(context),
        ),
        /*IconButton(
          icon: Icon(Icons.settings),
          onPressed: _setting,
        ),*/
      ],
      titleSpacing: 4,
    );
  }
}
