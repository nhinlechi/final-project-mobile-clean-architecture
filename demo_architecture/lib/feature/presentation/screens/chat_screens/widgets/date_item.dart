import 'package:demo_architecture/feature/presentation/data_prototype/girl.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/day_different_widget.dart';
import 'package:flutter/material.dart';

class DateItem extends StatelessWidget {
  final String friendName;
  final int datingTime;
  final String placePhotoUrl;
  final Function() onTap;

  const DateItem({
    Key key,
    @required this.friendName,
    @required this.placePhotoUrl,
    @required this.datingTime,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datingTime =
        DateTime.fromMillisecondsSinceEpoch(this.datingTime).toLocal();
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 16),
        child: Row(
          children: [
            CircleAvatar(
              radius: 30,
              backgroundImage: placePhotoUrl != null
                  ? NetworkImage(placePhotoUrl)
                  : AssetImage(girls[0]['img']),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: Column(
                children: [
                  getTopContent(context, datingTime),
                  const SizedBox(height: 8),
                  getBottomContent(context, datingTime),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getTopContent(BuildContext context, DateTime datingTime) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        RichText(
          maxLines: 1,
          text: TextSpan(
            style: DefaultTextStyle.of(context).style.copyWith(
                  fontSize: MyTextStyle.title_5_fontsize,
                ),
            children: [
              TextSpan(
                text: 'Dating With ',
              ),
              TextSpan(
                text: friendName,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        Text('${TimeOfDay.fromDateTime(datingTime).format(context)}'),
      ],
    );
  }

  getBottomContent(BuildContext context, DateTime datingTime) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        DayDifferentWidget(
          timeInMilliseconds: datingTime.millisecondsSinceEpoch,
        ),
        Text('${datingTime.toString().split(' ')[0]}'),
      ],
    );
  }
}
