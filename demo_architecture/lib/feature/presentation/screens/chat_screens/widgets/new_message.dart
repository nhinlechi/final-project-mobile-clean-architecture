import 'dart:developer';
import 'dart:io';

import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/upload_message_image_bloc/upload_message_image_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/profile_screens/edit_image_popup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/socket/socket_event.dart';
import '../../../themes/colors.dart';
import '../request_place_popup.dart';

class NewMessageWidget extends StatefulWidget {
  final Socket socket;
  final String chatRoomId;
  final Function(String text) addMessageCallback;
  final Function(String imageUrl) addMessageImageCallback;

  const NewMessageWidget({
    Key key,
    @required this.socket,
    @required this.addMessageCallback,
    @required this.addMessageImageCallback,
    @required this.chatRoomId,
  }) : super(key: key);

  @override
  _NewMessageWidgetState createState() => _NewMessageWidgetState();
}

class _NewMessageWidgetState extends State<NewMessageWidget>
    with SingleTickerProviderStateMixin {
  //
  FocusNode _focusNode = FocusNode();
  GlobalKey _globalKey = GlobalKey();

  // Messages
  String _newMessage = '';
  File _pickImage;
  final _newMessageController = TextEditingController();

  // Animation
  AnimationController _animController;

  //
  final widgetHeight = 80.0; // this calculate for listen event start typing

  void _chooseImage() async {
    File pickedImage = await showCupertinoModalPopup(
      context: context,
      builder: (context) => GetImagePopup(),
    );
    if (pickedImage != null) {
      //
      context.read<UploadMessageImageBloc>().add(
            GetLocalImageSuccessEvent(pickedImage: pickedImage),
          );
      //
      _pickImage = pickedImage;
    }
  }

  void _startUploadAndSendMessageImage() {
    if (_pickImage != null)
      context.read<UploadMessageImageBloc>().add(
            StartUploadAndSendImageEvent(
              params: UploadMessageImageParams(
                pickedImage: _pickImage,
              ),
            ),
          );
    // delete image
    _pickImage = null;
  }

  void _sendMessageImage(String imageUrl) {
    // Callback
    widget.addMessageImageCallback?.call(imageUrl);
    //
    widget.socket?.emitWithAck(
      SocketEvent.clientSentChat,
      {
        'message': imageUrl,
        'type': 'image',
        'roomId': widget.chatRoomId,
      },
      ack: (result) {
        print('ack $result');
        if (result != null) {
          // TODO: show message error here!
          print(result);
        } else {
          print("Send Succeed");
        }
      },
    );
  }

  void _sendMessageText() {
    // Callback
    if (_newMessage.isNotEmpty) {
      widget.addMessageCallback?.call(_newMessage);
    }
    // Emit event send message
    widget.socket?.emitWithAck(
      SocketEvent.clientSentChat,
      {
        'message': _newMessage,
        'type': 'text',
        'roomId': widget.chatRoomId,
      },
      ack: (result) {
        print('ack $result');
        if (result != null) {
          // TODO: show message error here!
          print(result);
        } else {
          print("Send Succeed");
        }
      },
    );
    _newMessageController.clear();
    _newMessage = '';
  }

  void _showRecommendPlaces() {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) => RecommendPlacePopup(
        matchId: widget.chatRoomId,
      ),
    );
  }

  void _onFocusChange() {
    if (widget.socket != null) {
      if (_focusNode.hasFocus) {
        log('start typing', name: 'socketIO');
        widget.socket.emit(
          SocketEvent.clientStartTyping,
          {
            'roomId': widget.chatRoomId,
          },
        );
      } else {
        log('end typing', name: 'socketIO');
        widget.socket.emit(
          SocketEvent.clientEndTyping,
          {
            'roomId': widget.chatRoomId,
          },
        );
      }
    }
  }

  void _sendMessage() {
    _startUploadAndSendMessageImage();
    _sendMessageText();
  }

  @override
  void initState() {
    super.initState();
    // Set up animation
    _animController = AnimationController(
      vsync: this,
      duration: Duration(
        seconds: 1,
      ),
      lowerBound: 1,
      upperBound: 1.2,
    );
    Future.delayed(Duration(seconds: 1), () {
      _animController..repeat(reverse: true);
    });

    // Add listener to focus node emit typing event to socket IO
    _focusNode.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return KeyboardVisibilityBuilder(
      builder: (context, isKeyboardVisible) {
        if (!isKeyboardVisible) {
          _focusNode.unfocus();
        }
        return BlocListener<UploadMessageImageBloc, UploadMessageImageState>(
          listener: (context, state) {
            if (state is UploadImageSuccessState) {
              _sendMessageImage(state.imageUrl);
            }
          },
          child: Container(
            key: _globalKey,
            height: widgetHeight,
            child: Row(
              children: [
                SizedBox(
                  width: 16,
                ),
                Container(
                  width: 36,
                  child: IconButton(
                    icon: ScaleTransition(
                      scale: _animController,
                      child: Icon(
                        Icons.restaurant,
                        color: MyAppTheme.primary,
                      ),
                    ),
                    highlightColor: MyAppTheme.primary,
                    splashColor: MyAppTheme.primary,
                    onPressed: _showRecommendPlaces,
                    padding: const EdgeInsets.only(right: 16),
                  ),
                ),
                Container(
                  width: 36,
                  child: IconButton(
                    icon: Icon(
                      Icons.image,
                      color: MyAppTheme.primary,
                    ),
                    highlightColor: MyAppTheme.primary,
                    splashColor: MyAppTheme.primary,
                    onPressed: _chooseImage,
                    padding: const EdgeInsets.only(right: 16),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 36,
                    child: TextField(
                      controller: _newMessageController,
                      cursorColor: MyAppTheme.primary,
                      focusNode: _focusNode,
                      onChanged: (value) => _newMessage = value,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          contentPadding: EdgeInsets.only(
                            left: 20,
                            right: 10,
                          ),
                          hintText: 'Aa ...'),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.send,
                    color: MyAppTheme.primary,
                  ),
                  onPressed: _sendMessage,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
