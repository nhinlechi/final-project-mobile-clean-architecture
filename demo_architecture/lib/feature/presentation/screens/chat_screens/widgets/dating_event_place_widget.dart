import 'package:demo_architecture/feature/data/models/place/coordinates.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/place_detail_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/place_map_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../domain/entities/place/place_entity.dart';
import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';
import '../../../widgets/generals/circle_button.dart';
import 'rating_widget.dart';

class DatingEventPlaceWidget extends StatelessWidget {
  final String label;
  final PlaceEntity place;

  const DatingEventPlaceWidget({
    Key key,
    @required this.label,
    @required this.place,
  }) : super(key: key);

  final String heroTag = 'dating place';

  void _openPlaceDetailScreen(BuildContext context) {
    Navigator.of(context).pushNamed(
      PlaceDetailScreen.routeName,
      arguments: ArgsPlaceDetailScreen(
        placeItem: place,
        imageUrl: place.photo[0],
        heroTag: heroTag,
      ),
    );
  }

  void _goToMap(BuildContext context, PlaceEntity place) {
    Navigator.of(context).pushNamed(
      PlaceMapScreen.routeName,
      arguments: ArgsPlaceMapScreen(
        placeLocation: LatLng(place.location.lat, place.location.long),
        placeTitle: place.name,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final imageWidth = size.width / 2.4;
    final height = imageWidth * 1.15;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 4),
          child: Text(
            label,
            style: TextStyle(
              color: MyAppTheme.black_grey,
              fontSize: MyTextStyle.title_5_fontsize,
            ),
          ),
        ),
        const SizedBox(height: 10),
        InkWell(
          onTap: () => _openPlaceDetailScreen(context),
          child: Container(
            height: height,
            child: Row(
              children: [
                Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Hero(
                      tag: heroTag,
                      child: Container(
                        width: imageWidth,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: NetworkImage(place.photo[0]),
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Positioned(
                      right: -10,
                      top: -10,
                      child: CircleButton(
                        backgroundColor: MyAppTheme.primary,
                        icon: Icons.location_on_rounded,
                        iconColor: MyAppTheme.secondary,
                        onTap: () => _goToMap(context, place),
                      ),
                    ),
                  ],
                ),
                const SizedBox(width: 32),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        place.name,
                        style: TextStyle(
                          fontSize: MyTextStyle.title_5_fontsize,
                        ),
                        maxLines: 2,
                      ),
                      const SizedBox(height: 8),
                      RatingWidget(
                        totalRating: place.total_rating,
                        rating: place.rating,
                      ),
                      const SizedBox(height: 8),
                      Text(
                        place.address,
                        style: TextStyle(
                          color: MyAppTheme.black_grey,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
