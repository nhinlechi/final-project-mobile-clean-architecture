import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class DatingEventTimeWidget extends StatelessWidget {
  final String label;
  final int datingTime;

  const DatingEventTimeWidget({
    Key key,
    @required this.label,
    @required this.datingTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final datingTime = DateTime.fromMillisecondsSinceEpoch(this.datingTime);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 4),
          child: Text(
            label,
            style: TextStyle(
              color: MyAppTheme.black_grey,
              fontSize: MyTextStyle.title_5_fontsize,
            ),
          ),
        ),
        Row(
          children: [
            Chip(
              label: Row(
                children: [
                  Icon(
                    Icons.access_time,
                    color: MyAppTheme.secondary,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    '${TimeOfDay.fromDateTime(datingTime).format(context)}',
                    style: TextStyle(color: MyAppTheme.secondary),
                  ),
                ],
              ),
              backgroundColor: MyAppTheme.black_grey,
            ),
            const SizedBox(width: 8),
            CircleAvatar(
              backgroundColor: MyAppTheme.grey,
              radius: 6,
            ),
            const SizedBox(width: 8),
            Chip(
              label: Row(
                children: [
                  Icon(
                    Icons.calendar_today_outlined,
                    color: MyAppTheme.secondary,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    '${datingTime.toString().split(' ')[0]}',
                    style: TextStyle(color: MyAppTheme.secondary),
                  ),
                ],
              ),
              backgroundColor: MyAppTheme.black_grey,
            ),
          ],
        ),
      ],
    );
  }
}
