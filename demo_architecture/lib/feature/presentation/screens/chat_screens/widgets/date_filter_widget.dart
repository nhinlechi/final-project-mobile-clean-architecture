import 'package:demo_architecture/feature/data/models/utils/filter_model.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/widgets/filter_action_chip.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/custom_filter_bottom_sheet.dart';
import 'package:flutter/material.dart';

enum StatusFilterType {
  All,
  Upcoming,
  History,
}

enum SortFilterType {
  None,
  Inc,
  Dec,
}

class DateFilterWidget extends StatefulWidget {
  final Function(dynamic) filterCallback;

  const DateFilterWidget({
    Key key,
    this.filterCallback,
  }) : super(key: key);

  @override
  _DateFilterWidgetState createState() => _DateFilterWidgetState();
}

class _DateFilterWidgetState extends State<DateFilterWidget> {
  // Filter value
  var statusFilter = FilterModel<StatusFilterType>(
    iconData: Icons.access_alarm,
    shortName: 'All',
    value: StatusFilterType.All,
  );
  var sortFilter = FilterModel<SortFilterType>(
    iconData: Icons.sort,
    value: SortFilterType.None,
    shortName: 'Sort',
  );

  //
  final List<FilterModel<StatusFilterType>> statusFilterModels = [
    FilterModel(
      iconData: Icons.access_alarm,
      shortName: 'All',
      value: StatusFilterType.All,
    ),
    FilterModel(
      iconData: Icons.access_alarm,
      shortName: 'Upcoming',
      value: StatusFilterType.Upcoming,
    ),
    FilterModel(
      iconData: Icons.update_outlined,
      shortName: 'History',
      value: StatusFilterType.History,
    ),
  ];

  final sortFilterModels = [
    FilterModel(
      iconData: Icons.sort,
      shortName: 'Sort',
      value: SortFilterType.None,
    ),
    FilterModel(
      iconData: Icons.arrow_circle_down,
      shortName: 'Decrease',
      value: SortFilterType.Dec,
    ),
    FilterModel(
      iconData: Icons.arrow_circle_up,
      shortName: 'Increase',
      value: SortFilterType.Inc,
    ),
  ];

  void _onStatusFilter() async {
    final filter = await customFilterBottomSheet<FilterModel<StatusFilterType>>(
      context: context,
      title: 'Status',
      current: statusFilter,
      filterModels: statusFilterModels,
    );
    if (filter != null) {
      setState(() {
        statusFilter = filter;
      });
      widget.filterCallback(filter.value);
    }
  }

  void _onSortFilter() async {
    final filter = await customFilterBottomSheet<FilterModel<SortFilterType>>(
      context: context,
      title: 'Sort By Dating Time',
      current: sortFilter,
      filterModels: sortFilterModels,
    );
    if (filter != null) {
      setState(() {
        sortFilter = filter;
      });
      widget.filterCallback(filter.value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(width: 16),
        FilterActionChip(
          label: statusFilter.shortName,
          bgColor: statusFilter.value != StatusFilterType.All
              ? MyAppTheme.primary
              : MyAppTheme.black_grey,
          iconData: statusFilter.iconData,
          onTap: () => _onStatusFilter(),
        ),
        FilterActionChip(
          label: sortFilter.shortName,
          bgColor: sortFilter.value != SortFilterType.None
              ? MyAppTheme.primary
              : MyAppTheme.black_grey,
          iconData: sortFilter.iconData,
          onTap: () => _onSortFilter(),
        ),
      ],
    );
  }
}
