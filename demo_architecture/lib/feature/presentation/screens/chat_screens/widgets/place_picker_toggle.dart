import 'package:flutter/material.dart';

import '../../../../domain/entities/place/list_place_entity.dart';
import '../../../../domain/entities/place/place_entity.dart';
import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';
import 'rating_widget.dart';

class PlacePickerToggle extends StatefulWidget {
  final List<PlaceEntity> places;
  final Function(PlaceEntity placeEntity) pickPlaceCallback;

  const PlacePickerToggle({
    Key key,
    @required this.places,
    @required this.pickPlaceCallback,
  }) : super(key: key);

  @override
  _PlacePickerToggleState createState() => _PlacePickerToggleState();
}

class _PlacePickerToggleState extends State<PlacePickerToggle> {
  List<bool> placesSelected;

  @override
  void initState() {
    super.initState();
    placesSelected = List.generate(
      widget.places.length,
      (index) => false,
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: List.generate(placesSelected.length, (index) {
          final places = widget.places;
          return Padding(
            padding: EdgeInsets.only(bottom: 20, right: 2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                      width: 68,
                      height: 68,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        image: DecorationImage(
                          image: NetworkImage(places[index].photo[0]),
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        LimitedBox(
                          maxWidth: size.width / 2,
                          child: Text(
                            places[index].name,
                            maxLines: 2,
                            style:
                                TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        RatingWidget(
                          rating: places[index].rating,
                          totalRating: places[index].total_rating,
                        ),
                      ],
                    ),
                  ],
                ),
                FilterChip(
                  shape: CircleBorder(),
                  label: Container(
                    width: 20,
                    child: placesSelected[index]
                        ? Icon(
                            Icons.check_circle,
                            size: 20,
                            color: MyAppTheme.secondary,
                          )
                        : SizedBox(),
                  ),
                  onSelected: (value) {
                    setState(() {
                      if (!value) return;

                      for (int i = 0; i < placesSelected.length; i++) {
                        placesSelected[i] = false;
                      }
                      placesSelected[index] = value;

                      // callback
                      widget.pickPlaceCallback(places[index]);
                    });
                  },
                  selected: placesSelected[index],
                  selectedColor: MyAppTheme.primary,
                  showCheckmark: false,
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
