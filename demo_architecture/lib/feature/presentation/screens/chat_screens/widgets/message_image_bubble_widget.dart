import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/message_bubble.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../view_full_image_screen.dart';

class MessageImageBubbleWidget extends StatelessWidget {
  final bool isMine;
  final String imageUrl;
  final String avatar;
  final MessageBubblePosition position;

  const MessageImageBubbleWidget({
    Key key,
    @required this.isMine,
    @required this.imageUrl,
    @required this.position,
    @required this.avatar,
  }) : super(key: key);

  void _openViewFullImage(BuildContext context) {
    Navigator.of(context).push(
      PageTransition(
        child: ViewFullImageScreen(
          imageUrl: this.imageUrl,
        ),
        type: PageTransitionType.rightToLeft,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(
        top: position == MessageBubblePosition.TOP ||
                position == MessageBubblePosition.SINGLE
            ? 10
            : 2,
      ),
      child: Row(
        mainAxisAlignment:
            isMine ? MainAxisAlignment.end : MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          if (!isMine)
            avatar != null
                ? CircleAvatar(
                    backgroundImage: NetworkImage(avatar),
                    radius: 14,
                  )
                : SizedBox(
                    width: 28,
                  ),
          const SizedBox(width: 8),
          InkWell(
            onTap: () => _openViewFullImage(context),
            child: Container(
              height: size.width / 3 * 1.2,
              width: size.width / 3,
              decoration: BoxDecoration(
                color: MyAppTheme.grey,
                borderRadius: BorderRadius.circular(10),
                image: imageUrl != null
                    ? DecorationImage(
                        image: NetworkImage(imageUrl),
                        fit: BoxFit.cover,
                      )
                    : null,
              ),
            ),
          )
        ],
      ),
    );
  }
}
