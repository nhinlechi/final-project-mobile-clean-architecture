import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import '../../../themes/colors.dart';
import '../confirm_place_popup.dart';

class PlaceRecommendFloatingWidget extends StatelessWidget {
  final double width;
  final double height;
  final bool isShowCircleButton;
  final RequestPlaceEntity data;

  const PlaceRecommendFloatingWidget({
    Key key,
    @required this.data,
    this.isShowCircleButton = true,
    this.width = 80,
    this.height = 80,
  }) : super(key: key);

  void _showConfirmPlaceDetailPopup(BuildContext context) {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) => ConfirmPlacePopup(
        data: this.data,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
          onTap: () => _showConfirmPlaceDetailPopup(context),
          child: AvatarGlow(
            endRadius: width,
            glowColor: MyAppTheme.primary,
            repeat: true,
            child: Container(
              height: height,
              width: width,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(data.place.photo[0]),
                  fit: BoxFit.cover,
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 4),
                    blurRadius: 4,
                    color: Colors.black.withOpacity(0.25),
                  ),
                ],
              ),
            ),
          ),
        ),
        if (isShowCircleButton)
          Positioned(
            bottom: 20,
            right: 20,
            child: ElevatedButton(
              onPressed: () => _showConfirmPlaceDetailPopup(context),
              child: Icon(Icons.open_in_new),
              style: ElevatedButton.styleFrom(
                shape: CircleBorder(),
                primary: MyAppTheme.primary,
              ),
            ),
          ),
      ],
    );
  }
}
