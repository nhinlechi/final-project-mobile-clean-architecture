import 'dart:io';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/upload_message_image_bloc/upload_message_image_bloc.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UploadMessageImageContainer extends StatelessWidget {
  final bool isUploading;
  final File imageFile;

  const UploadMessageImageContainer({
    Key key,
    this.imageFile,
    this.isUploading = false,
  }) : super(key: key);

  void _deleteImage(BuildContext context) {
    context.read<UploadMessageImageBloc>().add(DeleteLocalImageEvent());
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          height: size.width / 5 * 1.2,
          width: size.width / 5,
          decoration: BoxDecoration(
            color: MyAppTheme.grey,
            borderRadius: BorderRadius.circular(10),
            image: imageFile != null
                ? DecorationImage(
                    image: FileImage(imageFile),
                    fit: BoxFit.cover,
                  )
                : null,
          ),
          child: imageFile == null ? CircularLoading() : Container(),
        ),
        if (!isUploading)
          Positioned(
            right: -8,
            top: -8,
            child: SizedBox(
              width: 32,
              height: 32,
              child: ElevatedButton(
                onPressed: () => _deleteImage(context),
                child: Icon(
                  Icons.close,
                  size: 20,
                ),
                style: ElevatedButton.styleFrom(
                  primary: MyAppTheme.black_grey,
                  shape: CircleBorder(),
                  padding: const EdgeInsets.all(0),
                ),
              ),
            ),
          ),
      ],
    );
  }
}
