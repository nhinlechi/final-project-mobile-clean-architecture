import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';

import '../../../themes/colors.dart';

class TypingBubble extends StatelessWidget {
  final String avatar;
  final Artboard artBoard;

  const TypingBubble({Key key, this.avatar, this.artBoard}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            backgroundImage: NetworkImage(avatar),
            radius: 14,
          ),
          artBoard != null
              ? Container(
                  height: 24,
                  width: 48,
                  padding: EdgeInsets.all(4),
                  margin: EdgeInsets.only(left: 8),
                  decoration: BoxDecoration(
                    color: MyAppTheme.black_grey,
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Rive(
                    artboard: artBoard,
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
}
