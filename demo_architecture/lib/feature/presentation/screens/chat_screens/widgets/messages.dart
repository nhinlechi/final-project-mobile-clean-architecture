import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/upload_message_image_bloc/upload_message_image_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/message_image_bubble_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rive/rive.dart';

import '../../../../../core/socket/socket_event.dart';
import '../../../../../injection_container.dart';
import '../../../../data/models/chat/message_model.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/chats/get_chat_messages_uc.dart';
import '../../../bloc/chat_blocs/get_chat_bloc.dart';
import '../../../bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import '../../../repository_providers/user_repository_provider.dart';
import '../../../themes/colors.dart';
import 'message_bubble.dart';
import 'typing_bubble.dart';
import 'place_recommend_floating_widget.dart';
import 'upload_message_image_container.dart';

class MessagesWidget extends StatefulWidget {
  final socket;
  final String chatRoomId;
  final UserInform otherUserInfo;

  const MessagesWidget({
    Key key,
    @required this.socket,
    @required this.chatRoomId,
    @required this.otherUserInfo,
  }) : super(key: key);

  @override
  MessagesWidgetState createState() => MessagesWidgetState();
}

class MessagesWidgetState extends State<MessagesWidget> {
  // State
  UserInform _currentUserInfo;
  GetChatBloc _getChatBloc;
  bool _isTyping = false;
  List<MessageModel> _messageModels = [];
  MessageModel _typingMessage;
  DateTime _newestMessageTimer = DateTime.now().subtract(Duration(seconds: 30));
  int _expiredTimeInSeconds = 30;

  // Animations
  Artboard _typingAnimArtBoard;
  RiveAnimationController _controller;

  final _scrollController = ScrollController();

  void _joinRoomAndListen(UserInform userInform) {
    final socket = widget.socket;
    if (socket != null && socket.connected) {
      socket.emitWithAck(
        SocketEvent.clientJoinChat,
        {
          'roomId': widget.chatRoomId,
        },
        ack: (result) {
          print('ack $result');
          if (result != null) {
            print(result);
          } else {
            print("Join room ${widget.chatRoomId} succeed");
          }
        },
      );

      socket.on(
        SocketEvent.serverSentChat,
        (data) {
          // Update UI
          // Get 2 message to compare
          final serverMessage = MessageModel.fromJson(data);
          // Not have message yet
          if (_messageModels.isEmpty) {
            setState(() {
              _messageModels.insert(0, serverMessage);
            });
            return;
          }

          final latestMessage = _messageModels.first;
          // Get timestamp to compare
          final latestTime =
              DateTime.fromMillisecondsSinceEpoch(latestMessage.createdAt);
          final now = DateTime.now();
          // Compare
          if (serverMessage.userId == latestMessage.userId &&
              now.difference(latestTime).inSeconds < _expiredTimeInSeconds) {
            setState(() {
              _messageModels.first.messages.addAll(serverMessage.messages);
            });
          } else {
            setState(() {
              _messageModels.insert(0, serverMessage);
            });
          }
          _scrollChatList();
        },
      );
    }
  }

  void _clearOldSocketIOListeners() {
    widget.socket.clearListeners();
  }

  void _setupOnTypingListener() {
    final socket = widget.socket;
    if (socket != null) {
      socket.on(SocketEvent.serverStartTyping, (data) {
        this._typingMessage = MessageModel.fromTypingEventJson(data);
        setState(() {
          _isTyping = true;
        });
        _scrollChatList();
      });

      socket.on(SocketEvent.serverEndTyping, (data) {
        setState(() {
          _isTyping = false;
        });
      });
    }
  }

  void addMessage(String type, String message) {
    // No Message yet
    if (_messageModels.isEmpty) {
      _addNormalMessage(type, message);
      return;
    }
    // Get message and timer
    final latestMessage = _messageModels.first;
    final now = DateTime.now();
    // Compare
    if (latestMessage.userId == _currentUserInfo.userId &&
        now.difference(_newestMessageTimer).inSeconds < _expiredTimeInSeconds) {
      setState(() {
        _messageModels.first.messages.add(
          MessageData(message: message, type: type),
        );
      });
    } else {
      _addNormalMessage(type, message);
    }
    // Reset timer
    _newestMessageTimer = now;
    // Animation
    _scrollChatList();
  }

  void _addNormalMessage(String type, String message) {
    setState(() {
      _messageModels.insert(
        0,
        MessageModel(
          messages: [MessageData(message: message, type: type)],
          userId: _currentUserInfo.userId,
          userName: _currentUserInfo.firstname,
          avatar: _currentUserInfo.avt[0],
        ),
      );
    });
  }

  void _leaveRoom() {
    widget.socket.emit(SocketEvent.clientLeaveChat);
  }

  void _jumpBackChatList() {
    _scrollController.jumpTo(
      _scrollController.offset + 10,
    );
  }

  void _scrollChatList() {
    _jumpBackChatList();
    // Animation
    _scrollController.animateTo(
      _scrollController.position.minScrollExtent,
      duration: Duration(milliseconds: 800),
      curve: Curves.easeOutBack,
    );
  }

  void _onScrollToLoadOldMessage() {
    if (_scrollController.position.extentAfter == 0.0) {
      if (_getChatBloc.state is GetChatLoading ||
          _messageModels.last.index == 0) return;

      _getChatBloc.add(
        FetchChatEvent(
          params: GetChatParam(
            matchId: widget.chatRoomId,
            lastMessageIndex: _messageModels.last.index,
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _currentUserInfo = context.read<UserRepositoryProvider>().userInform;
    _getChatBloc = sl<GetChatBloc>();
    _getChatBloc.add(FetchChatEvent(
      params: GetChatParam(matchId: widget.chatRoomId, lastMessageIndex: null),
    ));
    // Join socket room and listen on it
    _joinRoomAndListen(_currentUserInfo);
    // Typing Listener
    _setupOnTypingListener();

    // Load Typing animation
    rootBundle
        .load('assets/animations/rives/typing_animation.riv')
        .then((data) async {
      try {
        final file = RiveFile.import(data);
        final artBoard = file.mainArtboard;
        artBoard.addController(_controller = SimpleAnimation('Typing'));
        setState(() {
          _typingAnimArtBoard = artBoard;
        });
      } catch (e) {
        print(e);
      }
    });
    // On Scroll Listener
    _scrollController.addListener(_onScrollToLoadOldMessage);
  }

  @override
  void dispose() {
    _getChatBloc.close();
    _leaveRoom();
    _clearOldSocketIOListeners();
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Theme(
      data: ThemeData(
        accentColor: Colors.white,
      ),
      child: Stack(
        children: [
          // Messages Stack -------------------------------------------------------------------------------
          Padding(
            padding: EdgeInsets.only(left: 12, right: 16),
            child: BlocBuilder<GetChatBloc, GetChatState>(
              bloc: _getChatBloc,
              builder: (context, state) {
                if (state is GetChatInitial) {
                  return getChatLoadingEffect();
                }

                if (state is GetChatFailed) {
                  return Center(
                    child: Text(state.message),
                  );
                }

                // Success
                if (state is GetChatSucceed) {
                  if (_messageModels.isEmpty ||
                      (state.messages.isNotEmpty &&
                          _messageModels.last.index != null &&
                          _messageModels.last.index !=
                              state.messages.last.index)) {
                    _messageModels.addAll(state.messages);
                  }
                }

                // Success but not have message yet
                if (_messageModels.isEmpty) {
                  return ListView.builder(
                    itemCount: 1,
                    reverse: true,
                    controller: _scrollController,
                    itemBuilder: (context, index) {
                      if (state is GetChatLoading) {
                        return getChatLoadingEffect();
                      }
                      return getOtherInfoWidget();
                    },
                  );
                }

                return ListView.builder(
                  controller: _scrollController,
                  reverse: true,
                  itemCount: _isTyping
                      ? _messageModels.length + 2
                      : _messageModels.length + 1,
                  itemBuilder: (context, index) {
                    // Calculate length and index of item in list view
                    int length = _messageModels.length + 1;
                    int messageIndex = index;
                    if (_isTyping) {
                      length++;
                      messageIndex--;
                    }
                    // Typing Animation
                    if (index == 0 && _isTyping) {
                      return TypingBubble(
                        artBoard: _typingAnimArtBoard,
                        avatar: _typingMessage.avatar,
                      );
                    }
                    // Normal message
                    if (index < length - 1) {
                      final mess = _messageModels[messageIndex];
                      return getAdjacentMessageBubble(mess, messageIndex);
                    } else if (_messageModels.length == 0 ||
                        (_messageModels.last.index != 0 &&
                            _messageModels.last.index != null)) {
                      return getChatLoadingEffect();
                    } else {
                      return getOtherInfoWidget();
                    }
                  },
                );
              },
            ),
          ),
          // Confirm Place Stack -----------------------------------------------------------------------------
          BlocBuilder<ConfirmPlaceBloc, ConfirmPlaceState>(
            builder: (context, state) {
              if (state is HavingRequestPlaceState) {
                final data = state.data;
                if (data.matchId == widget.chatRoomId &&
                    data.senderId == _currentUserInfo.userId)
                  return Positioned(
                    top: -10,
                    right: -10,
                    child: PlaceRecommendFloatingWidget(
                      height: 60,
                      width: 60,
                      data: state.data,
                      isShowCircleButton: false,
                    ),
                  );
                if (data.matchId == widget.chatRoomId)
                  return Positioned(
                    top: -10,
                    child: SizedBox(
                      width: size.width,
                      child: Align(
                        alignment: Alignment.center,
                        child: PlaceRecommendFloatingWidget(
                          data: state.data,
                        ),
                      ),
                    ),
                  );
              }
              if (state is ConfirmPlaceSuccess) {
                final data = state.data;
                if (data.matchId == widget.chatRoomId)
                  return Positioned(
                    top: -10,
                    right: -10,
                    child: PlaceRecommendFloatingWidget(
                      height: 60,
                      width: 60,
                      data: state.data,
                      isShowCircleButton: false,
                    ),
                  );
              }
              return Container();
            },
          ),
          // Upload Image Stack -------------------------------------------------------------------------
          Positioned(
            bottom: 8,
            right: size.width / 2 - size.width / 10,
            child: BlocBuilder<UploadMessageImageBloc, UploadMessageImageState>(
              builder: (context, state) {
                if (state is HavingLocalImageState) {
                  return UploadMessageImageContainer(
                    imageFile: state.pickedImage,
                  );
                }
                if (state is UploadImageLoadingState) {
                  return UploadMessageImageContainer(isUploading: true);
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget getAdjacentMessageBubble(MessageModel mess, int messModelIndex) =>
      Column(
        children: List.generate(mess.messages.length, (index) {
          MessageBubblePosition messagePosition;
          if (index == 0)
            messagePosition = MessageBubblePosition.TOP;
          else if (index == mess.messages.length - 1)
            messagePosition = MessageBubblePosition.BOTTOM;
          else
            messagePosition = MessageBubblePosition.MIDDLE;

          if (mess.messages.length == 1)
            messagePosition = MessageBubblePosition.SINGLE;

          // Message Text
          if (mess.messages[index].type == MessageData.TEXT)
            return MessageBubbleWidget(
              message: mess.messages[index].message,
              isMine: _currentUserInfo.userId == mess.userId,
              //key: ValueKey('$mess $messModelIndex'),
              username: mess.userName,
              imageUrl: index == mess.messages.length - 1 ? mess.avatar : null,
              position: messagePosition,
            );

          // Message Image
          if (mess.messages[index].type == MessageData.IMAGE)
            return MessageImageBubbleWidget(
              isMine: _currentUserInfo.userId == mess.userId,
              imageUrl: mess.messages[index].message,
              position: messagePosition,
              avatar: mess.avatar,
            );

          // Default:
          return Container();
        }),
      );

  Widget getOtherInfoWidget() => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 32),
          CircleAvatar(
            foregroundImage: NetworkImage(
              this.widget.otherUserInfo.avt[0],
            ),
            radius: 60,
          ),
          SizedBox(height: 8),
          Text(
            '${this.widget.otherUserInfo.firstname} ${this.widget.otherUserInfo.lastname}',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text('Lives in Ho Chi Minh City'),
          SizedBox(height: 32),
        ],
      );

  Widget getChatLoadingEffect() => Center(
        child: SpinKitPulse(
          size: 14,
          color: MyAppTheme.black_grey,
        ),
      );
}
