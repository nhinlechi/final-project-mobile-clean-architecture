import 'package:flutter/material.dart';

import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';

enum MessageBubblePosition {
  TOP,
  MIDDLE,
  BOTTOM,
  SINGLE,
}

class MessageBubbleWidget extends StatelessWidget {
  final String message;
  final bool isMine;
  final String username;
  final String imageUrl;
  final MessageBubblePosition position;

  const MessageBubbleWidget({
    Key key,
    this.message,
    this.isMine,
    this.username,
    this.imageUrl,
    this.position,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    double topRight, topLeft, bottomLeft, bottomRight;
    topRight = topLeft = bottomLeft = bottomRight = 20;

    if (position != MessageBubblePosition.SINGLE) {
      if (isMine) {
        if (position == MessageBubblePosition.TOP) bottomRight = 4;
        if (position == MessageBubblePosition.MIDDLE) bottomRight = topRight = 4;
        if (position == MessageBubblePosition.BOTTOM) topRight = 4;
      } else {
        if (position == MessageBubblePosition.TOP) bottomLeft = 4;
        if (position == MessageBubblePosition.MIDDLE) bottomLeft = topLeft = 4;
        if (position == MessageBubblePosition.BOTTOM) topLeft = 4;
      }
    }

    return Padding(
      padding: EdgeInsets.only(
        top: position == MessageBubblePosition.TOP ||
                position == MessageBubblePosition.SINGLE
            ? 10
            : 2,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: isMine ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          if (!isMine)
            imageUrl != null
                ? CircleAvatar(
                    backgroundImage: NetworkImage(imageUrl),
                    radius: 14,
                  )
                : SizedBox(
                    width: 28,
                  ),
          ConstrainedBox(
            constraints: BoxConstraints(
              maxWidth: size.width * 2 / 3,
            ),
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 8,
                horizontal: 16,
              ),
              margin: EdgeInsets.only(left: 8),
              decoration: BoxDecoration(
                color: isMine ? MyAppTheme.primary : MyAppTheme.black_grey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(topLeft),
                  topRight: Radius.circular(topRight),
                  bottomLeft: Radius.circular(bottomLeft),
                  bottomRight: Radius.circular(bottomRight),
                ),
              ),
              child: Column(
                children: [
                  Text(
                    message,
                    style: TextStyle(
                      color: MyAppTheme.secondary,
                      fontSize: MyTextStyle.title_5_fontsize,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
