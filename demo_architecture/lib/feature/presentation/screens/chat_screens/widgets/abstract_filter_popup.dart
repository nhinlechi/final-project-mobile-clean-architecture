import 'package:demo_architecture/feature/data/models/notification/notification_model.dart';
import 'package:demo_architecture/feature/data/models/utils/filter_model.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class AbstractFilterPopup extends StatelessWidget {
  final String title;
  final FilterModel currentFilterModel;
  final List<FilterModel> filterModels;

  AbstractFilterPopup({
    Key key,
    @required this.title,
    @required this.filterModels,
    @required this.currentFilterModel,
  }) : super(key: key);

  void _onFilter(BuildContext context, FilterModel filterModel) {
    Navigator.of(context).pop(filterModel);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height / 2,
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Feather.filter,
                  color: MyAppTheme.black_grey,
                  size: 20,
                ),
                const SizedBox(
                  width: 10,
                ),
                Text(
                  title,
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    color: MyAppTheme.black_grey,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: List.generate(
                    filterModels.length,
                    (index) => getFilterItem(context, filterModels[index]),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getFilterItem(BuildContext context, FilterModel filterModel) =>
      InkWell(
        onTap: () => _onFilter(context, filterModel),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (filterModel.iconData != null)
                Icon(
                  filterModel.iconData,
                  size: 20,
                  color: filterModel.value == currentFilterModel.value
                      ? MyAppTheme.primary
                      : MyAppTheme.black,
                ),
              const SizedBox(width: 8),
              Text(
                filterModel.shortName,
                style: TextStyle(
                  fontSize: MyTextStyle.title_5_fontsize,
                  color: filterModel.value == currentFilterModel.value
                      ? MyAppTheme.primary
                      : MyAppTheme.black,
                ),
              ),
            ],
          ),
        ),
      );
}
