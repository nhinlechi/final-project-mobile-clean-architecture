import 'package:flutter/material.dart';

import '../../../themes/colors.dart';

class RatingWidget extends StatelessWidget {
  final double rating;
  final int totalRating;

  const RatingWidget({Key key, this.rating, this.totalRating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text('$rating'),
        SizedBox(
          width: 2,
        ),
        Icon(
          Icons.star,
          color: MyAppTheme.primary,
        ),
        SizedBox(
          width: 8,
        ),
        Text('$totalRating reviews'),
      ],
    );
  }
}
