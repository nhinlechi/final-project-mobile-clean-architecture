import 'package:demo_architecture/feature/data/models/notification/notification_model.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../bloc/match_blocs/matching_bloc/matching_bloc.dart';
import '../../../repository_providers/match_repository_provider.dart';
import '../../../widgets/chat_item.dart';
import '../chat_screen.dart';

class CurrentMatchWidget extends StatefulWidget {
  final Socket socket;
  final Function(ArgsChatScreen args) openChatRoomCallback;

  const CurrentMatchWidget({
    Key key,
    @required this.openChatRoomCallback,
    @required this.socket,
  }) : super(key: key);

  @override
  _CurrentMatchWidgetState createState() => _CurrentMatchWidgetState();
}

class _CurrentMatchWidgetState extends State<CurrentMatchWidget> {
  MatchRepositoryProvider _matchRepos;
  AuthenticateToken _userInfo;

  @override
  void initState() {
    super.initState();
    _matchRepos = RepositoryProvider.of<MatchRepositoryProvider>(context);
    _userInfo = context.read<UserRepositoryProvider>().auth;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MatchingBloc, MatchState>(
      builder: (context, state) {
        if (state is MatchingState) {
          final currentMatchUsers = _matchRepos.currentMatchingInfo.users;
          final otherUserInfo = currentMatchUsers[0].userId == _userInfo.userId
              ? currentMatchUsers[1]
              : currentMatchUsers[0];
          return ChatItem(
            chatId: _matchRepos.currentMatchRoomId,
            imageUrl: otherUserInfo.avt[0],
            name: otherUserInfo.firstname,
            newestText: "Say hello !!!",
            isActive: true,
            onTap: () => widget.openChatRoomCallback(
              ArgsChatScreen(
                chatRoomId: _matchRepos.currentMatchRoomId,
                otherUserInfo: otherUserInfo,
                socket: widget.socket,
              ),
            ),
          );
        }
        return Container();
      },
    );
  }
}
