import 'package:demo_architecture/feature/presentation/widgets/generals/my_app_bar.dart';
import 'package:flutter/material.dart';

class ViewFullImageScreen extends StatelessWidget {
  final String imageUrl;

  const ViewFullImageScreen({
    Key key,
    @required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: MyAppBar(
        title: Text(''),
      ),
      body: Container(
        width: size.width,
        height: size.height,
        child: Image.network(
          imageUrl,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
