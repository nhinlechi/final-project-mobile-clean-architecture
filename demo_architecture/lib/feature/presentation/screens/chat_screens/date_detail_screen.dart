import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_place_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_time_widget.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'chat_screen.dart';

class DateDetailScreen extends StatelessWidget {
  final DateModel dateModel;

  const DateDetailScreen({
    Key key,
    @required this.dateModel,
  }) : super(key: key);

  void _openChatScreen(BuildContext context, UserInform otherUserInfo) {
    final socket = context.read<MatchRepositoryProvider>().chatSocket;
    Navigator.of(context).pushNamed(
      ChatScreen.routName,
      arguments: ArgsChatScreen(
          chatRoomId: dateModel.matchId,
          otherUserInfo: otherUserInfo,
          socket: socket),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final auth = context.read<UserRepositoryProvider>().auth;
    final matchRooms = context.read<MatchRepositoryProvider>().matchRooms;
    final currentRoom = matchRooms.firstWhere(
        (r) => r.matchId == dateModel.matchId,
        orElse: () => matchRooms.first);
    final otherUserInfo =
        currentRoom?.participants?.firstWhere((p) => p.userId != auth.userId);
    //
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'You have a date with ${otherUserInfo.firstname}',
              style: MyTextStyle.ts_title_Popup,
            ),
            const SizedBox(
              height: 32,
            ),
            DatingEventTimeWidget(
              label: 'Dating Time',
              datingTime: dateModel.datingTime,
            ),
            const SizedBox(
              height: 16,
            ),
            DatingEventPlaceWidget(
              label: 'Dating Place',
              place: dateModel.place,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.message,
          color: MyAppTheme.secondary,
        ),
        backgroundColor: MyAppTheme.primary,
        onPressed: () => _openChatScreen(context, otherUserInfo),
      ),
    );
  }
}
