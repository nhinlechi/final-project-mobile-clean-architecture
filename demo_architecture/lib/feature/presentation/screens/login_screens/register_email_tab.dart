import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../core/utils/validator.dart';
import '../register_screens/register_screen.dart';

class RegisterWithEmailTab extends StatefulWidget {
  @override
  _RegisterWithEmailTabState createState() => _RegisterWithEmailTabState();
}

class _RegisterWithEmailTabState extends State<RegisterWithEmailTab> {
  String _email;
  String _password;

  void _registerNewAccount() async {
    Navigator.of(context).push(
      PageTransition(
        child: RegisterScreen(
          email: _email,
          password: _password,
          isSocialRegister: false,
        ),
        type: PageTransitionType.rightToLeft,
      ),
    );
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            TextFormField(
              decoration: InputDecoration(
                hintText: 'Email',
                contentPadding: EdgeInsets.all(15),
                labelText: 'Email',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              onChanged: (value) {
                _email = value;
              },
              validator: (value) {
                return Validator.validatorEmail(value);
              },
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              obscureText: true,
              decoration: InputDecoration(
                hintText: 'Password',
                contentPadding: EdgeInsets.all(15),
                labelText: 'Password',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              onChanged: (value) {
                _password = value;
              },
              validator: (value) {
                return Validator.validatorPassword(value);
              },
            ),
            Expanded(child: Container()),
            Container(
              width: size.width,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _registerNewAccount();
                  }
                },
                child: Text(
                  'Continue',
                  style: TextStyle(
                    color: MyAppTheme.secondary,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
