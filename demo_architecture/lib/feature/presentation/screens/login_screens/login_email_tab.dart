import 'package:demo_architecture/feature/presentation/pages/home_page.dart';
import 'package:demo_architecture/feature/presentation/widgets/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../core/utils/validator.dart';
import '../../../../injection_container.dart';
import '../../bloc/authentication_blocs/loginv2/authenticate_bloc.dart';
import '../../themes/colors.dart';

class LoginWithEmailTab extends StatefulWidget {
  @override
  _LoginWithEmailTabState createState() => _LoginWithEmailTabState();
}

class _LoginWithEmailTabState extends State<LoginWithEmailTab> {
  String _email;
  String _password;
  AuthenticateBloc _loginV2Bloc;

  void _submit(String email, String password) async {
    _loginV2Bloc?.add(LoginSubmitted(email, password));
  }

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _loginV2Bloc = sl<AuthenticateBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Padding(
          padding: EdgeInsets.all(20),
          child: BlocListener<AuthenticateBloc, AuthenticateState>(
            bloc: _loginV2Bloc,
            listener: (context, state) {
              if (state is AuthenticateSuccess)
                Navigator.of(context).pushNamedAndRemoveUntil(
                  HomePage.routeName,
                  (route) => false,
                );
              if (state is AuthenticateFailed) {
                showCustomFailedDialog(context, state.message);
              }
            },
            child: Column(
              children: [
                TextFormField(
                  validator: (value) {
                    return Validator.validatorEmail(value);
                  },
                  decoration: InputDecoration(
                    hintText: 'Email',
                    contentPadding: EdgeInsets.all(15),
                    labelText: 'Email',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onChanged: (value) {
                    _email = value;
                  },
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Xin nhập Password';
                    }
                    return null;
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    contentPadding: EdgeInsets.all(15),
                    labelText: 'Password',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onChanged: (value) {
                    _password = value;
                  },
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: TextButton(
                    onPressed: () {},
                    child: Text(
                      'Forgot password?',
                      style: TextStyle(
                        color: MyAppTheme.primary,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    style: TextButton.styleFrom(
                      primary: MyAppTheme.primary,
                      padding: EdgeInsets.only(
                        left: 20,
                      ),
                    ),
                  ),
                ),
                Expanded(child: Container()),
                Container(
                  width: size.width,
                  height: 50,
                  child: BlocBuilder<AuthenticateBloc, AuthenticateState>(
                    bloc: _loginV2Bloc,
                    builder: (context, state) {
                      if (state is AuthenticateLoading)
                        return Center(
                          child: const CircularProgressIndicator(),
                        );
                      else
                        return ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState.validate())
                              _loginV2Bloc.add(LoginSubmitted(_email, _password));
                          },
                          child: Text(
                            'Continue',
                            style: TextStyle(
                              color: MyAppTheme.secondary,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                          ),
                        );
                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ),
      // floatingActionButton: kDebugMode ? Column(
      //   mainAxisAlignment: MainAxisAlignment.end,
      //   children: [
      //     FloatingActionButton(
      //       heroTag: 'login user 1',
      //       onPressed: () => _submit(
      //         accounts[0]['username'],
      //         accounts[0]['password'],
      //       ),
      //       child: Text('user 1'),
      //     ),
      //     SizedBox(height: 10),
      //     FloatingActionButton(
      //       heroTag: 'login user 2',
      //       onPressed: () => _submit(
      //         accounts[1]['username'],
      //         accounts[1]['password'],
      //       ),
      //       child: Text('user 2'),
      //     )
      //   ],
      // ) : Container(),
    );
  }
}
