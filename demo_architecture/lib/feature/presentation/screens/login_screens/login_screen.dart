import 'package:carousel_slider/carousel_slider.dart';
import 'package:demo_architecture/feature/presentation/pages/home_page.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../injection_container.dart';
import '../../bloc/authentication_blocs/loginv2/authenticate_bloc.dart';
import '../../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../register_screens/register_screen.dart';
import 'login_with_email_screen.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = 'login-screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final List<String> _logoUrls = [
    'assets/images/logos/together-logo.png',
    'assets/images/logos/chat-logo.png',
    'assets/images/logos/located-place-logo.png',
    'assets/images/logos/find-people-logo.png',
  ];

  final List<String> _slogans = [
    'Connect people, Celeb meet each other',
    'Chat and calling with your matcher',
    'Locate and review top place and group',
    'Find new friend with awesome matching feature',
  ];

  AuthenticateBloc _loginV2Bloc;

  @override
  void initState() {
    super.initState();
    _loginV2Bloc = context.read<AuthenticateBloc>();
  }

  void _loginWithGoogle(BuildContext context) async {
    _loginV2Bloc.add(LoginGGEvent());
  }

  void _loginWithEmail(BuildContext context) async {
    Navigator.of(context).pushNamed(LoginWithEmailScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return BlocListener(
      bloc: _loginV2Bloc,
      listener: (context, state) {
        // Login Success
        if (state is AuthenticateSuccess) {
          if (state.authenticateToken.isNewUser != null &&
              state.authenticateToken.isNewUser)
            Navigator.of(context).push(
              PageTransition(
                child: RegisterScreen(
                  isSocialRegister: true,
                ),
                type: PageTransitionType.rightToLeft,
              ),
            );
          else
            Navigator.of(context).pushNamedAndRemoveUntil(
              HomePage.routeName,
              (route) => false,
            );
        }
      },
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color(0xFFF68D0C),
                Color(0xFFeed186),
              ],
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Container(
                width: size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.person_search,
                          color: Colors.white,
                          size: 40,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'TOGETHER',
                          style: GoogleFonts.lora(
                            textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Expanded(
                child: CarouselSlider(
                  items: List.generate(
                    _logoUrls.length,
                    (index) => Container(
                      height: 100,
                      width: size.width,
                      child: Column(
                        children: [
                          Container(
                            height: 120,
                            width: 120,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(_logoUrls[index]),
                                  fit: BoxFit.cover),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: size.width / 3 * 2,
                            child: Text(
                              _slogans[index],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  options: CarouselOptions(
                    autoPlay: true,
                    autoPlayInterval: Duration(seconds: 2),
                    initialPage: 0,
                    aspectRatio: 2.0,
                    viewportFraction: 0.9,
                  ),
                ),
              ),
              BlocBuilder(
                bloc: _loginV2Bloc,
                builder: (context, state) {
                  if (state is AuthenticateInitial) {
                    _loginV2Bloc.add(AutoLoginEvent());
                  }
                  if (state is AuthenticateLoading) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 32),
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(MyAppTheme.secondary),
                      ),
                    );
                  }
                  return Padding(
                    padding: EdgeInsets.all(20),
                    child: Column(
                      children: [
                        Container(
                          width: size.width,
                          height: 50,
                          child: SignInButton(
                            Buttons.Google,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            onPressed: () {
                              _loginWithGoogle(context);
                            },
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          width: size.width,
                          height: 50,
                          child: SignInButton(
                            Buttons.FacebookNew,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            onPressed: () {
                              _loginV2Bloc.add(LoginFBEvent());
                            },
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        TextButton(
                          onPressed: () => _loginWithEmail(context),
                          child: Text(
                            'Login with E-mail',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
