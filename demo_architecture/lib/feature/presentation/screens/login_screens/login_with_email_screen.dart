import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../themes/colors.dart';
import 'login_email_tab.dart';
import 'register_email_tab.dart';

class LoginWithEmailScreen extends StatefulWidget {
  static const routeName = 'login-with-email-screen';

  @override
  _LoginWithEmailScreenState createState() => _LoginWithEmailScreenState();
}

class _LoginWithEmailScreenState extends State<LoginWithEmailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.person_search,
                color: Theme.of(context).primaryColor,
                size: 40,
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                'TOGETHER',
                style: GoogleFonts.lora(
                  textStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          DefaultTabController(
            length: 2,
            initialIndex: 0,
            child: Expanded(
              child: Column(
                children: [
                  TabBar(
                    tabs: [
                      Tab(
                        text: 'Log in',
                      ),
                      Tab(
                        text: 'New Account',
                      ),
                    ],
                    indicatorColor: MyAppTheme.primary,
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        LoginWithEmailTab(),
                        RegisterWithEmailTab(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
