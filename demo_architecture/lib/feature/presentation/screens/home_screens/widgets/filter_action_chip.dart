import 'package:flutter/material.dart';

import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';

class FilterActionChip extends StatelessWidget {
  final IconData iconData;
  final String label;
  final Function onTap;
  final Color bgColor;

  const FilterActionChip({
    Key key,
    this.iconData,
    @required this.label,
    this.onTap,
    this.bgColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      margin: EdgeInsets.only(right: 10),
      child: ActionChip(
        label: Text(
          label,
          style: TextStyle(
            color: MyAppTheme.secondary,
            fontSize: MyTextStyle.paragraph_fontsize,
          ),
        ),
        onPressed: onTap,
        padding: EdgeInsets.only(left: 10, right: 10),
        avatar: Icon(
          iconData,
          color: MyAppTheme.secondary,
        ),
        backgroundColor: bgColor != null ? bgColor : MyAppTheme.black_grey,
      ),
    );
  }
}
