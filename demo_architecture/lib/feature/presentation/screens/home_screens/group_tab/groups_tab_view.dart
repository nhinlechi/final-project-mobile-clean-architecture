import 'dart:developer';

import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/group_scroll_bloc/group_scroll_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/group_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_filter.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';
import '../../../../domain/entities/group/group_entity.dart';
import '../../../../domain/usecase/group/get_list_group.dart';
import '../../../bloc/group_blocs/get_list_group_of_user_bloc/get_list_group_of_user_bloc.dart';
import '../../../bloc/group_blocs/group_bloc.dart';
import '../../../widgets/generals/circular_loading.dart';
import '../../../widgets/generals/search_bar_widget.dart';
import 'group_detail_screen.dart';
import '../search_screen.dart';
import 'widgets/group_item.dart';

class GroupsTabView extends StatefulWidget {
  @override
  _GroupsTabViewState createState() => _GroupsTabViewState();
}

class _GroupsTabViewState extends State<GroupsTabView>
    with AutomaticKeepAliveClientMixin {
  //
  final String heroTagSearchBar = 'search-group-bar';

  // Bloc
  GroupBloc _groupBloc;
  GroupRepositoryProvider groupRepos;

  // Controller
  final _textSearchController = TextEditingController();
  final _scrollController = ScrollController();

  // Fields
  AuthenticateToken auth;
  List<GroupEntity> _listGroup = [];
  int _currentPage = 1;
  bool isAtMaxListGroup = false;

  // Filter fields
  bool isMine = false;
  bool isPrivate = false;

  Future<void> _openSearchScreen() async {
    final text = await Navigator.of(context).pushNamed(
      SearchScreen.routeName,
      arguments: ArgsSearchScreen(
        searchText: _textSearchController.text,
        heroTag: heroTagSearchBar,
      ),
    );
    setState(() {
      this._textSearchController.text = text;
    });
    // Http Request get place
    _groupBloc.add(
      GetListGroupEvent(
        getListGroupParams: GetListGroupParams(
          page: 1,
          rowsperpage: 26,
          textSearch: this._textSearchController.text.isNotEmpty
              ? this._textSearchController.text
              : null,
        ),
      ),
    );
    _currentPage = 1;
  }

  void _goToGroup(BuildContext context, String imageUrl, GroupEntity group) {
    Navigator.of(context).pushNamed(
      GroupDetailScreen.routeName,
      arguments: ArgsGroupDetailScreen(
        imageUrl: imageUrl,
        heroTag: 'groupItem ${group.id}',
        group: group,
      ),
    );
  }

  void _scrollOnTop() {
    _scrollController.animateTo(
      _scrollController.position.minScrollExtent,
      duration: Duration(
        milliseconds: 500,
      ),
      curve: Curves.bounceIn,
    );
  }

  void _scrollListener() {
    //
    if (_scrollController.position.extentBefore > 700) {
      context.read<GroupScrollBloc>().add(GroupScrollDown());
    } else {
      context.read<GroupScrollBloc>().add(GroupScrollToTop());
    }
    // Break condition
    if (_groupBloc.state is GroupLoading || this.isAtMaxListGroup) return;
    //
    if (_scrollController.position.extentAfter == 0.0) {
      _groupBloc.add(
        GetListGroupEvent(
          getListGroupParams: GetListGroupParams(
            page: ++_currentPage,
            rowsperpage: 16,
          ),
        ),
      );
    }
  }

  void _onFilter(GroupFilterType filterType) {
    switch (filterType) {
      case GroupFilterType.mine:
        {
          setState(() {
            this.isMine = !isMine;
          });
          break;
        }
      case GroupFilterType.private:
        {
          setState(() {
            this.isPrivate = !isPrivate;
          });
          break;
        }
    }
  }

  @override
  void initState() {
    super.initState();
    _groupBloc = sl<GroupBloc>();
    //
    auth = context.read<UserRepositoryProvider>().auth;
    groupRepos = context.read<GroupRepositoryProvider>();
    //
    _scrollController.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate([
                SearchBarWidget(
                  heroTag: heroTagSearchBar,
                  onTap: _openSearchScreen,
                  controller: _textSearchController,
                  readOnly: true,
                ),
                GroupFilter(
                  filterCallback: _onFilter,
                ),
                SizedBox(
                  height: 16,
                ),
              ]),
            ),
            BlocBuilder<GroupBloc, GroupState>(
              bloc: _groupBloc,
              builder: (context, state) {
                if (state is GroupInitial) {
                  _groupBloc.add(
                    GetListGroupEvent(
                      getListGroupParams: GetListGroupParams(
                        page: 1,
                        rowsperpage: 16,
                      ),
                    ),
                  );
                }
                if (state is GroupLoadedList) {
                  // Initial
                  _listGroup = [...groupRepos.groups];
                  // Filter text search
                  _listGroup = _listGroup
                      .where(
                        (group) =>
                            group.name.contains(_textSearchController.text),
                      )
                      .toList();
                  // Filter is mine
                  if (isMine) {
                    _listGroup = _listGroup
                        .where(
                          (group) => group.host == auth.userId,
                        )
                        .toList();
                  }
                  // Filter is private
                  if (isPrivate) {
                    _listGroup =
                        _listGroup.where((group) => group.isprivate).toList();
                  }
                }
                return SliverPadding(
                  padding: const EdgeInsets.only(left: 8),
                  sliver: SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 0.9,
                    ),
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return GroupItem(
                          heroTag: 'groupItem ${_listGroup[index].id}',
                          imageUrl: _listGroup[index].avatar,
                          nameGroup: _listGroup[index].name,
                          isPrivate: _listGroup[index].isprivate,
                          onTap: () => _goToGroup(
                            context,
                            _listGroup[index].avatar,
                            _listGroup[index],
                          ),
                        );
                      },
                      childCount: _listGroup.length,
                    ),
                  ),
                );
              },
            ),
            BlocBuilder<GroupBloc, GroupState>(
              bloc: _groupBloc,
              builder: (context, state) {
                if (state is GroupLoadedList &&
                    state.listGroupEntity.groups.length == 0) {
                  this.isAtMaxListGroup = true;
                  return SliverPadding(padding: const EdgeInsets.all(16));
                }
                return SliverPadding(
                  padding: const EdgeInsets.only(bottom: 40),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate([
                      CircularLoading(),
                    ]),
                  ),
                );
              },
            ),
          ],
        ),
        BlocBuilder<GroupScrollBloc, GroupScrollState>(
          builder: (context, state) {
            if (state is GroupScrollDownState)
              return Positioned(
                bottom: 10,
                right: 10,
                child: ElevatedButton(
                  onPressed: _scrollOnTop,
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    primary: MyAppTheme.primary,
                  ),
                  child: Icon(
                    Icons.arrow_upward,
                    color: MyAppTheme.secondary,
                  ),
                ),
              );
            return Container();
          },
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
