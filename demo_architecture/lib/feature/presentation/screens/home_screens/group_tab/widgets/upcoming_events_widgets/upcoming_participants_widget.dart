import 'dart:math';

import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';

class ParticipantAvatarsWidget extends StatelessWidget {
  final List<ParticipantModel> participants;

  const ParticipantAvatarsWidget({Key key, @required this.participants})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final maxAvatarLength = 5;
    final avatarRadius = 14.0;
    final avatarLength = min(maxAvatarLength, participants.length);
    //
    return participants.length > 0
        ? Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: avatarRadius * (avatarLength + 1),
                height: avatarRadius * 2,
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    ...List.generate(
                      avatarLength,
                      (index) => Positioned(
                        left: avatarRadius * index,
                        child: CircleAvatar(
                          backgroundImage:
                              NetworkImage(participants[index].avatar),
                          radius: avatarRadius,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 4),
              if (participants.length > maxAvatarLength)
                Text('+${participants.length - maxAvatarLength} Others'),
            ],
          )
        : getNotHaveParticipantYet();
  }

  getNotHaveParticipantYet() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          Icons.people,
          color: MyAppTheme.black_grey,
        ),
        const SizedBox(width: 8),
        Text('No participant yet'),
      ],
    );
  }
}
