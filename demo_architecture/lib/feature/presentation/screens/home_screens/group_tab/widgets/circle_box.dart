import 'package:flutter/material.dart';

class CircleBox extends StatelessWidget {
  final Color color;
  final double elevation;
  final Widget child;

  const CircleBox({Key key, this.color, this.elevation = 2, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 28,
      width: 28,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: this.color,
        boxShadow: [
          if(elevation > 0) BoxShadow(
            blurRadius: elevation,
            offset: Offset(0, elevation/2),
          ),
        ],
      ),
      child: this.child,
    );
  }
}
