import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/presentation/data_prototype/girl.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class UpcomingEventsHeaderWidget extends StatelessWidget {
  final GroupEntity group;

  const UpcomingEventsHeaderWidget({Key key, @required this.group}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                'Upcoming events of ${group.name}',
                style: TextStyle(
                  fontSize: MyTextStyle.title_3_fontsize,
                  color: MyAppTheme.black_grey,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            const SizedBox(width: 32),
            CircleAvatar(
              radius: 30,
              backgroundImage: group.avatar != null
                  ? NetworkImage(group.avatar)
                  : AssetImage(girls[1]['img']),
            ),
          ],
        ),
      ],
    );
  }
}
