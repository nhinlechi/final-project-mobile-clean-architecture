import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/title_text_widget.dart';
import 'package:flutter/material.dart';

class CreateGroupEventDescriptionWidget extends StatelessWidget {
  final TextEditingController titleController;
  final TextEditingController descController;

  const CreateGroupEventDescriptionWidget({
    Key key,
    @required this.titleController,
    @required this.descController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TitleTextWidget(text: 'Event Title'),
        const SizedBox(height: 16),
        TextField(
          controller: titleController,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15),
            labelText: 'title',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            prefixIcon: Icon(Icons.title),
          ),
          maxLines: 2,
        ),
        const SizedBox(height: 32),
        TitleTextWidget(text: 'Event Description'),
        const SizedBox(height: 16),
        TextField(
          controller: descController,
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(15),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          maxLines: 10,
        ),
      ],
    );
  }
}
