import 'package:demo_architecture/feature/presentation/data_prototype/girl.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class CreateGroupEventHeaderWidget extends StatelessWidget {
  final String avatar;

  const CreateGroupEventHeaderWidget({Key key, @required this.avatar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Row(
      children: [
        Expanded(
          child: Text(
            'Create New Group Event',
            style: TextStyle(
              fontSize: MyTextStyle.title_3_fontsize,
              color: MyAppTheme.black_grey,
            ),
          ),
        ),
        SizedBox(width: size.width / 4),
        CircleAvatar(
          radius: 30,
          backgroundImage:
              avatar != null ? NetworkImage(avatar) : AssetImage(girls[1]['img']),
        ),
      ],
    );
  }
}
