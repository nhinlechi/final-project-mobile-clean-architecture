import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class TitleTextWidget extends StatelessWidget {
  final String text;

  const TitleTextWidget({Key key,@required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 4),
      child: Text(
        text,
        style: TextStyle(
          color: MyAppTheme.black_grey,
          fontSize: MyTextStyle.title_5_fontsize,
        ),
      ),
    );
  }
}
