import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/create_group_event_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/upcoming_events_screen.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GroupUpComingEventWidget extends StatelessWidget {
  final GetListGroupEventBloc getListGroupEventBloc;
  final GroupEntity groupEntity;
  final bool isHost;

  const GroupUpComingEventWidget({
    Key key,
    @required this.groupEntity,
    @required this.getListGroupEventBloc,
    @required this.isHost,
  }) : super(key: key);

  void _openUpcomingEventsScreen(BuildContext context, GroupEventModel event) {
    Navigator.of(context).pushNamed(
      UpcomingEventsScreen.routeName,
      arguments: ArgsUpcomingEventsScreen(
        getListGroupEventBloc: getListGroupEventBloc,
        group: this.groupEntity,
      ),
    );
  }

  void _openCreateGroupEventsScreen(BuildContext context) {
    Navigator.of(context).pushNamed(
      CreateGroupEventScreen.routeName,
      arguments: CreateGroupEventScreenArgs(
        groupEntity: this.groupEntity,
        getListGroupEventBloc: getListGroupEventBloc,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 32),
        Text(
          'Upcoming Events',
          style: TextStyle(
            fontSize: MyTextStyle.title_4_fontsize,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 8),
        BlocBuilder<GetListGroupEventBloc, GetListGroupEventState>(
          bloc: getListGroupEventBloc,
          builder: (context, state) {
            if(state is GetListGroupEventInitial) {
              if (state is GetListGroupEventInitial) {
                getListGroupEventBloc.add(
                  FetchListGroupEventEvent(
                    params: GetListGroupEventParams(
                      page: 1,
                      rowsperpage: 100,
                      groupId: groupEntity.id,
                    ),
                  ),
                );
              }
            }
            if (state is GetListGroupEventSuccessState) {
              if (state.listGroupEventModel.isHaveUpcomingEvent) {
                return getHavingUpcomingGroupEventWidget(
                  context,
                  state.listGroupEventModel.upcomingEvent,
                );
              } else {
                return getNoUpcomingGroupEventWidget(context);
              }
            }
            return CircularLoading();
          },
        )
      ],
    );
  }

  Widget getHavingUpcomingGroupEventWidget(
    BuildContext context,
    GroupEventModel event,
  ) {
    final eventTime = DateTime.fromMillisecondsSinceEpoch(event.eventTime);
    return InkWell(
      onTap: () => _openUpcomingEventsScreen(context, event),
      child: ListTile(
        contentPadding: const EdgeInsets.all(0),
        leading: CircleAvatar(
          backgroundColor: MyAppTheme.primary,
          child: Icon(
            Icons.event,
            color: MyAppTheme.secondary,
          ),
        ),
        title: Text(
          event.title,
          style: TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        trailing: Chip(
          backgroundColor: MyAppTheme.black_grey,
          label: Text(
            TimeOfDay.fromDateTime(eventTime).format(context),
            style: TextStyle(color: MyAppTheme.secondary),
          ),
        ),
      ),
    );
  }

  getNoUpcomingGroupEventWidget(BuildContext context) => ListTile(
        title: Text('No Upcoming Events Yet.'),
        subtitle: isHost
            ? Text(
                'Create One?',
                style: TextStyle(color: MyAppTheme.black),
              )
            : null,
        contentPadding: const EdgeInsets.all(0),
        trailing: isHost
            ? SizedBox(
                width: 40,
                height: 40,
                child: ElevatedButton(
                  child: Icon(
                    Icons.add,
                    color: MyAppTheme.secondary,
                  ),
                  onPressed: () => _openCreateGroupEventsScreen(context),
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    padding: const EdgeInsets.all(0),
                  ),
                ),
              )
            : SizedBox(),
      );
}
