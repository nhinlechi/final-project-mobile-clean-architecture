import 'package:demo_architecture/feature/presentation/screens/home_screens/widgets/filter_action_chip.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';

enum GroupFilterType {
  mine,
  private,
}

class GroupFilter extends StatefulWidget {
  final Function(GroupFilterType type) filterCallback;

  const GroupFilter({Key key, this.filterCallback}) : super(key: key);

  @override
  _GroupFilterState createState() => _GroupFilterState();
}

class _GroupFilterState extends State<GroupFilter> with AutomaticKeepAliveClientMixin {
  bool isMine = false;
  bool isPrivate = false;

  void _onFilter(GroupFilterType type) {
    switch (type) {
      case GroupFilterType.mine:
        {
          setState(() {
            isMine = !isMine;
          });
        }
        break;
      case GroupFilterType.private:
        {
          setState(() {
            isPrivate = !isPrivate;
          });
        }
        break;
    }
    // Callback for parent
    widget.filterCallback?.call(type);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              FilterActionChip(
                label: 'Mine',
                bgColor: isMine ? MyAppTheme.primary : MyAppTheme.black_grey,
                iconData: Icons.person,
                onTap: () => _onFilter(GroupFilterType.mine),
              ),
              FilterActionChip(
                label: 'Private',
                bgColor: isPrivate ? MyAppTheme.primary : MyAppTheme.black_grey,
                iconData: Icons.shield,
                onTap: () => _onFilter(GroupFilterType.private),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
