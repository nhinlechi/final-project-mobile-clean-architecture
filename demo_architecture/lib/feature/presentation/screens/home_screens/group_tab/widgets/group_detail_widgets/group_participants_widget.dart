import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_group_by_id_bloc/get_group_by_id_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/user_blocs/get_user_by_id_bloc/get_user_by_id_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/group_screens/view_profile_bottom_sheet.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/upcoming_events_widgets/upcoming_participants_widget.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class GroupParticipantsWidget extends StatelessWidget {
  final GetUserByIdBloc getUserByIdBloc;
  final GetGroupByIdBloc getGroupByIdBloc;

  const GroupParticipantsWidget({
    Key key,
    @required this.getUserByIdBloc,
    @required this.getGroupByIdBloc,
  }) : super(key: key);

  void _viewHostInfo(BuildContext context, UserInform host) {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) {
        return ViewProfileBottomSheet(
          host: host,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        BlocBuilder<GetGroupByIdBloc, GetGroupByIdState>(
          bloc: getGroupByIdBloc,
          builder: (context, state) {
            if (state is GetGroupByIdLoaded) {
              return ParticipantAvatarsWidget(
                participants: state.groupEntity.participant,
              );
            }
            return CircularLoading();
          },
        ),
        Row(
          children: [
            const Text('Host: '),
            const SizedBox(width: 4),
            BlocBuilder<GetUserByIdBloc, GetUserByIdState>(
              bloc: getUserByIdBloc,
              builder: (context, state) {
                if (state is GetUserByIdLoaded) {
                  return InkWell(
                    onTap: () => _viewHostInfo(context, state.userInform),
                    child: CircleAvatar(
                      radius: 16,
                      backgroundImage: NetworkImage(state.userInform.avt[0]),
                    ),
                  );
                }
                return Container();
              },
            ),
          ],
        ),
      ],
    );
  }
}
