import 'dart:developer';

import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/places/get_places_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/place_blocs/get_places_bloc/get_places_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/place_picker_toggle.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/search_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/widgets/place_filter.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/search_bar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../../injection_container.dart';

class PlacePickerPopup extends StatefulWidget {
  const PlacePickerPopup({Key key}) : super(key: key);

  @override
  _PlacePickerPopupState createState() => _PlacePickerPopupState();
}

class _PlacePickerPopupState extends State<PlacePickerPopup> {
  GetPlacesBloc _getPlacesBloc;

  final heroTagSearchBar = 'search-place-bar';

  // filter
  String categoryType;
  String sortByType;
  String sortType = '-1';

  // search text
  final _textController = TextEditingController();
  GetPlacesParams _params = GetPlacesParams(rowsPerPage: 16, page: 1);

  // Place Picked Id
  PlaceEntity _placePicked;

  Future<void> _openSearchScreen() async {
    final text = await Navigator.of(context).pushNamed(
      SearchScreen.routeName,
      arguments: ArgsSearchScreen(
        searchText: _textController.text,
        heroTag: heroTagSearchBar,
      ),
    );
    setState(() {
      this._textController.text = text;
    });
    // Http Request get place
    _getPlaces();
  }

  void _filterCallback(String filter, FilterType type) {
    setState(() {
      switch (type) {
        case FilterType.categories:
          categoryType = filter;
          break;
        case FilterType.sortBy:
          sortByType = filter;
          break;
        case FilterType.sort:
          sortType = filter;
          break;
      }
    });
    _getPlaces();
  }

  void _getPlaces() {
    _params = GetPlacesParams(
      searchText: _textController.text,
      page: 1,
      rowsPerPage: 16,
      type: categoryType,
      sortBy: sortByType,
      sort: int.tryParse(sortType),
    );

    _getPlacesBloc.add(GetPlaces(params: _params));
  }

  void _pickPlaceCallback(PlaceEntity place) {
    _placePicked = place;
  }

  void _onFinishPickPlace() {
    if (_placePicked != null) {
      Navigator.of(context).pop(_placePicked);
    }
  }

  @override
  void initState() {
    super.initState();
    // Init get place bloc
    _getPlacesBloc = sl<GetPlacesBloc>();
    _getPlacesBloc.add(GetPlaces(
        params: GetPlacesParams(
      page: 1,
      rowsPerPage: 5,
    )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        margin: const EdgeInsets.only(top: 32),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only( left: 16, right: 16),
                child: Text(
                  'Pick Place For Group Event',
                  style: MyTextStyle.ts_register_title,
                ),
              ),
              SearchBarWidget(
                heroTag: 'pick-place-search',
                onTap: _openSearchScreen,
                readOnly: true,
                controller: _textController,
              ),
              ClipRRect(
                child: PlaceFilter(
                  filterCallback: _filterCallback,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
              SizedBox(
                height: 32,
              ),
              BlocBuilder<GetPlacesBloc, GetPlacesState>(
                bloc: _getPlacesBloc,
                builder: (context, state) {
                  if (state is GetPlacesSuccess) {
                    return PlacePickerToggle(
                      places: state.placesEntity.places,
                      pickPlaceCallback: _pickPlaceCallback,
                    );
                  }
                  return CircularLoading();
                },
              ),
              SizedBox(
                height: 50,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: MyAppTheme.primary,
        child: Icon(
          Icons.check,
          color: MyAppTheme.secondary,
        ),
        onPressed: _onFinishPickPlace,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
