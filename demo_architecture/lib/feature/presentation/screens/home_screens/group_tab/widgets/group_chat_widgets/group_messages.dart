import 'package:demo_architecture/core/socket/socket_event.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_group_event_messages_bloc/group_event_messages_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/message_bubble.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../../../../../injection_container.dart';

class GroupEventMessages extends StatefulWidget {
  final Socket socket;
  final GroupEntity group;
  final String eventId;

  const GroupEventMessages({
    Key key,
    @required this.group,
    @required this.eventId,
    @required this.socket,
  }) : super(key: key);

  @override
  GroupMessagesState createState() => GroupMessagesState();
}

class GroupMessagesState extends State<GroupEventMessages> {
  GroupEventMessagesBloc _groupEventMessagesBloc;
  int lastMessageIndex = 15;
  UserInform _currentUserInfo;
  List<MessageModel> _messageModels = [];

  DateTime _newestMessageTimer = DateTime.now().subtract(Duration(seconds: 30));
  int _expiredTimeInSeconds = 30;

  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    //
    _groupEventMessagesBloc = sl<GroupEventMessagesBloc>();
    _groupEventMessagesBloc.add(
      GetGroupEventMessagesEvent(
        params: GetGroupEventMessagesParams(
          groupId: widget.group.id,
          eventId: widget.eventId,
        ),
      ),
    );
    //
    _currentUserInfo = context.read<UserRepositoryProvider>().userInform;
    //
    _joinRoomAndListen();
    // On Scroll Listener
    _scrollController.addListener(_onScrollToLoadOldMessage);
  }

  void _joinRoomAndListen() {
    final socket = widget.socket;
    socket?.emitWithAck(
      SocketEvent.clientJoinChat,
      {
        'roomId': widget.eventId,
      },
      ack: (result) {
        if (result != null) {
          print(result);
        } else {
          print("Join event ${widget.eventId} succeed");
        }
      },
    );
    socket?.on(SocketEvent.serverSentChat, (data) {
      // Update UI
      final message = MessageModel.fromJson(data);
      // Not have message yet
      setState(() {
        _messageModels.insert(0, message);
      });
    });
  }

  void addNewMessageToList(String message, String type) {
    // No Message yet
    if (_messageModels.isEmpty) {
      _addNormalMessage(message, type);
      return;
    }
    // Get message and timer
    final latestMessage = _messageModels.first;
    final now = DateTime.now();
    // Compare
    if (latestMessage.userId == _currentUserInfo.userId &&
        now.difference(_newestMessageTimer).inSeconds < _expiredTimeInSeconds) {
      setState(() {
        _messageModels.first.messages.add(
          MessageData(message: message, type: type),
        );
      });
    } else {
      _addNormalMessage(message, type);
    }
    // Reset timer
    _newestMessageTimer = now;
  }

  void _addNormalMessage(String message, String type) {
    setState(() {
      _messageModels.insert(
        0,
        MessageModel(
          messages: [MessageData(message: message, type: type)],
          userId: _currentUserInfo.userId,
          userName: _currentUserInfo.firstname,
          avatar: _currentUserInfo.avt[0],
        ),
      );
    });
  }

  void _onScrollToLoadOldMessage() {
    if (_scrollController.position.extentAfter == 0.0) {
      if (_groupEventMessagesBloc.state is GetGroupEventMessagesLoading ||
          _messageModels.last.index == 0) return;

      _groupEventMessagesBloc.add(
        GetGroupEventMessagesEvent(
          params: GetGroupEventMessagesParams(
            groupId: widget.group.id,
            eventId: widget.eventId,
            lastMessageIndex: _messageModels.last.index,
          ),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GroupEventMessagesBloc, GroupEventMessagesState>(
      bloc: _groupEventMessagesBloc,
      builder: (context, state) {
        if (state is GetListGroupEventInitial) {
          return getChatLoadingEffect();
        }
        if (state is GetGroupEventMessagesSuccess) {
          if (_messageModels.isEmpty ||
              (state.messages.isNotEmpty &&
                  _messageModels.last.index != null &&
                  _messageModels.last.index != state.messages.last.index)) {
            _messageModels.addAll(state.messages);
          }
        }
        return ListView.builder(
          reverse: true,
          controller: _scrollController,
          itemCount: _messageModels.length + 1,
          itemBuilder: (context, index) {
            // Get Loading Effect
            if (_messageModels.length == 0) return getChatLoadingEffect();
            if (index >= _messageModels.length &&
                _messageModels[index - 1].index != 0) {
              return getChatLoadingEffect();
            }
            // Normal message
            if (index < _messageModels.length) {
              final mess = _messageModels[index];
              return getAdjacentMessageBubble(mess, index);
            }

            // No more
            return Container();
          },
        );
      },
    );
  }

  Widget getAdjacentMessageBubble(MessageModel mess, int messModelIndex) =>
      Column(
        children: List.generate(mess.messages.length, (index) {
          // Message Position
          MessageBubblePosition messagePosition;
          if (index == 0)
            messagePosition = MessageBubblePosition.TOP;
          else if (index == mess.messages.length - 1)
            messagePosition = MessageBubblePosition.BOTTOM;
          else
            messagePosition = MessageBubblePosition.MIDDLE;
          if (mess.messages.length == 1)
            messagePosition = MessageBubblePosition.SINGLE;

          // Message Text
          if (mess.messages[index].type == MessageData.TEXT)
            return MessageBubbleWidget(
              message: mess.messages[index].message,
              isMine: false,
              //key: ValueKey('$mess $messModelIndex'),
              username: mess.userName,
              imageUrl: index == mess.messages.length - 1 ? mess.avatar : null,
              position: messagePosition,
            );

          // Default:
          return Container();
        }),
      );

  Widget getChatLoadingEffect() => Center(
        child: SpinKitPulse(
          size: 14,
          color: MyAppTheme.black_grey,
        ),
      );
}
