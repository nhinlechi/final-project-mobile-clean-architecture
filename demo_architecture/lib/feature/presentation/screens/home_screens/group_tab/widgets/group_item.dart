import 'package:flutter/material.dart';

import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';
import 'circle_box.dart';

class GroupItem extends StatelessWidget {
  final String imageUrl;
  final String nameGroup;
  final Function() onTap;
  final String heroTag;
  final bool isPrivate;

  const GroupItem({
    Key key,
    @required this.imageUrl,
    @required this.nameGroup,
    @required this.onTap,
    @required this.heroTag,
    this.isPrivate = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final d = size.width / 2 - 26;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          children: [
            InkWell(
              onTap: () => this.onTap?.call(),
              child: Hero(
                tag: this.heroTag,
                child: Container(
                  height: d,
                  width: d,
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(0, 2),
                        color: Colors.grey,
                        blurRadius: 2,
                      )
                    ],
                    image: DecorationImage(
                      image: NetworkImage(
                        this.imageUrl,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              right: 5,
              child: Chip(
                label: Text(
                  this.isPrivate ? 'Private' :  'Public',
                  style: TextStyle(
                    color: this.isPrivate ? MyAppTheme.black : MyAppTheme.secondary,
                    fontSize: 12,
                  ),
                ),
                padding: const EdgeInsets.all(0),
                backgroundColor: this.isPrivate ? MyAppTheme.grey : Colors.green,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 8,
        ),
        Text(
          nameGroup,
          maxLines: 1,
          style: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
        ),
      ],
    );
  }
}
