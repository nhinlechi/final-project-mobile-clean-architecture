import 'dart:developer';

import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_place_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/place_picker_popup.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/title_text_widget.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circle_button.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class PlacePickerWidget extends StatefulWidget {
  final Function(String placeId) pickPlaceCallback;
  final PlaceEntity place;

  const PlacePickerWidget({
    Key key,
    @required this.pickPlaceCallback,
    this.place,
  }) : super(key: key);

  @override
  _PlacePickerWidgetState createState() => _PlacePickerWidgetState();
}

class _PlacePickerWidgetState extends State<PlacePickerWidget> {
  PlaceEntity place;

  void _showPlacePickerPopup(BuildContext context) async {
    final PlaceEntity place = await showCupertinoModalBottomSheet(
      context: context,
      builder: (context) => PlacePickerPopup(),
    );
    if (place != null) {
      widget.pickPlaceCallback?.call(place.id);
      setState(() {
        this.place = place;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    //
    this.place = widget.place;
  }

  @override
  Widget build(BuildContext context) {
    return place == null
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TitleTextWidget(text: 'Event Time'),
              const SizedBox(height: 16),
              Row(
                children: [
                  getPlaceContainer(context),
                  const SizedBox(width: 16),
                  Icon(
                    Icons.arrow_back,
                    color: MyAppTheme.black_grey,
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Text(
                      'Click to pick event place',
                      style: TextStyle(
                        color: MyAppTheme.black_grey,
                        fontSize: MyTextStyle.title_5_fontsize,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DatingEventPlaceWidget(
                label: 'Event Place',
                place: place,
              ),
              const SizedBox(height: 16),
              ElevatedButton(
                onPressed: () => _showPlacePickerPopup(context),
                child: Text(
                  'Pick Place Again',
                  style: TextStyle(
                    color: MyAppTheme.secondary,
                  ),
                ),
              ),
            ],
          );
  }

  getPlaceContainer(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final imageWidth = size.width / 2.4;
    final height = imageWidth * 1.15;
    return InkWell(
      onTap: () => _showPlacePickerPopup(context),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: height,
            width: imageWidth,
            decoration: BoxDecoration(
              color: MyAppTheme.grey,
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          Positioned(
            right: -10,
            top: -10,
            child: CircleButton(
              backgroundColor: MyAppTheme.primary,
              icon: Icons.location_on_rounded,
              iconColor: MyAppTheme.secondary,
              onTap: () => _showPlacePickerPopup(context),
            ),
          ),
        ],
      ),
    );
  }
}
