import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/title_text_widget.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class DateTimePickerWidget extends StatelessWidget {
  final TextEditingController dateController;
  final TextEditingController timeController;
  final Function(DateTime date) pickDateCallback;
  final Function(TimeOfDay time) pickTimeCallback;

  DateTimePickerWidget({
    Key key,
    @required this.pickDateCallback,
    @required this.pickTimeCallback,
    @required this.dateController,
    @required this.timeController,
  }) : super(key: key);

  void _pickDate(BuildContext context) async {
    final date = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        Duration(
          days: 30,
        ),
      ),
    );
    if (date != null) {
      pickDateCallback?.call(date);
      dateController.text = date.toString().split(' ')[0];
    }
  }

  void _pickTime(BuildContext context) async {
    final time = await showTimePicker(
      context: context,
      initialTime: TimeOfDay.fromDateTime(
        DateTime.now().add(
          Duration(hours: 1),
        ),
      ),
    );
    if (time != null) {
      pickTimeCallback?.call(time);
      timeController.text = '${time.format(context)}';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TitleTextWidget(text: 'Event Time'),
        const SizedBox(height: 16),
        SizedBox(
          height: 40,
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  controller: dateController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 0,
                      horizontal: 16,
                    ),
                    prefixIcon: Icon(Icons.date_range),
                  ),
                  readOnly: true,
                  onTap: () => _pickDate(context),
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              ElevatedButton(
                onPressed: () => _pickDate(context),
                child: Text(
                  'Pick Date',
                  style: TextStyle(
                    color: MyAppTheme.secondary,
                    fontSize: MyTextStyle.title_5_fontsize,
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 16),
        SizedBox(
          height: 40,
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  controller: timeController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    contentPadding: EdgeInsets.symmetric(
                      vertical: 0,
                      horizontal: 16,
                    ),
                    prefixIcon: Icon(Icons.access_time),
                  ),
                  readOnly: true,
                  onTap: () => _pickTime(context),
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              ElevatedButton(
                onPressed: () => _pickTime(context),
                child: Text(
                  'Pick Time',
                  style: TextStyle(
                    color: MyAppTheme.secondary,
                    fontSize: MyTextStyle.title_5_fontsize,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
