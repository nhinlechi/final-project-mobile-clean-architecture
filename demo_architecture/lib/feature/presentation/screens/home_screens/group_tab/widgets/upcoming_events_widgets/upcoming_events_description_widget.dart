import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class UpcomingEventsDescriptionWidget extends StatelessWidget {
  final String title;
  final String description;

  const UpcomingEventsDescriptionWidget({
    Key key,
    @required this.title,
    @required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: size.width * 3 / 4,
            child: Text(
              title,
              style: TextStyle(
                fontSize: MyTextStyle.title_3_fontsize,
                color: MyAppTheme.black_grey,
              ),
            ),
          ),
          const SizedBox(height: 16),
          Text(
            description,
            style: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
          ),
        ],
      ),
    );
  }
}
