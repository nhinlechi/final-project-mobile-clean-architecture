import 'package:flutter/material.dart';

import '../../../../../themes/colors.dart';
import '../circle_box.dart';

class HistoryItem extends StatelessWidget {
  final String text;
  final Color circleColor;

  const HistoryItem({Key key, @required this.text, this.circleColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleBox(
          color: circleColor ?? MyAppTheme.grey_light,
          elevation: 0,
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          text,
          style: TextStyle(fontSize: 12),
        ),
      ],
    );
  }
}
