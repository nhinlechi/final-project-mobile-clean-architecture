import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

import 'history_item.dart';

class HistoryMeetingWidget extends StatelessWidget {
  const HistoryMeetingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        Text(
          'Histories Meeting',
          style: TextStyle(
            fontSize: MyTextStyle.title_4_fontsize,
            fontWeight: FontWeight.w500,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Stack(
          children: [
            Positioned(
              top: 14,
              child: Container(
                width: size.width,
                height: 1,
                decoration: BoxDecoration(
                  color: MyAppTheme.grey,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                HistoryItem(
                  text: 'Start',
                  circleColor: MyAppTheme.primary,
                ),
                HistoryItem(text: '10/12/2019'),
                HistoryItem(text: '1/1/2020'),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
