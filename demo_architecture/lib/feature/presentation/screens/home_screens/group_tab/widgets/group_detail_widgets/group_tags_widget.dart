import 'dart:math';

import 'package:demo_architecture/feature/data/models/group/tag_model.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';

class GroupTagsWidget extends StatelessWidget {
  final List<TagModel> groupTagNames;

  const GroupTagsWidget({
    Key key,
    @required this.groupTagNames,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 8,
      runSpacing: 8,
      children: List.generate(
        min(groupTagNames.length, 2),
        (index) => SizedBox(
          height: 32,
          child: Chip(
            label: Text(groupTagNames[index].tagName),
            labelStyle: TextStyle(color: MyAppTheme.secondary),
            backgroundColor: MyAppTheme.black_grey,
          ),
        ),
      ),
    );
  }
}
