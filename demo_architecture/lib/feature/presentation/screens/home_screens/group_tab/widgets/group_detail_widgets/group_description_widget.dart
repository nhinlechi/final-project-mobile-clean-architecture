import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/material.dart';

class GroupDescriptionWidget extends StatelessWidget {
  final String description;

  const GroupDescriptionWidget({
    Key key,
    @required this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Descriptions',
          style: TextStyle(
            fontSize: MyTextStyle.title_4_fontsize,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          description,
          style: TextStyle(
            color: Color(0XFF626262),
            fontSize: MyTextStyle.paragraph_fontsize,
          ),
        ),
      ],
    );
  }
}
