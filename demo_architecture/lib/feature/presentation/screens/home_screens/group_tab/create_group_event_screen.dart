import 'package:demo_architecture/core/utils/enums.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/create_new_group_event_bloc/create_new_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/update_group_event_bloc/update_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/create_group_event_description_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/create_group_event_header_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/date_time_picker_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/create_group_events/place_picker_widget.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/my_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';

class CreateGroupEventScreenArgs {
  final EditType type;
  final GroupEntity groupEntity;
  final GroupEventModel oldEvent;
  final GetListGroupEventBloc getListGroupEventBloc;

  CreateGroupEventScreenArgs({
    this.type = EditType.AddNew,
    @required this.groupEntity,
    this.oldEvent,
    @required this.getListGroupEventBloc,
  });
}

class CreateGroupEventScreen extends StatefulWidget {
  static const String routeName = 'create-group-event-screen';

  final CreateGroupEventScreenArgs args;

  const CreateGroupEventScreen({Key key, @required this.args})
      : super(key: key);

  @override
  _CreateGroupEventScreenState createState() => _CreateGroupEventScreenState();
}

class _CreateGroupEventScreenState extends State<CreateGroupEventScreen> {
  CreateNewGroupEventBloc _createNewGroupEventBloc;
  UpdateGroupEventBloc _updateGroupEventBloc;
  DateTime date;
  TimeOfDay time;
  String placeId;

  int get eventTime => date.millisecondsSinceEpoch +
      Duration(hours: time.hour, minutes: time.minute).inMilliseconds;

  final _dateController = TextEditingController();
  final _timeController = TextEditingController();
  final _titleController = TextEditingController();
  final _descController = TextEditingController();

  void _pickDateHandler(DateTime date) {
    this.date = date;
  }

  void _pickTimeHandler(TimeOfDay time) {
    this.time = time;
  }

  void _pickPlaceHandler(String placeId) {
    this.placeId = placeId;
  }

  void _submitCreateGroupEvent() {
    _createNewGroupEventBloc.add(
      CreateGroupEventEvent(
        params: CreateNewGroupEventParams(
          groupId: widget.args.groupEntity.id,
          title: _titleController.text,
          description: _descController.text,
          date: eventTime,
          placeId: placeId,
        ),
      ),
    );
  }

  void _submitUpdateGroupEvent() {
    // Get dating time
    _updateGroupEventBloc.add(
      SubmitUpdateGroupEvent(
        params: UpdateGroupEventParams(
          groupId: widget.args.groupEntity.id,
          eventId: widget.args.oldEvent.id,
          title: _titleController.text,
          description: _descController.text,
          date: eventTime,
          placeId: placeId,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    // Initial bloc
    _createNewGroupEventBloc = sl<CreateNewGroupEventBloc>();
    _updateGroupEventBloc = sl<UpdateGroupEventBloc>();
    // Init data if editType is update
    if (widget.args.type == EditType.Update) {
      final oldEvent = widget.args.oldEvent;

      // Event Time
      var eventDate = DateTime.fromMillisecondsSinceEpoch(oldEvent.eventTime);
      final eventTime = TimeOfDay.fromDateTime(eventDate);
      eventDate = eventDate.subtract(
        Duration(
          hours: eventTime.hour,
          minutes: eventTime.minute,
        ),
      );
      Future.delayed(Duration(), () {
        _timeController.text = eventTime.format(context);
      });
      _dateController.text = eventDate.toString().split(' ')[0];
      date = eventDate;
      time = eventTime;

      // Event place
      placeId = oldEvent.place.id;

      // Title & Description
      _titleController.text = oldEvent.title;
      _descController.text = oldEvent.description;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: Text(''),
      ),
      body: BlocBuilder<UpdateGroupEventBloc, UpdateGroupEventState>(
        bloc: _updateGroupEventBloc,
        builder: (context, state) {
          if (state is UpdateGroupEventLoading) {
            return CircularLoading();
          }
          if (state is UpdateGroupEventSuccess) {
            _refreshUpcomingEvent();
            Future.delayed(Duration(), () {
              Navigator.of(context).pop();
            });
          }
          return getBody();
        },
      ),
    );
  }

  getBody() => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 40),
          child: Column(
            children: [
              CreateGroupEventHeaderWidget(
                avatar: widget.args.groupEntity.avatar,
              ),
              const SizedBox(height: 32),
              DateTimePickerWidget(
                dateController: _dateController,
                timeController: _timeController,
                pickDateCallback: _pickDateHandler,
                pickTimeCallback: _pickTimeHandler,
              ),
              const SizedBox(height: 32),
              PlacePickerWidget(
                pickPlaceCallback: _pickPlaceHandler,
                place: widget.args.oldEvent?.place,
              ),
              const SizedBox(height: 32),
              CreateGroupEventDescriptionWidget(
                titleController: _titleController,
                descController: _descController,
              ),
              const SizedBox(height: 32),
              widget.args.type == EditType.AddNew
                  ? getCreateGroupEventButton()
                  : getUpdateGroupEventButton(),
            ],
          ),
        ),
      );

  getCreateGroupEventButton() {
    return BlocBuilder<CreateNewGroupEventBloc, CreateNewGroupEventState>(
      bloc: _createNewGroupEventBloc,
      builder: (context, state) {
        if (state is CreateNewGroupEventLoading) {
          return CircularLoading();
        }
        if (state is CreateNewGroupEventSuccess) {
          _refreshUpcomingEvent();
          Future.delayed(Duration(), () {
            Navigator.of(context).pop();
          });
        }
        return SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 44,
          child: ElevatedButton(
            onPressed: _submitCreateGroupEvent,
            child: Text(
              'Create Group Event',
              style: TextStyle(
                color: MyAppTheme.secondary,
                fontSize: MyTextStyle.title_5_fontsize,
              ),
            ),
          ),
        );
      },
    );
  }

  getUpdateGroupEventButton() {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 44,
      child: ElevatedButton(
        onPressed: _submitUpdateGroupEvent,
        child: Text(
          'Save',
          style: TextStyle(
            color: MyAppTheme.secondary,
            fontSize: MyTextStyle.title_5_fontsize,
          ),
        ),
      ),
    );
  }

  void _refreshUpcomingEvent() {
    widget.args.getListGroupEventBloc.add(
      FetchListGroupEventEvent(
        params: GetListGroupEventParams(
          groupId: widget.args.groupEntity.id,
          page: 1,
          rowsperpage: 100,
        ),
      ),
    );
  }
}
