import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_detail_widgets/group_description_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_detail_widgets/group_participants_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_detail_widgets/group_upcoming_event_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../../injection_container.dart';
import '../../../../domain/entities/group/group_entity.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/group/get_group_by_id.dart';
import '../../../../domain/usecase/group/leave_group.dart';
import '../../../../domain/usecase/group/request_join_group_uc.dart';
import '../../../../domain/usecase/user/get_user_inform_by_id.dart';
import '../../../bloc/group_blocs/get_group_by_id_bloc/get_group_by_id_bloc.dart';
import '../../../bloc/group_blocs/leave_group_bloc/leave_group_bloc.dart';
import '../../../bloc/group_blocs/request_join_group_bloc/request_join_group_bloc.dart';
import '../../../bloc/user_blocs/get_user_by_id_bloc/get_user_by_id_bloc.dart';
import '../../../repository_providers/user_repository_provider.dart';
import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';
import '../../../widgets/generals/circle_button.dart';
import 'add_group_screens/add_group_screen.dart';
import 'widgets/group_detail_widgets/group_tags_widget.dart';

class ArgsGroupDetailScreen {
  final String imageUrl;
  final String heroTag;
  final GroupEntity group;

  ArgsGroupDetailScreen({@required this.imageUrl, this.heroTag, this.group});
}

class GroupDetailScreen extends StatefulWidget {
  static const routeName = 'group-detail-screen';

  final String imageUrl;
  final String heroTag;
  final GroupEntity group;

  GroupDetailScreen(
      {Key key, @required this.imageUrl, this.heroTag, this.group})
      : super(key: key);

  @override
  _PlaceDetailScreenState createState() => _PlaceDetailScreenState();
}

class _PlaceDetailScreenState extends State<GroupDetailScreen> {
  GetUserByIdBloc _getUserByIdBloc;
  RequestJoinGroupBloc _requestJoinGroupBloc;
  bool _isMember = false;
  bool _isHost = false;
  GetGroupByIdBloc _getGroupByIdBloc;
  LeaveGroupBloc _leaveGroupBloc;
  UserInform _userInfo;
  GetListGroupEventBloc _getListGroupEventBloc;

  @override
  void initState() {
    super.initState();
    _getUserByIdBloc = sl<GetUserByIdBloc>();
    _getGroupByIdBloc = sl<GetGroupByIdBloc>();
    _leaveGroupBloc = sl<LeaveGroupBloc>();
    _getListGroupEventBloc = sl<GetListGroupEventBloc>();
    _getGroupByIdBloc.add(
      GetGroupByIdEventSubmit(GetGroupByIdParams(widget.group.id)),
    );
    _getUserByIdBloc.add(
      GetUserEvent(GetUserInformByIdParams(widget.group.host, ["favorites"])),
    );
    _getListGroupEventBloc.add(
      FetchListGroupEventEvent(
        params: GetListGroupEventParams(
          groupId: widget.group.id,
          page: 1,
          rowsperpage: 100,
        ),
      ),
    );
    _requestJoinGroupBloc = sl<RequestJoinGroupBloc>();
    _userInfo = context.read<UserRepositoryProvider>().userInform;
    _isMember =
        widget.group.participant.indexWhere((p) => p.id == _userInfo.userId) >=
            0;
    _isHost = _userInfo.userId == widget.group.host;
  }

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<void> _showJoinGroupDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          final TextEditingController _descController = TextEditingController();
          return AlertDialog(
            content: Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      controller: _descController,
                      validator: (value) {
                        return value.isNotEmpty
                            ? null
                            : "Write something to host";
                      },
                      decoration: InputDecoration(
                        hintText: 'Reason want to join group ...',
                        hintStyle: TextStyle(color: MyAppTheme.black_grey),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      minLines: 2,
                      maxLines: 2,
                    ),
                  ],
                )),
            title: Text("Join group"),
            actions: <Widget>[
              BlocBuilder<RequestJoinGroupBloc, RequestJoinGroupState>(
                  bloc: _requestJoinGroupBloc,
                  builder: (context, state) {
                    if (state is RequestJoinGroupLoaded) {
                      _getGroupByIdBloc.add(
                        GetGroupByIdEventSubmit(
                            GetGroupByIdParams(widget.group.id)),
                      );
                      Navigator.of(context).pop();
                    }
                    if (state is RequestJoinGroupLoading)
                      return const CircularProgressIndicator();
                    else
                      return ElevatedButton(
                        child: Text(
                          'OK',
                          style: TextStyle(
                            color: MyAppTheme.secondary,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          primary: MyAppTheme.primary,
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _requestJoinGroupBloc.add(
                              RequestJoinGroupEventSubmitted(
                                RequestJoinGroupParams(
                                  idGroup: widget.group.id,
                                  description: _descController.text,
                                ),
                              ),
                            );
                          }
                        },
                      );
                  }),
            ],
          );
        });
  }

  Future<void> _showLeaveGroupDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Do you sure you wanna leave this group?"),
            actions: <Widget>[
              BlocBuilder<LeaveGroupBloc, LeaveGroupState>(
                  bloc: _leaveGroupBloc,
                  builder: (context, state) {
                    if (state is LeaveGroupLoaded) {
                      _getGroupByIdBloc.add(GetGroupByIdEventSubmit(
                          GetGroupByIdParams(widget.group.id)));
                      Navigator.of(context).pop();
                    }
                    if (state is LeaveGroupLoading)
                      return const CircularProgressIndicator();
                    else
                      return ElevatedButton(
                        child: Text(
                          'Leave',
                          style: TextStyle(color: MyAppTheme.secondary),
                        ),
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                                MyAppTheme.primary)),
                        onPressed: () {
                          _leaveGroupBloc.add(LeaveGroupEventSubmit(
                              LeaveGroupParams(widget.group.id)));
                        },
                      );
                  }),
            ],
          );
        });
  }

  void _updateGroup() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AddGroupScreen(
          updateGroupArgs:
              UpdateGroupArgs(_getGroupByIdBloc, widget.group, "Update"),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final double placeImageHeight = size.height * 2 / 5;
    final double sizedBoxHeight = placeImageHeight - 30;
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final double backButtonContainerHeight = 30;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        color: MyAppTheme.secondary,
        child: Stack(
          children: [
            Hero(
              tag: widget.heroTag,
              child: Container(
                height: placeImageHeight,
                width: size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(this.widget.imageUrl),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SingleChildScrollView(
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(height: statusBarHeight),
                      Container(
                        height: backButtonContainerHeight,
                        alignment: Alignment.centerLeft,
                        child: CircleButton(
                          icon: Icons.arrow_back,
                          iconColor: MyAppTheme.secondary,
                          backgroundColor:
                              MyAppTheme.secondary.withOpacity(0.4),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(
                        height: sizedBoxHeight -
                            statusBarHeight -
                            backButtonContainerHeight,
                      ),
                      getBodyContent(size),
                    ],
                  ),
                  getFloatingButton(size, sizedBoxHeight),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getBodyContent(Size size) {
    final group = widget.group;
    return Container(
      width: size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(20)),
        color: Colors.white,
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 40, 16, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LimitedBox(
                  maxWidth: size.width * 3 / 5,
                  child: Text(
                    group.name,
                    style: TextStyle(
                      fontSize: MyTextStyle.title_3_fontsize,
                      fontWeight: FontWeight.w500,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            GroupParticipantsWidget(
              getGroupByIdBloc: _getGroupByIdBloc,
              getUserByIdBloc: _getUserByIdBloc,
            ),
            const SizedBox(height: 10),
            GroupTagsWidget(groupTagNames: group.tag.listTagModel),
            GroupUpComingEventWidget(
              groupEntity: group,
              getListGroupEventBloc: _getListGroupEventBloc,
              isHost: _isHost,
            ),
            const SizedBox(height: 32),
            GroupDescriptionWidget(description: group.description),
          ],
        ),
      ),
    );
  }

  Widget getFloatingButton(Size size, double sizedBoxHeight) {
    return Positioned(
      top: sizedBoxHeight - 25,
      right: 16,
      child: BlocBuilder<GetGroupByIdBloc, GetGroupByIdState>(
        bloc: _getGroupByIdBloc,
        builder: (context, state) {
          GroupEntity group = widget.group;
          if (state is GetGroupByIdLoaded) {
            group = state.groupEntity;
            _isMember =
                group.participant.indexWhere((p) => p.id == _userInfo.userId) >=
                    0;
          }
          return Row(
            children: [
              Chip(
                label: Text(
                  group.isprivate ? 'Private' : 'Public',
                  style: TextStyle(
                    color: widget.group.isprivate
                        ? MyAppTheme.black
                        : MyAppTheme.secondary,
                  ),
                ),
                backgroundColor:
                    group.isprivate ? MyAppTheme.grey : Colors.green,
              ),
              const SizedBox(width: 8),
              if (_isHost)
                CircleButton(
                  onTap: _updateGroup,
                  icon: Icons.edit,
                  iconColor: Colors.white,
                  backgroundColor: MyAppTheme.primary,
                )
              else if (_isMember)
                CircleButton(
                  onTap: () async {
                    await _showLeaveGroupDialog(context);
                  },
                  icon: Icons.exit_to_app,
                  iconColor: Colors.white,
                  backgroundColor: MyAppTheme.black_grey,
                )
              else
                CircleButton(
                  onTap: () async {
                    await _showJoinGroupDialog(context);
                  },
                  icon: Icons.add,
                  iconColor: Colors.white,
                  backgroundColor: MyAppTheme.primary,
                ),
            ],
          );
        },
      ),
    );
  }
}
