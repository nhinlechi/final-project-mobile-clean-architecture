import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../injection_container.dart';
import '../../../../../data/models/group/list_tag_model.dart';
import '../../../../bloc/group_blocs/get_list_tag_group_bloc/get_list_tag_group_bloc.dart';
import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';
import '../../../../widgets/generals/circular_loading.dart';

class AddGroupTagTab extends StatefulWidget {
  final Function(List<String>) getListTag;
  final ListTagModel listTagGroup;

  const AddGroupTagTab({Key key, this.getListTag, this.listTagGroup})
      : super(key: key);
  @override
  _AddGroupTagTabState createState() => _AddGroupTagTabState();
}

class _AddGroupTagTabState extends State<AddGroupTagTab> {
  GetListTagGroupBloc _getListTagGroupBloc;
  List<String> _selectedLabels = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListTagGroupBloc = sl<GetListTagGroupBloc>();
    if (widget.listTagGroup != null)
      widget.listTagGroup.listTagModel.forEach((element) {
        _selectedLabels.add(element.tagId);
      });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Add your group tag',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 16,
          ),
          BlocBuilder<GetListTagGroupBloc, GetListTagGroupState>(
              bloc: _getListTagGroupBloc,
              builder: (context, state) {
                if (state is GetListTagGroupInitial)
                  _getListTagGroupBloc.add(GetListTagGroupEventSubmitted());
                if (state is GetListTagGroupLoaded) {
                  final listTag = state.listTagModel.listTagModel;
                  return Wrap(
                    spacing: 8,
                    children: List.generate(listTag.length, (index) {
                      return FilterChip(
                        label: Text(listTag[index].tagName),
                        backgroundColor: MyAppTheme.black_grey,
                        labelStyle: TextStyle(color: MyAppTheme.secondary),
                        selected:
                            _selectedLabels.contains(listTag[index].tagId),
                        checkmarkColor: MyAppTheme.secondary,
                        selectedColor: MyAppTheme.primary,
                        onSelected: (value) {
                          setState(() {
                            if (value) {
                              _selectedLabels.add(listTag[index].tagId);
                            } else {
                              _selectedLabels.removeWhere(
                                  (element) => element == listTag[index].tagId);
                            }
                            widget.getListTag(_selectedLabels);
                          });
                        },
                      );
                    }),
                  );
                }
                return CircularLoading();
              })
        ],
      ),
    );
  }
}
