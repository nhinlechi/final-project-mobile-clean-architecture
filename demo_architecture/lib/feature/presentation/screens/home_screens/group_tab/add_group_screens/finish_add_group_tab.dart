import 'package:demo_architecture/feature/presentation/bloc/group_blocs/add_group_bloc/add_group_bloc.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';

class FinishAddGroupTab extends StatelessWidget {
  final Function() addGroupEvent;
  final AddGroupBloc addGroupBloc;

  const FinishAddGroupTab({
    Key key,
    this.addGroupEvent,
    this.addGroupBloc,
  }) : super(key: key);

  void _createGroup() {
    addGroupEvent();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            'Already add new group?',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 16,
          ),
          Text(
            'Creating groups and organizing real-life group meetings is a challenge and an opportunity.',
            style: TextStyle(
              color: MyAppTheme.black_grey,
              fontSize: MyTextStyle.title_4_fontsize,
            ),
          ),
          Expanded(
            child: Container(),
          ),
          BlocBuilder<AddGroupBloc, AddGroupState>(
            bloc: addGroupBloc,
            builder: (context, state) {
              if (state is AddGroupLoading) {
                return CircularLoading();
              }
              return SizedBox(
                height: 80,
                width: 80,
                child: ElevatedButton(
                  child: Icon(
                    Icons.check,
                    size: 36,
                    color: MyAppTheme.secondary,
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    primary: MyAppTheme.black_grey,
                  ),
                  onPressed: _createGroup,
                ),
              );
            },
          ),
          SizedBox(
            height: 16,
          ),
        ],
      ),
    );
  }
}
