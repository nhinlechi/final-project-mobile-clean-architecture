import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../../../themes/text_style.dart';

class AddGroupNameTab extends StatefulWidget {
  final Function(bool) togglePrivateGroupCallback;
  final TextEditingController nameController;
  final bool isPrivateGroup;

  const AddGroupNameTab({
    Key key,
    @required this.togglePrivateGroupCallback,
    @required this.nameController,
    @required this.isPrivateGroup,
  }) : super(key: key);

  @override
  _AddGroupNameTabState createState() => _AddGroupNameTabState();
}

class _AddGroupNameTabState extends State<AddGroupNameTab> {
  bool isPrivateGroup = false;

  @override
  void initState() {
    super.initState();
    this.isPrivateGroup = widget.isPrivateGroup;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Group name',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 16,
          ),
          TextField(
            controller: widget.nameController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(15),
              labelText: 'Group Name',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              prefixIcon: Icon(Icons.group_sharp),
            ),
          ),
          const SizedBox(height: 16),
          SwitchListTile(
            value: isPrivateGroup,
            contentPadding: const EdgeInsets.only(left: 6),
            title: Text(
              'Is Private',
              style: MyTextStyle.ts_filter_title,
            ),
            activeColor: MyAppTheme.primary,
            onChanged: (value) {
              setState(() {
                isPrivateGroup = !isPrivateGroup;
              });
              widget.togglePrivateGroupCallback(isPrivateGroup);
            },
          ),
          Center(
            child: Lottie.asset(
                'assets/animations/lotties/group_communicating.json'),
          ),
        ],
      ),
    );
  }
}
