import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../injection_container.dart';
import '../../../../../domain/entities/group/group_entity.dart';
import '../../../../../domain/usecase/group/add_new_group.dart';
import '../../../../../domain/usecase/group/get_group_by_id.dart';
import '../../../../../domain/usecase/group/update_group.dart';
import '../../../../bloc/group_blocs/add_group_bloc/add_group_bloc.dart';
import '../../../../bloc/group_blocs/get_group_by_id_bloc/get_group_by_id_bloc.dart';
import '../../../../bloc/group_blocs/update_group_bloc/update_group_bloc.dart';
import '../../../../themes/colors.dart';
import 'add_group_avatar_tab.dart';
import 'add_group_description_tab.dart';
import 'add_group_name_tab.dart';
import 'add_group_tag_tab.dart';
import 'finish_add_group_tab.dart';

class UpdateGroupArgs {
  final GetGroupByIdBloc getGroupByIdBloc;
  final GroupEntity groupEntity;
  final String title;

  UpdateGroupArgs(this.getGroupByIdBloc, this.groupEntity, this.title);
}

class AddGroupScreen extends StatefulWidget {
  static const routeName = 'add-group-screen';
  final UpdateGroupArgs updateGroupArgs;

  const AddGroupScreen({Key key, this.updateGroupArgs}) : super(key: key);

  @override
  _AddGroupScreenState createState() => _AddGroupScreenState();
}

class _AddGroupScreenState extends State<AddGroupScreen> {
  CarouselController _carouselController = CarouselController();
  final int _maxPages = 5;
  int _pageIndex = 0;

  // Data for add new group
  final _nameController = TextEditingController();
  final _descController = TextEditingController();
  bool _isPrivate = false;
  List<String> _tagIds;
  File _avatarFile;
  AddGroupBloc _addGroupBloc;
  UpdateGroupBloc _updateGroupBloc;
  GroupEntity _groupEntity;

  @override
  void initState() {
    super.initState();
    _updateGroupBloc = sl<UpdateGroupBloc>();
    _addGroupBloc = sl<AddGroupBloc>();
    _groupEntity = widget.updateGroupArgs?.groupEntity;
    _isPrivate = _groupEntity?.isprivate ?? false;
    _nameController.text = _groupEntity?.name;
    _descController.text = _groupEntity?.description;
  }

  void togglePrivateGroupHandler(bool isPrivate) {
    _isPrivate = isPrivate;
  }

  void getAvatarFile(File avatarFile) {
    _avatarFile = avatarFile;
  }

  void getListTag(List<String> listTag) {
    _tagIds = listTag;
  }

  void addGroupEvent() {
    if (widget.updateGroupArgs == null) {
      AddNewGroupParams addNewGroupParams = AddNewGroupParams(
          _nameController.text,
          _descController.text,
          _isPrivate,
          _tagIds,
          _avatarFile);
      _addGroupBloc.add(ConfirmAddGroupEvent(addNewGroupParams));
    } else {
      UpdateGroupParams updateGroupParams = UpdateGroupParams(
          groupId: _groupEntity.id,
          name: _nameController.text,
          description: _descController.text,
          isPrivate: _isPrivate,
          tags: _tagIds,
          avatarFile: _avatarFile);
      _updateGroupBloc.add(UpdateGroupEventSubmit(updateGroupParams));
    }
  }

  void _onPressNext() async {
    if (_pageIndex < _maxPages - 1) {
      setState(() {
        _pageIndex++;
      });
      await _carouselController.animateToPage(
        _pageIndex,
        duration: Duration(milliseconds: 300),
      );
    }
  }

  void _onPressBack() async {
    if (_pageIndex > 0) {
      setState(() {
        _pageIndex--;
      });
      await _carouselController.animateToPage(
        _pageIndex,
        duration: Duration(milliseconds: 300),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return MultiBlocListener(
      listeners: [
        BlocListener<AddGroupBloc, AddGroupState>(
            bloc: _addGroupBloc,
            listener: (context, state) {
              if (state is AddGroupLoaded) {
                Navigator.of(context).pop(true);
              }
            }),
        BlocListener<UpdateGroupBloc, UpdateGroupState>(
            bloc: _updateGroupBloc,
            listener: (context, state) {
              if (state is UpdateGroupLoaded) {
                widget.updateGroupArgs.getGroupByIdBloc.add(
                    GetGroupByIdEventSubmit(
                        GetGroupByIdParams(_groupEntity.id)));
                Navigator.of(context).pop(true);
              }
            })
      ],
      child: Scaffold(
        body: SafeArea(
          child: PageView(
            physics: new NeverScrollableScrollPhysics(),
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        children: List.generate(_maxPages, (index) {
                          return Container(
                            margin: const EdgeInsets.only(right: 16),
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: index <= _pageIndex
                                  ? MyAppTheme.primary
                                  : MyAppTheme.grey,
                            ),
                          );
                        }),
                      ),
                    ),
                    CarouselSlider(
                      items: [
                        AddGroupNameTab(
                          nameController: _nameController,
                          togglePrivateGroupCallback: togglePrivateGroupHandler,
                          isPrivateGroup: _isPrivate,
                        ),
                        AddGroupDescriptionTab(
                          descController: _descController,
                        ),
                        AddGroupAvatarTab(
                          avtUrl: _groupEntity?.avatar,
                          getAvatarFile: getAvatarFile,
                        ),
                        AddGroupTagTab(
                          getListTag: getListTag,
                          listTagGroup: _groupEntity?.tag,
                        ),
                        FinishAddGroupTab(
                          addGroupEvent: addGroupEvent,
                          addGroupBloc: _addGroupBloc,
                        ),
                      ],
                      carouselController: _carouselController,
                      options: CarouselOptions(
                        autoPlay: false,
                        enlargeCenterPage: true,
                        viewportFraction: 0.9,
                        aspectRatio: size.width / (size.height - 100),
                        initialPage: 0,
                        scrollPhysics: const NeverScrollableScrollPhysics(),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        floatingActionButton: Padding(
          padding: const EdgeInsets.only(left: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FloatingActionButton(
                heroTag: 'fab-back',
                child: Icon(
                  Icons.arrow_back_ios_rounded,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
                onPressed: _onPressBack,
              ),
              FloatingActionButton(
                heroTag: 'fab-next',
                child: Icon(
                  Icons.arrow_forward_ios_rounded,
                  color: Colors.white,
                ),
                backgroundColor: MyAppTheme.primary,
                onPressed: _onPressNext,
              )
            ],
          ),
        ),
      ),
    );
  }
}
