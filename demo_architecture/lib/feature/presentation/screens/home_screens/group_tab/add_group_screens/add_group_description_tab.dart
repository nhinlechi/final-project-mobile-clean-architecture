import 'package:flutter/material.dart';

import '../../../../themes/text_style.dart';

class AddGroupDescriptionTab extends StatelessWidget {
  final TextEditingController descController;

  const AddGroupDescriptionTab({Key key, this.descController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Write somethings about your group',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 16,
          ),
          TextField(
            controller: descController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(15),
              labelText: 'Descriptions',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              prefixIcon: Icon(Icons.description),
            ),
            maxLines: 10,
            minLines: 3,
          ),
        ],
      ),
    );
  }
}
