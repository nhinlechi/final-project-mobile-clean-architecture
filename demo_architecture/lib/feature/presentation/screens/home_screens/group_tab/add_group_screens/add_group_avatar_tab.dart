import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';

class AddGroupAvatarTab extends StatefulWidget {
  final Function(File) getAvatarFile;
  final String avtUrl;

  const AddGroupAvatarTab({Key key, this.getAvatarFile, this.avtUrl})
      : super(key: key);

  @override
  _AddGroupAvatarTabState createState() => _AddGroupAvatarTabState();
}

class _AddGroupAvatarTabState extends State<AddGroupAvatarTab> {
  PickedFile _imageFile;

  final ImagePicker _imagePicker = ImagePicker();

  void _pickImageFromGallery() async {
    final pickedFile = await _imagePicker.getImage(
      source: ImageSource.gallery,
      maxHeight: 1000,
      maxWidth: 1000,
    );
    widget.getAvatarFile(File(pickedFile.path));
    setState(() {
      _imageFile = pickedFile;
    });
  }

  void _addAvatarPhoto() {
    _pickImageFromGallery();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Hey, You missing your group avatar!',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 16,
          ),
          InkWell(
            onTap: _addAvatarPhoto,
            child: _imageFile != null || widget.avtUrl != null
                ? Container(
                    width: size.width / 2,
                    height: size.width / 2 * 4 / 3,
                    decoration: BoxDecoration(
                      color: MyAppTheme.grey,
                      borderRadius: BorderRadius.circular(20),
                      image: DecorationImage(
                        image: _imageFile != null
                            ? FileImage(File(_imageFile.path))
                            : NetworkImage(widget.avtUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  )
                : getEmptyAvatarContainer(size),
          )
        ],
      ),
    );
  }

  getEmptyAvatarContainer(Size size) {
    return Container(
      width: size.width / 2,
      height: size.width / 2 * 4 / 3,
      decoration: BoxDecoration(
        color: MyAppTheme.grey,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FloatingActionButton(
            onPressed: _addAvatarPhoto,
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            backgroundColor: MyAppTheme.primary,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Add your profile photo',
            style: TextStyle(
              color: Colors.black.withOpacity(0.5),
            ),
          ),
        ],
      ),
    );
  }
}
