import 'package:demo_architecture/core/socket/socket_event.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_group_event_messages_bloc/group_event_messages_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/message_bubble.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/group_chat_widgets/group_messages.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:socket_io_client/socket_io_client.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';

class GroupChatPopup extends StatelessWidget {
  final Socket socket;
  final String eventId;
  final GroupEntity group;

  GroupChatPopup({
    Key key,
    @required this.socket,
    @required this.eventId,
    @required this.group,
  }) : super(key: key);

  final _newMessageController = TextEditingController();
  final GlobalKey<GroupMessagesState> _messagesKey = GlobalKey();

  void _sendMessageText() {
    // Callback
    if (_newMessageController.text.isNotEmpty) {
      _messagesKey.currentState.addNewMessageToList(
        _newMessageController.text,
        MessageData.TEXT,
      );
    }
    // Emit event send message
    socket?.emitWithAck(
      SocketEvent.clientSentChat,
      {
        'message': _newMessageController.text,
        'type': MessageData.TEXT,
        'roomId': eventId,
      },
      ack: (result) {
        if (result != null) {
          print(result);
        } else {
          print("Send Succeed");
        }
      },
    );
    _newMessageController.clear();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 16),
        color: MyAppTheme.secondary,
        child: Column(
          children: [
            getGroupChatHeader(size),
            Expanded(
              child: GroupEventMessages(
                socket: socket,
                eventId: eventId,
                group: group,
                key: _messagesKey,
              ),
            ),
            const SizedBox(height: 8),
            getNewMessageWidget(),
          ],
        ),
      ),
    );
  }

  getGroupChatHeader(Size size) => Row(
        children: [
          CircleAvatar(
            radius: 20,
            backgroundImage: NetworkImage(group.avatar),
          ),
          const SizedBox(width: 8),
          SizedBox(
            width: size.width / 1.5,
            child: Text(
              'Tiệc họp mặt đầu tiên của Guitar Sài Thành',
              style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
            ),
          ),
        ],
      );

  getNewMessageWidget() => Row(
        children: [
          Expanded(
            child: Container(
              height: 36,
              child: TextField(
                controller: _newMessageController,
                cursorColor: MyAppTheme.primary,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    contentPadding: EdgeInsets.only(
                      left: 20,
                      right: 10,
                    ),
                    hintText: 'Aa ...'),
              ),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.send,
              color: MyAppTheme.primary,
            ),
            onPressed: _sendMessageText,
          ),
        ],
      );
}
