import 'dart:developer';

import 'package:demo_architecture/core/platform/api_base.dart';
import 'package:demo_architecture/core/utils/enums.dart';
import 'package:demo_architecture/core/utils/get_token.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/get_list_group_event_bloc/get_list_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/join_group_event_bloc/join_group_event_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/chat_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_place_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/dating_event_time_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/create_group_event_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/group_chat_popup.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/upcoming_events_widgets/upcoming_events_description_widget.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/upcoming_events_widgets/upcoming_events_header_widget.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../../../injection_container.dart';
import 'widgets/upcoming_events_widgets/upcoming_participants_widget.dart';

class ArgsUpcomingEventsScreen {
  final GroupEntity group;
  final GetListGroupEventBloc getListGroupEventBloc;

  ArgsUpcomingEventsScreen({
    @required this.group,
    @required this.getListGroupEventBloc,
  });
}

class UpcomingEventsScreen extends StatefulWidget {
  static const String routeName = 'upcoming-events-screen';

  final ArgsUpcomingEventsScreen args;

  const UpcomingEventsScreen({
    Key key,
    @required this.args,
  }) : super(key: key);

  @override
  _UpcomingEventsScreenState createState() => _UpcomingEventsScreenState();
}

class _UpcomingEventsScreenState extends State<UpcomingEventsScreen> {
  JoinGroupEventBloc _joinGroupEventBloc;
  Socket _socket;
  AuthenticateToken auth;

  void _submitJoinGroupEvent(bool status, String eventId) {
    _joinGroupEventBloc.add(
      SubmitJoinGroupEvent(
        params: JoinGroupEventParams(
          groupId: widget.args.group.id,
          eventId: eventId,
          status: status,
        ),
      ),
    );
  }

  void _showEditGroupEvent(GroupEventModel event) {
    Navigator.of(context).pushNamed(
      CreateGroupEventScreen.routeName,
      arguments: CreateGroupEventScreenArgs(
        type: EditType.Update,
        groupEntity: widget.args.group,
        oldEvent: event,
        getListGroupEventBloc: widget.args.getListGroupEventBloc,
      ),
    );
  }

  void _showGroupChatPopup(String eventId) {
    // showCupertinoModalBottomSheet(
    //   context: context,
    //   builder: (context) => GroupChatPopup(
    //     eventId: eventId,
    //     socket: _socket,
    //     group: widget.args.group,
    //   ),
    // );
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => GroupChatPopup(
          eventId: eventId,
          socket: _socket,
          group: widget.args.group,
        ),
      ),
    );
  }

  void _connectSocketIO() {
    // Get accessToken
    final token = sl<Token>().getToken();

    // Connect to server socket
    final socketOption = OptionBuilder()
        .setTransports(['websocket'])
        .setQuery({'token': token})
        .disableAutoConnect()
        .build();
    _socket = io(APIBase.chatGroupEventSocketUrl, socketOption);
    _socket.connect();

    // On Connect
    _socket.onConnect((data) {
      log('Socket connected', name: 'SocketIO');
    });
  }

  void _disconnectSocketIO() {
    _socket.onDisconnect((data) {
      log('Disconnected', name: 'socketIO');
    });
    _socket.disconnect();
    _socket.dispose();
  }

  @override
  void initState() {
    super.initState();
    //
    _joinGroupEventBloc = sl<JoinGroupEventBloc>();
    // Create And Connect socket
    _connectSocketIO();
    // Get user info
    auth = context.read<UserRepositoryProvider>().auth;
  }

  @override
  void dispose() {
    _disconnectSocketIO();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: getBody(),
      floatingActionButton:
          BlocListener<JoinGroupEventBloc, JoinGroupEventState>(
        bloc: _joinGroupEventBloc,
        listener: (context, state) {
          if (state is JoinGroupEventSuccess) {
            _refreshUpcomingEvent();
          }
        },
        child: getFloatingButton(context),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget getAppBar() => AppBar(
        backgroundColor: MyAppTheme.secondary,
        elevation: 0,
        iconTheme: IconThemeData(color: MyAppTheme.primary),
      );

  Widget getBody() => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
          child: BlocBuilder<GetListGroupEventBloc, GetListGroupEventState>(
            bloc: widget.args.getListGroupEventBloc,
            builder: (context, state) {
              if (state is GetListGroupEventLoadingState) {
                return CircularLoading();
              }
              if (state is GetListGroupEventSuccessState) {
                final event = state.listGroupEventModel.upcomingEvent;
                return Column(
                  children: [
                    UpcomingEventsHeaderWidget(group: widget.args.group),
                    const SizedBox(height: 8),
                    ParticipantAvatarsWidget(
                      participants:
                          state.listGroupEventModel.upcomingEvent.participants,
                    ),
                    const SizedBox(height: 32),
                    DatingEventTimeWidget(
                      label: 'Event Time',
                      datingTime: event.eventTime,
                    ),
                    const SizedBox(height: 32),
                    DatingEventPlaceWidget(
                      label: 'Event Place',
                      place: event.place,
                    ),
                    const SizedBox(height: 32),
                    UpcomingEventsDescriptionWidget(
                      title: event.title,
                      description: event.description,
                    ),
                    const SizedBox(height: 64),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      );

  Widget getFloatingButton(BuildContext context) => Padding(
        padding: const EdgeInsets.all(16),
        child: Row(
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 16),
                child:
                    BlocBuilder<GetListGroupEventBloc, GetListGroupEventState>(
                  bloc: widget.args.getListGroupEventBloc,
                  builder: (context, getListState) {
                    if (getListState is GetListGroupEventSuccessState) {
                      return BlocBuilder<JoinGroupEventBloc,
                          JoinGroupEventState>(
                        bloc: _joinGroupEventBloc,
                        builder: (context, joinGroupState) {
                          // ---------------------------------------------------
                          if (joinGroupState is JoinGroupEventLoading) {
                            return Container(
                              height: 50,
                              child: CircularLoading(),
                            );
                          }
                          // ---------------------------------------------------
                          final event =
                              getListState.listGroupEventModel.upcomingEvent;
                          final auth =
                              context.read<UserRepositoryProvider>().auth;
                          final isJoined = event.participants
                                  .indexWhere((e) => e.id == auth.userId) >=
                              0;
                          final isHost = auth.userId == widget.args.group.host;

                          // Host ----------------------------------------------
                          if (isHost) {
                            return ElevatedButton(
                              onPressed: () => _showEditGroupEvent(event),
                              child: Text(
                                'Edit',
                                style: TextStyle(
                                  color: MyAppTheme.secondary,
                                ),
                              ),
                            );
                          }

                          // Member ---------------------------------------------
                          if (getListState is GetListGroupEventSuccessState) {
                            // Joined
                            if (isJoined) {
                              return ElevatedButton(
                                onPressed: () =>
                                    _submitJoinGroupEvent(false, event.id),
                                child: Text(
                                  'Leave!',
                                  style: TextStyle(
                                    color: MyAppTheme.secondary,
                                  ),
                                ),
                                style: ElevatedButton.styleFrom(
                                  primary: MyAppTheme.black_grey,
                                ),
                              );
                            } else {
                              return ElevatedButton(
                                onPressed: () =>
                                    _submitJoinGroupEvent(true, event.id),
                                child: Text(
                                  'I will come',
                                  style: TextStyle(
                                    color: MyAppTheme.secondary,
                                  ),
                                ),
                              );
                            }
                          }
                          return Container();
                        },
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ),
            BlocBuilder<GetListGroupEventBloc, GetListGroupEventState>(
              bloc: widget.args.getListGroupEventBloc,
              builder: (context, state) {
                if (state is GetListGroupEventSuccessState) {
                  return FloatingActionButton(
                    onPressed: () => _showGroupChatPopup(
                      state.listGroupEventModel.upcomingEvent.id,
                    ),
                    backgroundColor: MyAppTheme.primary,
                    child: Icon(
                      Icons.comment,
                      color: MyAppTheme.secondary,
                    ),
                  );
                }
                return Container();
              },
            ),
          ],
        ),
      );

  void _refreshUpcomingEvent() {
    widget.args.getListGroupEventBloc.add(
      FetchListGroupEventEvent(
        params: GetListGroupEventParams(
          groupId: widget.args.group.id,
          page: 1,
          rowsperpage: 100,
        ),
      ),
    );
  }
}
