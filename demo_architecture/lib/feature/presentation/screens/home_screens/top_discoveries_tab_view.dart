import 'package:demo_architecture/feature/domain/usecase/places/get_places_uc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../injection_container.dart';
import '../../../domain/entities/group/group_entity.dart';
import '../../../domain/entities/place/place_entity.dart';
import '../../../domain/usecase/group/get_list_group.dart';
import '../../bloc/group_blocs/group_bloc.dart';
import '../../bloc/place_blocs/get_top_place/top_place_bloc.dart';
import '../../themes/text_style.dart';
import '../../widgets/generals/circular_loading.dart';
import '../../widgets/top_discoveries_item.dart';
import 'group_tab/group_detail_screen.dart';
import 'place_tab/place_detail_screen.dart';

class TopDiscoveriesTabView extends StatefulWidget {
  @override
  _TopDiscoveriesTabViewState createState() => _TopDiscoveriesTabViewState();
}

class _TopDiscoveriesTabViewState extends State<TopDiscoveriesTabView>
    with AutomaticKeepAliveClientMixin {
  TopPlaceBloc _topPlaceBloc;
  TopPlaceBloc _placeForYouBloc;

  void _goToPlace(
      BuildContext context, PlaceEntity placeItem, String imageUrl, String heroTag) {
    Navigator.of(context).pushNamed(
      PlaceDetailScreen.routeName,
      arguments: ArgsPlaceDetailScreen(
        heroTag: heroTag,
        imageUrl: imageUrl,
        placeItem: placeItem,
      ),
    );
  }

  void _goToGroup(BuildContext context, String imageUrl, GroupEntity group) {
    Navigator.of(context).pushNamed(
      GroupDetailScreen.routeName,
      arguments: ArgsGroupDetailScreen(
        imageUrl: imageUrl,
        heroTag: imageUrl,
        group: group,
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    // Initial bloc
    _topPlaceBloc = sl<TopPlaceBloc>();
    _placeForYouBloc = sl<TopPlaceBloc>();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(
            height: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  'Top Place',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<TopPlaceBloc, TopPlaceState>(
                  bloc: _topPlaceBloc,
                  builder: (context, state) {
                    if (state is TopPlaceInitial)
                      _topPlaceBloc.add(
                        GetTopPlaceEvent(
                          params: GetPlacesParams(
                            page: 1,
                            rowsPerPage: 10,
                            sortBy: 'rating',
                            sort: -1,
                          ),
                        ),
                      );
                    else if (state is TopPlaceLoading) {
                      return CircularLoading();
                    }
                    if (state is TopPlaceLoaded)
                      return SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            SizedBox(
                              width: 16,
                            ),
                            Row(
                              children: List.generate(
                                state.listPlaceModel.places.length,
                                (index) {
                                  final place = state.listPlaceModel.places[index];
                                  return InkWell(
                                    onTap: () => _goToPlace(
                                      context,
                                      place,
                                      place.photo[0],
                                      place.id + 'top-place',
                                    ),
                                    child: TopDiscoveriesItem(
                                      type: TopDiscoveriesType.place,
                                      imageUrl: place.photo[0],
                                      name: place.name,
                                      heroTag: place.id + 'top-place',
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    return Container();
                  }),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  'Top Group',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<GroupBloc, GroupState>(builder: (context, state) {
                if (state is GroupInitial)
                  context.read<GroupBloc>().add(GetListGroupEvent(
                      getListGroupParams: GetListGroupParams(rowsperpage: 10, page: 1)));
                else if (state is GroupLoading) {
                  return CircularLoading();
                }
                if (state is GroupLoadedList)
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        SizedBox(
                          width: 16,
                        ),
                        Row(
                          children: List.generate(
                            state.listGroupEntity.groups.length,
                            (index) {
                              final group = state.listGroupEntity.groups[index];
                              return InkWell(
                                onTap: () => _goToGroup(
                                  context,
                                  group.avatar,
                                  group,
                                ),
                                child: TopDiscoveriesItem(
                                  type: TopDiscoveriesType.group,
                                  heroTag: group.id + "topdiscover",
                                  name: group.name,
                                  imageUrl: group.avatar,
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                return Container();
              })
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  'For you',
                  style: TextStyle(
                    fontSize: MyTextStyle.title_4_fontsize,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<TopPlaceBloc, TopPlaceState>(
                  bloc: _placeForYouBloc,
                  builder: (context, state) {
                    if (state is TopPlaceInitial)
                      _placeForYouBloc.add(
                        GetTopPlaceEvent(
                          params: GetPlacesParams(
                            page: 2,
                            rowsPerPage: 10,
                            sortBy: 'rating',
                            sort: -1,
                          ),
                        ),
                      );
                    else if (state is TopPlaceLoading) {
                      return CircularLoading();
                    }
                    if (state is TopPlaceLoaded)
                      return SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            SizedBox(
                              width: 16,
                            ),
                            Row(
                              children: List.generate(
                                state.listPlaceModel.places.length,
                                (index) {
                                  final place = state.listPlaceModel.places[index];
                                  return InkWell(
                                    onTap: () => _goToPlace(
                                      context,
                                      place,
                                      place.photo[0],
                                      place.id + 'near you',
                                    ),
                                    child: TopDiscoveriesItem(
                                      type: TopDiscoveriesType.place,
                                      imageUrl: place.photo[0],
                                      name: place.name,
                                      heroTag: place.id + 'near you',
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    return Container();
                  }),
            ],
          ),
          SizedBox(
            height: 36,
          ),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
