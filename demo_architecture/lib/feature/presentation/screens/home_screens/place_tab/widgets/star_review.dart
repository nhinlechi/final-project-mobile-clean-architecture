import 'package:flutter/material.dart';

import '../../../../themes/colors.dart';

class StarReviewWidget extends StatefulWidget {
  final Function(int star) reviewStarCallback;

  const StarReviewWidget({Key key, @required this.reviewStarCallback})
      : super(key: key);

  @override
  _StarReviewWidgetState createState() => _StarReviewWidgetState();
}

class _StarReviewWidgetState extends State<StarReviewWidget> {
  int currentStar = 5;
  final int maxStarLength = 5;

  void _onTapStar(int index) {
    setState(() {
      currentStar = index + 1;
    });
    widget.reviewStarCallback(currentStar);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(
        maxStarLength,
        (index) => InkWell(
          onTap: () => _onTapStar(index),
          child: index < currentStar
              ? Icon(
                  Icons.star,
                  color: MyAppTheme.primary,
                )
              : Icon(
                  Icons.star_border,
                  color: MyAppTheme.primary,
                ),
        ),
      ),
    );
  }
}
