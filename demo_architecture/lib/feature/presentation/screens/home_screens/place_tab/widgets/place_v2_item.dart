import 'package:flutter/material.dart';

import '../../../../../domain/entities/place/place_entity.dart';
import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';
import '../place_detail_screen.dart';
import 'avg_star_review.dart';

class PlaceV2Item extends StatelessWidget {
  final PlaceEntity place;

  const PlaceV2Item({Key key, this.place}) : super(key: key);

  void _openPlaceDetail(BuildContext context) {
    Navigator.of(context).pushNamed(
      PlaceDetailScreen.routeName,
      arguments: ArgsPlaceDetailScreen(
        heroTag: place.id + 'v2',
        imageUrl: place.photo[0],
        placeItem: place,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final d = size.width / 2 - 26;
    return GestureDetector(
      onTap: () => _openPlaceDetail(context),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: d,
            width: d,
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: MyAppTheme.grey_light,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.8),
                  blurRadius: 4,
                  offset: Offset(0, 4), // changes position of shadow
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  place.name,
                  style: TextStyle(
                    fontSize: MyTextStyle.title_5_fontsize,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      '${place.total_rating} reviews',
                      style: TextStyle(fontWeight: FontWeight.w300),
                    ),
                    AVGStarReviewWidget(
                      rating: place.rating,
                      fontSize: MyTextStyle.paragraph_fontsize,
                    ),
                  ],
                )
              ],
            ),
          ),
          Positioned(
            bottom: -10,
            child: Hero(
              tag: place.id + 'v2',
              child: Container(
                width: d,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: d / 1.6,
                      width: d / 1.25,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        image: DecorationImage(
                          image: NetworkImage(place.photo[0]),
                          fit: BoxFit.cover,
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.8),
                            blurRadius: 4,
                            offset: Offset(0, 4), // changes position of shadow
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
