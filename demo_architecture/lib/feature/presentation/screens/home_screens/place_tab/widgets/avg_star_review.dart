import 'package:flutter/material.dart';

import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';

class AVGStarReviewWidget extends StatelessWidget {
  final double rating;
  final int totalRating;
  final Color color;
  final double fontSize;

  const AVGStarReviewWidget({
    Key key,
    @required this.rating,
    this.totalRating,
    this.color,
    this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          children: [
            Text(
              rating.toString(),
              style: TextStyle(
                fontSize: this.fontSize == null
                    ? MyTextStyle.title_4_fontsize
                    : this.fontSize,
                color: this.color,
              ),
            ),
            SizedBox(
              width: 2,
            ),
            Icon(
              Icons.star,
              color: MyAppTheme.primary,
            ),
          ],
        ),
        if (totalRating != null)
          Text(
            '$totalRating reviews',
          ),
      ],
    );
  }
}
