import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../injection_container.dart';
import '../../../../../domain/usecase/places/comment_place_uc.dart';
import '../../../../bloc/place_blocs/comment_place/comment_in_place_bloc.dart';
import '../../../../bloc/place_blocs/get_comment_place/comment_place_bloc.dart';
import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';
import '../../../../widgets/generals/circular_loading.dart';
import 'star_review.dart';

class CommentDialog extends StatefulWidget {
  final String idPlace;
  final GetCommentPlaceBloc getCommentBloc;

  const CommentDialog({Key key, this.idPlace, this.getCommentBloc})
      : super(key: key);

  @override
  _CommentDialogState createState() => _CommentDialogState();
}

class _CommentDialogState extends State<CommentDialog> {
  int _currentStar = 5;
  String _comment;
  CommentInPlaceBloc _commentPlaceBloc;

  void _updateStar(int star) {
    this._currentStar = star;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _commentPlaceBloc = sl<CommentInPlaceBloc>();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SimpleDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      title: Align(
        child: Text(
          'What do you think?',
          style: TextStyle(
            fontSize: MyTextStyle.title_4_fontsize,
          ),
        ),
        alignment: Alignment.center,
      ),
      children: [
        BlocListener(
          bloc: _commentPlaceBloc,
          listener: (context, state) {
            if (state is CommentInPlaceLoaded) {
              widget.getCommentBloc.add(GetCommentPlaceEvent(widget.idPlace));
              Navigator.of(context).pop();
            }
          },
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                StarReviewWidget(
                  reviewStarCallback: _updateStar,
                ),
                SizedBox(
                  height: 16,
                ),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'write comment ...',
                    hintStyle: TextStyle(color: MyAppTheme.black_grey),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                  onChanged: (value) {
                    _comment = value;
                  },
                  minLines: 2,
                  maxLines: 3,
                ),
                SizedBox(
                  height: 10,
                ),
                BlocBuilder<CommentInPlaceBloc, CommentInPlaceState>(
                  bloc: _commentPlaceBloc,
                  builder: (context, state) {
                    if (state is CommentInPlaceLoading) {
                      return CircularLoading();
                    }
                    return Container(
                      width: size.width,
                      child: TextButton(
                        onPressed: () {
                          _commentPlaceBloc.add(
                            CommentEvent(
                                commentPlaceParams: CommentPlaceParams(
                              id: widget.idPlace,
                              rating: _currentStar,
                              comment: _comment,
                            )),
                          );
                        },
                        style:
                            TextButton.styleFrom(primary: MyAppTheme.primary),
                        child: Text(
                          'Send',
                          style: TextStyle(color: MyAppTheme.primary),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
