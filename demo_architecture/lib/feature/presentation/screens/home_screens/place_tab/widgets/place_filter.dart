import 'package:flutter/material.dart';

import '../../../../themes/colors.dart';
import '../../../../themes/text_style.dart';
import '../../widgets/filter_action_chip.dart';

enum FilterType {
  categories,
  sortBy,
  sort,
}

class PlaceFilter extends StatefulWidget {
  final Function(String filter, FilterType filterType) filterCallback;

  const PlaceFilter({Key key, this.filterCallback}) : super(key: key);

  @override
  _PlaceFilterState createState() => _PlaceFilterState();
}

class _PlaceFilterState extends State<PlaceFilter>
    with AutomaticKeepAliveClientMixin {
  final categories = [
    {'icon': Icons.category, 'name': 'All', 'type': null},
    {'icon': Icons.restaurant, 'name': 'Restaurant', 'type': 'restaurant'},
    {'icon': Icons.wine_bar_rounded, 'name': 'Bar', 'type': 'bar'},
    {'icon': Icons.local_bar, 'name': 'Coffee', 'type': 'cafe'},
    {
      'icon': Icons.local_mall,
      'name': 'Shopping Mall',
      'type': 'shopping_mall'
    },
  ];

  final sortBy = [
    {'icon': Icons.sort, 'name': 'All', 'type': null},
    {'icon': Icons.drive_file_rename_outline, 'name': 'Name', 'type': 'name'},
    {'icon': Icons.star, 'name': 'Rating', 'type': 'rating'},
  ];

  final sort = [
    {'icon': Icons.arrow_circle_down_outlined, 'name': 'DEC', 'type': '-1'},
    {'icon': Icons.arrow_circle_up, 'name': 'ASC', 'type': '1'},
  ];

  int categoriesIndex = -1;
  int sortByIndex = -1;
  int sortIndex = 0;

  void _onCategoriesFilter() async {
    final filter = await showModalBottomSheet(
      context: context,
      builder: (context) {
        return getModalBottomSheet(
          title: 'Categories',
          titleIconData: Icons.category,
          data: categories,
          onTap: (index) {
            setState(() {
              categoriesIndex = index;
            });
          },
        );
      },
    );
    if (filter != null && filter)
      widget.filterCallback(
        categories[categoriesIndex]['type'],
        FilterType.categories,
      );
  }

  void _onSortByFilter() async {
    final filter = await showModalBottomSheet(
      context: context,
      builder: (context) {
        return getModalBottomSheet(
          title: 'Sort By',
          titleIconData: Icons.sort,
          data: sortBy,
          onTap: (index) {
            setState(() {
              sortByIndex = index;
            });
          },
        );
      },
    );
    if (filter != null && filter)
      widget.filterCallback(
        sortBy[sortByIndex]['type'],
        FilterType.sortBy,
      );
  }

  void _onSortFilter() async {
    if (sortByIndex <= 0) return;

    final filter = await showModalBottomSheet(
      context: context,
      builder: (context) {
        return getModalBottomSheet(
          title: 'Sort',
          titleIconData: Icons.sort_by_alpha,
          data: sort,
          onTap: (index) {
            setState(() {
              sortIndex = index;
            });
          },
        );
      },
    );
    if (filter != null && filter)
      widget.filterCallback(
        sort[sortIndex]['type'],
        FilterType.sort,
      );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          const SizedBox(width: 16),
          FilterActionChip(
            label: categoriesIndex <= 0
                ? 'Categories'
                : categories[categoriesIndex]['name'],
            iconData: Icons.category,
            bgColor: categoriesIndex <= 0 ? null : MyAppTheme.primary,
            onTap: _onCategoriesFilter,
          ),
          FilterActionChip(
            label: sortByIndex <= 0 ? 'Sort By' : sortBy[sortByIndex]['name'],
            iconData: Icons.sort,
            bgColor: sortByIndex <= 0 ? null : MyAppTheme.primary,
            onTap: _onSortByFilter,
          ),
          FilterActionChip(
            label: sortIndex < 0 ? 'Dec' : sort[sortIndex]['name'],
            iconData: sort[sortIndex]['icon'],
            bgColor:
                sortByIndex <= 0 ? MyAppTheme.black_grey : MyAppTheme.primary,
            onTap: sortByIndex > 0 ? _onSortFilter : _onSortFilter,
          ),
          const SizedBox(width: 16),
        ],
      ),
    );
  }

  getModalBottomSheet({
    String title,
    IconData titleIconData,
    List<Map<String, Object>> data,
    Function(int index) onTap,
  }) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                titleIconData,
                color: MyAppTheme.primary,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: MyTextStyle.title_4_fontsize,
                  color: MyAppTheme.primary,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            height: 1,
          ),
          Column(
            children: List.generate(
              data.length,
              (index) => ListTile(
                leading: Icon(data[index]['icon']),
                title: Text(
                  data[index]['name'],
                ),
                trailing: Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 16,
                ),
                contentPadding: EdgeInsets.only(left: 10, right: 10),
                onTap: () {
                  onTap(index);
                  Navigator.of(context).pop(true);
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
