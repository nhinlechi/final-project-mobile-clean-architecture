import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../../injection_container.dart';
import '../../../../data/models/place/comment_place_model.dart';
import '../../../../data/models/place/coordinates.dart';
import '../../../../domain/entities/place/place_entity.dart';
import '../../../bloc/place_blocs/get_comment_place/comment_place_bloc.dart';
import '../../../repository_providers/place_comments_repository_provider.dart';
import '../../../themes/colors.dart';
import '../../../themes/text_style.dart';
import '../../../widgets/generals/circle_button.dart';
import 'place_map_screen.dart';
import 'widgets/avg_star_review.dart';
import 'widgets/comment_dialog.dart';

// This useful for pass argument (data) to this screen;
class ArgsPlaceDetailScreen {
  final PlaceEntity placeItem;
  final String imageUrl;
  final String heroTag;

  ArgsPlaceDetailScreen({
    @required this.placeItem,
    @required this.imageUrl,
    @required this.heroTag,
  });
}

class PlaceDetailScreen extends StatefulWidget {
  static const routeName = 'place-detail-screen';

  final ArgsPlaceDetailScreen args;

  PlaceDetailScreen({Key key, @required this.args}) : super(key: key);

  @override
  _PlaceDetailScreenState createState() => _PlaceDetailScreenState();
}

class _PlaceDetailScreenState extends State<PlaceDetailScreen> {
  GetCommentPlaceBloc _getCommentPlaceBloc;
  PlaceCommentsRP _placeCommentsRP;

  void _goToMap(BuildContext context, PlaceEntity place) {
    Navigator.of(context).pushNamed(
      PlaceMapScreen.routeName,
      arguments: ArgsPlaceMapScreen(
        placeLocation: LatLng(place.location.lat, place.location.long),
        placeTitle: place.name,
      ),
    );
  }

  void _reviewPlace(context, String id, GetCommentPlaceBloc bloc) {
    showDialog(
      context: context,
      builder: (context) => CommentDialog(idPlace: id, getCommentBloc: bloc),
    );
  }

  @override
  void initState() {
    super.initState();
    _getCommentPlaceBloc = sl<GetCommentPlaceBloc>();
    _placeCommentsRP = sl<PlaceCommentsRP>();
    if (_placeCommentsRP.placeComments[widget.args.placeItem.id] == null)
      _getCommentPlaceBloc.add(
        GetCommentPlaceEvent(this.widget.args.placeItem.id),
      );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final double placeImageHeight = size.height * 2 / 5;
    final double sizedBoxHeight = placeImageHeight - 30;
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final double backButtonContainerHeight = 30;
    return Scaffold(
      body: Container(
        height: size.height,
        color: MyAppTheme.secondary,
        child: Stack(
          children: [
            Hero(
              tag: widget.args.heroTag,
              child: Container(
                height: placeImageHeight,
                width: size.width,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(this.widget.args.imageUrl),
                        fit: BoxFit.cover)),
              ),
            ),
            SingleChildScrollView(
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(
                        height: statusBarHeight,
                      ),
                      Container(
                        height: backButtonContainerHeight,
                        alignment: Alignment.centerLeft,
                        child: CircleButton(
                          icon: Icons.arrow_back,
                          iconColor: MyAppTheme.secondary,
                          backgroundColor:
                              MyAppTheme.secondary.withOpacity(0.4),
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                        ),
                      ),
                      SizedBox(
                        height: sizedBoxHeight -
                            statusBarHeight -
                            backButtonContainerHeight,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(20)),
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 40,
                            bottom: 20,
                            right: 20,
                            left: 20,
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  LimitedBox(
                                    maxWidth: size.width * 3 / 5,
                                    child: Text(
                                      widget.args.placeItem.name,
                                      style: TextStyle(
                                        fontSize: MyTextStyle.title_4_fontsize,
                                        fontWeight: FontWeight.w500,
                                      ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  BlocBuilder<GetCommentPlaceBloc,
                                      GetCommentPlaceState>(
                                    bloc: _getCommentPlaceBloc,
                                    builder: (context, state) {
                                      if (state is GetCommentPlaceLoaded) {
                                        // Get from bloc
                                        return AVGStarReviewWidget(
                                          rating: state
                                              .listCommentPlaceModel.avgRating,
                                          totalRating: state
                                              .listCommentPlaceModel
                                              .ratingTotal,
                                        );
                                      }
                                      // Get from provider
                                      final model =
                                          _placeCommentsRP.placeComments[
                                              widget.args.placeItem.id];
                                      if (model != null) {
                                        return AVGStarReviewWidget(
                                          rating: model.avgRating,
                                          totalRating: model.ratingTotal,
                                        );
                                      }
                                      // Get AVG Star from place item
                                      return AVGStarReviewWidget(
                                        rating: widget.args.placeItem.rating,
                                        totalRating:
                                            widget.args.placeItem.total_rating,
                                      );
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  LimitedBox(
                                    maxWidth: size.width * 3 / 4,
                                    child: Text(
                                      widget.args.placeItem.address,
                                      style: TextStyle(
                                        fontSize: MyTextStyle.title_5_fontsize,
                                        color: Colors.black.withOpacity(0.7),
                                      ),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Icon(
                                    Icons.copy,
                                    color: MyAppTheme.primary,
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: [
                                  Flexible(
                                    child: Divider(
                                      height: 1,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text(
                                    'Reviews',
                                    style: TextStyle(
                                      fontSize: MyTextStyle.title_4_fontsize,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Flexible(
                                    child: Divider(),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              BlocBuilder<GetCommentPlaceBloc,
                                  GetCommentPlaceState>(
                                bloc: _getCommentPlaceBloc,
                                builder: (context, state) {
                                  if (state is GetCommentPlaceLoading ||
                                      state is GetCommentPlaceInitial) {
                                    // Load list from repos provider
                                    final placeCommentsModel =
                                        _placeCommentsRP.placeComments[
                                            widget.args.placeItem.id];
                                    if (placeCommentsModel != null) {
                                      return getListCommentsWidget(
                                          placeCommentsModel.listCommentPlace);
                                    }
                                    // Return Loading effect
                                    return Container(
                                      height: size.height / 4,
                                      child: Center(
                                        child: CircularProgressIndicator(),
                                      ),
                                    );
                                  }
                                  if (state is GetCommentPlaceLoaded) {
                                    // Return from bloc
                                    final placeComments = state
                                        .listCommentPlaceModel.listCommentPlace;
                                    return getListCommentsWidget(placeComments);
                                  }
                                  return Container();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    top: sizedBoxHeight - 25,
                    right: 20,
                    child: Row(
                      children: [
                        CircleButton(
                          onTap: () => _reviewPlace(context,
                              widget.args.placeItem.id, _getCommentPlaceBloc),
                          icon: Icons.edit,
                          iconColor: MyAppTheme.primary,
                          backgroundColor: Colors.white,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        CircleButton(
                          onTap: () => _goToMap(
                            context,
                            widget.args.placeItem,
                          ),
                          icon: Icons.location_on,
                          iconColor: Colors.white,
                          backgroundColor: MyAppTheme.primary,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getListCommentsWidget(List<CommentPlaceModel> placeComments) {
    return Column(
      children: List.generate(placeComments.length, (index) {
        return ListTile(
          contentPadding: EdgeInsets.only(
              bottom: placeComments[index].comment != null ? 10 : 0),
          leading: CircleAvatar(
            backgroundImage: NetworkImage(
              placeComments[index].avatar,
            ),
          ),
          title: Text(
            '${placeComments[index].firstName} ${placeComments[index].lastName}',
            style: TextStyle(
              fontSize: MyTextStyle.title_5_fontsize,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: List.generate(
                  placeComments[index].rating,
                  (index) => Icon(
                    Icons.star,
                    size: 14,
                    color: MyAppTheme.primary,
                  ),
                ),
              ),
              if (placeComments[index].comment != null)
                Text(
                  placeComments[index].comment,
                ),
            ],
          ),
          trailing: Icon(Icons.more_vert),
        );
      }),
    );
  }
}
