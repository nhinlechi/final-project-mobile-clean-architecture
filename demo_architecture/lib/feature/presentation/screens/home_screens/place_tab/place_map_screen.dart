import 'dart:async';

import 'package:demo_architecture/core/firebase/api_keys.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:demo_architecture/feature/presentation/widgets/dialogs.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ArgsPlaceMapScreen {
  final String placeTitle;
  final LatLng placeLocation;

  ArgsPlaceMapScreen({
    @required this.placeLocation,
    @required this.placeTitle,
  });
}

class PlaceMapScreen extends StatefulWidget {
  static const routeName = 'place-map-screen';

  final ArgsPlaceMapScreen args;

  PlaceMapScreen({
    Key key,
    @required this.args,
  }) : super(key: key);

  @override
  _PlaceMapScreenState createState() => _PlaceMapScreenState();
}

class _PlaceMapScreenState extends State<PlaceMapScreen> {
  final defaultLocation = LatLng(10.8231, 106.6297);
  final polylinePoints = PolylinePoints();
  final polylines = Set<Polyline>();
  LocationPermission permission;
  LatLng _myLocation;

  final Completer<GoogleMapController> _mapController = Completer();

  Future<Position> _getCurrentPosition() async {
    // Test permission
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever ||
          permission == LocationPermission.denied) {
        await showCustomFailedDialog(
            context, 'Get location permission failed!');
        return null;
      }
    }

    // Get current location
    var pos = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    setState(() {
      _myLocation = LatLng(pos.latitude, pos.longitude);
    });
    final GoogleMapController controller = await _mapController.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: kDebugMode
              ? defaultLocation
              : LatLng(pos.latitude, pos.longitude),
          zoom: 20,
        ),
      ),
    );
  }

  void directToPlace() async {
    final pos = await _getCurrentPosition();
    //
    final result = await polylinePoints.getRouteBetweenCoordinates(
      APIKeys.googleAPIKey,
      kDebugMode
          ? PointLatLng(
              defaultLocation.latitude,
              defaultLocation.longitude,
            )
          : PointLatLng(
              _myLocation.latitude,
              _myLocation.longitude,
            ),
      PointLatLng(
        widget.args.placeLocation.latitude,
        widget.args.placeLocation.longitude,
      ),
    );
    setState(() {
      polylines.add(
        Polyline(
          polylineId: PolylineId('place-direction'),
          points: result.points
              .map(
                (p) => LatLng(p.latitude, p.longitude),
              )
              .toList(),
          color: Colors.blue,
          jointType: JointType.round,
        ),
      );
    });
    print(result.points);
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          getGoogleMap(),
          getPlaceDirection(size),
        ],
      ),
      floatingActionButton: getFloatingActionButton(),
    );
  }

  getGoogleMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: widget.args.placeLocation,
        zoom: 13,
      ),
      onMapCreated: (controller) {
        _mapController.complete(controller);
      },
      polylines: polylines,
      markers: <Marker>{
        if(_myLocation != null) Marker(
          markerId: MarkerId('current-location'),
          position: kDebugMode ? defaultLocation : _myLocation,
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        ),
        Marker(
          markerId: MarkerId('place-location'),
          position: widget.args.placeLocation,
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
        ),
      },
      mapToolbarEnabled: false,
      trafficEnabled: false,
      zoomControlsEnabled: false,
    );
  }

  getPlaceDirection(Size size) {
    return Column(
      children: [
        Container(
          width: size.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: MyAppTheme.secondary,
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 4),
                color: MyAppTheme.grey,
                blurRadius: 8,
              )
            ],
          ),
          padding: const EdgeInsets.all(16),
          margin: const EdgeInsets.fromLTRB(8, 32, 8, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getLocationField(
                Icon(
                  Icons.my_location,
                  color: Colors.blue,
                ),
                'My Location',
              ),
              const SizedBox(height: 8),
              Icon(Icons.more_vert),
              const SizedBox(height: 8),
              getLocationField(
                Icon(
                  Icons.location_on,
                  color: MyAppTheme.primary,
                ),
                widget.args.placeTitle,
              ),
            ],
          ),
        ),
      ],
    );
  }

  getLocationField(Widget leading, String title) {
    return Row(
      children: [
        leading,
        const SizedBox(width: 16),
        Text(
          title,
          style: TextStyle(
            fontSize: MyTextStyle.title_5_fontsize,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }

  getFloatingActionButton() {
    return FloatingActionButton(
      onPressed: directToPlace,
      child: Icon(
        Icons.directions,
        color: MyAppTheme.secondary,
      ),
      backgroundColor: MyAppTheme.primary,
    );
  }
}
