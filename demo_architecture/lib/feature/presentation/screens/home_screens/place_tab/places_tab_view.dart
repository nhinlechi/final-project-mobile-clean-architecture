import 'dart:developer';

import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/presentation/bloc/place_blocs/place_scroll_bloc/place_scroll_bloc.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../injection_container.dart';
import '../../../../domain/usecase/places/get_places_uc.dart';
import '../../../bloc/place_blocs/get_places_bloc/get_places_bloc.dart';
import '../../../widgets/generals/circular_loading.dart';
import '../../../widgets/generals/search_bar_widget.dart';
import 'widgets/place_v2_item.dart';
import '../search_screen.dart';
import 'widgets/place_filter.dart';

class PlacesTabView extends StatefulWidget {
  @override
  _PlacesTabViewState createState() => _PlacesTabViewState();
}

class _PlacesTabViewState extends State<PlacesTabView>
    with AutomaticKeepAliveClientMixin {
  //
  final heroTagSearchBar = 'search-place-bar';

  // filter
  String categoryType;
  String sortByType;
  String sortType = '-1';

  // search text
  final _textController = TextEditingController();
  GetPlacesParams _params = GetPlacesParams(rowsPerPage: 16, page: 1);

  // Bloc
  GetPlacesBloc _getPlacesBloc;
  List<PlaceEntity> places = [];
  int currentPage = 1;

  // Scroll controller
  final _scrollController = ScrollController();

  Future<void> _openSearchScreen() async {
    final text = await Navigator.of(context).pushNamed(
      SearchScreen.routeName,
      arguments: ArgsSearchScreen(
        searchText: _textController.text,
        heroTag: heroTagSearchBar,
      ),
    );
    setState(() {
      this._textController.text = text;
    });
    // Http Request get place
    _getPlaces();
  }

  void _filterCallback(String filter, FilterType type) {
    setState(() {
      switch (type) {
        case FilterType.categories:
          categoryType = filter;
          break;
        case FilterType.sortBy:
          sortByType = filter;
          break;
        case FilterType.sort:
          sortType = filter;
          break;
      }
    });
    _getPlaces();
  }

  void _getPlaces() {
    _params = GetPlacesParams(
      searchText: _textController.text,
      page: 1,
      rowsPerPage: 16,
      type: categoryType,
      sortBy: sortByType,
      sort: int.tryParse(sortType),
    );
    // Reset current page
    currentPage = 1;
    // Reset places
    places = [];
    // Get Places
    _getPlacesBloc.add(GetPlaces(params: _params));
  }

  void _scrollOnTop() {
    _scrollController.animateTo(
      _scrollController.position.minScrollExtent,
      duration: Duration(
        milliseconds: 500,
      ),
      curve: Curves.bounceIn,
    );
  }

  void _scrollListener() {
    //
    if (_scrollController.position.extentBefore > 700) {
      // Add Event Scroll to bloc
      context.read<PlaceScrollBloc>().add(PlaceScrollDown());
    } else {
      // Add Event Scroll to bloc
      context.read<PlaceScrollBloc>().add(PlaceScrollToTop());
    }
    //
    if (_getPlacesBloc.state is GetPlacesLoading) return;
    if (_scrollController.position.extentAfter == 0.0) {
      _getPlacesBloc.add(
        GetPlaces(
          params: GetPlacesParams(
            page: ++currentPage,
            rowsPerPage: 16,
            type: categoryType,
            sortBy: sortByType,
            sort: int.tryParse(sortType),
            searchText: _textController.text,
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    // Initial Bloc
    _getPlacesBloc = sl<GetPlacesBloc>();
    // Init scroller listener
    _scrollController.addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  SearchBarWidget(
                    heroTag: heroTagSearchBar,
                    onTap: _openSearchScreen,
                    controller: _textController,
                    readOnly: true,
                  ),
                  PlaceFilter(
                    filterCallback: _filterCallback,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
            ),
            BlocBuilder<GetPlacesBloc, GetPlacesState>(
              bloc: _getPlacesBloc,
              builder: (context, state) {
                if (state is GetPlacesInitial) {
                  _getPlacesBloc.add(
                    GetPlaces(
                      params: GetPlacesParams(
                        page: 1,
                        rowsPerPage: 10,
                      ),
                    ),
                  );
                }
                if (state is GetPlacesSuccess) {
                  places.addAll(state.placesEntity.places);
                }
                return SliverPadding(
                  padding: const EdgeInsets.only(left: 16, bottom: 40),
                  sliver: SliverGrid(
                    gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: size.width / 2,
                      mainAxisSpacing: 40,
                    ),
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return PlaceV2Item(
                          key: Key(places[index].id),
                          place: places[index],
                        );
                      },
                      childCount: places.length,
                    ),
                  ),
                );
              },
            ),
            SliverPadding(
              padding: const EdgeInsets.only(bottom: 40),
              sliver: SliverList(
                delegate: SliverChildListDelegate([
                  CircularLoading(),
                ]),
              ),
            ),
          ],
        ),
        BlocBuilder<PlaceScrollBloc, PlaceScrollState>(
          builder: (context, state) {
            if (state is PlaceScrollDownState)
              return Positioned(
                bottom: 10,
                right: 10,
                child: ElevatedButton(
                  onPressed: _scrollOnTop,
                  style: ElevatedButton.styleFrom(
                    shape: CircleBorder(),
                    primary: MyAppTheme.primary,
                  ),
                  child: Icon(
                    Icons.arrow_upward,
                    color: MyAppTheme.secondary,
                  ),
                ),
              );
            return Container();
          },
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
