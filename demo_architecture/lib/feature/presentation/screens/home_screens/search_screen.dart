import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../themes/colors.dart';
import '../../widgets/generals/search_bar_widget.dart';

class ArgsSearchScreen {
  final String searchText;
  final String heroTag;

  ArgsSearchScreen({this.searchText, this.heroTag});
}

class SearchScreen extends StatefulWidget {
  static const routeName = 'search-screen';

  final String searchText;
  final String heroTag;

  const SearchScreen({Key key, this.searchText, this.heroTag})
      : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final _focusNode = FocusNode();
  final _textController = TextEditingController();

  void _onSearch() {
    Navigator.of(context).pop(_textController.text);
  }

  void _onChange(String value) {}

  @override
  void initState() {
    super.initState();
    // Init search text
    _textController.text = widget.searchText;
    // Control focus node
    Future.delayed(Duration(milliseconds: 350), () {
      _focusNode.requestFocus();
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final statusBarHeight = MediaQuery.of(context).padding.top;
    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(_textController.text);
        return true;
      },
      child: Scaffold(
        backgroundColor: MyAppTheme.secondary,
        body: Column(
          children: [
            SizedBox(
              height: statusBarHeight + 20,
            ),
            SearchBarWidget(
              heroTag: widget.heroTag,
              onChange: _onChange,
              onSearch: (_) => _onSearch(),
              controller: _textController,
              focusNode: _focusNode,
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyAppTheme.primary,
          child: Icon(
            Feather.arrow_right,
            color: MyAppTheme.secondary,
          ),
          onPressed: _onSearch,
        ),
      ),
    );
  }
}
