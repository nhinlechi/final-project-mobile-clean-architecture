import 'dart:developer';

import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../../themes/text_style.dart';

class RegisterBirthDatePage extends StatelessWidget {
  final TextEditingController dateController;
  final TextEditingController monthController;
  final TextEditingController yearController;
  final GlobalKey<FormState> formKey;

  const RegisterBirthDatePage(
      {Key key,
      @required this.dateController,
      @required this.monthController,
      @required this.yearController,
      @required this.formKey})
      : super(key: key);

  void _pickDate(BuildContext context) async {
    final now = DateTime.now();
    final lastDate = DateTime(now.year - 18, now.month, now.day);
    final pickDate = await showDatePicker(
      context: context,
      initialDate: lastDate,
      firstDate: DateTime(now.year - 100),
      lastDate: lastDate,
    );
    if (pickDate != null) {
      dateController.text = pickDate.day.toString();
      monthController.text = pickDate.month.toString();
      yearController.text = pickDate.year.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'When is your birthday?',
              style: MyTextStyle.ts_register_title,
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: size.width / 3 - 20,
                  child: TextFormField(
                    controller: dateController,
                    validator: (value) {
                      if (value.length == 0) {
                        return "Sai ngày";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(20),
                      labelText: 'Day',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    readOnly: true,
                    onTap: () => _pickDate(context),
                  ),
                ),
                SizedBox(
                  width: size.width / 3 - 20,
                  child: TextFormField(
                    controller: monthController,
                    validator: (value) {
                      if (value.length == 0 ||
                          int.parse(value) < 1 ||
                          int.parse(value) > 12) {
                        return "Sai tháng";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(20),
                      labelText: 'Month',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    readOnly: true,
                    onTap: () => _pickDate(context),
                  ),
                ),
                SizedBox(
                  width: size.width / 3 - 20,
                  child: TextFormField(
                    controller: yearController,
                    validator: (value) {
                      if (value.length == 0) {
                        return "Sai năm";
                      }
                      final year = int.parse(value);
                      final dateNow = new DateTime.now();
                      log("age: " + (dateNow.year - year).toString());

                      if (dateNow.year - year < 18) {
                        return "Dưới 18 tuổi";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(20),
                      labelText: 'Year',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    readOnly: true,
                    onTap: () => _pickDate(context),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            ElevatedButton(
              onPressed: () => _pickDate(context),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    Feather.calendar,
                    color: MyAppTheme.secondary,
                  ),
                  const SizedBox(width: 8),
                  Text(
                    'Pick your birthday',
                    style: TextStyle(
                      color: MyAppTheme.secondary,
                    ),
                  ),
                ],
              ),
              style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 8,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
