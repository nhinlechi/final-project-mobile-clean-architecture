import 'package:flutter/material.dart';

import '../../themes/text_style.dart';

class RegisterNamePage extends StatelessWidget {
  final TextEditingController firstNameController;
  final TextEditingController lastNameController;
  final GlobalKey<FormState> formKey;

  const RegisterNamePage({
    Key key,
    this.firstNameController,
    this.lastNameController,
    this.formKey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return Container(
      width: size.width,
      child: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Hey there!',
              style: MyTextStyle.ts_register_title,
            ),
            Text(
              'What\'s your name?',
              style: MyTextStyle.ts_register_title,
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: firstNameController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please input first name';
                }
                return null;
              },
              decoration: InputDecoration(
                hintText: 'First Name',
                contentPadding: EdgeInsets.all(15),
                labelText: 'First Name',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: lastNameController,
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please input last name';
                }
                return null;
              },
              decoration: InputDecoration(
                hintText: 'Last Name',
                labelText: 'Last Name',
                contentPadding: EdgeInsets.all(15),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
