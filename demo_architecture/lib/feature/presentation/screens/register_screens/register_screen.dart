import 'dart:developer';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:demo_architecture/feature/presentation/pages/home_page.dart';
import 'package:demo_architecture/feature/presentation/widgets/generals/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';

import '../../../../injection_container.dart';
import '../../../domain/usecase/user/register_uc.dart';
import '../../../domain/usecase/user/update_user_inform_uc.dart';
import '../../bloc/authentication_blocs/register/register_bloc.dart';
import '../../bloc/favorite_bloc/get_list_favorite/get_list_favorite_bloc.dart';
import '../../themes/colors.dart';
import 'register_birth_date_page.dart';
import 'register_favorites_page.dart';
import 'register_gender_page.dart';
import 'register_name_page.dart';
import 'register_photo_page.dart';

class RegisterScreen extends StatefulWidget {
  final String email;
  final String password;
  final bool isSocialRegister;

  const RegisterScreen(
      {Key key, this.email, this.password, this.isSocialRegister})
      : super(key: key);

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  CarouselController _buttonCarouselController = CarouselController();
  final int _maxPages = 5;
  int _pageIndex = 0;

  // Data for register
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _birthDayDateController = TextEditingController();
  final _birthDayMonthController = TextEditingController();
  final _birthDayYearController = TextEditingController();
  int _gender;
  int _relationship;
  File _avatar;
  List<String> _idListFavorite;
  final formKeyName = GlobalKey<FormState>();
  final formKeyBirthday = GlobalKey<FormState>();

  // Register Bloc for email, password, firstName, lastName
  RegisterBloc _registerBloc;
  GetListFavoriteBloc _getListFavoriteBloc;

  // Callback to get data from child widget
  void _getGender(int gender) {
    _gender = gender ?? "1";
  }

  void _getRelationship(int relationship) {
    _relationship = relationship ?? "1";
  }

  void _getAvatarHandler(File avt) {
    setState(() {
      _avatar = avt;
    });
  }

  void _getListFavorite(List<String> listFavorite) {
    _idListFavorite = listFavorite;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = sl<RegisterBloc>();
    _getListFavoriteBloc = sl<GetListFavoriteBloc>();
    _getListFavoriteBloc.add(GetFavoriteRegister());
  }

  void _onPressBack() async {
    if (_pageIndex > 0) {
      setState(() {
        _pageIndex--;
      });
      await _buttonCarouselController.animateToPage(
        _pageIndex,
        duration: Duration(milliseconds: 300),
      );
    }
  }

  void _onPressNext() async {
    if (_pageIndex < _maxPages - 1) {
      log("page index " + _pageIndex.toString());
      bool flagValidate = false;
      if (_pageIndex == 0 && formKeyName.currentState.validate())
        flagValidate = true;
      else if (_pageIndex == 1 && formKeyBirthday.currentState.validate())
        flagValidate = true;
      if (_pageIndex > 1) flagValidate = true;
      if (flagValidate == true) {
        setState(() {
          _pageIndex++;
        });
        await _buttonCarouselController.animateToPage(
          _pageIndex,
          duration: Duration(milliseconds: 300),
        );
      }
    } else {
      _loginSuccess();
    }
  }

  void _loginSuccess() async {
    DateTime dateTime = DateTime(
        int.parse(_birthDayYearController.text ?? "1970"),
        int.parse(_birthDayMonthController.text ?? "1"),
        int.parse(_birthDayDateController.text ?? "1"));
    if (widget.isSocialRegister == false)
      _registerBloc.add(RegisterSubmited(ParamsRegister(
          email: widget.email,
          password: widget.password,
          updateUserInformParams: UpdateUserInformParams(
              firstname: _firstNameController.text,
              lastname: _lastNameController.text,
              dateofbirth: dateTime.millisecondsSinceEpoch,
              gender: _gender ?? 1,
              relationship: _relationship ?? 1,
              favorites: _idListFavorite ?? [],
              avatar: [_avatar]))));
    else {
      _registerBloc.add(RegisterSocialSubmited(UpdateUserInformParams(
          firstname: _firstNameController.text,
          lastname: _lastNameController.text,
          dateofbirth: dateTime.millisecondsSinceEpoch,
          gender: _gender ?? 1,
          relationship: _relationship ?? 1,
          favorites: _idListFavorite ?? [],
          avatar: [_avatar])));
    }
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return BlocListener<RegisterBloc, RegisterState>(
      bloc: _registerBloc,
      listener: (context, state) {
        if (state is RegisterLoaded) {
          Navigator.of(context).pushNamedAndRemoveUntil(
            HomePage.routeName,
            (route) => false,
          );
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        bloc: _registerBloc,
        builder: (context, state) {
          if (state is RegisterLoading || state is RegisterLoaded) {
            return registerLoading();
          }
          return Scaffold(
            body: SafeArea(
              child: PageView(
                physics: new NeverScrollableScrollPhysics(),
                children: [
                  SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(20),
                          child: Row(
                            children: List.generate(_maxPages, (index) {
                              return Container(
                                margin: EdgeInsets.only(right: 16),
                                height: 10,
                                width: 10,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: index <= _pageIndex
                                      ? MyAppTheme.primary
                                      : MyAppTheme.grey,
                                ),
                              );
                            }),
                          ),
                        ),
                        CarouselSlider(
                          items: [
                            RegisterNamePage(
                              formKey: formKeyName,
                              firstNameController: _firstNameController,
                              lastNameController: _lastNameController,
                            ),
                            RegisterBirthDatePage(
                              formKey: formKeyBirthday,
                              dateController: _birthDayDateController,
                              monthController: _birthDayMonthController,
                              yearController: _birthDayYearController,
                            ),
                            RegisterGenderPage(
                              getGender: _getGender,
                              getRelationship: _getRelationship,
                            ),
                            RegisterPhotoPage(
                              getAvatarCallback: _getAvatarHandler,
                              imageFile: _avatar,
                            ),
                            RegisterFavoritesPage(
                                getListFavoriteBloc: _getListFavoriteBloc,
                                getListFavoriteCallback: _getListFavorite),
                          ],
                          carouselController: _buttonCarouselController,
                          options: CarouselOptions(
                            autoPlay: false,
                            enlargeCenterPage: true,
                            viewportFraction: 0.9,
                            aspectRatio: size.width / (size.height - 100),
                            // TODO: Refactor this code line to flexible height
                            initialPage: 0,
                            scrollPhysics: NeverScrollableScrollPhysics(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            floatingActionButton: Padding(
              padding: EdgeInsets.only(left: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FloatingActionButton(
                    heroTag: 'fab-back',
                    child: Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.white,
                    ),
                    backgroundColor: MyAppTheme.primary,
                    onPressed: _onPressBack,
                  ),
                  FloatingActionButton(
                    heroTag: 'fab-next',
                    child: Icon(
                      Icons.arrow_forward_ios_rounded,
                      color: Colors.white,
                    ),
                    backgroundColor: MyAppTheme.primary,
                    onPressed: _onPressNext,
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget registerLoading() => Scaffold(
        body: Center(
          child: CircularLoading(),
        ),
      );
}
