import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import '../../themes/colors.dart';
import '../../themes/text_style.dart';

class RegisterPhotoPage extends StatelessWidget {
  final File imageFile;
  final Function(File avt) getAvatarCallback;

  RegisterPhotoPage({
    Key key,
    @required this.getAvatarCallback,
    @required this.imageFile,
  }) : super(key: key);

  final picker = ImagePicker();

  Future _onTapAddProfilePhoto() async {
    final pickedFile = await picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 1000,
      maxHeight: 1000,
    );
    if (pickedFile != null) {
      getAvatarCallback(File(pickedFile.path));
    } else {
      print('No image selected.');
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'We are already your fan!',
            style: MyTextStyle.ts_register_title,
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: _onTapAddProfilePhoto,
            child: Container(
              width: size.width / 2,
              height: size.width / 2 * 4 / 3,
              decoration: BoxDecoration(
                color: MyAppTheme.grey,
                borderRadius: BorderRadius.circular(20),
                image: imageFile != null
                    ? DecorationImage(
                        image: FileImage(imageFile),
                        fit: BoxFit.cover,
                      )
                    : null,
              ),
              child: imageFile != null
                  ? Container()
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FloatingActionButton(
                          onPressed: _onTapAddProfilePhoto,
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          backgroundColor: MyAppTheme.primary,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          'Add your profile photo',
                          style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                          ),
                        ),
                      ],
                    ),
            ),
          )
        ],
      ),
    );
  }
}
