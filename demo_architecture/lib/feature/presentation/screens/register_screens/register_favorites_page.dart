import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/favorite_bloc/get_list_favorite/get_list_favorite_bloc.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';

class RegisterFavoritesPage extends StatefulWidget {
  final GetListFavoriteBloc getListFavoriteBloc;
  final Function(List<String> listFavorite) getListFavoriteCallback;

  const RegisterFavoritesPage(
      {Key key, this.getListFavoriteBloc, this.getListFavoriteCallback})
      : super(key: key);

  @override
  _RegisterFavoritesPageState createState() => _RegisterFavoritesPageState();
}

class _RegisterFavoritesPageState extends State<RegisterFavoritesPage> {
  List<String> _idFilters = [];

  @override
  void initState() {
    super.initState();
    widget.getListFavoriteBloc.add(GetFavoriteRegister());
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'You Like what...?',
              style: MyTextStyle.ts_register_title,
            ),
            BlocBuilder<GetListFavoriteBloc, GetListFavoriteState>(
                bloc: widget.getListFavoriteBloc,
                builder: (context, state) {
                  if (state is GetListFavoriteLoaded)
                    return Wrap(
                      spacing: 10,
                      children: List.generate(
                        (state.listFavorite.length < 25) ? state.listFavorite.length : 25,
                        (index) => FilterChip(
                          label: Text(
                            state.listFavorite[index].name,
                          ),
                          labelStyle: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                          selectedColor: MyAppTheme.primary,
                          checkmarkColor: MyAppTheme.secondary,
                          selected: _idFilters.contains(state.listFavorite[index].id),
                          onSelected: (value) {
                            setState(() {
                              if (value) {
                                _idFilters.add(state.listFavorite[index].id);
                              } else {
                                _idFilters.removeWhere(
                                    (id) => id == state.listFavorite[index].id);
                              }
                            });
                            widget.getListFavoriteCallback(_idFilters);
                          },
                        ),
                      ),
                    );
                  return Container();
                }),
            SizedBox(
              height: 16,
            ),
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                primary: MyAppTheme.primary,
              ),
              child: Text(
                'See more...',
                style: TextStyle(
                  color: MyAppTheme.secondary,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
