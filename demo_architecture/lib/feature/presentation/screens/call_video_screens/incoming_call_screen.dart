import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';

import '../../../domain/entities/user/user_inform.dart';
import '../../themes/colors.dart';
import '../../themes/text_style.dart';
import 'call_video_screen.dart';

class InComingCallScreenArgs {
  final String avatar;
  final String fullName;
  final String callerId;

  InComingCallScreenArgs({
    @required this.avatar,
    @required this.fullName,
    @required this.callerId,
  });
}

class IncomingCallScreen extends StatelessWidget {
  static const routeName = 'incoming-call-screen';

  final InComingCallScreenArgs args;

  const IncomingCallScreen({Key key, @required this.args}) : super(key: key);

  void _acceptCall(BuildContext context) {
    Navigator.of(context).popAndPushNamed(
      CallVideoScreen.routeName,
      arguments: ArgsCallVideoScreen(
        isAnswer: true,
        callerId: this.args.callerId,
      ),
    );
  }

  void _rejectCall(BuildContext context) {
    Navigator.of(context).pop(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Column(
            children: [
              AvatarGlow(
                glowColor: MyAppTheme.primary,
                endRadius: 140,
                duration: Duration(seconds: 2),
                showTwoGlows: true,
                repeat: true,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(this.args.avatar),
                  backgroundColor: Colors.grey,
                  radius: 80,
                ),
              ),
              const SizedBox(height: 16),
              Text(
                '${this.args.fullName} is calling',
                style: TextStyle(fontSize: MyTextStyle.title_4_fontsize),
              ),
            ],
          ),
          SizedBox(height: 32),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              getCallKitButton(
                bgColor: MyAppTheme.black_grey,
                iconData: Icons.close,
                onTap: () => _rejectCall(context),
              ),
              getCallKitButton(
                bgColor: MyAppTheme.primary,
                iconData: Icons.call,
                onTap: () => _acceptCall(context),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget getCallKitButton({
    IconData iconData,
    Function() onTap,
    Color bgColor = Colors.orange,
  }) =>
      ElevatedButton(
        onPressed: onTap,
        child: Icon(
          iconData,
          size: 28,
          color: MyAppTheme.secondary,
        ),
        style: ElevatedButton.styleFrom(
          primary: bgColor,
          shape: CircleBorder(),
          padding: EdgeInsets.all(16),
        ),
      );
}
