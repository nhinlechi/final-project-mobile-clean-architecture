import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:provider/provider.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import '../../../../core/socket/signaling.dart';
import '../../../../core/utils/get_token.dart';
import '../../../../injection_container.dart';
import '../../repository_providers/user_repository_provider.dart';
import '../../themes/colors.dart';

class ArgsCallVideoScreen {
  final bool isAnswer;
  final String callerId;
  final String receiverId;

  ArgsCallVideoScreen({this.isAnswer, this.callerId, this.receiverId});
}

class CallVideoScreen extends StatefulWidget {
  static const routeName = 'call-video-screen';

  final ArgsCallVideoScreen args;

  const CallVideoScreen({
    Key key,
    this.args,
  }) : super(key: key);

  @override
  _CallVideoScreenState createState() => _CallVideoScreenState();
}

class _CallVideoScreenState extends State<CallVideoScreen> {
  // RTC video renderer
  final RTCVideoRenderer _localRenderer = RTCVideoRenderer();
  final RTCVideoRenderer _remoteRenderer = RTCVideoRenderer();
  final userIdController = TextEditingController();

  // RTC Connections
  Signaling _signaling;
  bool isEnableVideo = true;
  bool isEnableAudio = true;
  bool isFacingMode = true;

  double _localRendererSize;

  void _initRenderer() async {
    await _localRenderer.initialize();
    await _remoteRenderer.initialize();
  }

  void _initSignaling() async {
    final userInfo = context.read<UserRepositoryProvider>().userInform;
    final token = sl<Token>().getToken();
    final args = this.widget.args;

    if (args.isAnswer) {
      _signaling = Signaling(
        userId: userInfo.userId,
        token: token,
        otherId: args.callerId,
        isCaller: !args.isAnswer,
      )..connect();
    } else {
      _signaling = Signaling(
        userId: userInfo.userId,
        token: token,
        otherId: args.receiverId,
        isCaller: !args.isAnswer,
      )..connect();
    }

    _signaling.onLocalStream = (stream) {
      setState(() {
        _localRenderer.srcObject = stream;
      });
    };

    _signaling.onAddRemoteStream = (stream) {
      setState(() {
        _remoteRenderer.srcObject = stream;
      });
    };

    _signaling.onRemoveRemoteStream = (stream) {
      setState(() {
        _remoteRenderer.srcObject = null;
      });
    };

    _signaling.onCallStateChanged = (state) {
      if (state == CallState.CallStateDisconnected) {
        log('disconnected', name: 'absdfweefwfeoufw');
        _endCall();
      }
    };
  }

  @override
  void initState() {
    super.initState();
    _initRenderer();
    _initSignaling();
  }

  @override
  void dispose() {
    _localRenderer.dispose();
    _remoteRenderer.dispose();
    _signaling.close();
    super.dispose();
  }

  Future<void> _callVideo() async {
    _signaling.startCallVideo();
  }

  void _switchEnableVideo() {
    setState(() {
      isEnableVideo = !isEnableVideo;
      if (isEnableVideo) {
        _localRenderer.srcObject = _signaling.localStream;
      } else {
        _localRenderer.srcObject = null;
      }
    });

    _signaling.switchEnableVideo(isEnableVideo);
  }

  void _switchEnableAudio() {
    setState(() {
      isEnableAudio = !isEnableAudio;
      _signaling.muteMic(isEnableAudio);
    });
  }

  void _switchCamera() {
    setState(() {
      _signaling.switchCamera();
    });
  }

  void _endCall() {
    if (mounted) {
      Navigator.of(context).pop();
    }
  }

  void _switchSizeLocalRenderer(Size size) {
    setState(() {
      if (_localRendererSize == null)
        _localRendererSize = size.height / 3.5;
      else
        _localRendererSize = _localRendererSize == size.height / 5
            ? size.height / 3.5
            : size.height / 5;
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          getRemoteVideoRenderer(size),
          SlidingUpPanel(
            header: Container(
              height: 100,
              width: size.width,
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  getCallKitButton(
                    iconData:
                        isEnableVideo ? Icons.videocam : Icons.videocam_off,
                    onTap: _switchEnableVideo,
                    bgColor: isEnableVideo ? Colors.orange : Colors.grey,
                  ),
                  getCallKitButton(
                    iconData: Icons.flip_camera_ios_rounded,
                    onTap: _switchCamera,
                  ),
                  getCallKitButton(
                    iconData: isEnableAudio
                        ? Icons.keyboard_voice
                        : Icons.keyboard_voice_outlined,
                    onTap: _switchEnableAudio,
                    bgColor: isEnableAudio ? Colors.orange : Colors.grey,
                  ),
                  getCallKitButton(
                    iconData: Icons.call_end,
                    bgColor: Colors.red,
                    onTap: _endCall,
                  ),
                ],
              ),
            ),
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(24.0),
              topRight: Radius.circular(24.0),
            ),
            panel: Center(
              child: Text('This is sliding up panel'),
            ),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  children: [
                    getLocalVideoRenderer(size),
                    SizedBox(
                      height: 120,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget getRemoteVideoRenderer(Size size) {
    return Container(
      color: Colors.grey,
      child: RTCVideoView(
        _remoteRenderer,
        objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
      ),
    );
  }

  Widget getLocalVideoRenderer(Size size) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Padding(
          padding: EdgeInsets.only(right: 16),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: GestureDetector(
              onTap: () => _switchSizeLocalRenderer(size),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 250),
                height: _localRendererSize ?? size.height / 5,
                width: (_localRendererSize ?? size.height / 5) * 10 / 14,
                decoration: BoxDecoration(
                  color: Colors.grey,
                ),
                child: RTCVideoView(
                  _localRenderer,
                  objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget offerAndAnswerButton() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ElevatedButton(
            onPressed: _callVideo,
            child: Text('Offer'),
          ),
        ],
      );

  Widget getCallKitButton({
    IconData iconData,
    Function() onTap,
    Color bgColor = Colors.orange,
  }) =>
      ElevatedButton(
        onPressed: onTap,
        child: Icon(
          iconData,
          size: 28,
          color: MyAppTheme.secondary,
        ),
        style: ElevatedButton.styleFrom(
          primary: bgColor,
          shape: CircleBorder(),
          padding: EdgeInsets.all(16),
        ),
      );
}
