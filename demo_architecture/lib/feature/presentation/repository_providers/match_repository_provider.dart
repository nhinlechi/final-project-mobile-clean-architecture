import 'package:demo_architecture/feature/data/models/chat/match_room_model.dart';
import 'package:demo_architecture/feature/data/models/notification/notification_model.dart';
import 'package:socket_io_client/socket_io_client.dart';

class MatchRepositoryProvider {
  // Socket
  Socket chatSocket;

  // This is flag to control current matching state
  bool isMatching;

  //
  String currentMatchRoomId;

  // Current your match info
  RequestMatchData currentMatchingInfo;

  // Current friends
  List<MatchRoomModel> matchRooms = [];
}
