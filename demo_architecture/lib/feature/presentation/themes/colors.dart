import 'package:flutter/material.dart';

class MyAppTheme {
  static final Color primary = Color(0XFFFFA500);
  static final Color accentColor = Colors.orangeAccent.withOpacity(0.5);
  static final Color secondary = Colors.white;
  static final Color accent = Colors.orangeAccent.withOpacity(0.2);
  static final Color grey = Colors.grey.withOpacity(0.5);
  static final Color grey_light = Color(0XFFE8E8E8);
  static const Color black_grey = Color(0XFF7C7C7C);
  static final Color success = Colors.green;
  static final Color fail = Colors.red;
  static final Color black = Colors.black;
}


