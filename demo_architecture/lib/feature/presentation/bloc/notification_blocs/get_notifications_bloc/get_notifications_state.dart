part of 'get_notifications_bloc.dart';

abstract class GetNotificationsState extends Equatable {
  const GetNotificationsState();

  @override
  List<Object> get props => [];
}

class GetNotificationsInitial extends GetNotificationsState {}

class GetNotificationsLoading extends GetNotificationsState {}

class GetNotificationsSuccess extends GetNotificationsState {
  final List<NotificationModel> notifications;

  GetNotificationsSuccess({@required this.notifications});

  @override
  List<Object> get props => [notifications];
}

class GetNotificationsFailed extends GetNotificationsState {
  final String message;

  GetNotificationsFailed({@required this.message});

  @override
  List<Object> get props => [message];
}
