import 'dart:async';

import 'package:bloc/bloc.dart';
import '../../../../data/models/notification/notification_model.dart';
import '../../../../domain/usecase/notifications/get_notification_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_notifications_event.dart';

part 'get_notifications_state.dart';

class GetNotificationsBloc
    extends Bloc<GetNotificationsEvent, GetNotificationsState> {
  final GetNotificationUC useCase;

  GetNotificationsBloc({@required this.useCase})
      : super(GetNotificationsInitial());

  @override
  Stream<GetNotificationsState> mapEventToState(
    GetNotificationsEvent event,
  ) async* {
    if (event is FetchNotificationsEvent) {
      yield GetNotificationsLoading();
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => GetNotificationsFailed(message: ''),
        (data) {
          return GetNotificationsSuccess(notifications: data);
        },
      );
    }
  }
}
