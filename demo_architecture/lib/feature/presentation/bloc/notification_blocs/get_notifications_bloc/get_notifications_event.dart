part of 'get_notifications_bloc.dart';

abstract class GetNotificationsEvent extends Equatable {
  const GetNotificationsEvent();

  @override
  List<Object> get props => [];
}

class FetchNotificationsEvent extends GetNotificationsEvent {
  final GetNotificationParams params;

  FetchNotificationsEvent({@required this.params});
}
