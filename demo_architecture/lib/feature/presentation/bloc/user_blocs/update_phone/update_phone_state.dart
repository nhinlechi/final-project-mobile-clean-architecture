part of 'update_phone_bloc.dart';

abstract class UpdatePhoneState extends Equatable {
  const UpdatePhoneState();
}

class UpdatePhoneInitial extends UpdatePhoneState {
  @override
  List<Object> get props => [];
}
class LoadingUpdatePhone extends UpdatePhoneState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadedUpdatePhone extends UpdatePhoneState {
  final UserInform userInform;

  LoadedUpdatePhone({@required this.userInform});

  @override
  List<Object> get props => [userInform];
}

class ErrorUpdatePhone extends UpdatePhoneState {
  final String message;

  ErrorUpdatePhone({@required this.message});

  @override
  List<Object> get props => [message];
}