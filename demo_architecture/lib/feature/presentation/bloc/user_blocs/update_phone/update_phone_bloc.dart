import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/update_phone_inform_uc.dart';

part 'update_phone_event.dart';
part 'update_phone_state.dart';

class UpdatePhoneBloc extends Bloc<UpdatePhoneEvent, UpdatePhoneState> {
  final UpdatePhoneInform updatePhoneUserInform;
  UpdatePhoneBloc({@required UpdatePhoneInform updateInform})
      : assert(updateInform != null),
        updatePhoneUserInform = updateInform,
        super(UpdatePhoneInitial());

  @override
  Stream<UpdatePhoneState> mapEventToState(
    UpdatePhoneEvent event,
  ) async* {
    if (event is UpdatePhoneClickEvent) {
      yield LoadingUpdatePhone();
      final failureOrsuccess = await updatePhoneUserInform(Params(event.phone));
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<UpdatePhoneState> _eitherLoadedOrErrorState(
    Either<Failure, UserInform> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => ErrorUpdatePhone(message: _mapFailureToMessage(failure)),
      (user) => LoadedUpdatePhone(userInform: user),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
