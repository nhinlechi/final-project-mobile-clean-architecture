part of 'update_phone_bloc.dart';

abstract class UpdatePhoneEvent extends Equatable {
  const UpdatePhoneEvent();
}

class UpdatePhoneClickEvent extends UpdatePhoneEvent {
  final String phone;

  UpdatePhoneClickEvent(this.phone);
  @override
  // TODO: implement props
  List<Object> get props => [phone];
}