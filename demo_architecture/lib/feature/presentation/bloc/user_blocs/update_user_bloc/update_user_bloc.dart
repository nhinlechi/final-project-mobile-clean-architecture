import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../domain/usecase/user/update_user_inform_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'update_user_event.dart';
part 'update_user_state.dart';

class UpdateUserBloc extends Bloc<UpdateUserEvent, UpdateUserState> {
  final UpdateUserInformUseCase _updateUserInformUseCase;
  UpdateUserBloc({@required UpdateUserInformUseCase useCase})
      : assert(useCase != null),
        _updateUserInformUseCase = useCase,
        super(UpdateUserInitial());

  @override
  Stream<UpdateUserState> mapEventToState(
    UpdateUserEvent event,
  ) async* {
    if (event is UpdateUserEventSubmit) {
      yield UpdateUserLoading();
      final failureOrsuccess =
          await _updateUserInformUseCase(event.updateUserInformParams);
      yield failureOrsuccess.fold(
          (l) => UpdateUserError(), (r) => UpdateUserLoaded());
    }
  }
}
