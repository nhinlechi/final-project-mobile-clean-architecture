part of 'update_user_bloc.dart';

abstract class UpdateUserEvent extends Equatable {
  const UpdateUserEvent();
}

class UpdateUserEventSubmit extends UpdateUserEvent{
  final UpdateUserInformParams updateUserInformParams;

  UpdateUserEventSubmit(this.updateUserInformParams);

  @override
  // TODO: implement props
  List<Object> get props => [updateUserInformParams];

}
