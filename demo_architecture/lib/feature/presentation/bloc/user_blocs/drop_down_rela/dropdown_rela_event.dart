part of 'dropdown_rela_bloc.dart';

abstract class DropdownRelaEvent extends Equatable {
  const DropdownRelaEvent();
}
class ChangedValueDropdown extends DropdownRelaEvent {

  const ChangedValueDropdown(this.valueOnchange);

  final String valueOnchange;

  @override
  List<Object> get props => [valueOnchange];
}
