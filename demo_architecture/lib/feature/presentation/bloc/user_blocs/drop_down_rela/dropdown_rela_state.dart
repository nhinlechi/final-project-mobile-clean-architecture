part of 'dropdown_rela_bloc.dart';

abstract class DropdownRelaState extends Equatable {
  const DropdownRelaState();
}

class DropdownRelaInitial extends DropdownRelaState {
  @override
  List<Object> get props => [];
}

class DropdownChange extends DropdownRelaState {
  final String onValueChange;

  DropdownChange({@required this.onValueChange});

  @override
  List<Object> get props => [onValueChange];

}
