import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'dropdown_rela_event.dart';
part 'dropdown_rela_state.dart';

class DropdownRelaBloc extends Bloc<DropdownRelaEvent, DropdownRelaState> {
  DropdownRelaBloc() : super(DropdownRelaInitial());

  @override
  Stream<DropdownRelaState> mapEventToState(
    DropdownRelaEvent event,
  ) async* {
    if (event is ChangedValueDropdown)
      yield DropdownChange(onValueChange: event.valueOnchange);
  }
}
