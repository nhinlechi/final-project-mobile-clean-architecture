part of 'update_basic_bloc.dart';

@immutable
abstract class UpdateBasicState {}

class UpdateBasicInitial extends UpdateBasicState {}
class LoadingUpdateBasic extends UpdateBasicState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadedUpdateBasic extends UpdateBasicState {
  final UserInform userInform;

  LoadedUpdateBasic({@required this.userInform});

  @override
  List<Object> get props => [userInform];
}

class ErrorUpdateBasic extends UpdateBasicState {
  final String message;

  ErrorUpdateBasic({@required this.message});

  @override
  List<Object> get props => [message];
}