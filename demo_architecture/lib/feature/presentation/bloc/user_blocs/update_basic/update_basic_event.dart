part of 'update_basic_bloc.dart';

@immutable
abstract class UpdateBasicEvent {}
class UpdateBasicClickEvent extends UpdateBasicEvent {
  final String birdthDay;
  final String firstName;
  final String lastName;
  final String gender;

  UpdateBasicClickEvent(this.birdthDay, this.firstName, this.lastName, this.gender);


  @override
  // TODO: implement props
  List<Object> get props => [birdthDay, firstName, lastName, gender];
}