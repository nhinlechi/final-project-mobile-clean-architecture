import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/update_basic_inform_uc.dart';

part 'update_basic_event.dart';
part 'update_basic_state.dart';

class UpdateBasicBloc extends Bloc<UpdateBasicEvent, UpdateBasicState> {
  final UpdateBasicInform updateBasicUserInform;
  UpdateBasicBloc({@required UpdateBasicInform updateInform})
      : assert(updateInform != null),
        updateBasicUserInform = updateInform,
        super(UpdateBasicInitial());

  @override
  Stream<UpdateBasicState> mapEventToState(
    UpdateBasicEvent event,
  ) async* {
    if (event is UpdateBasicClickEvent) {
      yield LoadingUpdateBasic();
      final failureOrsuccess = await updateBasicUserInform(Params(
          event.gender, event.birdthDay, event.lastName, event.firstName));
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<UpdateBasicState> _eitherLoadedOrErrorState(
    Either<Failure, UserInform> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => ErrorUpdateBasic(message: _mapFailureToMessage(failure)),
      (user) => LoadedUpdateBasic(userInform: user),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
