part of 'update_user_avatar_bloc.dart';

abstract class UpdateUserAvatarEvent extends Equatable {
  const UpdateUserAvatarEvent();

  @override
  List<Object> get props => [];
}

class StartUpdateUserAvatarEvent extends UpdateUserAvatarEvent {
  final UpdateUserAvatarParams params;

  StartUpdateUserAvatarEvent({@required this.params});

  @override
  List<Object> get props => [params];
}

class EditUserAvatarEvent extends UpdateUserAvatarEvent {}
