part of 'update_user_avatar_bloc.dart';

abstract class UpdateUserAvatarState extends Equatable {
  const UpdateUserAvatarState();

  @override
  List<Object> get props => [];
}

class UpdateUserAvatarInitial extends UpdateUserAvatarState {}

class UpdateUserAvatarSuccessState extends UpdateUserAvatarState {}

class UpdateUserAvatarFailedState extends UpdateUserAvatarState {}

class UpdateUserAvatarLoadingState extends UpdateUserAvatarState {}
