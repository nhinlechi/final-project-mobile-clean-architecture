import 'dart:async';

import 'package:bloc/bloc.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/update_user_avatar_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'update_user_avatar_event.dart';

part 'update_user_avatar_state.dart';

class UpdateUserAvatarBloc
    extends Bloc<UpdateUserAvatarEvent, UpdateUserAvatarState> {
  final UpdateUserAvatarUC updateUserAvatarUC;

  UpdateUserAvatarBloc({@required this.updateUserAvatarUC})
      : super(UpdateUserAvatarInitial());

  @override
  Stream<UpdateUserAvatarState> mapEventToState(
    UpdateUserAvatarEvent event,
  ) async* {
    if (event is EditUserAvatarEvent) {
      yield UpdateUserAvatarInitial();
    }
    if (event is StartUpdateUserAvatarEvent) {
      yield UpdateUserAvatarLoadingState();
      final rs = await this.updateUserAvatarUC.call(event.params);
      yield rs.fold(
        (failure) => UpdateUserAvatarFailedState(),
        (data) => UpdateUserAvatarSuccessState(),
      );
    }
  }
}
