part of 'update_rela_bloc.dart';

abstract class UpdateRelaEvent extends Equatable {
  const UpdateRelaEvent();
}
class UpdateRelaClickEvent extends UpdateRelaEvent {
  final int rela;

  UpdateRelaClickEvent(this.rela);
  @override
  // TODO: implement props
  List<Object> get props => [rela];
}