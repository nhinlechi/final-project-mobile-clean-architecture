part of 'update_rela_bloc.dart';

abstract class UpdateRelaState extends Equatable {
  const UpdateRelaState();
}

class UpdateRelaInitial extends UpdateRelaState {
  @override
  List<Object> get props => [];
}
class LoadingUpdateRela extends UpdateRelaState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class LoadedUpdateRela extends UpdateRelaState {
  final UserInform userInform;

  LoadedUpdateRela({@required this.userInform});

  @override
  List<Object> get props => [userInform];
}

class ErrorUpdateRela extends UpdateRelaState {
  final String message;

  ErrorUpdateRela({@required this.message});

  @override
  List<Object> get props => [message];
}
