import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/update_rela_inform_uc.dart';

part 'update_rela_event.dart';
part 'update_rela_state.dart';

class UpdateRelaBloc extends Bloc<UpdateRelaEvent, UpdateRelaState> {
  final UpdateRelaInform updateRelaUserInform;
  UpdateRelaBloc({@required UpdateRelaInform updateInform})
      : assert(updateInform != null),
        updateRelaUserInform = updateInform,
        super(UpdateRelaInitial());

  @override
  Stream<UpdateRelaState> mapEventToState(
    UpdateRelaEvent event,
  ) async* {
    if (event is UpdateRelaClickEvent) {
      yield LoadingUpdateRela();
      final failureOrsuccess = await updateRelaUserInform(Params(event.rela));
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<UpdateRelaState> _eitherLoadedOrErrorState(
    Either<Failure, UserInform> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => ErrorUpdateRela(message: _mapFailureToMessage(failure)),
      (user) => LoadedUpdateRela(userInform: user),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
