part of 'get_user_by_id_bloc.dart';

abstract class GetUserByIdState extends Equatable {
  const GetUserByIdState();
}

class GetUserByIdInitial extends GetUserByIdState {
  @override
  List<Object> get props => [];
}

class GetUserByIdLoading extends GetUserByIdState {
  @override
  List<Object> get props => [];
}

class GetUserByIdLoaded extends GetUserByIdState {
  final UserInform userInform;

  GetUserByIdLoaded(this.userInform);
  @override
  List<Object> get props => [userInform];
}

class GetUserByIdError extends GetUserByIdState {
  final String message;

  GetUserByIdError(this.message);
  @override
  List<Object> get props => [message];
}
