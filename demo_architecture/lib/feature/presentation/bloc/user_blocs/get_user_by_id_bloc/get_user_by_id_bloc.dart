import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/get_user_inform_by_id.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_user_by_id_event.dart';
part 'get_user_by_id_state.dart';

class GetUserByIdBloc extends Bloc<GetUserByIdEvent, GetUserByIdState> {
  final GetUserInformByIdUC getUserInformByIdUC;
  GetUserByIdBloc({@required GetUserInformByIdUC useCase})
      : assert(useCase != null),
        getUserInformByIdUC = useCase,
        super(GetUserByIdInitial());

  @override
  Stream<GetUserByIdState> mapEventToState(
    GetUserByIdEvent event,
  ) async* {
    if (event is GetUserEvent) {
      yield GetUserByIdLoading();
      final failureOrsuccess =
          await getUserInformByIdUC(event.getUserInformByIdParams);
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<GetUserByIdState> _eitherLoadedOrErrorState(
    Either<Failure, UserInform> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => GetUserByIdError(_mapFailureToMessage(failure)),
      (user) {
        return GetUserByIdLoaded(user);
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
