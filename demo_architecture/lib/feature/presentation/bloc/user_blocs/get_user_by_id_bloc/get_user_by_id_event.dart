part of 'get_user_by_id_bloc.dart';

abstract class GetUserByIdEvent extends Equatable {
  const GetUserByIdEvent();
}

class GetUserEvent extends GetUserByIdEvent{
  final GetUserInformByIdParams getUserInformByIdParams;

  GetUserEvent(this.getUserInformByIdParams);

  @override
  List<Object> get props => [getUserInformByIdParams];

}
