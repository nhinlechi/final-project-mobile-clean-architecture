import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/get_user_inform.dart';
import '../../../repository_providers/user_repository_provider.dart';

part 'user_inform_event.dart';
part 'user_inform_state.dart';

class UserInformBloc extends Bloc<UserInformEvent, UserInformState> {
  final GetUserInform getUserInform;
  final UserRepositoryProvider userReposProvider;

  UserInformBloc({
    @required this.getUserInform,
    @required this.userReposProvider,
  })  : assert(
          getUserInform != null,
          userReposProvider != null,
        ),
        super(UserInformInitial());

  @override
  Stream<UserInformState> mapEventToState(
    UserInformEvent event,
  ) async* {
    if (event is GetUserInformEvent) {
      yield UserInformLoading();
      final failureOrsuccess = await getUserInform(event.getUserInformParams);
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<UserInformState> _eitherLoadedOrErrorState(
    Either<Failure, UserInform> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => Error(message: _mapFailureToMessage(failure)),
      (user) {
        log(user.hoten, name: 'user info');
        // Cache user information
        userReposProvider.userInform = user;
        // Return user
        return Loaded(userInform: user);
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
