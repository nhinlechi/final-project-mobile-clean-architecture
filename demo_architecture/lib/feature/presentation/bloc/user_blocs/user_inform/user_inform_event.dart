part of 'user_inform_bloc.dart';

abstract class UserInformEvent extends Equatable {
  const UserInformEvent();
}

class GetUserInformEvent extends UserInformEvent {
  final GetUserInformParams getUserInformParams;

  GetUserInformEvent(this.getUserInformParams);
  @override
  List<Object> get props => [getUserInformParams];
}

