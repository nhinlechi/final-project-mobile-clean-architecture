part of 'user_inform_bloc.dart';

abstract class UserInformState extends Equatable {
  const UserInformState();
}

class UserInformInitial extends UserInformState {
  @override
  List<Object> get props => [];
}

class UserInformLoading extends UserInformState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class Loaded extends UserInformState {
  final UserInform userInform;

  Loaded({@required this.userInform});

  @override
  List<Object> get props => [userInform];
}

class Error extends UserInformState {
  final String message;

  Error({@required this.message});

  @override
  List<Object> get props => [message];
}
