part of 'comment_place_bloc.dart';

abstract class GetCommentPlaceState extends Equatable {
  const GetCommentPlaceState();
}

class GetCommentPlaceInitial extends GetCommentPlaceState {
  @override
  List<Object> get props => [];
}

class GetCommentPlaceLoading extends GetCommentPlaceState{
  @override
  List<Object> get props => [];

}

class GetCommentPlaceLoaded extends GetCommentPlaceState{
  final ListCommentPlaceModel listCommentPlaceModel;

  GetCommentPlaceLoaded(this.listCommentPlaceModel);

  @override
  List<Object> get props => [listCommentPlaceModel];

}

class GetCommentPlaceError extends GetCommentPlaceState{
  final String message;

  GetCommentPlaceError(this.message);

  @override
  List<Object> get props => [message];
}
