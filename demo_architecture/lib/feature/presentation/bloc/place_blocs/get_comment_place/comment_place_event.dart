part of 'comment_place_bloc.dart';

abstract class CommentPlaceEvent extends Equatable {
  const CommentPlaceEvent();
}

class GetCommentPlaceEvent extends CommentPlaceEvent{
  final String id;

  GetCommentPlaceEvent(this.id);


  @override
  // TODO: implement props
  List<Object> get props =>[id];

}
