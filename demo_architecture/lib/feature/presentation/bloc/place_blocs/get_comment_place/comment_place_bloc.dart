import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../data/models/place/list_comment_place_model.dart';
import '../../../../domain/usecase/places/get_comment_place_uc.dart';
import '../../../repository_providers/place_comments_repository_provider.dart';

part 'comment_place_event.dart';
part 'comment_place_state.dart';

class GetCommentPlaceBloc
    extends Bloc<CommentPlaceEvent, GetCommentPlaceState> {
  final GetCommentPlaceUsecase usecase;
  final PlaceCommentsRP placeCommentsRP;

  GetCommentPlaceBloc({
    @required this.usecase,
    @required this.placeCommentsRP,
  }) : super(GetCommentPlaceInitial());

  @override
  Stream<GetCommentPlaceState> mapEventToState(
    CommentPlaceEvent event,
  ) async* {
    if (event is GetCommentPlaceEvent) {
      yield GetCommentPlaceLoading();
      final failureOrSuccess = await usecase(Params(event.id));
      yield* _eitherLoadedOrError(failureOrSuccess, event.id);
    }
  }

  Stream<GetCommentPlaceState> _eitherLoadedOrError(
      Either<Failure, ListCommentPlaceModel> failureOrSuccess,
      String id) async* {
    yield failureOrSuccess.fold(
      (failure) => GetCommentPlaceError("Load comment Error"),
      (listComment) {
        // Cache value to repos provider
        if (placeCommentsRP.placeComments.containsKey(id)) {
          placeCommentsRP.placeComments[id] = listComment;
        } else {
          placeCommentsRP.placeComments.addAll({id: listComment});
        }
        return GetCommentPlaceLoaded(listComment);
      },
    );
  }
}
