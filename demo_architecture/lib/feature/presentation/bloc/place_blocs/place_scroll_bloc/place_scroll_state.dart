part of 'place_scroll_bloc.dart';

@immutable
abstract class PlaceScrollState {}

class PlaceScrollInitial extends PlaceScrollState {}

class PlaceScrollDownState extends PlaceScrollState {}

class PlacesScrollOnTopState extends PlaceScrollState {}
