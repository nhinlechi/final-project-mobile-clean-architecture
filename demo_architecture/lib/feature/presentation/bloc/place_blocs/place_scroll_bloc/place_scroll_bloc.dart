import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'place_scroll_event.dart';
part 'place_scroll_state.dart';

class PlaceScrollBloc extends Bloc<PlaceScrollEvent, PlaceScrollState> {
  PlaceScrollBloc() : super(PlaceScrollInitial());

  @override
  Stream<PlaceScrollState> mapEventToState(
    PlaceScrollEvent event,
  ) async* {
    if(event is PlaceScrollToTop)
      yield PlacesScrollOnTopState();
    if(event is PlaceScrollDown)
      yield PlaceScrollDownState();
  }
}
