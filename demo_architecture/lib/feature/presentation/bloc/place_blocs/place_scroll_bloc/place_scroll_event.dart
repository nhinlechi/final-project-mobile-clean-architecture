part of 'place_scroll_bloc.dart';

@immutable
abstract class PlaceScrollEvent {}

class PlaceScrollDown extends PlaceScrollEvent {}

class PlaceScrollToTop extends PlaceScrollEvent {}
