part of 'comment_in_place_bloc.dart';

abstract class CommentInPlaceState extends Equatable {
  const CommentInPlaceState();
}

class CommentInPlaceInitial extends CommentInPlaceState {
  @override
  List<Object> get props => [];
}

class CommentInPlaceLoading extends CommentInPlaceState{
  @override
  // TODO: implement props
  List<Object> get props =>[];

}

class CommentInPlaceLoaded extends CommentInPlaceState{
  @override
  // TODO: implement props
  List<Object> get props =>[];

}


class CommentInPlaceError extends CommentInPlaceState{
  final String message;

  CommentInPlaceError(this.message);
  @override
  // TODO: implement props
  List<Object> get props =>[message];

}