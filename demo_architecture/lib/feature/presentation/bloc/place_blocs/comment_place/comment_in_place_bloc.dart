import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/usecase/places/comment_place_uc.dart';

part 'comment_in_place_event.dart';
part 'comment_in_place_state.dart';

class CommentInPlaceBloc
    extends Bloc<CommentInPlaceEvent, CommentInPlaceState> {
  final CommentPlaceUseCase commentPlaceUseCase;
  CommentInPlaceBloc({@required CommentPlaceUseCase usecase})
      : assert(usecase != null),
        commentPlaceUseCase = usecase,
        super(CommentInPlaceInitial());

  @override
  Stream<CommentInPlaceState> mapEventToState(
    CommentInPlaceEvent event,
  ) async* {
    if (event is CommentEvent) {
      yield CommentInPlaceLoading();
      final failureOrsuccess =
          await commentPlaceUseCase(event.commentPlaceParams);
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<CommentInPlaceState> _eitherLoadedOrErrorState(
      Either<Failure, bool> failureOrsuccess) async* {
    yield failureOrsuccess.fold(
      (failure) => CommentInPlaceError(_mapFailureToMessage(failure)),
      (success) => CommentInPlaceLoaded(),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
