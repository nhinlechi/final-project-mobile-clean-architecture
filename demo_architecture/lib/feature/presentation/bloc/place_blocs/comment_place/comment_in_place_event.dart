part of 'comment_in_place_bloc.dart';

abstract class CommentInPlaceEvent extends Equatable {
  const CommentInPlaceEvent();
}


class CommentEvent extends CommentInPlaceEvent{
  final CommentPlaceParams commentPlaceParams;

  CommentEvent({@required this.commentPlaceParams});

  @override
  List<Object> get props => [commentPlaceParams];

}
