import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../domain/entities/place/list_place_entity.dart';
import '../../../../domain/usecase/places/get_places_uc.dart';

part 'get_places_event.dart';
part 'get_places_state.dart';

class GetPlacesBloc extends Bloc<GetPlacesEvent, GetPlacesState> {
  final GetPlaceUseCase getPlaceUseCase;

  GetPlacesBloc({
    @required this.getPlaceUseCase,
  }) : super(GetPlacesInitial());

  @override
  Stream<GetPlacesState> mapEventToState(
    GetPlacesEvent event,
  ) async* {
    if (event is GetPlaces) {
      yield GetPlacesLoading();
      final rs = await getPlaceUseCase.call(event.params);
      yield rs.fold(
        (failure) => GetPlacesError(),
        (data) {
          return GetPlacesSuccess(placesEntity: data);
        },
      );
    }
  }
}
