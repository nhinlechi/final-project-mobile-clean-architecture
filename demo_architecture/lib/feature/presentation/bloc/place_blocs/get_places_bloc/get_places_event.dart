part of 'get_places_bloc.dart';

abstract class GetPlacesEvent extends Equatable {
  const GetPlacesEvent();

  @override
  List<Object> get props => [];
}

class GetPlaces extends GetPlacesEvent {
  final GetPlacesParams params;

  GetPlaces({@required this.params});
}
