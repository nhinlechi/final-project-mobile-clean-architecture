part of 'get_places_bloc.dart';

abstract class GetPlacesState extends Equatable {
  const GetPlacesState();

  @override
  List<Object> get props => [];
}

class GetPlacesInitial extends GetPlacesState {

}

class GetPlacesSuccess extends GetPlacesState {
  final ListPlaceEntity placesEntity;

  GetPlacesSuccess({@required this.placesEntity});

  @override
  List<Object> get props => [placesEntity];
}

class GetPlacesError extends GetPlacesState {}

class GetPlacesLoading extends GetPlacesState {}
