part of 'top_place_bloc.dart';

abstract class TopPlaceState extends Equatable {
  const TopPlaceState();
}

class TopPlaceInitial extends TopPlaceState {
  @override
  List<Object> get props => [];
}
class TopPlaceLoaded extends TopPlaceState{
  final ListPlaceEntity listPlaceModel;

  TopPlaceLoaded(this.listPlaceModel);

  @override
  // TODO: implement props
  List<Object> get props => [listPlaceModel];

}

class TopPlaceLoading extends TopPlaceState{
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class TopPlaceError extends TopPlaceState{
  final String message;

  TopPlaceError(this.message);
  @override
  // TODO: implement props
  List<Object> get props => [message];
}
