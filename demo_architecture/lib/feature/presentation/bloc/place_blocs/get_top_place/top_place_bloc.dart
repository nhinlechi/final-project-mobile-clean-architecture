import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/place/list_place_entity.dart';
import '../../../../domain/usecase/places/get_places_uc.dart';

part 'top_place_event.dart';

part 'top_place_state.dart';

class TopPlaceBloc extends Bloc<TopPlaceEvent, TopPlaceState> {
  final GetPlaceUseCase getPlaceUseCase;

  TopPlaceBloc({@required GetPlaceUseCase useCase})
      : assert(useCase != null),
        getPlaceUseCase = useCase,
        super(TopPlaceInitial());

  @override
  Stream<TopPlaceState> mapEventToState(
    TopPlaceEvent event,
  ) async* {
    if (event is GetTopPlaceEvent) {
      yield TopPlaceLoading();
      final failureOrsuccess = await getPlaceUseCase(event.params);
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
  }

  Stream<TopPlaceState> _eitherLoadedOrErrorState(
    Either<Failure, ListPlaceEntity> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => TopPlaceError(_mapFailureToMessage(failure)),
      (list) => TopPlaceLoaded(list),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
