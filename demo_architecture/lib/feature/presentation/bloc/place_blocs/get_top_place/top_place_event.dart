part of 'top_place_bloc.dart';

abstract class TopPlaceEvent extends Equatable {
  const TopPlaceEvent();
}

class GetTopPlaceEvent extends TopPlaceEvent {
  final GetPlacesParams params;

  GetTopPlaceEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
