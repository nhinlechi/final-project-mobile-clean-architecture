import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../data/models/group/list_tag_model.dart';
import '../../../../domain/usecase/group/get_list_tag_of_group_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_list_tag_group_event.dart';
part 'get_list_tag_group_state.dart';

class GetListTagGroupBloc
    extends Bloc<GetListTagGroupEvent, GetListTagGroupState> {
  final GetListTagOfGroupUseCase _getListTagOfGroupUseCase;
  GetListTagGroupBloc({@required GetListTagOfGroupUseCase useCase})
      : assert(useCase != null),
        _getListTagOfGroupUseCase = useCase,
        super(GetListTagGroupInitial());

  @override
  Stream<GetListTagGroupState> mapEventToState(
    GetListTagGroupEvent event,
  ) async* {
    if (event is GetListTagGroupEventSubmitted) {
      yield GetListTagGroupLoading();
      final failureOrSuccess = await _getListTagOfGroupUseCase(NoParams());
      yield* _eitherFailureOrSuccess(failureOrSuccess);
    }
  }

  Stream<GetListTagGroupState> _eitherFailureOrSuccess(
      Either<Failure, ListTagModel> failureOrSuccess) async* {
    yield failureOrSuccess.fold((l) => GetListTagGroupError(),
        (r) => GetListTagGroupLoaded(listTagModel: r));
  }
}
