part of 'get_list_tag_group_bloc.dart';

abstract class GetListTagGroupState extends Equatable {
  const GetListTagGroupState();
}

class GetListTagGroupInitial extends GetListTagGroupState {
  @override
  List<Object> get props => [];
}


class GetListTagGroupLoading extends GetListTagGroupState {
  @override
  List<Object> get props => [];
}


class GetListTagGroupLoaded extends GetListTagGroupState {
  final ListTagModel listTagModel;

  GetListTagGroupLoaded({this.listTagModel});
  @override
  List<Object> get props => [listTagModel];
}
class GetListTagGroupError extends GetListTagGroupState {
  @override
  List<Object> get props => [];
}


