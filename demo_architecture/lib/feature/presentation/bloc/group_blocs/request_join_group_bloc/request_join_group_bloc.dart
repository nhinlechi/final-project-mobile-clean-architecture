import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../domain/usecase/group/request_join_group_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'request_join_group_event.dart';
part 'request_join_group_state.dart';

class RequestJoinGroupBloc
    extends Bloc<RequestJoinGroupEvent, RequestJoinGroupState> {
  final RequestJoinGroupUseCase requestJoinGroupUseCase;
  RequestJoinGroupBloc({@required RequestJoinGroupUseCase useCase})
      : assert(useCase != null),
        requestJoinGroupUseCase = useCase,
        super(RequestJoinGroupInitial());

  @override
  Stream<RequestJoinGroupState> mapEventToState(
    RequestJoinGroupEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is RequestJoinGroupEventSubmitted) {
      yield RequestJoinGroupLoading();
      final successOrFailure =
          await requestJoinGroupUseCase(event.requestJoinGroupParams);
      yield* _eitherSuccessOrFailure(successOrFailure);
    }
  }

  Stream<RequestJoinGroupState> _eitherSuccessOrFailure(
      Either<Failure, void> successOrFailure) async* {
    yield successOrFailure.fold(
        (l) => RequestJoinGroupError(), (r) => RequestJoinGroupLoaded());
  }
}
