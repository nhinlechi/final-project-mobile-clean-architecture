part of 'request_join_group_bloc.dart';

abstract class RequestJoinGroupEvent extends Equatable {
  const RequestJoinGroupEvent();
}

class RequestJoinGroupEventSubmitted extends RequestJoinGroupEvent{
  final RequestJoinGroupParams requestJoinGroupParams;

  RequestJoinGroupEventSubmitted(this.requestJoinGroupParams);

  @override
  // TODO: implement props
  List<Object> get props => [requestJoinGroupParams];
}
