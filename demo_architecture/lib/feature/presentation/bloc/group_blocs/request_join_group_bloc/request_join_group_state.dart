part of 'request_join_group_bloc.dart';

abstract class RequestJoinGroupState extends Equatable {
  const RequestJoinGroupState();
}

class RequestJoinGroupInitial extends RequestJoinGroupState {
  @override
  List<Object> get props => [];
}

class RequestJoinGroupLoading extends RequestJoinGroupState {
  @override
  List<Object> get props => [];
}

class RequestJoinGroupLoaded extends RequestJoinGroupState {
  @override
  List<Object> get props => [];
}

class RequestJoinGroupError extends RequestJoinGroupState {
  @override
  List<Object> get props => [];
}