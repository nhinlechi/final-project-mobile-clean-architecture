part of 'update_group_event_bloc.dart';

abstract class UpdateGroupEventState extends Equatable {
  const UpdateGroupEventState();

  @override
  List<Object> get props => [];
}

class UpdateGroupEventInitial extends UpdateGroupEventState {}

class UpdateGroupEventLoading extends UpdateGroupEventState {}

class UpdateGroupEventSuccess extends UpdateGroupEventState {}

class UpdateGroupEventFailed extends UpdateGroupEventState {}
