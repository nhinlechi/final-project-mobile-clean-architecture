import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'update_group_event_event.dart';

part 'update_group_event_state.dart';

class UpdateGroupEventBloc
    extends Bloc<UpdateGroupEventEvent, UpdateGroupEventState> {
  final UpdateGroupEventUC useCase;

  UpdateGroupEventBloc({@required this.useCase})
      : super(UpdateGroupEventInitial());

  @override
  Stream<UpdateGroupEventState> mapEventToState(
    UpdateGroupEventEvent event,
  ) async* {
    if (event is SubmitUpdateGroupEvent) {
      yield UpdateGroupEventLoading();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (l) => UpdateGroupEventFailed(),
        (r) => UpdateGroupEventSuccess(),
      );
    }
  }
}
