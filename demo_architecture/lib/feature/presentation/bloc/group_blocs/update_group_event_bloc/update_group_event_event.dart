part of 'update_group_event_bloc.dart';

abstract class UpdateGroupEventEvent extends Equatable {
  const UpdateGroupEventEvent();
}

class SubmitUpdateGroupEvent extends UpdateGroupEventEvent {
  final UpdateGroupEventParams params;

  SubmitUpdateGroupEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
