part of 'confirm_request_join_group_bloc.dart';

abstract class ConfirmRequestJoinGroupState extends Equatable {
  const ConfirmRequestJoinGroupState();

  @override
  List<Object> get props => [];
}

class ConfirmRequestJoinGroupInitial extends ConfirmRequestJoinGroupState {}

class ConfirmRequestJoinGroupSuccessState extends ConfirmRequestJoinGroupState {}

class ConfirmRequestJoinGroupFailedState extends ConfirmRequestJoinGroupState {}

class ConfirmRequestJoinGroupLoadingState extends ConfirmRequestJoinGroupState {}
