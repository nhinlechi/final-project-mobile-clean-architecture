part of 'confirm_request_join_group_bloc.dart';

abstract class ConfirmRequestJoinGroupEvent extends Equatable {
  const ConfirmRequestJoinGroupEvent();
}

class ConfirmRequestJoinGroup extends ConfirmRequestJoinGroupEvent {
  final ConfirmRequestJoinGroupParams params;

  ConfirmRequestJoinGroup({
    @required this.params,
  });

  @override
  List<Object> get props => [params];
}
