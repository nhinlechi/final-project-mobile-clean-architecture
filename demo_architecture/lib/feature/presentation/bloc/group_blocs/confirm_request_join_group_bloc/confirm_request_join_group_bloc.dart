import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'confirm_request_join_group_event.dart';

part 'confirm_request_join_group_state.dart';

class ConfirmRequestJoinGroupBloc
    extends Bloc<ConfirmRequestJoinGroupEvent, ConfirmRequestJoinGroupState> {
  final ConfirmRequestJoinGroupUC useCase;

  ConfirmRequestJoinGroupBloc({
    @required this.useCase,
  }) : super(ConfirmRequestJoinGroupInitial());

  @override
  Stream<ConfirmRequestJoinGroupState> mapEventToState(
    ConfirmRequestJoinGroupEvent event,
  ) async* {
    if (event is ConfirmRequestJoinGroup) {
      yield ConfirmRequestJoinGroupLoadingState();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (failure) => ConfirmRequestJoinGroupFailedState(),
        (_) => ConfirmRequestJoinGroupSuccessState(),
      );
    }
  }
}
