part of 'group_bloc.dart';

abstract class GroupEvent extends Equatable {
  const GroupEvent();
}

class GetListGroupEvent extends GroupEvent{

  final GetListGroupParams getListGroupParams;

  GetListGroupEvent({this.getListGroupParams});

  @override
  // TODO: implement props
  List<Object> get props => [getListGroupParams];

}
