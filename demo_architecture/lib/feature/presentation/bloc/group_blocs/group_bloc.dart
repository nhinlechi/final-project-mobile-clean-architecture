import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:demo_architecture/feature/domain/entities/group/group_entity.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/group_repository_provider.dart';
import '../../../../core/error/failures.dart';
import '../../../domain/entities/group/list_group_entity.dart';
import '../../../domain/usecase/group/get_list_group.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'group_event.dart';

part 'group_state.dart';

class GroupBloc extends Bloc<GroupEvent, GroupState> {
  final GetListGroupUC getListGroupUC;
  final GroupRepositoryProvider groupReposProvider;

  GroupBloc(
      {@required GetListGroupUC useCase, @required this.groupReposProvider})
      : assert(useCase != null),
        getListGroupUC = useCase,
        super(GroupInitial());

  @override
  Stream<GroupState> mapEventToState(
    GroupEvent event,
  ) async* {
    if (event is GetListGroupEvent) {
      yield GroupLoading();
      final failureOrsuccess = await getListGroupUC(event.getListGroupParams);
      yield failureOrsuccess.fold(
        (failure) => GroupError(_mapFailureToMessage(failure)),
        (list) {
          if(event.getListGroupParams.page == 1){
            groupReposProvider.groups.clear();
          }
          groupReposProvider.groups.addAll(list.groups);
          return GroupLoadedList(list);
        },
      );
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
