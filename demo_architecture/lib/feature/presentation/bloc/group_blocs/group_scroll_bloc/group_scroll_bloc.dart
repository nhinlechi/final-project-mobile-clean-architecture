import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'group_scroll_event.dart';
part 'group_scroll_state.dart';

class GroupScrollBloc extends Bloc<GroupScrollEvent, GroupScrollState> {
  GroupScrollBloc() : super(GroupScrollInitial());

  @override
  Stream<GroupScrollState> mapEventToState(
    GroupScrollEvent event,
  ) async* {
    if(event is GroupScrollToTop)
      yield GroupScrollOnTopState();
    if(event is GroupScrollDown)
      yield GroupScrollDownState();
  }
}
