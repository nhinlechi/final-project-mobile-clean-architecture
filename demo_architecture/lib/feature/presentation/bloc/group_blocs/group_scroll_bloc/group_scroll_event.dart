part of 'group_scroll_bloc.dart';

@immutable
abstract class GroupScrollEvent {}

class GroupScrollToTop extends GroupScrollEvent {}

class GroupScrollDown extends GroupScrollEvent {}
