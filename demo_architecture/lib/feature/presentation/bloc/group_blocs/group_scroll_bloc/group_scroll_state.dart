part of 'group_scroll_bloc.dart';

@immutable
abstract class GroupScrollState {}

class GroupScrollInitial extends GroupScrollState {}

class GroupScrollOnTopState extends GroupScrollState {}

class GroupScrollDownState extends GroupScrollState {}
