part of 'create_new_group_event_bloc.dart';

abstract class CreateNewGroupEventEvent extends Equatable {
  const CreateNewGroupEventEvent();

  @override
  List<Object> get props => [];
}

class CreateGroupEventEvent extends CreateNewGroupEventEvent {
  final CreateNewGroupEventParams params;

  CreateGroupEventEvent({@required this.params});
}
