part of 'create_new_group_event_bloc.dart';

abstract class CreateNewGroupEventState extends Equatable {
  const CreateNewGroupEventState();

  @override
  List<Object> get props => [];
}

class CreateNewGroupEventInitial extends CreateNewGroupEventState {}

//

class CreateNewGroupEventSuccess extends CreateNewGroupEventState {}

class CreateNewGroupEventFailed extends CreateNewGroupEventState {}

class CreateNewGroupEventLoading extends CreateNewGroupEventState {}
