import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'create_new_group_event_event.dart';

part 'create_new_group_event_state.dart';

class CreateNewGroupEventBloc
    extends Bloc<CreateNewGroupEventEvent, CreateNewGroupEventState> {
  final CreateNewGroupEventUC useCase;

  CreateNewGroupEventBloc({@required this.useCase}) : super(CreateNewGroupEventInitial());

  @override
  Stream<CreateNewGroupEventState> mapEventToState(
    CreateNewGroupEventEvent event,
  ) async* {
    if (event is CreateGroupEventEvent) {
      yield CreateNewGroupEventLoading();
      //
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => CreateNewGroupEventFailed(),
        (r) => CreateNewGroupEventSuccess(),
      );
    }
  }
}
