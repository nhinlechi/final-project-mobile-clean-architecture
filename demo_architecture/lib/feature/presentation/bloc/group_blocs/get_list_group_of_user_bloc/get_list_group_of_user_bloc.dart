import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../domain/entities/group/list_group_entity.dart';
import '../../../../domain/usecase/group/get_list_group_of_user_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_list_group_of_user_event.dart';
part 'get_list_group_of_user_state.dart';

class GetListGroupOfUserBloc
    extends Bloc<GetListGroupOfUserEvent, GetListGroupOfUserState> {
  final GetListGroupOfUserUseCase getListGroupOfUserUseCase;
  GetListGroupOfUserBloc({@required GetListGroupOfUserUseCase useCase})
      : assert(useCase != null),
        getListGroupOfUserUseCase = useCase,
        super(GetListGroupOfUserInitial());

  @override
  Stream<GetListGroupOfUserState> mapEventToState(
    GetListGroupOfUserEvent event,
  ) async* {
    if (event is GetListGroupOfUserEventSubmitted) {
      yield GetListGroupOfUserLoading();
      final successOrFailure = await getListGroupOfUserUseCase(event.noParams);
      yield* _eitherLoadedOrErrorState(successOrFailure);
    }
  }

  Stream<GetListGroupOfUserState> _eitherLoadedOrErrorState(
      Either<Failure, ListGroupEntity> successOrFailure) async* {
    yield successOrFailure.fold(
        (l) => GetListGroupOfUserError(), (r) => GetListGroupOfUserLoaded(r));
  }
}
