part of 'get_list_group_of_user_bloc.dart';

abstract class GetListGroupOfUserState extends Equatable {
  const GetListGroupOfUserState();
}

class GetListGroupOfUserInitial extends GetListGroupOfUserState {
  @override
  List<Object> get props => [];
}

class GetListGroupOfUserLoading extends GetListGroupOfUserState {
  @override
  List<Object> get props => [];
}

class GetListGroupOfUserLoaded extends GetListGroupOfUserState {
  final ListGroupEntity listGroupEntity;

  GetListGroupOfUserLoaded(this.listGroupEntity);
  @override
  List<Object> get props => [listGroupEntity];
}

class GetListGroupOfUserError extends GetListGroupOfUserState {
  @override
  List<Object> get props => [];
}
