part of 'get_list_group_of_user_bloc.dart';

abstract class GetListGroupOfUserEvent extends Equatable {
  const GetListGroupOfUserEvent();
}


class GetListGroupOfUserEventSubmitted extends GetListGroupOfUserEvent{
  final NoParams noParams;

  GetListGroupOfUserEventSubmitted(this.noParams);
  @override
  List<Object> get props => [noParams];

}
