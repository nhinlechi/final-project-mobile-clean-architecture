part of 'update_group_bloc.dart';

abstract class UpdateGroupEvent extends Equatable {
  const UpdateGroupEvent();
}

class UpdateGroupEventSubmit extends UpdateGroupEvent{
  final UpdateGroupParams updateGroupParams;

  UpdateGroupEventSubmit(this.updateGroupParams);

  @override
  // TODO: implement props
  List<Object> get props => [updateGroupParams];

}
