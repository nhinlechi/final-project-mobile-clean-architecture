part of 'update_group_bloc.dart';

abstract class UpdateGroupState extends Equatable {
  const UpdateGroupState();
}

class UpdateGroupInitial extends UpdateGroupState {
  @override
  List<Object> get props => [];
}

class UpdateGroupLoaded extends UpdateGroupState {

  @override
  List<Object> get props => [];
}

class UpdateGroupLoading extends UpdateGroupState {
  @override
  List<Object> get props => [];
}

class UpdateGroupError extends UpdateGroupState {
  @override
  List<Object> get props => [];
}
