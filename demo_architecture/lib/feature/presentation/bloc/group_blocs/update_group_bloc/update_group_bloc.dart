import 'dart:async';

import 'package:bloc/bloc.dart';
import '../../../../domain/entities/group/group_entity.dart';
import '../../../../domain/usecase/group/update_group.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'update_group_event.dart';
part 'update_group_state.dart';

class UpdateGroupBloc extends Bloc<UpdateGroupEvent, UpdateGroupState> {
  final UpdateGroupUseCase updateGroupUseCase;

  UpdateGroupBloc({@required UpdateGroupUseCase useCase})
      : assert(useCase != null),
        updateGroupUseCase = useCase,
        super(UpdateGroupInitial());

  @override
  Stream<UpdateGroupState> mapEventToState(
    UpdateGroupEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is UpdateGroupEventSubmit) {
      yield UpdateGroupLoading();
      final successOrFailure =
          await updateGroupUseCase(event.updateGroupParams);
      yield successOrFailure.fold(
          (l) => UpdateGroupError(), (r) => UpdateGroupLoaded());
    }
  }
}
