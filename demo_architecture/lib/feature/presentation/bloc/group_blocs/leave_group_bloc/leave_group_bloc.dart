import 'dart:async';

import 'package:bloc/bloc.dart';
import '../../../../domain/usecase/group/leave_group.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'leave_group_event.dart';
part 'leave_group_state.dart';

class LeaveGroupBloc extends Bloc<LeaveGroupEvent, LeaveGroupState> {
  final LeaveGroupUseCase leaveGroupUseCase;
  LeaveGroupBloc({@required LeaveGroupUseCase useCase})
      : assert(useCase != null),
        leaveGroupUseCase = useCase,
        super(LeaveGroupInitial());

  @override
  Stream<LeaveGroupState> mapEventToState(
    LeaveGroupEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is LeaveGroupEventSubmit) {
      yield LeaveGroupLoading();
      final successOrFailure = await leaveGroupUseCase(event.leaveGroupParams);
      yield successOrFailure.fold(
          (l) => LeaveGroupError(), (r) => LeaveGroupLoaded());
    }
  }
}
