part of 'leave_group_bloc.dart';

abstract class LeaveGroupEvent extends Equatable {
  const LeaveGroupEvent();
}

class LeaveGroupEventSubmit extends LeaveGroupEvent{
  final LeaveGroupParams leaveGroupParams;

  LeaveGroupEventSubmit(this.leaveGroupParams);

  @override
  // TODO: implement props
  List<Object> get props => [leaveGroupParams];

}
