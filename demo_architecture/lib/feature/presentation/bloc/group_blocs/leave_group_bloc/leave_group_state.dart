part of 'leave_group_bloc.dart';

abstract class LeaveGroupState extends Equatable {
  const LeaveGroupState();
}

class LeaveGroupInitial extends LeaveGroupState {
  @override
  List<Object> get props => [];
}

class LeaveGroupLoading extends LeaveGroupState {
  @override
  List<Object> get props => [];
}

class LeaveGroupLoaded extends LeaveGroupState {
  @override
  List<Object> get props => [];
}

class LeaveGroupError extends LeaveGroupState {
  @override
  List<Object> get props => [];
}
