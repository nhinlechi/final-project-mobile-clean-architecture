part of 'get_list_group_event_bloc.dart';

abstract class GetListGroupEventEvent extends Equatable {
  const GetListGroupEventEvent();

  @override
  List<Object> get props => [];
}

class FetchListGroupEventEvent extends GetListGroupEventEvent {
  final GetListGroupEventParams params;

  FetchListGroupEventEvent({@required this.params});
}
