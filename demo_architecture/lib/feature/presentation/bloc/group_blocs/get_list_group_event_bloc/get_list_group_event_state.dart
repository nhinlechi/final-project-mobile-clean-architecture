part of 'get_list_group_event_bloc.dart';

abstract class GetListGroupEventState extends Equatable {
  const GetListGroupEventState();

  @override
  List<Object> get props => [];
}

class GetListGroupEventInitial extends GetListGroupEventState {}

class GetListGroupEventSuccessState extends GetListGroupEventState {
  final ListGroupEventModel listGroupEventModel;

  GetListGroupEventSuccessState({@required this.listGroupEventModel});
}

class GetListGroupEventFailedState extends GetListGroupEventState {}

class GetListGroupEventLoadingState extends GetListGroupEventState {}
