import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_list_group_event_event.dart';

part 'get_list_group_event_state.dart';

class GetListGroupEventBloc extends Bloc<GetListGroupEventEvent, GetListGroupEventState> {
  final GetListGroupEventUC useCase;

  GetListGroupEventBloc({@required this.useCase}) : super(GetListGroupEventInitial());

  @override
  Stream<GetListGroupEventState> mapEventToState(
    GetListGroupEventEvent event,
  ) async* {
    if (event is FetchListGroupEventEvent) {
      yield GetListGroupEventLoadingState();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (failure) => GetListGroupEventFailedState(),
        (data) => GetListGroupEventSuccessState(listGroupEventModel: data),
      );
    }
  }
}
