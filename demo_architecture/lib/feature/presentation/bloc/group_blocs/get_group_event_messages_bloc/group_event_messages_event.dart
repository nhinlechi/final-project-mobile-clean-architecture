part of 'group_event_messages_bloc.dart';

abstract class GroupEventMessagesEvent extends Equatable {
  const GroupEventMessagesEvent();
}

class GetGroupEventMessagesEvent extends GroupEventMessagesEvent {
  final GetGroupEventMessagesParams params;

  GetGroupEventMessagesEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
