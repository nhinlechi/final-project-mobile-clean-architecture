import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'group_event_messages_event.dart';

part 'group_event_messages_state.dart';

class GroupEventMessagesBloc
    extends Bloc<GroupEventMessagesEvent, GroupEventMessagesState> {
  final GetGroupEventMessagesUC useCase;

  GroupEventMessagesBloc({@required this.useCase})
      : super(GroupEventMessagesInitial());

  @override
  Stream<GroupEventMessagesState> mapEventToState(
    GroupEventMessagesEvent event,
  ) async* {
    if (event is GetGroupEventMessagesEvent) {
      yield GetGroupEventMessagesLoading();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (l) => GetGroupEventMessagesFailed(),
        (data) => GetGroupEventMessagesSuccess(messages: data),
      );
    }
  }
}
