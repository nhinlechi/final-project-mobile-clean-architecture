part of 'group_event_messages_bloc.dart';

abstract class GroupEventMessagesState extends Equatable {
  const GroupEventMessagesState();

  @override
  List<Object> get props => [];
}

class GroupEventMessagesInitial extends GroupEventMessagesState {}

class GetGroupEventMessagesLoading extends GroupEventMessagesState {}

class GetGroupEventMessagesFailed extends GroupEventMessagesState {}

class GetGroupEventMessagesSuccess extends GroupEventMessagesState {
  final List<MessageModel> messages;

  GetGroupEventMessagesSuccess({@required this.messages});

  @override
  List<Object> get props => [messages];
}
