part of 'join_group_event_bloc.dart';

abstract class JoinGroupEventEvent extends Equatable {
  const JoinGroupEventEvent();
}

class SubmitJoinGroupEvent extends JoinGroupEventEvent {

  final JoinGroupEventParams params;

  SubmitJoinGroupEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
