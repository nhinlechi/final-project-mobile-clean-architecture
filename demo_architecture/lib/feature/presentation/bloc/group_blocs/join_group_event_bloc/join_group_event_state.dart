part of 'join_group_event_bloc.dart';

abstract class JoinGroupEventState extends Equatable {
  const JoinGroupEventState();

  @override
  List<Object> get props => [];
}

class JoinGroupEventInitial extends JoinGroupEventState {}

class JoinGroupEventSuccess extends JoinGroupEventState {}

class JoinGroupEventLoading extends JoinGroupEventState {}

class JoinGroupEventFailed extends JoinGroupEventState {}
