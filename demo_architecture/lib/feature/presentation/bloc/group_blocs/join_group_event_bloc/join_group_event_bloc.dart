import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'join_group_event_event.dart';

part 'join_group_event_state.dart';

class JoinGroupEventBloc
    extends Bloc<JoinGroupEventEvent, JoinGroupEventState> {
  final JoinGroupEventUC useCase;

  JoinGroupEventBloc({@required this.useCase}) : super(JoinGroupEventInitial());

  @override
  Stream<JoinGroupEventState> mapEventToState(
    JoinGroupEventEvent event,
  ) async* {
    if (event is SubmitJoinGroupEvent) {
      yield JoinGroupEventLoading();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (l) => JoinGroupEventFailed(),
        (r) => JoinGroupEventSuccess(),
      );
    }
  }
}
