part of 'group_bloc.dart';

abstract class GroupState extends Equatable {
  const GroupState();
}

class GroupInitial extends GroupState {
  @override
  List<Object> get props => [];
}

class GroupLoading extends GroupState{
  @override
  List<Object> get props => [];
}

class GroupLoadedList extends GroupState{
  final ListGroupEntity listGroupEntity;

  GroupLoadedList(this.listGroupEntity);

  @override
  List<Object> get props => [listGroupEntity];

}

class GroupError extends GroupState{
  final String message;

  GroupError(this.message);
  @override
  List<Object> get props => [message];
}
