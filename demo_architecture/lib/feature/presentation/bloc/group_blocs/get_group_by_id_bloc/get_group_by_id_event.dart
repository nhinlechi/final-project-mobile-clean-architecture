part of 'get_group_by_id_bloc.dart';

abstract class GetGroupByIdEvent extends Equatable {
  const GetGroupByIdEvent();
}

class GetGroupByIdEventSubmit extends GetGroupByIdEvent{
  final GetGroupByIdParams getGroupByIdParams;

  GetGroupByIdEventSubmit(this.getGroupByIdParams);

  @override
  // TODO: implement props
  List<Object> get props => [getGroupByIdParams];
}