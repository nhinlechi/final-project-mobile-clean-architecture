part of 'get_group_by_id_bloc.dart';

abstract class GetGroupByIdState extends Equatable {
  const GetGroupByIdState();
}

class GetGroupByIdInitial extends GetGroupByIdState {
  @override
  List<Object> get props => [];
}

class GetGroupByIdLoading extends GetGroupByIdState {
  @override
  List<Object> get props => [];
}


class GetGroupByIdLoaded extends GetGroupByIdState {
  final GroupEntity groupEntity;

  GetGroupByIdLoaded(this.groupEntity);
  @override
  List<Object> get props => [groupEntity];
}

class GetGroupByIdError extends GetGroupByIdState {
  @override
  List<Object> get props => [];
}
