import 'dart:async';

import 'package:bloc/bloc.dart';
import '../../../../domain/entities/group/group_entity.dart';
import '../../../../domain/usecase/group/get_group_by_id.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_group_by_id_event.dart';
part 'get_group_by_id_state.dart';

class GetGroupByIdBloc extends Bloc<GetGroupByIdEvent, GetGroupByIdState> {
  final GetGroupByIdUseCase getGroupByIdUseCase;
  GetGroupByIdBloc({@required GetGroupByIdUseCase useCase})
      : assert(useCase != null),
        getGroupByIdUseCase = useCase,
        super(GetGroupByIdInitial());

  @override
  Stream<GetGroupByIdState> mapEventToState(
    GetGroupByIdEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is GetGroupByIdEventSubmit) {
      yield GetGroupByIdLoading();
      final successOrFailure =
          await getGroupByIdUseCase(event.getGroupByIdParams);
      yield successOrFailure.fold(
          (l) => GetGroupByIdError(), (r) => GetGroupByIdLoaded(r));
    }
  }
}
