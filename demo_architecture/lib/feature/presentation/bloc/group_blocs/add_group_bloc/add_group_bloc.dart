import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/group/group_entity.dart';
import '../../../../domain/usecase/group/add_new_group.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'add_group_event.dart';
part 'add_group_state.dart';

class AddGroupBloc extends Bloc<AddGroupEvent, AddGroupState> {
  final AddNewGroupUseCase addNewGroupUseCase;

  AddGroupBloc({@required AddNewGroupUseCase useCase})
      : assert(useCase != null),
        addNewGroupUseCase = useCase,
        super(AddGroupInitial());

  @override
  Stream<AddGroupState> mapEventToState(
    AddGroupEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is ConfirmAddGroupEvent) {
      yield AddGroupLoading();
      final failureOrSuccess =
          await addNewGroupUseCase(event.addNewGroupParams);
      yield* _eitherLoadedOrErrorState(failureOrSuccess);
    }
  }

  Stream<AddGroupState> _eitherLoadedOrErrorState(
    Either<Failure, void> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => AddGroupError(_mapFailureToMessage(failure)),
      (list) => AddGroupLoaded(),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
