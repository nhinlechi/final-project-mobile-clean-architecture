part of 'add_group_bloc.dart';

abstract class AddGroupEvent extends Equatable {
  const AddGroupEvent();
}

class ConfirmAddGroupEvent extends AddGroupEvent{
  final AddNewGroupParams addNewGroupParams;

  ConfirmAddGroupEvent(this.addNewGroupParams);
  @override
  // TODO: implement props
  List<Object> get props => [addNewGroupParams];

}
