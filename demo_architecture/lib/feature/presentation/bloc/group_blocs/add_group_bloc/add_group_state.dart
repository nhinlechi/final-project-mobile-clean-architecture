part of 'add_group_bloc.dart';

abstract class AddGroupState extends Equatable {
  const AddGroupState();
}

class AddGroupInitial extends AddGroupState {
  @override
  List<Object> get props => [];
}

class AddGroupLoading extends AddGroupState {
  @override
  List<Object> get props => [];
}

class AddGroupLoaded extends AddGroupState {

  @override
  List<Object> get props => [];
}

class AddGroupError extends AddGroupState {
  final String message;

  AddGroupError(this.message);

  @override
  List<Object> get props => [message];
}
