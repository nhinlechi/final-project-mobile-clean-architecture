part of 'get_list_favorite_bloc.dart';

abstract class GetListFavoriteState extends Equatable {
  const GetListFavoriteState();
}

class GetListFavoriteInitial extends GetListFavoriteState {
  @override
  List<Object> get props => [];
}


class GetListFavoriteLoading extends GetListFavoriteState {
  @override
  List<Object> get props => [];
}


class GetListFavoriteLoaded extends GetListFavoriteState {
  final List<FavoriteModel> listFavorite;

  GetListFavoriteLoaded(this.listFavorite);
  @override
  List<Object> get props => [listFavorite];
}


class GetListFavoriteError extends GetListFavoriteState {
  final String message;

  GetListFavoriteError(this.message);
  @override
  List<Object> get props => [message];
}
