import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../data/models/favorite/favorite_model.dart';
import '../../../../domain/usecase/favorites/get_favorite_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_list_favorite_event.dart';
part 'get_list_favorite_state.dart';

class GetListFavoriteBloc
    extends Bloc<GetListFavoriteEvent, GetListFavoriteState> {
  final GetFavoriteUsecase getFavoriteUsecase;
  GetListFavoriteBloc({@required GetFavoriteUsecase useCase})
      : assert(useCase != null),
        getFavoriteUsecase = useCase,
        super(GetListFavoriteInitial());

  @override
  Stream<GetListFavoriteState> mapEventToState(
    GetListFavoriteEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is GetFavoriteRegister) {
      yield GetListFavoriteLoading();
      final failureOrSuccess = await getFavoriteUsecase(NoParams());
      yield* _eitherLoadedOrErrorState(failureOrSuccess);
    }
  }

  Stream<GetListFavoriteState> _eitherLoadedOrErrorState(
    Either<Failure, List<FavoriteModel>> failureOrsuccess,
  ) async* {
    yield failureOrsuccess.fold(
      (failure) => GetListFavoriteError(_mapFailureToMessage(failure)),
      (list) => GetListFavoriteLoaded(list),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
