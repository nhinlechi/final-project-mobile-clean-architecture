part of 'get_my_list_favorites_bloc.dart';

abstract class GetMyListFavoritesState extends Equatable {
  const GetMyListFavoritesState();
}

class GetMyListFavoritesInitial extends GetMyListFavoritesState {
  @override
  List<Object> get props => [];
}


class GetMyListFavoritesLoading extends GetMyListFavoritesState {
  @override
  List<Object> get props => [];
}



class GetMyListFavoritesLoaded extends GetMyListFavoritesState {
  final List<FavoriteModel> myListFavorites;

  GetMyListFavoritesLoaded(this.myListFavorites);
  @override
  List<Object> get props => [myListFavorites];
}



class GetMyListFavoritesError extends GetMyListFavoritesState {
  final String message;

  GetMyListFavoritesError(this.message);

  @override
  List<Object> get props => [message];
}

