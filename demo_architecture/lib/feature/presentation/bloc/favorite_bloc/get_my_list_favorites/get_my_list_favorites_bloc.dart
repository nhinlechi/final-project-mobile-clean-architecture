import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../data/models/favorite/favorite_model.dart';
import '../../../../domain/usecase/favorites/get_my_favorite_uc.dart';

part 'get_my_list_favorites_event.dart';
part 'get_my_list_favorites_state.dart';

class GetMyListFavoritesBloc
    extends Bloc<GetMyListFavoritesEvent, GetMyListFavoritesState> {
  final GetMyFavoriteListUseCase _getMyFavoriteListUseCase;
  GetMyListFavoritesBloc({@required GetMyFavoriteListUseCase useCase})
      : assert(useCase != null),
        _getMyFavoriteListUseCase = useCase,
        super(GetMyListFavoritesInitial());

  @override
  Stream<GetMyListFavoritesState> mapEventToState(
    GetMyListFavoritesEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is GetMyListFavoriteUserEvent) {
      yield GetMyListFavoritesLoading();
      final failureOrSuccess = await _getMyFavoriteListUseCase(NoParams());
      yield* _eitherLoadedErrorState(failureOrSuccess);
    }
  }

  Stream<GetMyListFavoritesState> _eitherLoadedErrorState(
      Either<Failure, List<FavoriteModel>> failureOrSuccess) async* {
    yield failureOrSuccess.fold(
        (l) => GetMyListFavoritesError(_mapFailureToMessage(l)),
        (r) => GetMyListFavoritesLoaded(r));
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
