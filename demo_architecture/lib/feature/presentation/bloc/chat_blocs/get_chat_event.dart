part of 'get_chat_bloc.dart';

abstract class GetChatEvent extends Equatable {
  const GetChatEvent();

  @override
  List<Object> get props => [];
}

class FetchChatEvent extends GetChatEvent {
  final GetChatParam params;

  FetchChatEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
