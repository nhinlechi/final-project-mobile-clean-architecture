part of 'get_recommend_place_bloc.dart';

abstract class GetRecommendPlaceEvent extends Equatable {
  const GetRecommendPlaceEvent();
}

class SubmitGetRecommendPlace extends GetRecommendPlaceEvent {
  final GetRecommendPlaceParams params;

  SubmitGetRecommendPlace({@required this.params});

  @override
  List<Object> get props => [params];
}
