part of 'get_recommend_place_bloc.dart';

abstract class GetRecommendPlaceState extends Equatable {
  const GetRecommendPlaceState();

  @override
  List<Object> get props => [];
}

class GetRecommendPlaceInitial extends GetRecommendPlaceState {}

class GetRecommendPlaceLoading extends GetRecommendPlaceState {}

class GetRecommendPlaceSuccess extends GetRecommendPlaceState {
  final List<PlaceEntity> places;

  GetRecommendPlaceSuccess({@required this.places});
}

class GetRecommendPlaceFailed extends GetRecommendPlaceState {}
