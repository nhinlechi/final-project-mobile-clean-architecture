import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_recommend_place_event.dart';

part 'get_recommend_place_state.dart';

class GetRecommendPlaceBloc
    extends Bloc<GetRecommendPlaceEvent, GetRecommendPlaceState> {
  final GetRecommendPlaceUC useCase;

  GetRecommendPlaceBloc({@required this.useCase})
      : super(GetRecommendPlaceInitial());

  @override
  Stream<GetRecommendPlaceState> mapEventToState(
    GetRecommendPlaceEvent event,
  ) async* {
    if (event is SubmitGetRecommendPlace) {
      yield GetRecommendPlaceLoading();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (l) => GetRecommendPlaceFailed(),
        (data) => GetRecommendPlaceSuccess(places: data),
      );
    }
  }
}
