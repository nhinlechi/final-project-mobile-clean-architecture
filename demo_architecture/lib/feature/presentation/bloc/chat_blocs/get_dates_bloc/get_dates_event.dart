part of 'get_dates_bloc.dart';

abstract class GetDatesEvent extends Equatable {
  const GetDatesEvent();
}

class FetchDatesEvent extends GetDatesEvent {
  final GetDatesParams params;

  FetchDatesEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
