part of 'get_dates_bloc.dart';

abstract class GetDatesState extends Equatable {
  const GetDatesState();

  @override
  List<Object> get props => [];
}

class GetDatesInitial extends GetDatesState {}

class GetDatesLoading extends GetDatesState {}

class GetDatesFailed extends GetDatesState {}

class GetDatesSuccess extends GetDatesState {
  final List<DateModel> dates;

  GetDatesSuccess({@required this.dates});
}
