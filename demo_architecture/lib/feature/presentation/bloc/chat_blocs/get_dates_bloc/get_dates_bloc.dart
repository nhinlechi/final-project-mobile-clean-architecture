import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'get_dates_event.dart';

part 'get_dates_state.dart';

class GetDatesBloc extends Bloc<GetDatesEvent, GetDatesState> {
  final GetDatesUC useCase;

  GetDatesBloc({@required this.useCase}) : super(GetDatesInitial());

  @override
  Stream<GetDatesState> mapEventToState(
    GetDatesEvent event,
  ) async* {
    if (event is FetchDatesEvent) {
      yield GetDatesLoading();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (l) => GetDatesFailed(),
        (data) => GetDatesSuccess(dates: data),
      );
    }
  }
}
