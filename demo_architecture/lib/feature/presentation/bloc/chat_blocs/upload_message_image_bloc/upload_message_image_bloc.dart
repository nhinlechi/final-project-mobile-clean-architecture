import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'upload_message_image_event.dart';

part 'upload_message_image_state.dart';

class UploadMessageImageBloc
    extends Bloc<UploadMessageImageEvent, UploadMessageImageState> {
  final UploadMessageImageUC useCase;

  UploadMessageImageBloc({@required this.useCase}) : super(UploadMessageImageInitial());

  @override
  Stream<UploadMessageImageState> mapEventToState(
    UploadMessageImageEvent event,
  ) async* {
    //
    if (event is GetLocalImageSuccessEvent) {
      yield HavingLocalImageState(pickedImage: event.pickedImage);
    }
    //
    if (event is DeleteLocalImageEvent) {
      yield NoLocalImageState();
    }
    //
    if (event is StartUploadAndSendImageEvent) {
      yield UploadImageLoadingState();
      final rs = await useCase.call(event.params);
      //
      yield rs.fold(
        (_) => UploadImageFailedState(),
        (data) => UploadImageSuccessState(imageUrl: data),
      );
    }
  }
}
