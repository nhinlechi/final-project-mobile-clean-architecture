part of 'upload_message_image_bloc.dart';

abstract class UploadMessageImageState extends Equatable {
  const UploadMessageImageState();

  @override
  List<Object> get props => [];
}

class UploadMessageImageInitial extends UploadMessageImageState {}

class HavingLocalImageState extends UploadMessageImageState {
  final File pickedImage;

  const HavingLocalImageState({@required this.pickedImage});

  @override
  List<Object> get props => [pickedImage];
}

class NoLocalImageState extends UploadMessageImageState {}

class UploadImageSuccessState extends UploadMessageImageState {
  final String imageUrl;

  UploadImageSuccessState({@required this.imageUrl});
}

class UploadImageFailedState extends UploadMessageImageState {}

class UploadImageLoadingState extends UploadMessageImageState {}
