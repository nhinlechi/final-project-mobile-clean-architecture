part of 'upload_message_image_bloc.dart';

abstract class UploadMessageImageEvent extends Equatable {
  const UploadMessageImageEvent();

  @override
  List<Object> get props => [];
}

class GetLocalImageSuccessEvent extends UploadMessageImageEvent {
  final File pickedImage;

  GetLocalImageSuccessEvent({@required this.pickedImage});

  @override
  List<Object> get props => [pickedImage];
}

class StartUploadAndSendImageEvent extends UploadMessageImageEvent {
  final UploadMessageImageParams params;

  StartUploadAndSendImageEvent({@required this.params});

  @override
  List<Object> get props => [params];
}

class DeleteLocalImageEvent extends UploadMessageImageEvent {}
