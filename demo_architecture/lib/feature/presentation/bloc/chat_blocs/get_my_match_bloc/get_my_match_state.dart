part of 'get_my_match_bloc.dart';

abstract class GetMyMatchState extends Equatable {
  const GetMyMatchState();

  @override
  List<Object> get props => [];
}

class GetMyMatchInitial extends GetMyMatchState {}

class GetMyMatchLoading extends GetMyMatchState {}

class GetMyMatchFailed extends GetMyMatchState {
  final String message;

  GetMyMatchFailed(this.message);
}

class GetMyMatchSuccess extends GetMyMatchState {
  final List<MatchRoomModel> matchRooms;

  GetMyMatchSuccess({@required this.matchRooms});

  @override
  List<Object> get props => [matchRooms];
}
