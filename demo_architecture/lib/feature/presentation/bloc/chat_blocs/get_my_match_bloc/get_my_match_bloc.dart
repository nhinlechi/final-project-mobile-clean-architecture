import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../data/models/chat/match_room_model.dart';
import '../../../../domain/usecase/matching/get_my_match_uc.dart';

part 'get_my_match_event.dart';

part 'get_my_match_state.dart';

class GetMyMatchBloc extends Bloc<GetMyMatchEvent, GetMyMatchState> {
  final GetMyMatchUC useCase;
  final MatchRepositoryProvider provider;

  GetMyMatchBloc({
    @required this.useCase,
    @required this.provider,
  }) : super(GetMyMatchInitial());

  @override
  Stream<GetMyMatchState> mapEventToState(
    GetMyMatchEvent event,
  ) async* {
    if (event is GetAllMyMatchEvent) {
      yield GetMyMatchLoading();
      final rs = await useCase.call(NoParams());
      yield rs.fold(
        (failure) => GetMyMatchFailed(failure.message),
        (data) {
          provider.matchRooms.addAll(data);
          return GetMyMatchSuccess(matchRooms: data);
        },
      );
    }
  }
}
