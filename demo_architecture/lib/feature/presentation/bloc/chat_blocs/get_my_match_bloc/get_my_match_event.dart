part of 'get_my_match_bloc.dart';

abstract class GetMyMatchEvent extends Equatable {
  const GetMyMatchEvent();

  @override
  List<Object> get props => [];
}

class GetAllMyMatchEvent extends GetMyMatchEvent {}
