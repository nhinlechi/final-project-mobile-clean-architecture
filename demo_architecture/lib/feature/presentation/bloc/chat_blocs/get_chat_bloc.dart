import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../data/models/chat/message_model.dart';
import '../../../domain/usecase/chats/get_chat_messages_uc.dart';

part 'get_chat_event.dart';
part 'get_chat_state.dart';

class GetChatBloc extends Bloc<GetChatEvent, GetChatState> {
  final GetChatUC useCase;

  GetChatBloc({@required this.useCase}) : super(GetChatInitial());

  @override
  Stream<GetChatState> mapEventToState(
    GetChatEvent event,
  ) async* {
    if (event is FetchChatEvent) {
      yield GetChatLoading();
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => GetChatFailed(message: 'get message failed'),
        (data) {
          // TODO: Cache to repos provider here!
          return GetChatSucceed(messages: data);
        },
      );
    }
  }
}
