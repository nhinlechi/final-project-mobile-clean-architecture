part of 'get_chat_bloc.dart';

abstract class GetChatState extends Equatable {
  const GetChatState();

  @override
  List<Object> get props => [];
}

class GetChatInitial extends GetChatState {}

class GetChatSucceed extends GetChatState {
  final List<MessageModel> messages;

  GetChatSucceed({@required this.messages});
}

class GetChatFailed extends GetChatState {
  final String message;

  GetChatFailed({@required this.message});
}

class GetChatLoading extends GetChatState {}
