part of 'matching_bloc.dart';

abstract class MatchingEvent extends Equatable {
  const MatchingEvent();

  @override
  List<Object> get props => [];
}

class MatchingSuccessEvent extends MatchingEvent {
  final String matchRoomId;

  MatchingSuccessEvent({@required this.matchRoomId});

  @override
  List<Object> get props => [matchRoomId];
}
