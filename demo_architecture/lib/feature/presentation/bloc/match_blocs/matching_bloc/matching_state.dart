part of 'matching_bloc.dart';

abstract class MatchState extends Equatable {
  const MatchState();

  @override
  List<Object> get props => [];
}

class MatchingInitial extends MatchState {}

/// When 2 people accept match
class MatchSuccessState extends MatchState {}

/// 2 people Matching and begin chat with each other
class MatchingState extends MatchState {}