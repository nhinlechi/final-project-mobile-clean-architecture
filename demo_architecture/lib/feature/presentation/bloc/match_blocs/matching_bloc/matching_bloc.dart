import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/data/models/chat/match_room_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../repository_providers/match_repository_provider.dart';

part 'matching_event.dart';

part 'matching_state.dart';

class MatchingBloc extends Bloc<MatchingEvent, MatchState> {
  MatchRepositoryProvider matchRepositoryProvider;

  MatchingBloc({@required this.matchRepositoryProvider})
      : super(MatchingInitial());

  @override
  Stream<MatchState> mapEventToState(
    MatchingEvent event,
  ) async* {
    if (event is MatchingSuccessEvent) {
      matchRepositoryProvider.currentMatchRoomId = event.matchRoomId;
      matchRepositoryProvider.matchRooms.add(
        MatchRoomModel(
          participants: matchRepositoryProvider.currentMatchingInfo.users,
          matchId: event.matchRoomId,
        ),
      );
      yield MatchSuccessState();
      yield MatchingState();
    }
  }
}
