part of 'confirm_match_bloc.dart';

abstract class ConfirmMatchEvent extends Equatable {
  const ConfirmMatchEvent();

  @override
  List<Object> get props => [];
}


class ConfirmMatchingEvent extends ConfirmMatchEvent {
  final ConfirmMatchingParams params;

  const ConfirmMatchingEvent({@required this.params});

  @override
  List<Object> get props => [params];
}
