part of 'confirm_match_bloc.dart';

abstract class ConfirmMatchState extends Equatable {
  const ConfirmMatchState();

  @override
  List<Object> get props => [];
}

class ConfirmMatchingInitialState extends ConfirmMatchState {}

class ConfirmMatchingLoadingState extends ConfirmMatchState {}

class ConfirmMatchingFailureState extends ConfirmMatchState {
  final String message;

  ConfirmMatchingFailureState({@required this.message});
}

class ConfirmMatchingSuccessState extends ConfirmMatchState {}
