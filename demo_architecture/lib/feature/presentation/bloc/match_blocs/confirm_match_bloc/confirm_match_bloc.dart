import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../domain/usecase/matching/confirm_matching_uc.dart';

part 'confirm_match_event.dart';
part 'confirm_match_state.dart';

class ConfirmMatchBloc extends Bloc<ConfirmMatchEvent, ConfirmMatchState> {
  final ConfirmMatchingUC confirmMatchingUC;

  ConfirmMatchBloc({
    @required this.confirmMatchingUC,
  }) : super(ConfirmMatchingInitialState());

  @override
  Stream<ConfirmMatchState> mapEventToState(
    ConfirmMatchEvent event,
  ) async* {
    if (event is ConfirmMatchingEvent) {
      yield ConfirmMatchingLoadingState();
      final rs = await confirmMatchingUC.call(event.params);
      yield rs.fold(
        (l) =>
            ConfirmMatchingFailureState(message: 'can\'t confirm your match'),
        (r) => ConfirmMatchingSuccessState(),
      );
    }
  }

  @override
  Future<void> close() {
    // TODO: implement close
    log('Matching bloc close', name: 'Matching Bloc');
    return super.close();
  }
}
