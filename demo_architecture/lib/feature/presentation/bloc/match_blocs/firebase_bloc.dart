import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../domain/usecase/matching/update_firebase_uc.dart';

part 'firebase_event.dart';
part 'firebase_state.dart';

class FirebaseBloc extends Bloc<FirebaseEvent, FirebaseState> {
  final UpdateFirebaseTokenUC useCase;

  FirebaseBloc({@required this.useCase}) : super(FirebaseInitial());

  @override
  Stream<FirebaseState> mapEventToState(
    FirebaseEvent event,
  ) async* {
    if (event is UpdateFirebaseTokenEvent) {
      final rs = await useCase.call(event.firebaseToken);
      yield rs.fold(
        (failure) => UpdateFirebaseFailed(),
        (_) => UpdateFirebaseSuccess(),
      );
    }
  }
}
