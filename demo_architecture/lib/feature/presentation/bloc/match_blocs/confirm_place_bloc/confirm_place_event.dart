part of 'confirm_place_bloc.dart';

abstract class ConfirmPlaceEvent extends Equatable {
  const ConfirmPlaceEvent();

  @override
  List<Object> get props => [];
}

class ReceiveRequestPlaceEvent extends ConfirmPlaceEvent {
  final RequestPlaceEntity data;

  ReceiveRequestPlaceEvent({@required this.data});

  @override
  List<Object> get props => [data];
}

class ConfirmDatingEvent extends ConfirmPlaceEvent {
  final RequestPlaceEntity data;
  final ConfirmPlaceParams params;

  ConfirmDatingEvent({@required this.data, @required this.params});

  @override
  List<Object> get props => [params];
}

class RejectDatingEvent extends ConfirmPlaceEvent {
  final RequestPlaceEntity data;
  final ConfirmPlaceParams params;

  RejectDatingEvent({@required this.data,@required this.params});

  @override
  List<Object> get props => [params];
}
