import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../domain/entities/place/place_entity.dart';
import '../../../../domain/usecase/matching/confirm_place_uc.dart';

part 'confirm_place_event.dart';
part 'confirm_place_state.dart';

class RequestPlaceEntity extends Equatable {
  final String matchId;
  final String notificationId;
  final String senderId;
  final PlaceEntity place;
  final int datingTime;

  RequestPlaceEntity({
    @required this.matchId,
    @required this.notificationId,
    @required this.senderId,
    @required this.place,
    @required this.datingTime,
  });

  @override
  List<Object> get props => [notificationId];
}

class ConfirmPlaceBloc extends Bloc<ConfirmPlaceEvent, ConfirmPlaceState> {
  ConfirmPlaceUC useCase;

  ConfirmPlaceBloc({@required this.useCase}) : super(ConfirmPlaceInitial());

  @override
  Stream<ConfirmPlaceState> mapEventToState(
    ConfirmPlaceEvent event,
  ) async* {
    if (event is ReceiveRequestPlaceEvent) {
      yield HavingRequestPlaceState(data: event.data);
    } else if (event is ConfirmDatingEvent) {
      yield ConfirmPlaceLoading();
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => ConfirmPlaceFailed(),
        (_) => ConfirmPlaceSuccess(data: event.data),
      );
    } else if (event is RejectDatingEvent) {
      yield ConfirmPlaceLoading();
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => ConfirmPlaceFailed(),
        (_) => RejectPlaceSuccess(),
      );
    }
  }
}
