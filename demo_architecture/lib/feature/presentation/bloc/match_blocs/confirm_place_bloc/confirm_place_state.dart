part of 'confirm_place_bloc.dart';

abstract class ConfirmPlaceState extends Equatable {
  const ConfirmPlaceState();

  @override
  List<Object> get props => [];
}

class ConfirmPlaceInitial extends ConfirmPlaceState {}

class HavingRequestPlaceState extends ConfirmPlaceState {
  final RequestPlaceEntity data;

  HavingRequestPlaceState({@required this.data});

  @override
  List<Object> get props => [data];
}

class ConfirmPlaceLoading extends ConfirmPlaceState {}

class ConfirmPlaceSuccess extends ConfirmPlaceState {
  final RequestPlaceEntity data;

  ConfirmPlaceSuccess({@required this.data});

  @override
  List<Object> get props => [data];
}

class RejectPlaceSuccess extends ConfirmPlaceState {}

class ConfirmPlaceFailed extends ConfirmPlaceState {}
