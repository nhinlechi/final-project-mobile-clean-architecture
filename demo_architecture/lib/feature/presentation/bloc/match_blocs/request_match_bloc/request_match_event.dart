part of 'request_match_bloc.dart';

abstract class RequestMatchEvent extends Equatable {
  const RequestMatchEvent();

  @override
  List<Object> get props => [];
}

class RequestMatchNowEvent extends RequestMatchEvent {
  final RequestMatchParams params;

  const RequestMatchNowEvent({@required this.params});

  @override
  List<Object> get props => [params];
}

class RequestMatchLaterEvent extends RequestMatchEvent {
  final RequestMatchParams params;

  RequestMatchLaterEvent({@required this.params});

  @override
  List<Object> get props => [params];
}

class CancelRequestMatchEvent extends RequestMatchEvent {}
