import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:demo_architecture/feature/domain/usecase/matching/request_match_later_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../../../../core/usecases/usecase.dart';
import '../../../../domain/usecase/matching/cancel_request_match_uc.dart';
import '../../../../domain/usecase/matching/request_match_uc.dart';

part 'request_match_event.dart';

part 'request_match_state.dart';

class RequestMatchBloc extends Bloc<RequestMatchEvent, RequestMatchState> {
  final RequestMatchNowUC requestMatchUC;
  final CancelRequestMatchUC cancelRequestMatchUC;
  final RequestMatchLaterUC requestMatchLaterUC;

  RequestMatchBloc({
    @required this.requestMatchUC,
    @required this.requestMatchLaterUC,
    @required this.cancelRequestMatchUC,
  }) : super(NoRequestingMatchState());

  @override
  Stream<RequestMatchState> mapEventToState(
    RequestMatchEvent event,
  ) async* {
    if (event is RequestMatchNowEvent) {
      yield RequestMatchLoadingState();
      final rs = await requestMatchUC.call(event.params);
      // Start timer
      startIncTimer();
      // -----------
      yield rs.fold(
        (failure) => RequestMatchingFailureState(message: failure.message),
        (status) => RequestMatchNowSuccessState(waitingTimerStream: _waitingTimerStream),
      );
      if(rs.isLeft()) {
        await Future.delayed(Duration(seconds: 1));
        yield NoRequestingMatchState();
      }
    } else if (event is CancelRequestMatchEvent) {
      // Stop timer
      stopTimer();
      // ----------
      final rs = await cancelRequestMatchUC.call(NoParams());
      yield rs.fold(
        (failure) =>
            RequestMatchingFailureState(message: failure.message),
        (status) => NoRequestingMatchState(),
      );
    } else if (event is RequestMatchLaterEvent) {
      yield RequestMatchLoadingState();
      final rs = await requestMatchLaterUC.call(event.params);
      //
      yield rs.fold(
        (failure) => RequestMatchingFailureState(message: failure.message),
        (r) => RequestMatchLaterSuccessState(),
      );
      if(rs.isLeft()) {
        await Future.delayed(Duration(seconds: 1));
        yield NoRequestingMatchState();
      }
    }
  }

  Timer timer;
  int waitingTimeInSeconds = 0;
  final waitingTimerController = StreamController<int>.broadcast();

  void startIncTimer() {
    // reset
    timer?.cancel();
    waitingTimeInSeconds = 0;
    // start periodic
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      waitingTimerController.sink.add(++waitingTimeInSeconds);
    });
  }

  void stopTimer() {
    timer?.cancel();
  }

  Stream get _waitingTimerStream => waitingTimerController.stream;

  @override
  Future<void> close() {
    waitingTimerController.close();
    return super.close();
  }
}
