part of 'request_match_bloc.dart';

abstract class RequestMatchState extends Equatable {
  const RequestMatchState();

  @override
  List<Object> get props => [];
}

class NoRequestingMatchState extends RequestMatchState {}

class RequestMatchLoadingState extends RequestMatchState {}

class RequestMatchNowSuccessState extends RequestMatchState {
  final Stream waitingTimerStream;

  RequestMatchNowSuccessState({@required this.waitingTimerStream});
}

class RequestMatchLaterSuccessState extends RequestMatchState {
  
}

class RequestMatchingFailureState extends RequestMatchState {
  final String message;

  RequestMatchingFailureState({@required this.message});
}
