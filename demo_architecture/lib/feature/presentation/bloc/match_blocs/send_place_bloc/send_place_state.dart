part of 'send_place_bloc.dart';

abstract class SendPlaceState extends Equatable {
  const SendPlaceState();

  List<Object> get props => [];
}

class SendPlaceInitial extends SendPlaceState {}

class SendPlaceLoading extends SendPlaceState {}

class SendPlaceSuccess extends SendPlaceState {}

class SendPlaceFailed extends SendPlaceState {}
