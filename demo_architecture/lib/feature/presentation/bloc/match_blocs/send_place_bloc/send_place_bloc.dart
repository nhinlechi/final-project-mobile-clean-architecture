import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../domain/usecase/matching/send_place_uc.dart';

part 'send_place_event.dart';
part 'send_place_state.dart';

class SendPlaceBloc extends Bloc<SendPlaceEvent, SendPlaceState> {
  final SendPlaceUC useCase;

  SendPlaceBloc({@required this.useCase}) : super(SendPlaceInitial());

  @override
  Stream<SendPlaceState> mapEventToState(
    SendPlaceEvent event,
  ) async* {
    if (event is SendDatingPlaceEvent) {
      yield SendPlaceLoading();
      final rs = await useCase.call(event.params);
      yield rs.fold(
        (failure) => SendPlaceFailed(),
        (_) => SendPlaceSuccess(),
      );
    }
  }
}
