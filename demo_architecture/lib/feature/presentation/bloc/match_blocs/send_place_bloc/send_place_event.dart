part of 'send_place_bloc.dart';

abstract class SendPlaceEvent extends Equatable {
  const SendPlaceEvent();

  @override
  List<Object> get props => [];
}

class SendDatingPlaceEvent extends SendPlaceEvent {
  final SendPlaceParams params;

  SendDatingPlaceEvent({@required this.params});
}
