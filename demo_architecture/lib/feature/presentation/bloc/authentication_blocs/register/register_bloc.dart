import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import '../../../../../core/error/failures.dart';
import '../../../../domain/entities/user/auth_token.dart';
import '../../../../domain/entities/user/user_inform.dart';
import '../../../../domain/usecase/user/register_social_usecase.dart';
import '../../../../domain/usecase/user/register_uc.dart';
import '../../../../domain/usecase/user/update_user_inform_uc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'register_event.dart';
part 'register_state.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final RegisterUseCase registerUseCase;
  final RegisterSocialUseCase registerSocialUseCase;

  RegisterBloc(
      {@required RegisterUseCase useCase, RegisterSocialUseCase socialUseCase})
      : assert(useCase != null),
        assert(socialUseCase != null),
        registerSocialUseCase = socialUseCase,
        registerUseCase = useCase,
        super(RegisterInitial());

  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if (event is RegisterSubmited) {
      yield RegisterLoading();
      final failureOrSuccess = await registerUseCase(event.paramsRegister);
      yield* _eitherLoadedOrErrorState(failureOrSuccess);
    }
    if (event is RegisterSocialSubmited) {
      yield RegisterLoading();
      final failureOrSuccess =
          await registerSocialUseCase(event.updateUserInformParams);
      yield* _eitherLoadedOrErrorStateSocial(failureOrSuccess);
    }
  }

  Stream<RegisterState> _eitherLoadedOrErrorState(
      Either<Failure, bool> failureOrSuccess) async* {
    yield failureOrSuccess.fold(
        (l) => RegisterError(_mapFailureToMessage(l)), (r) => RegisterLoaded());
  }

  Stream<RegisterState> _eitherLoadedOrErrorStateSocial(
      Either<Failure, void> failureOrSuccess) async* {
    yield failureOrSuccess.fold(
        (l) => RegisterError(_mapFailureToMessage(l)), (r) => RegisterLoaded());
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case APIFailure:
        return "SERVER_FAILURE_MESSAGE";
      case CacheFailure:
        return "CACHE_FAILURE_MESSAGE";
      default:
        return 'Unexpected error';
    }
  }
}
