part of 'register_bloc.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitial extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterLoading extends RegisterState {
  @override
  List<Object> get props => [];
}


class RegisterLoaded extends RegisterState {


  @override
  List<Object> get props => [];
}


class RegisterError extends RegisterState {
  final String message;

  RegisterError(this.message);



  @override
  List<Object> get props => [message];
}
