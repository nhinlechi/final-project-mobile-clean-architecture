part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class RegisterSubmited extends RegisterEvent{
  final ParamsRegister paramsRegister;

  RegisterSubmited(this.paramsRegister);


  @override
  // TODO: implement props
  List<Object> get props => [paramsRegister];

}

class RegisterSocialSubmited extends RegisterEvent{
  final UpdateUserInformParams updateUserInformParams;

  RegisterSocialSubmited(this.updateUserInformParams);


  @override
  // TODO: implement props
  List<Object> get props => [updateUserInformParams];

}
