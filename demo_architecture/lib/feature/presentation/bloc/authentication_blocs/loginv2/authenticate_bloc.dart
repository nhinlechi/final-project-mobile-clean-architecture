import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:demo_architecture/feature/data/datasources/localsources/autheticate_local_data.dart';
import 'package:demo_architecture/feature/domain/usecase/user/auto_login_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/user/logout_uc.dart';
import '../../../repository_providers/user_repository_provider.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/failures.dart';
import '../../../../../core/usecases/usecase.dart';
import '../../../../domain/entities/user/auth_token.dart';
import '../../../../domain/usecase/user/login_fb_uc.dart';
import '../../../../domain/usecase/user/login_gg_uc.dart';
import '../../../../domain/usecase/user/login_uc.dart';

part 'authenticate_event.dart';

part 'authenticate_state.dart';

class AuthenticateBloc extends Bloc<AuthenticateEvent, AuthenticateState> {
  final LoginUseCase loginUseCase;
  final LoginGoogleUseCase loginGoogleUseCase;
  final LoginFacebookUseCase loginFacebookUseCase;
  final UserRepositoryProvider userRepositoryProvider;
  final LogoutUC logoutUC;
  final AutoLoginUC autoLoginUC;

  AuthenticateBloc({
    @required this.loginUseCase,
    @required this.loginGoogleUseCase,
    @required this.loginFacebookUseCase,
    @required this.userRepositoryProvider,
    @required this.logoutUC,
    @required this.autoLoginUC,
  }) : super(AuthenticateInitial());

  @override
  Stream<AuthenticateState> mapEventToState(
    AuthenticateEvent event,
  ) async* {
    if (event is LoginSubmitted) {
      yield AuthenticateLoading();
      final failureOrsuccess =
          await loginUseCase(Params(event.username, event.password));
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
    if (event is LoginGGEvent) {
      yield AuthenticateLoading();
      final failureOrsuccess = await loginGoogleUseCase(NoParams());
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
    if (event is LoginFBEvent) {
      yield AuthenticateLoading();
      final failureOrsuccess = await loginFacebookUseCase(NoParams());
      yield* _eitherLoadedOrErrorState(failureOrsuccess);
    }
    if (event is LogoutEvent) {
      yield AuthenticateLoading();
      await logoutUC.call(NoParams());
      yield LogoutState();
    }
    if (event is AutoLoginEvent) {
      yield AuthenticateLoading();
      final rs = await autoLoginUC.call(NoParams());
      yield* _eitherLoadedOrErrorState(rs);
    }
  }

  Stream<AuthenticateState> _eitherLoadedOrErrorState(
      Either<Failure, AuthenticateToken> failureOrsuccess) async* {
    yield failureOrsuccess.fold(
      (failure) => AuthenticateFailed(message: failure.message),
      (userLogin) {
        userRepositoryProvider.auth = userLogin;
        return AuthenticateSuccess(userLogin);
      },
    );
  }
}
