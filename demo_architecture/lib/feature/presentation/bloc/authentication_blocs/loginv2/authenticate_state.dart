part of 'authenticate_bloc.dart';

abstract class AuthenticateState extends Equatable {
  const AuthenticateState();

  @override
  List<Object> get props => [];
}

class AuthenticateInitial extends AuthenticateState {}

class AuthenticateLoading extends AuthenticateState {}

class AuthenticateSuccess extends AuthenticateState {
  final AuthenticateToken authenticateToken;

  AuthenticateSuccess(this.authenticateToken);

  @override
  List<Object> get props => [authenticateToken];
}

class AuthenticateFailed extends AuthenticateState {
  final String message;

  AuthenticateFailed({@required this.message});

  @override
  List<Object> get props => [message];
}

class LogoutState extends AuthenticateState {}


