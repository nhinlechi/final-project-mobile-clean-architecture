part of 'authenticate_bloc.dart';

abstract class AuthenticateEvent extends Equatable {
  const AuthenticateEvent();

  @override
  List<Object> get props => [];
}

class LoginSubmitted extends AuthenticateEvent {
  final String username;
  final String password;

  LoginSubmitted(this.username, this.password);

  @override
  List<Object> get props => [username, password];
}

class LoginGGEvent extends AuthenticateEvent {}

class LoginFBEvent extends AuthenticateEvent {}

class AutoLoginEvent extends AuthenticateEvent {}

class LogoutEvent extends AuthenticateEvent {}
