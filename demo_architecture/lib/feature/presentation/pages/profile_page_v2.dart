import 'package:flutter/material.dart';

import '../screens/profile_screens/avatar_slider.dart';
import '../screens/profile_screens/profile_info.dart';

class ProfilePageV2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Stack(
          children: [
            AvatarSlider(),
            ProfileInfo(),
          ],
        ),
      ),
    );
  }
}
