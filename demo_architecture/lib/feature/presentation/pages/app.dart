import 'package:bot_toast/bot_toast.dart';
import 'package:demo_architecture/feature/presentation/bloc/authentication_blocs/loginv2/authenticate_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/match_blocs/firebase_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/match_blocs/matching_bloc/matching_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import 'package:demo_architecture/feature/presentation/pages/home_page.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/group_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/place_comments_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/add_group_screens/add_group_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/create_group_event_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/group_detail_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/upcoming_events_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/place_detail_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/place_map_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/search_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/notification_screens/confirm_request_join_group_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:demo_architecture/feature/presentation/bloc/chat_blocs/get_dates_bloc/get_dates_bloc.dart';
import 'package:flutter/cupertino.dart';
import '../../../injection_container.dart';
import '../bloc/match_blocs/request_match_bloc/request_match_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/group_blocs/group_scroll_bloc/group_scroll_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/place_blocs/place_scroll_bloc/place_scroll_bloc.dart';
import '../bloc/chat_blocs/get_my_match_bloc/get_my_match_bloc.dart';
import '../bloc/group_blocs/get_list_tag_group_bloc/get_list_tag_group_bloc.dart';
import '../bloc/group_blocs/group_bloc.dart';
import '../bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import '../bloc/match_blocs/send_place_bloc/send_place_bloc.dart';
import '../bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import '../bloc/place_blocs/get_places_bloc/get_places_bloc.dart';
import '../bloc/place_blocs/get_top_place/top_place_bloc.dart';

import '../bloc/user_blocs/user_inform/user_inform_bloc.dart';
import '../repository_providers/user_repository_provider.dart';
import '../screens/call_video_screens/call_video_screen.dart';
import '../screens/call_video_screens/incoming_call_screen.dart';
import '../screens/chat_screens/chat_screen.dart';
import '../screens/find_people_screens/countdown_screen.dart';
import '../screens/find_people_screens/stranger_filter_screen.dart';
import '../screens/group_screens/my_group_screen.dart';
import '../screens/login_screens/login_screen.dart';
import '../screens/login_screens/login_with_email_screen.dart';
import '../themes/colors.dart';
import 'find_people_page.dart';
import 'splash.dart';

class App extends StatelessWidget {
  const App({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => sl<UserRepositoryProvider>(),
        ),
        RepositoryProvider(
          create: (context) => sl<MatchRepositoryProvider>(),
        ),
        RepositoryProvider(
          create: (context) => sl<PlaceCommentsRP>(),
        ),
        RepositoryProvider(
          create: (context) => sl<GroupRepositoryProvider>(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => sl<AuthenticateBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<UserInformBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<FirebaseBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GetNotificationsBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<RequestMatchBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<TopPlaceBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GetPlacesBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GroupBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GetListTagGroupBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<SendPlaceBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GetMyMatchBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<ConfirmPlaceBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<PlaceScrollBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GroupScrollBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<GetDatesBloc>(),
          ),
          BlocProvider(
            create: (context) => sl<MatchingBloc>(),
          ),
        ],
        child: RootApp(),
      ),
    );
  }
}

class RootApp extends StatefulWidget {
  @override
  _RootAppState createState() => _RootAppState();
}

class _RootAppState extends State<RootApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: _navigatorKey,
      theme: ThemeData(
        primaryColor: MyAppTheme.primary,
        backgroundColor: Colors.white,
        accentColor: MyAppTheme.accentColor,
        primarySwatch: Colors.orange,
        sliderTheme: SliderThemeData(
          valueIndicatorTextStyle: TextStyle(
            color: MyAppTheme.secondary,
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
            primary: MyAppTheme.primary,
          ),
        ),
        buttonTheme: ButtonThemeData(
          textTheme: ButtonTextTheme.normal,
          splashColor: MyAppTheme.secondary,
        ),
      ),
      navigatorObservers: [BotToastNavigatorObserver()],
      home: LoginScreen(),
      builder: BotToastInit(),
      onGenerateRoute: routes,
    );
  }

  Route<dynamic> routes(RouteSettings settings) {
    switch (settings.name) {
      case SplashPage.routeName:
        return MaterialPageRoute(builder: (context) => SplashPage());
      case LoginScreen.routeName:
        return MaterialPageRoute(builder: (context) => LoginScreen());
      case HomePage.routeName:
        return MaterialPageRoute(
          builder: (context) => HomePage(),
          settings: RouteSettings(name: HomePage.routeName),
        );
      case LoginWithEmailScreen.routeName:
        return PageTransition(
          child: LoginWithEmailScreen(),
          type: PageTransitionType.bottomToTop,
        );
      case PlaceDetailScreen.routeName:
        {
          final ArgsPlaceDetailScreen args = settings.arguments;
          return PageTransition(
            child: PlaceDetailScreen(args: args),
            type: PageTransitionType.bottomToTop,
            settings: RouteSettings(name: PlaceDetailScreen.routeName),
          );
        }
      case PlaceMapScreen.routeName:
        {
          final ArgsPlaceMapScreen args = settings.arguments;
          return PageTransition(
            child: PlaceMapScreen(
              args: args,
            ),
            type: PageTransitionType.rightToLeft,
          );
        }
      case FindPeoplePage.routeName:
        return PageRouteBuilder(
          pageBuilder: (context, animation, secondaryAnimation) =>
              FindPeoplePage(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return FadeTransition(
              opacity: animation,
              child: child,
            );
          },
          transitionDuration: Duration(milliseconds: 600),
        );
      case ChatScreen.routName:
        {
          final ArgsChatScreen args = settings.arguments;
          return PageTransition(
            child: ChatScreen(
              args: args,
            ),
            type: PageTransitionType.rightToLeft,
          );
        }
      case StrangerFilterScreen.routeName:
        return PageTransition(
          child: StrangerFilterScreen(),
          type: PageTransitionType.topToBottom,
        );
      case SearchScreen.routeName:
        {
          final ArgsSearchScreen args = settings.arguments;
          return MaterialPageRoute(
            builder: (context) => SearchScreen(
              searchText: args.searchText,
              heroTag: args.heroTag,
            ),
          );
        }
      case GroupDetailScreen.routeName:
        {
          final ArgsGroupDetailScreen args = settings.arguments;
          return PageTransition(
            child: GroupDetailScreen(
              imageUrl: args.imageUrl,
              heroTag: args.heroTag,
              group: args.group,
            ),
            type: PageTransitionType.bottomToTop,
          );
        }
      case AddGroupScreen.routeName:
        {
          return PageTransition(
            child: AddGroupScreen(),
            type: PageTransitionType.rightToLeft,
          );
        }
      case CallVideoScreen.routeName:
        {
          final ArgsCallVideoScreen args = settings.arguments;
          return MaterialPageRoute(
            builder: (context) => CallVideoScreen(
              args: args,
            ),
          );
        }
      case IncomingCallScreen.routeName:
        {
          final args = settings.arguments as InComingCallScreenArgs;
          return MaterialPageRoute(
            builder: (context) => IncomingCallScreen(
              args: args,
            ),
          );
        }
      case MyGroupScreen.routeName:
        {
          return MaterialPageRoute(
            builder: (context) => MyGroupScreen(),
          );
        }
      case CountdownScreen.routeName:
        {
          final args = settings.arguments as CountdownScreenArgs;
          return MaterialPageRoute(
            builder: (context) => CountdownScreen(
              args: args,
            ),
          );
        }
      case UpcomingEventsScreen.routeName:
        {
          final args = settings.arguments as ArgsUpcomingEventsScreen;
          return PageTransition(
            child: UpcomingEventsScreen(args: args),
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 300),
          );
        }
      case CreateGroupEventScreen.routeName:
        {
          final args = settings.arguments as CreateGroupEventScreenArgs;
          return PageTransition(
            child: CreateGroupEventScreen(args: args),
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 300),
          );
        }
      case ConfirmRequestJoinGroupScreen.routeName:
        {
          final args = settings.arguments as ArgsConfirmRequestJoinGroupScreen;
          return MaterialPageRoute(
            builder: (context) => ConfirmRequestJoinGroupScreen(args: args),
          );
        }
    }
  }
}
