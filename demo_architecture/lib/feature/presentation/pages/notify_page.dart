import 'package:demo_architecture/core/utils/datetime_utils.dart';
import 'package:demo_architecture/feature/data/datasources/remotesources/matching/matching_remote_data.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/presentation/bloc/match_blocs/confirm_place_bloc/confirm_place_bloc.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/match_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/repository_providers/user_repository_provider.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/confirm_place_popup.dart';
import 'package:demo_architecture/feature/presentation/screens/find_people_screens/countdown_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/notification_screens/confirm_request_join_group_screen.dart';
import 'package:demo_architecture/feature/presentation/screens/notification_screens/widgets/notification_filter_popup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../data/models/notification/notification_model.dart';
import '../../domain/usecase/notifications/get_notification_uc.dart';
import '../bloc/notification_blocs/get_notifications_bloc/get_notifications_bloc.dart';
import '../screens/notification_screens/widgets/notification_item.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/generals/circular_loading.dart';
import '../widgets/generals/my_app_bar.dart';

class NotifyPage extends StatefulWidget {
  @override
  _NotifyPageState createState() => _NotifyPageState();
}

class _NotifyPageState extends State<NotifyPage> {
  List<NotificationModel> notifies = [];
  AuthenticateToken auth;

  int codeFilter = NotificationModel.ALL; // -1 -> ALL

  void _notifyFilterHandler() async {
    final code = await showMaterialModalBottomSheet(
      context: context,
      duration: Duration(milliseconds: 300),
      builder: (context) => NotificationFilterPopup(codeFilter: codeFilter),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
        ),
      ),
    );
    if (code != null) {
      setState(() {
        codeFilter = code;
      });
    }
  }

  void _openConfirmPlacePopup(String notificationId, RequestPlaceData data) {
    showCupertinoModalBottomSheet(
      context: context,
      builder: (context) => ConfirmPlacePopup(
        data: RequestPlaceEntity(
          place: data.place,
          senderId: data.senderId,
          matchId: data.matchId,
          datingTime: data.datingTime,
          notificationId: notificationId,
        ),
      ),
    );
  }

  void _openConfirmJoinGroup(String notificationId, RequestJoinGroupData data) {
    Navigator.of(context).pushNamed(
      ConfirmRequestJoinGroupScreen.routeName,
      arguments: ArgsConfirmRequestJoinGroupScreen(
        notificationId: notificationId,
        groupId: data.groupId,
        senderId: data.userId,
        reasonWantToJoinGroup: data.description,
      ),
    );
  }

  void _openConfirmMatchLaterCountdown(
      String notificationId, RequestMatchData data) {
    final currentTimeStamp = DateTime.now().millisecondsSinceEpoch;
    final timeOut = (data.timeout - currentTimeStamp) / 1000;
    Navigator.of(context).pushNamed(
      CountdownScreen.routeName,
      arguments: CountdownScreenArgs(
        mode: MatchMode.Later,
        timeRemaining: timeOut.toInt(),
        notificationId: notificationId,
        otherInfo:
            auth.userId == data.users[0].userId ? data.users[1] : data.users[0],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    // Get user info
    auth = context.read<UserRepositoryProvider>().auth;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
        title: Text(
          'Notifies',
          style: MyTextStyle.ts_AppBar_title,
        ),
        actions: [
          IconButton(
            icon: Icon(Feather.filter),
            color: MyAppTheme.primary,
            onPressed: _notifyFilterHandler,
          )
        ],
      ),
      body: getBody(context),
    );
  }

  getBody(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () async {
        final getNotificationBloc = context.read<GetNotificationsBloc>();
        getNotificationBloc.add(
          FetchNotificationsEvent(
            params: GetNotificationParams(
              page: 1,
              rowPerPage: 100,
            ),
          ),
        );
        return Future.delayed(
          Duration(seconds: 1),
        );
      },
      child: Container(
          color: MyAppTheme.secondary,
          child: BlocBuilder<GetNotificationsBloc, GetNotificationsState>(
            builder: (context, state) {
              if (state is GetNotificationsInitial) {
                context.read<GetNotificationsBloc>().add(
                      FetchNotificationsEvent(
                        params: GetNotificationParams(
                          page: 1,
                          rowPerPage: 100,
                        ),
                      ),
                    );
              }
              if (state is GetNotificationsLoading) {
                if (notifies.length == 0)
                  return CircularLoading();
                else
                  return ListView.builder(
                    itemCount: notifies.length,
                    itemBuilder: (context, index) {
                      return notificationFactory(notifies[index]);
                    },
                  );
              }
              if (state is GetNotificationsSuccess) {
                notifies = state.notifications;
                return ListView.builder(
                  itemCount: state.notifications.length,
                  itemBuilder: (context, index) {
                    if (state.notifications[index].type != codeFilter &&
                        codeFilter != NotificationModel.ALL) {
                      return Container();
                    }
                    return notificationFactory(state.notifications[index]);
                  },
                );
              }
              return SizedBox();
            },
          )),
    );
  }

  Widget notificationFactory(NotificationModel model) {
    final differentDay = DateTimeUtils.differentDayText(model.createAt);
    switch (model.type) {
      case NotificationModel.REQUEST_JOIN_GROUP:
        {
          final data = model.data as RequestJoinGroupData;
          return NotificationItem(
            title: 'Someone want to join your Group',
            subtitle: differentDay,
            subIconData: Icons.group,
            onTapCallback: () => _openConfirmJoinGroup(model.id, data),
          );
        }
      case NotificationModel.REQUEST_MATCH_LATER:
        {
          final data = model.data as RequestMatchData;
          return NotificationItem(
            avatarURL: auth.userId != data.users[0].userId
                ? data.users[0].avt[0]
                : data.users[1].avt[0],
            title: "Someone matching with you",
            subtitle: differentDay,
            subIconData: Icons.person_add_rounded,
            onTapCallback: () =>
                _openConfirmMatchLaterCountdown(model.id, data),
          );
        }
      case NotificationModel.REQUEST_PLACE:
        {
          final data = model.data as RequestPlaceData;
          final matchRooms = context.read<MatchRepositoryProvider>().matchRooms;
          final currentRoom = matchRooms.length > 0
              ? matchRooms.firstWhere((r) => r.matchId == data.matchId)
              : null;
          final otherUserInfo = currentRoom?.participants
              ?.firstWhere((p) => p.userId == data.senderId);
          final String notifyTitle = data.senderId != auth.userId
              ? '${otherUserInfo?.firstname} requested dating place'
              : 'You requested dating place to ${otherUserInfo?.firstname}';

          return NotificationItem(
            avatarURL: data.place.photo[0],
            title: notifyTitle,
            subtitle: differentDay,
            subIconData: Icons.place,
            onTapCallback: () => _openConfirmPlacePopup(model.id, data),
          );
        }
    }
    return Container();
  }
}
