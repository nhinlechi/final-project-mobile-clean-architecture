import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/groups_tab_view.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/place_tab/places_tab_view.dart';
import 'package:demo_architecture/feature/presentation/screens/home_screens/top_discoveries_tab_view.dart';
import 'package:flutter/material.dart';

import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/generals/my_app_bar.dart';

class DiscoverPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getAppBar(),
      body: getBody(),
    );
  }

  getAppBar() {
    return MyAppBar(
      title: Text(
        'Discoveries',
        style: MyTextStyle.ts_AppBar_title,
      ),
    );
  }

  getBody() {
    return Container(
      color: MyAppTheme.secondary,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: DefaultTabController(
              length: 3,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: TabBar(
                      labelPadding: EdgeInsets.all(10),
                      labelStyle: TextStyle(
                        fontSize: MyTextStyle.title_5_fontsize,
                      ),
                      labelColor: MyAppTheme.primary,
                      unselectedLabelColor: MyAppTheme.black_grey,
                      tabs: [
                        Text('For you'),
                        Text('Places'),
                        Text('Groups'),
                      ],
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        TopDiscoveriesTabView(),
                        PlacesTabView(),
                        GroupsTabView(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
