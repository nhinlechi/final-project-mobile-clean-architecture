import 'package:bot_toast/bot_toast.dart';
import 'package:demo_architecture/core/firebase/fcm_manager.dart';
import 'package:demo_architecture/feature/domain/usecase/user/get_user_inform.dart';
import 'package:demo_architecture/feature/presentation/bloc/match_blocs/matching_bloc/matching_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/match_blocs/request_match_bloc/request_match_bloc.dart';
import 'package:demo_architecture/feature/presentation/bloc/user_blocs/user_inform/user_inform_bloc.dart';
import 'package:demo_architecture/feature/presentation/pages/profile_page_v2.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/widgets/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'discover_page.dart';
import 'find_people_page.dart';
import 'match_page.dart';
import 'notify_page.dart';

class HomePage extends StatefulWidget {
  static const routeName = 'home-page';

  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _pageIndex = 0;
  List<Widget> _pages = [
    DiscoverPage(),
    NotifyPage(),
    Container(),
    MatchPage(),
    ProfilePageV2(),
  ];
  final int _chatPageIndex = 3;
  int waitingTimeInSeconds = 0;

  // FCM Manager Initialize
  FCMManager _fcmManager;

  @override
  void initState() {
    super.initState();
    // Create Firebase Cloud Messaging Manager
    _fcmManager = FCMManager(context: context);
    _fcmManager.init();
    _fcmManager.onMatchSuccess = () {
      setState(() {
        Future.delayed(Duration(milliseconds: 500), () {
          setState(() {
            _pageIndex = _chatPageIndex;
          });
        });
      });
    };

    // Get User Inform to use later
    context
        .read<UserInformBloc>()
        .add(GetUserInformEvent(GetUserInformParams(["favorites"])));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MatchingBloc, MatchState>(
      listener: (context, state) {
        if (state is MatchSuccessState) {
          context.read<RequestMatchBloc>().add(CancelRequestMatchEvent());
        }
      },
      child: Scaffold(
        bottomNavigationBar: getBottomNavbar(),
        body: getBody(),
        floatingActionButton: getFloatingActionButton(context),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }

  getBottomNavbar() {
    List<Widget> navIcons = [
      Icon(Icons.home),
      Icon(Icons.notifications),
      null,
      Icon(Icons.chat),
      Icon(Icons.person),
    ];

    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: List.generate(
          _pages.length,
          (index) {
            return index != 2
                ? IconButton(
                    icon: navIcons[index],
                    iconSize: 26,
                    color: _pageIndex == index
                        ? MyAppTheme.primary
                        : MyAppTheme.black_grey,
                    onPressed: () {
                      setState(
                        () {
                          _pageIndex = index;
                        },
                      );
                    },
                  )
                : SizedBox(
                    width: 50,
                  );
          },
        ),
      ),
    );
  }

  getBody() {
    return WillPopScope(
      onWillPop: () async {
        final confirm = await showCustomConfirmDialog(
          context,
          'Quit Application',
          'We will miss you! Do you wanna quit app?',
        );
        return confirm;
      },
      child: IndexedStack(
        index: _pageIndex,
        children: _pages,
      ),
    );
  }

  Widget getFloatingActionButton(BuildContext ctx) => FloatingActionButton(
        child: BlocBuilder(
          bloc: BlocProvider.of<RequestMatchBloc>(ctx),
          builder: (context, state) {
            if (state is NoRequestingMatchState) {
              return Icon(
                Icons.person_search,
                color: MyAppTheme.secondary,
              );
            }
            if (state is RequestMatchingFailureState) {
              BotToast.showText(text: state.message);
              return Icon(
                Icons.person_search,
                color: MyAppTheme.secondary,
              );
            }
            if (state is RequestMatchNowSuccessState) {
              return Stack(
                children: [
                  SpinKitDualRing(
                    color: MyAppTheme.secondary,
                    size: 44,
                    lineWidth: 4,
                  ),
                  StreamBuilder<int>(
                      stream: state.waitingTimerStream,
                      builder: (context, timer) {
                        if (timer.hasData) {
                          int second = timer.data % 60;
                          int minute = timer.data ~/ 60;
                          return Center(
                            child: Text(
                              '$minute:$second',
                              style: TextStyle(
                                color: MyAppTheme.secondary,
                                fontSize: 12,
                              ),
                            ),
                          );
                        }
                        return Container();
                      }),
                ],
              );
            }
            if (state is RequestMatchLaterSuccessState) {
              return SpinKitChasingDots(
                color: MyAppTheme.secondary,
                size: 24,
              );
            }
            return SpinKitDualRing(
              color: MyAppTheme.secondary,
              size: 24,
              lineWidth: 24,
            );
          },
        ),
        backgroundColor: MyAppTheme.primary,
        heroTag: 'find-people-button',
        onPressed: () {
          Navigator.of(context).pushNamed(FindPeoplePage.routeName);
        },
      );
}
