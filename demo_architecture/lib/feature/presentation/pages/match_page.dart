import 'dart:developer';

import 'package:demo_architecture/feature/presentation/screens/chat_screens/chat_tab_view.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/date_tab_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:socket_io_client/socket_io_client.dart';

import '../../../core/platform/api_base.dart';
import '../../../core/utils/get_token.dart';
import '../../../injection_container.dart';
import '../../domain/entities/user/auth_token.dart';
import '../bloc/chat_blocs/get_my_match_bloc/get_my_match_bloc.dart';
import '../data_prototype/girl.dart';
import '../repository_providers/user_repository_provider.dart';
import '../screens/chat_screens/chat_screen.dart';
import '../screens/chat_screens/widgets/current_match.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/chat_item.dart';
import '../widgets/generals/circular_loading.dart';
import '../widgets/generals/my_app_bar.dart';
import '../widgets/generals/search_bar_widget.dart';

class MatchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyAppTheme.secondary,
      appBar: getAppBar(),
      body: DefaultTabController(
        length: 2,
        child: Column(
          children: [
            TabBar(
              tabs: [
                Tab(text: 'Matches'),
                Tab(text: 'Dates'),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  ChatTabView(),
                  DateTabView(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getAppBar() {
    return MyAppBar(
      title: Text(
        'Matches',
        style: MyTextStyle.ts_AppBar_title,
      ),
      actions: [
        /*IconButton(
          icon: Icon(Feather.edit),
          color: MyAppTheme.primary,
          iconSize: 26,
          onPressed: () {},
        ),
        SizedBox(
          width: 10,
        ),*/
      ],
    );
  }
}
