import 'package:bot_toast/bot_toast.dart';
import 'package:demo_architecture/core/share_ref/stranger_filter_share_ref.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:lottie/lottie.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import '../../../injection_container.dart';
import '../../domain/usecase/matching/request_match_uc.dart';
import '../bloc/match_blocs/request_match_bloc/request_match_bloc.dart';
import '../screens/find_people_screens/stranger_filter_screen.dart';
import '../themes/colors.dart';
import '../themes/text_style.dart';
import '../widgets/generals/circle_button.dart';
import '../widgets/generals/my_app_bar.dart';

class FindPeoplePage extends StatefulWidget {
  static const String routeName = 'find-people-page';

  @override
  _FindPeoplePageState createState() => _FindPeoplePageState();
}

class _FindPeoplePageState extends State<FindPeoplePage>
    with SingleTickerProviderStateMixin {
  RequestMatchBloc _requestMatchBloc;
  AnimationController _animController;
  StrangerFilterShareRef _shareRef;

  bool matchLaterMode = false;

  void _startFindPeople(BuildContext ctx) async {
    // Stop animation for render performance
    _animController.stop();
    //
    if (matchLaterMode) {
      BlocProvider.of<RequestMatchBloc>(context).add(
        RequestMatchLaterEvent(
          params: RequestMatchParams(
            lat: _shareRef.latitude,
            long: _shareRef.longitude,
          ),
        ),
      );
    } else {
      BlocProvider.of<RequestMatchBloc>(context).add(
        RequestMatchNowEvent(
          params: RequestMatchParams(
            lat: _shareRef.latitude,
            long: _shareRef.longitude,
          ),
        ),
      );
    }

    Navigator.of(context).pop(true);
  }

  void _strangerFilter(BuildContext ctx) async {
    // Stop animation for render performance
    _animController.stop();
    // Show bottom bar
    await showBarModalBottomSheet(
      context: context,
      builder: (context) => StrangerFilterScreen(),
    );
    // Forward animation again
    _animController..repeat();
  }

  void _cancelRequestMatching() {
    BlocProvider.of<RequestMatchBloc>(context).add(CancelRequestMatchEvent());
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    super.initState();
    _requestMatchBloc = BlocProvider.of<RequestMatchBloc>(context);
    _animController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 3),
    );
    Future.delayed(const Duration(milliseconds: 600), () {
      _animController..repeat();
    });
    // Get Stranger Filter Share reference
    _shareRef = sl<StrangerFilterShareRef>();
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _animController.stop();
        return true;
      },
      child: Scaffold(
        appBar: getAppBar(),
        body: getBody(context),
      ),
    );
  }

  Widget getBody(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset(
            'assets/animations/lotties/friend.json',
            controller: _animController,
          ),
          SizedBox(
            height: 16,
          ),
          Center(
            child: Hero(
              tag: 'find-people-button',
              child: GestureDetector(
                child: Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: MyAppTheme.primary,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0, 4),
                          blurRadius: 4,
                        )
                      ]),
                  child: BlocBuilder(
                    bloc: _requestMatchBloc,
                    builder: (context, state) {
                      if (state is RequestMatchNowSuccessState ||
                          state is RequestMatchLaterSuccessState) {
                        return SpinKitDualRing(
                          color: MyAppTheme.secondary,
                          size: 40,
                          lineWidth: 40,
                        );
                      }
                      return Icon(
                        Icons.person_search,
                        color: MyAppTheme.secondary,
                        size: 40,
                      );
                    },
                  ),
                ),
                onTap: () => _startFindPeople(context),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          BlocBuilder<RequestMatchBloc, RequestMatchState>(
            builder: (context, state) {
              if (state is NoRequestingMatchState)
                return Text(
                  matchLaterMode
                      ? 'Find friend later'
                      : 'Find friend near you now!',
                  style: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                );
              if (state is RequestMatchNowSuccessState) {
                return Text(
                  'You are in match now mode',
                  style: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                );
              }
              if (state is RequestMatchLaterSuccessState) {
                return Text(
                  'You are in match later mode',
                  style: TextStyle(fontSize: MyTextStyle.title_5_fontsize),
                );
              }
              return Container();
            },
          ),
          SizedBox(
            height: 32,
          ),
          BlocBuilder<RequestMatchBloc, RequestMatchState>(
            bloc: _requestMatchBloc,
            builder: (context, state) {
              if (state is NoRequestingMatchState ||
                  state is RequestMatchingFailureState)
                return getFilterOptions();

              return CircleButton(
                onTap: _cancelRequestMatching,
                icon: Icons.clear,
                iconColor: MyAppTheme.secondary,
                backgroundColor: MyAppTheme.grey,
              );
            },
          ),
        ],
      );

  getAppBar() {
    return MyAppBar(
      title: Text(
        '',
        style: MyTextStyle.ts_AppBar_title,
      ),
      actions: [
        IconButton(
          icon: Icon(Feather.filter),
          color: MyAppTheme.primary,
          onPressed: () => _strangerFilter(context),
        )
      ],
    );
  }

  Widget getFilterOptions() => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Column(
            children: [
              CupertinoSwitch(
                value: matchLaterMode,
                activeColor: MyAppTheme.primary,
                onChanged: (value) {
                  setState(() {
                    matchLaterMode = value;
                  });
                },
              ),
              const SizedBox(height: 4),
              Text('Match Later'),
            ],
          ),
          Column(
            children: [
              ElevatedButton(
                onPressed: () => _strangerFilter(context),
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                  padding: const EdgeInsets.all(10),
                ),
                child: Icon(
                  Feather.filter,
                  color: MyAppTheme.secondary,
                ),
              ),
              const SizedBox(height: 4),
              Text('Match Filter'),
            ],
          ),
        ],
      );
}
