import 'package:flutter/material.dart';

import '../themes/colors.dart';

class AvatarChatting extends StatelessWidget {
  final bool isDefaultImage;
  final String imageUrl;
  final bool isActive;

  const AvatarChatting({
    Key key,
    @required this.imageUrl,
    this.isActive = false,
    this.isDefaultImage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 60,
      child: Stack(
        children: [
          CircleAvatar(
            backgroundImage: this.isDefaultImage ? AssetImage(imageUrl) : NetworkImage(imageUrl),
            radius: 35,
          ),
          if (isActive)
            Positioned(
              right: 0,
              bottom: 0,
              child: Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: MyAppTheme.primary,
                  border: Border.all(width: 2, color: MyAppTheme.secondary),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
