import 'package:demo_architecture/feature/presentation/screens/home_screens/group_tab/widgets/circle_box.dart';
import 'package:flutter/material.dart';

import '../themes/colors.dart';
import '../themes/text_style.dart';

enum TopDiscoveriesType {
  place,
  group,
}

class TopDiscoveriesItem extends StatelessWidget {
  final TopDiscoveriesType type;
  final String imageUrl;
  final String name;
  final String heroTag;

  const TopDiscoveriesItem({
    Key key,
    @required this.imageUrl,
    @required this.name,
    @required this.heroTag,
    @required this.type,
  }) : super(key: key);

  Widget getTypeIcon() {
    switch (this.type) {
      case TopDiscoveriesType.place:
        return CircleBox(
          color: MyAppTheme.primary,
          elevation: 0,
          child: Icon(
            Icons.place,
            color: MyAppTheme.secondary,
            size: 18,
          ),
        );
      case TopDiscoveriesType.group:
        return CircleBox(
          color: MyAppTheme.grey_light,
          elevation: 0,
          child: Icon(
            Icons.group_sharp,
            color: MyAppTheme.black_grey,
            size: 18,
          ),
        );
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: 14),
      child: Container(
        width: 120,
        child: Column(
          children: [
            Stack(
              children: [
                Hero(
                  tag: this.heroTag,
                  child: Container(
                    height: 140,
                    width: 120,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        // TODO: when get from api -> delete AssetImage()
                        image: NetworkImage(imageUrl),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.8),
                          blurRadius: 4,
                          offset: Offset(0, 4), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 5,
                  right: 5,
                  child: getTypeIcon(),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: MyTextStyle.paragraph_fontsize * 2,
              ),
              child: Text(
                this.name,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                maxLines: 2,
                style: TextStyle(fontSize: MyTextStyle.paragraph_fontsize),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
