import 'package:flutter/material.dart';

class InputFormField extends StatelessWidget {

  final String label;
  final String hintText;
  final String initText;
  final String Function(String) validatorCallback;
  final void Function(String) onSavedCallback;
  final void Function(String) onChangedCallback;
  final bool isObscure;

  const InputFormField({
    this.label,
    this.hintText,
    this.initText,
    this.validatorCallback,
    this.onSavedCallback,
    this.onChangedCallback,
    this.isObscure = false,
  });

  Widget _labelFormField({BuildContext context ,String label}){
    return Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 40.0),
            child: Text(
              label,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor,
                fontSize: 15.0,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _inputFormField({
    BuildContext context,
    String hintText,
    String initText,
    Function validatorCallback,
    Function onSavedCallback,
    Function onChangedCallback,
    bool isObscure = false,
  }){
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 40.0, right: 40.0,),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
              color: Theme.of(context).primaryColor,
              width: 0.5,
              style: BorderStyle.solid),
        ),
      ),
      padding: const EdgeInsets.only(left: 0.0, right: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: TextFormField(
              obscureText: isObscure,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: TextStyle(color: Colors.grey),
              ),
              initialValue: initText,
              onChanged: onChangedCallback,
              validator: validatorCallback,
              onSaved: onSavedCallback,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
     children: [
       _labelFormField(
         context: context,
         label: this.label,
       ),
       _inputFormField(
         context: context,
         hintText: this.hintText,
         initText: this.initText,
         onSavedCallback: this.onSavedCallback,
         validatorCallback: this.validatorCallback,
         onChangedCallback: this.onChangedCallback,
         isObscure: this.isObscure,
       ),
     ],
    );
  }
}
