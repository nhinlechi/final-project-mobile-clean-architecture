import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget {
  final Function() onTap;
  final IconData icon;
  final Color iconColor;
  final Color backgroundColor;

  const CircleButton({
    Key key,
    @required this.onTap,
    @required this.icon,
    @required this.iconColor,
    @required this.backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 50,
      child: ElevatedButton(
        onPressed: onTap,
        child: Icon(
          icon,
          color: iconColor,
        ),
        style: ElevatedButton.styleFrom(
          shape: CircleBorder(),
          primary: backgroundColor,
          padding: const EdgeInsets.all(0),
        ),
      ),
    );
  }
}
