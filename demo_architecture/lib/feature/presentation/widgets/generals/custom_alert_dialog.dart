import 'package:flutter/material.dart';

class CustomAlertDialog extends StatelessWidget {

  final String message;

  const CustomAlertDialog(this.message);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Error!'),
      content: Text(message),
      actions: [
        FlatButton(
          child: Text('Try Again'),
          onPressed: (){
            Navigator.of(context).pop();
          },
        )
      ],
    );
  }
}
