import 'package:demo_architecture/feature/data/models/utils/filter_model.dart';
import 'package:demo_architecture/feature/presentation/screens/chat_screens/widgets/abstract_filter_popup.dart';
import 'package:flutter/cupertino.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

Future<T> customFilterBottomSheet<T>({
  BuildContext context,
  String title,
  FilterModel current,
  List<FilterModel> filterModels,
}) {
  return showMaterialModalBottomSheet<T>(
    context: context,
    duration: Duration(milliseconds: 300),
    builder: (context) => AbstractFilterPopup(
      title: title,
      currentFilterModel: current,
      filterModels: filterModels,
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(16),
        topRight: Radius.circular(16),
      ),
    ),
  );
}
