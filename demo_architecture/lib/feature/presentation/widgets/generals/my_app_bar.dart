import 'package:flutter/material.dart';

import '../../themes/colors.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final List<Widget> actions;
  final Widget title;
  final bool autoLeading;
  final double titleSpacing;

  const MyAppBar({
    Key key,
    this.actions,
    @required this.title,
    this.autoLeading = true,
    this.titleSpacing = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: autoLeading,
      iconTheme: IconThemeData(color: MyAppTheme.primary),
      title: this.title,
      actions: this.actions,
      backgroundColor: MyAppTheme.secondary,
      elevation: 0,
      titleSpacing: this.titleSpacing,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => const Size.fromHeight(55);
}
