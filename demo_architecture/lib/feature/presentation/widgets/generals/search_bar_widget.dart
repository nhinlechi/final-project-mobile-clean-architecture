import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import '../../themes/colors.dart';

class SearchBarWidget extends StatelessWidget {
  final Function onTap;
  final bool readOnly;
  final String heroTag;
  final Function onChange;
  final Function onSearch;
  final FocusNode focusNode;
  final TextEditingController controller;

  const SearchBarWidget({
    Key key,
    this.onTap,
    this.readOnly,
    @required this.heroTag,
    this.onChange,
    this.focusNode,
    this.controller,
    this.onSearch,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Container(
        width: size.width,
        child: Hero(
          tag: this.heroTag,
          child: Card(
            elevation: 0,
            color: Colors.transparent,
            margin: EdgeInsets.all(0),
            child: Container(
              height: 40,
              padding: EdgeInsets.only(
                left: 16,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: MyAppTheme.grey_light,
              ),
              child: TextField(
                textInputAction: TextInputAction.search,
                controller: controller,
                onTap: onTap,
                onChanged: onChange,
                focusNode: focusNode,
                onSubmitted: onSearch,
                readOnly: readOnly == null ? false : readOnly,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Search',
                  icon: Icon(
                    Feather.search,
                    size: 18,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
