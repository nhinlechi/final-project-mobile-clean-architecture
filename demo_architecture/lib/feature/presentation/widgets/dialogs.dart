import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:demo_architecture/feature/presentation/themes/text_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

showCustomFailedDialog(BuildContext context, String message) async {
  showDialog(
    context: context,
    builder: (context) => AlertDialog(
      title: Text('Failed'),
      content: Text(
        message,
        style: TextStyle(
          fontSize: MyTextStyle.title_5_fontsize,
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text(
            'OK',
            style: TextStyle(color: MyAppTheme.secondary),
          ),
        ),
      ],
    ),
  );
}

Future<bool> showCustomConfirmDialog(
    BuildContext context, String title, String content) async {
  return showDialog<bool>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext dialogContext) {
      return AlertDialog(
        title: Text(title),
        content: Text(
          content,
          style: TextStyle(
            fontSize: MyTextStyle.title_5_fontsize,
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: Text(
              'No',
              style: TextStyle(
                color: MyAppTheme.secondary,
              ),
            ),
            onPressed: () {
              Navigator.of(dialogContext).pop(false);
            },
          ),
          TextButton(
            child: Text('Yes'),
            onPressed: () {
              Navigator.of(dialogContext).pop(true);
            },
          ),
        ],
      );
    },
  );
}
