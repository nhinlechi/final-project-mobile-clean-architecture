import 'package:demo_architecture/core/utils/datetime_utils.dart';
import 'package:demo_architecture/feature/presentation/themes/colors.dart';
import 'package:flutter/material.dart';

class DayDifferentWidget extends StatelessWidget {
  final int timeInMilliseconds;
  final IconData iconData;

  const DayDifferentWidget({
    Key key,
    this.timeInMilliseconds,
    this.iconData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final diff =
    DateTime.fromMillisecondsSinceEpoch(timeInMilliseconds).difference(
      DateTime.now(),
    );
    final isHistory = diff.inMilliseconds < 0;
    final diffText = DateTimeUtils.differentDayText(timeInMilliseconds);
    return Row(
      children: [
        Icon(
          iconData ?? Icons.update_outlined,
          color: isHistory ? MyAppTheme.black_grey : MyAppTheme.primary,
        ),
        const SizedBox(width: 8),
        Text(diffText),
      ],
    );
  }
}
