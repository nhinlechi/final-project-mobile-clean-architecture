import 'package:flutter/material.dart';

import '../themes/colors.dart';
import '../themes/text_style.dart';
import 'avatar_chatting.dart';

class ChatItem extends StatelessWidget {
  final String chatId;
  final String imageUrl;
  final String name;
  final String newestText;
  final bool isActive;
  final Function() onTap;
  final bool isDefaultImage;

  const ChatItem({
    Key key,
    @required this.chatId,
    @required this.imageUrl,
    @required this.name,
    @required this.newestText,
    @required this.onTap,
    this.isActive = false,
    this.isDefaultImage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 16, left: 16, right: 16),
      child: InkWell(
        onTap: onTap,
        splashColor: MyAppTheme.primary,
        child: Row(
          children: [
            AvatarChatting(
              imageUrl: imageUrl,
              isActive: isActive,
            ),
            SizedBox(
              width: 16,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.name,
                  style: TextStyle(
                      fontSize: MyTextStyle.title_5_fontsize,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  this.newestText,
                  style: TextStyle(),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
