import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../data/models/place/list_comment_place_model.dart';
import '../../entities/place/list_place_entity.dart';
import '../../usecase/places/comment_place_uc.dart';
import '../../usecase/places/get_places_uc.dart';

abstract class PlaceRepository {
  Future<Either<Failure, ListPlaceEntity>> getPlaces(GetPlacesParams params);

  Future<Either<Failure, ListCommentPlaceModel>> getCommentPlace(String id);

  Future<Either<Failure, bool>> commentPlace(CommentPlaceParams params);
}
