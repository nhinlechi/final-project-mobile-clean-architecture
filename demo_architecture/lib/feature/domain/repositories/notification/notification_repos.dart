import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../data/models/notification/notification_model.dart';
import '../../usecase/notifications/get_notification_uc.dart';

abstract class NotificationRepos {
  Future<Either<Failure, List<NotificationModel>>> getNotifications(
    GetNotificationParams params,
  );
}
