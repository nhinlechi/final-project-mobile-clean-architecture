import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../entities/user/user_inform.dart';
import '../../usecase/user/register_social_usecase.dart';
import '../../usecase/user/update_user_avatar_uc.dart';

abstract class UpdateBasicUserInformRepo {
  Future<Either<Failure, UserInform>> updateBasicUserInform(
      String birthDay, String lastname, String firstName, String gender);
  Future<Either<Failure, UserInform>> updateUserInformForSocialRegister(
      ParamsRegisterSocial paramsRegisterSocial);
  Future<Either<Failure, bool>> updateUserAvatar(UpdateUserAvatarParams params);
}
