import '../../entities/user/auth_token.dart';

abstract class AuthenRepository {
  Future<AuthenticateToken> logIn(String userName, String passWord);
  Future<AuthenticateToken> authenticate();
}
