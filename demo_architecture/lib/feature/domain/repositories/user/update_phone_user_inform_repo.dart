import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../entities/user/user_inform.dart';

abstract class UpdatePhoneUserInformRepo {
  Future<Either<Failure, UserInform>> updatePhoneUserInform(String phone);
}
