import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../entities/user/user_inform.dart';
import '../../usecase/user/get_user_inform.dart';
import '../../usecase/user/get_user_inform_by_id.dart';
import '../../usecase/user/update_user_inform_uc.dart';

abstract class UserInformRepository {
  Future<Either<Failure, UserInform>> getUserInform(GetUserInformParams params);
  Future<Either<Failure, UserInform>> getUserInformById(
      GetUserInformByIdParams params);
  Future<Either<Failure, void>> updateUserInform(UpdateUserInformParams params);
}
