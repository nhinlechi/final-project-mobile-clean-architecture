import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../entities/user/user_inform.dart';

abstract class UpdateRelaUserInformRepo {
  Future<Either<Failure, UserInform>> updateRelaUserInform(int rela);
}
