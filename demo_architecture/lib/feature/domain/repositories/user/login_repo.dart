import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../entities/user/auth_token.dart';
import '../../usecase/user/register_uc.dart';

abstract class LoginRepository {
  Future<Either<Failure, AuthenticateToken>> login(
      String username, String password);
  Future<Either<Failure, AuthenticateToken>> loginGG();
  Future<Either<Failure, AuthenticateToken>> loginFB();
  Future<Either<Failure, bool>> register(ParamsRegister paramsRegister);
  Future<Either<Failure, void>> logout();
  Future<Either<Failure, AuthenticateToken>> autoLogin();
}
