import 'package:dartz/dartz.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';

import '../../../../core/error/failures.dart';
import '../../../data/models/group/list_tag_model.dart';
import '../../entities/group/group_entity.dart';
import '../../entities/group/list_group_entity.dart';
import '../../usecase/group/add_new_group.dart';
import '../../usecase/group/get_group_by_id.dart';
import '../../usecase/group/get_list_group.dart';
import '../../usecase/group/leave_group.dart';
import '../../usecase/group/request_join_group_uc.dart';
import '../../usecase/group/update_group.dart';

abstract class GroupRepository {
  Future<Either<Failure, ListGroupEntity>> getListGroup(
    GetListGroupParams getListGroupParams,
  );

  Future<Either<Failure, void>> addNewGroup(
    AddNewGroupParams addNewGroupParams,
  );

  Future<Either<Failure, ListTagModel>> getListTagOfGroup();

  Future<Either<Failure, ListGroupEntity>> getListGroupOfUser();

  Future<Either<Failure, void>> requestJoinGroup(
    RequestJoinGroupParams requestJoinGroupParams,
  );

  Future<Either<Failure, void>> leaveGroup(
    LeaveGroupParams leaveGroupParams,
  );

  Future<Either<Failure, void>> updateGroup(
    UpdateGroupParams updateGroupParams,
  );

  Future<Either<Failure, GroupEntity>> getGroupById(
      GetGroupByIdParams getGroupByIdParams);

  Future<Either<Failure, bool>> createNewGroupEvent(
    CreateNewGroupEventParams params,
  );

  Future<Either<Failure, ListGroupEventModel>> getListGroupEvent(
    GetListGroupEventParams params,
  );

  Future<Either<Failure, void>> confirmRequestJoinGroup(
    ConfirmRequestJoinGroupParams params,
  );

  Future<Either<Failure, void>> joinGroupEvent(
    JoinGroupEventParams params,
  );

  Future<Either<Failure, void>> updateGroupEvent(
    UpdateGroupEventParams params,
  );

  Future<Either<Failure, List<MessageModel>>> getGroupEventMessages(
    GetGroupEventMessagesParams params,
  );
}
