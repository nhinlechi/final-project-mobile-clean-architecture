import 'package:dartz/dartz.dart';
import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';

import '../../../../core/error/failures.dart';
import '../../../data/models/chat/message_model.dart';
import '../../usecase/chats/get_chat_messages_uc.dart';

abstract class ChatRepository {
  Future<Either<Failure, List<MessageModel>>> getChat(GetChatParam params);
  Future<Either<Failure, String>> uploadMessageImage(UploadMessageImageParams params);
  Future<Either<Failure, List<PlaceEntity>>> getRecommendPlace(GetRecommendPlaceParams params);
  Future<Either<Failure, List<DateModel>>> getAllDates(GetDatesParams params);
}