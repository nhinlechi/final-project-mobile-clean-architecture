import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/chat/match_room_model.dart';
import '../../usecase/matching/confirm_matching_uc.dart';
import '../../usecase/matching/confirm_place_uc.dart';
import '../../usecase/matching/request_match_uc.dart';
import '../../usecase/matching/send_place_uc.dart';

abstract class MatchingRepository {
  Future<Either<Failure, bool>> updateFirebaseToken(String firebaseToken);
  Future<Either<Failure, bool>> requestMatchNow(RequestMatchParams params);
  Future<Either<Failure, bool>> requestMatchLater(RequestMatchParams params);
  Future<Either<Failure, void>> confirmMatching(ConfirmMatchingParams params);
  Future<Either<Failure, void>> cancelRequestMatch();
  Future<Either<Failure, void>> sendPlace(SendPlaceParams params);
  Future<Either<Failure, List<MatchRoomModel>>> getMyMatch(NoParams params);
  Future<Either<Failure, void>> confirmPlaceToDating(ConfirmPlaceParams params);
}
