import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../data/models/favorite/favorite_model.dart';

abstract class FavoriteRepository {
  Future<Either<Failure, List<FavoriteModel>>> getListFavorite();
  Future<Either<Failure, List<FavoriteModel>>> getMyListFavorite();
}
