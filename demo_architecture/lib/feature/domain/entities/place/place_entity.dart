import 'package:equatable/equatable.dart';

import '../../../data/models/place/coordinates.dart';
import '../../../data/models/place/place_model.dart';

class PlaceEntity extends Equatable {
  final String address;
  final List<String> photo;
  final double rating;
  final String id;
  final CoordinatesPlace location;
  final String icon;
  final String name;
  final String type;
  final int total_rating;

  PlaceEntity(this.address, this.photo, this.rating, this.id, this.location,
      this.icon, this.name, this.type, this.total_rating);

  @override
  // TODO: implement props
  List<Object> get props =>
      [address, photo, rating, id, location, icon, name, type];

  factory PlaceEntity.fromModel(PlaceModel placeModel) {
    List<String> photoItem = [
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Whitehouse_north.jpg/250px-Whitehouse_north.jpg"
    ];
    List<String> photos =
        placeModel.photo.length != 0 ? placeModel.photo : photoItem;
    return PlaceEntity(
        placeModel.address ?? "Chưa cập nhật",
        photos,
        placeModel.rating ?? 0.0,
        placeModel.id,
        placeModel.location,
        placeModel.icon,
        placeModel.name ?? "Chưa cập nhật",
        placeModel.type,
        placeModel.user_ratings_total);
  }
}
