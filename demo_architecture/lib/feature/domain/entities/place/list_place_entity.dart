import 'package:equatable/equatable.dart';

import '../../../data/models/place/list_place_model.dart';
import 'place_entity.dart';

class ListPlaceEntity extends Equatable {
  final List<PlaceEntity> places;
  final int total;

  ListPlaceEntity(this.places, this.total);

  @override
  // TODO: implement props
  List<Object> get props => [places, total];

  factory ListPlaceEntity.fromModel(ListPlaceModel listPlaceModel) {
    List<PlaceEntity> listPlaces = [];
    listPlaceModel.listPlaceModel
        .forEach((element) => listPlaces.add(PlaceEntity.fromModel(element)));
    return ListPlaceEntity(listPlaces, listPlaceModel.total);
  }
}
