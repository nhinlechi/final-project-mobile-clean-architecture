import '../../../data/models/group/list_group_model.dart';
import 'package:equatable/equatable.dart';

import 'group_entity.dart';

class ListGroupEntity extends Equatable {
  final List<GroupEntity> groups;
  final int total;

  ListGroupEntity(this.groups, this.total);

  @override
  List<Object> get props => [groups, total];

  factory ListGroupEntity.fromModel(ListGroupModel listGroupModel) {
    List<GroupEntity> listGroup = [];
    listGroupModel.listGroup
        .forEach((element) => listGroup.add(GroupEntity.fromModel(element)));
    return ListGroupEntity(listGroup, listGroupModel.total);
  }

  List<GroupEntity> groupsOfMine(String userId) =>
      this.groups.where((group) => group.host == userId).toList();

  List<GroupEntity> get privateGroups =>
      this.groups.where((group) => group.isprivate).toList();
}
