import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';

import '../../../data/models/group/group_model.dart';
import '../../../data/models/group/list_tag_model.dart';
import 'package:equatable/equatable.dart';

class GroupEntity extends Equatable {
  final String description;
  final List<ParticipantModel> participant;
  final bool isprivate;
  final int star;
  final String id;
  final String host;
  final String name;
  final String createAt;
  final ListTagModel tag;
  final String avatar;

  GroupEntity(this.description, this.participant, this.isprivate, this.star,
      this.id, this.host, this.name, this.createAt, this.tag, this.avatar);

  @override
  List<Object> get props => [id];

  factory GroupEntity.fromModel(GroupModel model) {
    return GroupEntity(
        model.description ?? "",
        model.participant ?? [],
        model.isprivate ?? true,
        model.star ?? 0,
        model.id,
        model.host,
        model.name,
        model.createAt ?? "",
        model.tag,
        model.avatar);
  }
}
