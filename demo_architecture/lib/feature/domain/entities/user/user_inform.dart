import '../../../data/models/favorite/favorite_model.dart';
import 'package:equatable/equatable.dart';

class UserInform extends Equatable {
  final String userId;
  final String lastname;
  final String firstname;
  final String hoten;
  final String sdt;
  final String gioitinh;
  final String birthDay;
  final String phone;
  final String rela;
  final List<String> avt;
  final DateTime birth;
  final List<FavoriteModel> favorites;
  final String age;

  UserInform(
      this.userId,
      this.hoten,
      this.sdt,
      this.gioitinh,
      this.birthDay,
      this.phone,
      this.rela,
      this.avt,
      this.lastname,
      this.firstname,
      this.birth,
      this.favorites,
      this.age);

  @override
  List<Object> get props => [
        hoten,
        birthDay,
        sdt,
        userId,
        lastname,
        firstname,
        gioitinh,
        phone,
        rela,
        avt,
        birth,
        favorites,
        age
      ];
}
