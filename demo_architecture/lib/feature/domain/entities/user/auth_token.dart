

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class AuthenticateToken extends Equatable{

  final String firstName;
  final String token;
  final String userId;
  final bool isNewUser;


 const AuthenticateToken({
    @required this.firstName,
    @required this.token,
   @required this.userId,
   @required this.isNewUser
  });


  @override
  List<Object> get props => [firstName, token, userId, isNewUser];
}