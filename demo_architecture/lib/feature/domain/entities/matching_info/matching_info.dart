import 'dart:convert';

import '../../../../core/utils/convertToUserInform.dart';
import '../../../data/models/user/user_inform.dart';
import '../user/user_inform.dart';
import 'package:equatable/equatable.dart';

class MatchingInfo extends Equatable {
  final UserInform otherUserInfo;
  final String notificationId;
  final int timeOut;

  MatchingInfo({
    this.otherUserInfo,
    this.notificationId,
    this.timeOut,
  });

  @override
  List<Object> get props => [notificationId];

  factory MatchingInfo.fromJson(
      Map<String, dynamic> jsonData, String currentUserId) {
    final user1 = jsonData['user1'];
    final user2 = jsonData['user2'];

    final user1Info = ConvertUserInform.convert(
      UserInformModel.fromNotifyData(user1),
    );
    final user2Info = ConvertUserInform.convert(
      UserInformModel.fromNotifyData(user2),
    );

    final String matchId = jsonData['notificationid'];
    final int timeOut = int.tryParse(jsonData['timeout']);

    return MatchingInfo(
      otherUserInfo: currentUserId == user1Info.userId ? user2Info : user1Info,
      notificationId: matchId,
      timeOut: timeOut,
    );
  }
}
