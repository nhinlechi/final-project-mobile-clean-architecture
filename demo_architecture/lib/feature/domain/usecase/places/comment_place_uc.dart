import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/place/place_repository.dart';

class CommentPlaceUseCase implements UseCase<bool, CommentPlaceParams> {
  final PlaceRepository repository;

  CommentPlaceUseCase(@required this.repository);

  @override
  Future<Either<Failure, bool>> call(CommentPlaceParams params) async {
    return await repository.commentPlace(params);
  }
}

class CommentPlaceParams extends Equatable {
  final String id;
  final String comment;
  final int rating;

  CommentPlaceParams({@required this.id, this.comment, @required this.rating});

  @override
  // TODO: implement props
  List<Object> get props => [id, comment, rating];
}
