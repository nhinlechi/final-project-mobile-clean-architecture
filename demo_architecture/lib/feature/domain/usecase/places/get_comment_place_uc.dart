import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/place/list_comment_place_model.dart';
import '../../repositories/place/place_repository.dart';

class GetCommentPlaceUsecase implements UseCase<ListCommentPlaceModel, Params> {
  final PlaceRepository repository;

  GetCommentPlaceUsecase(this.repository);

  @override
  Future<Either<Failure, ListCommentPlaceModel>> call(params) async {
    return await repository.getCommentPlace(params.id);
  }
}

class Params extends Equatable {
  final String id;

  Params(this.id);

  @override
  // TODO: implement props
  List<Object> get props => [id];
}
