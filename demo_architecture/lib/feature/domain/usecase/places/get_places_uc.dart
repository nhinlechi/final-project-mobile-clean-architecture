import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/place/list_place_entity.dart';
import '../../repositories/place/place_repository.dart';

class GetPlaceUseCase implements UseCase<ListPlaceEntity, GetPlacesParams> {
  final PlaceRepository repository;

  GetPlaceUseCase(this.repository);

  @override
  Future<Either<Failure, ListPlaceEntity>> call(GetPlacesParams params) async {
    return await repository.getPlaces(params);
  }
}

// ignore: must_be_immutable
class GetPlacesParams extends Equatable {
  final int rowsPerPage;
  final int page;
  final String type;
  final int sort;
  final String sortBy;
  final String searchText;

  GetPlacesParams({
    @required this.rowsPerPage,
    @required this.page,
    this.type,
    this.sort,
    this.sortBy,
    this.searchText,
  });

  @override
  // TODO: implement props
  List<Object> get props => [rowsPerPage, page];
}
