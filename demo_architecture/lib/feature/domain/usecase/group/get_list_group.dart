import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/group/list_group_entity.dart';
import '../../repositories/group/group_repository.dart';

class GetListGroupUC implements UseCase<ListGroupEntity, GetListGroupParams>{
  final GroupRepository repository;

  GetListGroupUC(this.repository);
  @override
  Future<Either<Failure, ListGroupEntity>> call(params) async {
    return await repository.getListGroup(params);
  }

}

class GetListGroupParams extends Equatable{
  final int page;
  final int rowsperpage;
  final String tagId;
  final bool isPrivate;
  final int sort;
  final String textSearch;

  GetListGroupParams(
      {@required this.page,
      @required this.rowsperpage,
      this.tagId,
      this.isPrivate,
      this.sort, this.textSearch}); // 1 ASC, -1 DECS


  @override
  // TODO: implement props
  List<Object> get props => [page, rowsperpage, tagId, isPrivate, sort, textSearch];

}