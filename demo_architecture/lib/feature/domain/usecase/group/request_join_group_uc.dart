import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/group/group_repository.dart';

class RequestJoinGroupUseCase implements UseCase<void, RequestJoinGroupParams> {
  final GroupRepository groupRepository;

  RequestJoinGroupUseCase(this.groupRepository);

  @override
  Future<Either<Failure, void>> call(RequestJoinGroupParams params) async {
    return await groupRepository.requestJoinGroup(params);
  }
}

class RequestJoinGroupParams extends Equatable {
  final String idGroup;
  final String description;

  RequestJoinGroupParams({@required this.idGroup, @required this.description});

  @override
  List<Object> get props => [idGroup, description];
}
