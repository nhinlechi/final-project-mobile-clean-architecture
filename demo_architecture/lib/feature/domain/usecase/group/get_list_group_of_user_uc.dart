import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/group/list_group_entity.dart';
import '../../repositories/group/group_repository.dart';

class GetListGroupOfUserUseCase implements UseCase<ListGroupEntity, NoParams>{
  final GroupRepository groupRepository;

  GetListGroupOfUserUseCase({this.groupRepository});
  @override
  Future<Either<Failure, ListGroupEntity>> call(NoParams params) async {
    return await groupRepository.getListGroupOfUser();
  }

}