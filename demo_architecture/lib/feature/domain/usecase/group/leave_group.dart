import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/group/group_repository.dart';

class LeaveGroupUseCase implements UseCase<void, LeaveGroupParams>{
  final GroupRepository groupRepository;
  LeaveGroupUseCase(this.groupRepository);

  @override
  Future<Either<Failure, void>> call(LeaveGroupParams params) async {
      return await groupRepository.leaveGroup(params);
  }

}

class LeaveGroupParams extends Equatable{
  final String groupId;

  LeaveGroupParams(this.groupId);

  @override
  // TODO: implement props
  List<Object> get props => [groupId];
  
}