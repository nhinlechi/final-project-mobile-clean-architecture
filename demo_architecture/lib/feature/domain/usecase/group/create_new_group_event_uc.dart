import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:flutter/foundation.dart';

class CreateNewGroupEventUC extends UseCase<bool, CreateNewGroupEventParams> {
  final GroupRepository repos;

  CreateNewGroupEventUC({@required this.repos});

  @override
  Future<Either<Failure, bool>> call(CreateNewGroupEventParams params) async {
    return await repos.createNewGroupEvent(params);
  }
}

class CreateNewGroupEventParams {
  final String groupId;
  final int date;
  final String title;
  final String description;
  final String placeId;

  CreateNewGroupEventParams({
    @required this.groupId,
    @required this.date,
    @required this.title,
    @required this.description,
    @required this.placeId,
  });
}
