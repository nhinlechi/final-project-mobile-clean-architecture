import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/group/list_tag_model.dart';
import '../../repositories/group/group_repository.dart';

class GetListTagOfGroupUseCase implements UseCase<ListTagModel, NoParams>{
  final GroupRepository repository;

  GetListTagOfGroupUseCase({this.repository});
  @override
  Future<Either<Failure, ListTagModel>> call(NoParams params) async {
    return await repository.getListTagOfGroup();
  }



}
