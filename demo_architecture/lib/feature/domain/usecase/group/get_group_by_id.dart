import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/group/group_entity.dart';
import '../../repositories/group/group_repository.dart';

class GetGroupByIdUseCase implements UseCase<GroupEntity, GetGroupByIdParams>{
  final GroupRepository groupRepository;

  GetGroupByIdUseCase(this.groupRepository);

  @override
  Future<Either<Failure, GroupEntity>> call(GetGroupByIdParams params) async {
      return await groupRepository.getGroupById(params);
  }

}


class GetGroupByIdParams extends Equatable{
  final String groudId;

  GetGroupByIdParams(this.groudId);

  @override
  // TODO: implement props
  List<Object> get props => [groudId];
}