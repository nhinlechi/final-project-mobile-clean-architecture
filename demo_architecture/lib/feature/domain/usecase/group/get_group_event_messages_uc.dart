import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:flutter/foundation.dart';

class GetGroupEventMessagesUC
    extends UseCase<List<MessageModel>, GetGroupEventMessagesParams> {
  final GroupRepository repos;

  GetGroupEventMessagesUC({@required this.repos});

  @override
  Future<Either<Failure, List<MessageModel>>> call(
      GetGroupEventMessagesParams params) async {
    return await repos.getGroupEventMessages(params);
  }
}

class GetGroupEventMessagesParams {
  final String groupId;
  final String eventId;
  final int lastMessageIndex;

  GetGroupEventMessagesParams({
    @required this.groupId,
    @required this.eventId,
    this.lastMessageIndex,
  });
}
