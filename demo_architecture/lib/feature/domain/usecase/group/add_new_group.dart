import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/group/group_repository.dart';

class AddNewGroupUseCase implements UseCase<void, AddNewGroupParams>{
  final GroupRepository repository;

  AddNewGroupUseCase(this.repository);
  @override
  Future<Either<Failure, void>> call(AddNewGroupParams params) async {
    return await repository.addNewGroup(params);
  }

}

class AddNewGroupParams extends Equatable{
  final String name;
  final String description;
  final bool isPrivate;
  final List<String> tags;
  final File avatarFile;

  AddNewGroupParams(this.name, this.description, this.isPrivate, this.tags, this.avatarFile);

  @override
  // TODO: implement props
  List<Object> get props => [name, description, isPrivate, tags, avatarFile];
  
}