import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ConfirmRequestJoinGroupUC extends UseCase<void, ConfirmRequestJoinGroupParams> {
  final GroupRepository repos;

  ConfirmRequestJoinGroupUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(ConfirmRequestJoinGroupParams params) async {
    return await repos.confirmRequestJoinGroup(params);
  }
}

class ConfirmRequestJoinGroupParams extends Equatable {
  final String notificationId;
  final bool status;

  ConfirmRequestJoinGroupParams({
    @required this.notificationId,
    @required this.status,
  });

  @override
  List<Object> get props => [notificationId, status];
}
