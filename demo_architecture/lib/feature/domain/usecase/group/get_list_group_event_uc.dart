import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:flutter/cupertino.dart';

class GetListGroupEventUC extends UseCase<ListGroupEventModel, GetListGroupEventParams> {
  final GroupRepository repos;

  GetListGroupEventUC({@required this.repos});

  @override
  Future<Either<Failure, ListGroupEventModel>> call(
      GetListGroupEventParams params) async {
    return await repos.getListGroupEvent(params);
  }
}

class GetListGroupEventParams {
  final String groupId;
  final int page;
  final int rowsperpage;

  GetListGroupEventParams({
    @required this.groupId,
    @required this.page,
    @required this.rowsperpage,
  });
}
