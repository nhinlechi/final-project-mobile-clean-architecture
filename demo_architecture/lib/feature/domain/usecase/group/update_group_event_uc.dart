import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:flutter/foundation.dart';

class UpdateGroupEventUC extends UseCase<void, UpdateGroupEventParams>{
  final GroupRepository repos;

  UpdateGroupEventUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(UpdateGroupEventParams params) async {
    return await repos.updateGroupEvent(params);
  }

}

class UpdateGroupEventParams {
  final String groupId;
  final String eventId;
  final int date;
  final String title;
  final String description;
  final String placeId;

  UpdateGroupEventParams({
    @required this.groupId,
    @required this.eventId,
    @required this.date,
    @required this.title,
    @required this.description,
    @required this.placeId,
  });
}