import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/group/group_repository.dart';

class UpdateGroupUseCase implements UseCase<void, UpdateGroupParams> {
  final GroupRepository groupRepository;

  UpdateGroupUseCase(this.groupRepository);

  @override
  Future<Either<Failure, void>> call(UpdateGroupParams params) async {
    return await groupRepository.updateGroup(params);
  }
}

class UpdateGroupParams extends Equatable {
  final String groupId;
  final String name;
  final String description;
  final bool isPrivate;
  final List<String> tags;
  final File avatarFile;

  UpdateGroupParams(
      {this.groupId,
      this.name,
      this.description,
      this.isPrivate,
      this.tags,
      this.avatarFile});

  @override
  // TODO: implement props
  List<Object> get props =>
      [name, description, isPrivate, tags, avatarFile, groupId];
}
