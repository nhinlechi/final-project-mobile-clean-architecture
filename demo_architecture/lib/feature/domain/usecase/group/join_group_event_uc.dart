import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/group/group_repository.dart';
import 'package:flutter/foundation.dart';

class JoinGroupEventUC extends UseCase<void, JoinGroupEventParams> {
  final GroupRepository repos;

  JoinGroupEventUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(JoinGroupEventParams params) async {
    return await repos.joinGroupEvent(params);
  }

}

class JoinGroupEventParams {
  final String groupId;
  final String eventId;
  final bool status;

  JoinGroupEventParams({
    @required this.groupId,
    @required this.eventId,
    @required this.status,
  });
}
