import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/user/login_repo.dart';
import 'package:flutter/cupertino.dart';

class LogoutUC extends UseCase<void, NoParams> {
  final LoginRepository repos;

  LogoutUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    return await repos.logout();
  }
}