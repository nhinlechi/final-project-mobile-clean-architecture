import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/entities/user/auth_token.dart';
import 'package:demo_architecture/feature/domain/repositories/user/login_repo.dart';
import 'package:flutter/cupertino.dart';

class AutoLoginUC extends UseCase<AuthenticateToken, NoParams> {
  final LoginRepository repos;

  AutoLoginUC({@required this.repos});

  @override
  Future<Either<Failure, AuthenticateToken>> call(NoParams params) async {
    return await repos.autoLogin();
  }

}