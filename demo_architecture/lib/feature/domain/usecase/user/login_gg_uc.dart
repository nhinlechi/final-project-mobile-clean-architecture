import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/auth_token.dart';
import '../../repositories/user/login_repo.dart';

class LoginGoogleUseCase implements UseCase<AuthenticateToken, NoParams> {
  final LoginRepository _repository;

  LoginGoogleUseCase(@required this._repository);
  @override
  Future<Either<Failure, AuthenticateToken>> call(NoParams params) async {
    return await _repository.loginGG();
  }
}
