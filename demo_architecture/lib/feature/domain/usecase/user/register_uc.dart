import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/user/login_repo.dart';
import 'update_user_inform_uc.dart';

class RegisterUseCase implements UseCase<bool, ParamsRegister> {
  final LoginRepository loginRepository;

  RegisterUseCase(this.loginRepository);

  @override
  Future<Either<Failure, bool>> call(ParamsRegister params) async {
    return await loginRepository.register(params);
  }
}

class ParamsRegister extends Equatable {
  final String email;
  final String password;
  final UpdateUserInformParams updateUserInformParams;

  ParamsRegister({this.email, this.password, this.updateUserInformParams});

  @override
  // TODO: implement props
  List<Object> get props => [email, password, updateUserInformParams];
}
