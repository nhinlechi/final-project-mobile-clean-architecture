import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/user_inform.dart';
import '../../repositories/user/user_inform_repository.dart';

class GetUserInformByIdUC
    implements UseCase<UserInform, GetUserInformByIdParams> {
  final UserInformRepository repository;
  GetUserInformByIdUC(this.repository);
  @override
  Future<Either<Failure, UserInform>> call(params) async {
    return await repository.getUserInformById(params);
  }
}

class GetUserInformByIdParams extends Equatable {
  final String id;
  final List<String> select;

  GetUserInformByIdParams(this.id, this.select);

  @override
  // TODO: implement props
  List<Object> get props => [id, select];
}
