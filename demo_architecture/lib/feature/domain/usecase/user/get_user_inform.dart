import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/user_inform.dart';
import '../../repositories/user/user_inform_repository.dart';

class GetUserInform implements UseCase<UserInform, GetUserInformParams> {
  final UserInformRepository repository;
  GetUserInform(this.repository);

  @override
  Future<Either<Failure, UserInform>> call(GetUserInformParams params) async {
    return await repository.getUserInform(params);
  }
}

class GetUserInformParams extends Equatable {
  final List<String> select;

  GetUserInformParams(this.select);

  @override
  // TODO: implement props
  List<Object> get props => [select];
}
