import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/user_inform.dart';
import '../../repositories/user/update_basic_user_inform_repo.dart';

class UpdateBasicInform implements UseCase<UserInform, Params> {
  final UpdateBasicUserInformRepo repository;

  UpdateBasicInform(this.repository);

  @override
  Future<Either<Failure, UserInform>> call(Params params) async {
    return await repository.updateBasicUserInform(
        params.birthDay, params.lastname, params.first, params.gender);
  }
}

class Params extends Equatable {
  final String gender;
  final String birthDay;
  final String lastname;
  final String first;

  Params(this.gender, this.birthDay, this.lastname, this.first);

  @override
  // TODO: implement props
  List<Object> get props => [gender, birthDay, lastname, first];
}
