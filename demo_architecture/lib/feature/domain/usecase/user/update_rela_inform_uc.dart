import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/user_inform.dart';
import '../../repositories/user/update_rela_user_inform_repo.dart';

class UpdateRelaInform implements UseCase<UserInform, Params> {
  final UpdateRelaUserInformRepo repository;

  UpdateRelaInform(this.repository);

  @override
  Future<Either<Failure, UserInform>> call(Params params) async {
    return await repository.updateRelaUserInform(params.rela);
  }
}

class Params extends Equatable {
  final int rela;

  Params(this.rela);

  @override
  // TODO: implement props
  List<Object> get props => [rela];
}
