import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/user_inform.dart';
import '../../repositories/user/update_phone_user_inform_repo.dart';

class UpdatePhoneInform implements UseCase<UserInform, Params> {
  final UpdatePhoneUserInformRepo repository;

  UpdatePhoneInform(this.repository);

  @override
  Future<Either<Failure, UserInform>> call(Params params) async {
    return await repository.updatePhoneUserInform(params.phone);
  }
}

class Params extends Equatable {
  final String phone;

  Params(this.phone);

  @override
  // TODO: implement props
  List<Object> get props => [phone];
}
