import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../entities/user/auth_token.dart';
import '../../repositories/user/login_repo.dart';

class LoginUseCase implements UseCase<AuthenticateToken, Params> {
  final LoginRepository repositoryImpl;

  LoginUseCase(this.repositoryImpl);
  @override
  Future<Either<Failure, AuthenticateToken>> call(Params params) async {
    return await repositoryImpl.login(params.username, params.password);
  }
}

class Params extends Equatable {
  final String username;
  final String password;

  Params(this.username, this.password);
  @override
  List<Object> get props => [username, password];
}
