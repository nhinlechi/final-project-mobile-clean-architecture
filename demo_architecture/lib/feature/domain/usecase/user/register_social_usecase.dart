import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/user/user_inform_repository.dart';
import 'update_user_inform_uc.dart';

class RegisterSocialUseCase implements UseCase<void, UpdateUserInformParams> {
  final UserInformRepository repository;

  RegisterSocialUseCase(this.repository);
  @override
  Future<Either<Failure, void>> call(UpdateUserInformParams params) async {
    return await repository.updateUserInform(params);
  }
}

class ParamsRegisterSocial extends Equatable {
  final String firstName;
  final String lastName;
  final String birthDay;
  final String gender;
  final String relationship;
  final List<String> idFavoriteList;

  ParamsRegisterSocial(this.firstName, this.lastName, this.birthDay,
      this.gender, this.relationship, this.idFavoriteList);

  @override
  // TODO: implement props
  List<Object> get props =>
      [firstName, lastName, birthDay, gender, relationship, idFavoriteList];
}
