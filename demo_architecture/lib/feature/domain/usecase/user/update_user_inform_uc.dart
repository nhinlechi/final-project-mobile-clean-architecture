import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/user/user_inform_repository.dart';

class UpdateUserInformUseCase implements UseCase<void, UpdateUserInformParams> {
  final UserInformRepository repository;

  UpdateUserInformUseCase(this.repository);

  @override
  Future<Either<Failure, void>> call(UpdateUserInformParams params) async {
    return await repository.updateUserInform(params);
  }
}

class UpdateUserInformParams extends Equatable {
  final String username;
  final String email;
  final String id;
  final String phone;
  final String firstname;
  final String lastname;
  final int registerdate;
  final int dateofbirth;
  final int gender;
  final int relationship;
  final int lat;
  final int long;
  final List<File> avatar;
  final List<String> favorites;

  UpdateUserInformParams(
      {this.id,
      this.phone,
      this.firstname,
      this.lastname,
      this.registerdate,
      this.dateofbirth,
      this.gender,
      this.relationship,
      this.lat,
      this.long,
      this.username,
      this.email,
      this.avatar,
      this.favorites});

  @override
  List<Object> get props => [
        id,
        phone,
        firstname,
        lastname,
        registerdate,
        dateofbirth,
        gender,
        relationship,
        lat,
        long,
        username,
        email,
        avatar,
        favorites
      ];
}
