import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/user/update_basic_user_inform_repo.dart';

class UpdateUserAvatarUC extends UseCase<bool, UpdateUserAvatarParams> {
  final UpdateBasicUserInformRepo repos;

  UpdateUserAvatarUC({@required this.repos});

  @override
  Future<Either<Failure, bool>> call(UpdateUserAvatarParams params) async {
    return await repos.updateUserAvatar(params);
  }
}

class UpdateUserAvatarParams extends Equatable {
  final List<String> oldAvatarUrls;
  final List<File> imageFiles;

  UpdateUserAvatarParams(
      {@required this.oldAvatarUrls, @required this.imageFiles});

  @override
  List<Object> get props => [oldAvatarUrls, imageFiles];
}
