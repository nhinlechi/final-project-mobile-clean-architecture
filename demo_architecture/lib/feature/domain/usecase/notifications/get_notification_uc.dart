import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/notification/notification_model.dart';
import '../../repositories/notification/notification_repos.dart';

class GetNotificationUC
    extends UseCase<List<NotificationModel>, GetNotificationParams> {
  final NotificationRepos repos;

  GetNotificationUC({@required this.repos});

  @override
  Future<Either<Failure, List<NotificationModel>>> call(
      GetNotificationParams params) async {
    return await repos.getNotifications(params);
  }
}

class GetNotificationParams {
  final int page;
  final int rowPerPage;
  final int code;
  final String serviceId;

  GetNotificationParams({
    @required this.page,
    @required this.rowPerPage,
    this.code,
    this.serviceId,
  });
}
