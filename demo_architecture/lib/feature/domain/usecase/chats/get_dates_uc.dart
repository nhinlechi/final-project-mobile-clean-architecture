import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/repositories/chat/get_chat_repos.dart';
import 'package:flutter/foundation.dart';

class GetDatesUC extends UseCase<List<DateModel>, GetDatesParams> {
  final ChatRepository repos;

  GetDatesUC({@required this.repos});

  @override
  Future<Either<Failure, List<DateModel>>> call(GetDatesParams params) async {
    return await repos.getAllDates(params);
  }
}

class GetDatesParams {
  final String matchId;

  GetDatesParams({this.matchId});
}
