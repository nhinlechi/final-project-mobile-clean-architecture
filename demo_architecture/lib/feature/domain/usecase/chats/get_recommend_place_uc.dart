import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/repositories/chat/get_chat_repos.dart';
import 'package:flutter/cupertino.dart';

class GetRecommendPlaceUC
    extends UseCase<List<PlaceEntity>, GetRecommendPlaceParams> {
  final ChatRepository repos;

  GetRecommendPlaceUC({@required this.repos});

  @override
  Future<Either<Failure, List<PlaceEntity>>> call(
      GetRecommendPlaceParams params) async {
    return await repos.getRecommendPlace(params);
  }
}

class GetRecommendPlaceParams {
  final String matchId;

  GetRecommendPlaceParams({@required this.matchId});
}
