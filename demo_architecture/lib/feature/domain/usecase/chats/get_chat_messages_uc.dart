import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/chat/message_model.dart';
import '../../repositories/chat/get_chat_repos.dart';

class GetChatUC extends UseCase<List<MessageModel>, GetChatParam> {
  final ChatRepository repos;

  GetChatUC({@required this.repos});

  @override
  Future<Either<Failure, List<MessageModel>>> call(GetChatParam params) async {
    return await repos.getChat(params);
  }
}

class GetChatParam extends Equatable {
  final String matchId;
  final int lastMessageIndex;

  GetChatParam({
    @required this.matchId,
    @required this.lastMessageIndex,
  });

  @override
  List<Object> get props => [matchId, lastMessageIndex];
}
