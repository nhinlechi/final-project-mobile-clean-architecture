import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/chat/get_chat_repos.dart';
import 'package:flutter/foundation.dart';

class UploadMessageImageUC extends UseCase<String, UploadMessageImageParams> {
  final ChatRepository repos;

  UploadMessageImageUC({@required this.repos});

  @override
  Future<Either<Failure, String>> call(UploadMessageImageParams params) async {
    return await repos.uploadMessageImage(params);
  }
}

class UploadMessageImageParams {
  final File pickedImage;

  UploadMessageImageParams({@required this.pickedImage});
}
