import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/favorite/favorite_model.dart';
import '../../repositories/favorite/favorite_repository.dart';

class GetMyFavoriteListUseCase
    implements UseCase<List<FavoriteModel>, NoParams> {
  final FavoriteRepository favoriteRepository;

  GetMyFavoriteListUseCase(this.favoriteRepository);

  @override
  Future<Either<Failure, List<FavoriteModel>>> call(NoParams params) async {
    return await favoriteRepository.getMyListFavorite();
  }
}
