import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/favorite/favorite_model.dart';
import '../../repositories/favorite/favorite_repository.dart';

class GetFavoriteUsecase implements UseCase<List<FavoriteModel>, NoParams> {
  final FavoriteRepository repository;

  GetFavoriteUsecase(this.repository);
  @override
  Future<Either<Failure, List<FavoriteModel>>> call(params) async {
    return await repository.getListFavorite();
  }
}
