import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class SendPlaceUC extends UseCase<void, SendPlaceParams> {
  final MatchingRepository repos;

  SendPlaceUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(SendPlaceParams params) async {
    return await repos.sendPlace(params);
  }
}

class SendPlaceParams {
  final String matchId;
  final String placeId;
  final int datingTime;

  SendPlaceParams({
    @required this.matchId,
    @required this.placeId,
    @required this.datingTime,
  });
}
