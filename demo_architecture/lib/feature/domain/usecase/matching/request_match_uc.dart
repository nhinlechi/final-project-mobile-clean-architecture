import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class RequestMatchNowUC extends UseCase<bool, RequestMatchParams> {
  final MatchingRepository repos;

  RequestMatchNowUC({@required this.repos});

  @override
  Future<Either<Failure, bool>> call(RequestMatchParams params) async {
    return await repos.requestMatchNow(params);
  }
}

class RequestMatchParams {
  final double lat;
  final double long;
  final int gender;
  final int minAge;
  final int maxAge;
  final int distance;
  final int relationship;

  RequestMatchParams({
    @required this.lat,
    @required this.long,
    this.gender,
    this.minAge,
    this.maxAge,
    this.distance,
    this.relationship,
  });
}
