import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class CancelRequestMatchUC extends UseCase<void, NoParams> {
  final MatchingRepository repos;

  CancelRequestMatchUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(NoParams params) async {
    return await repos.cancelRequestMatch();
  }
}
