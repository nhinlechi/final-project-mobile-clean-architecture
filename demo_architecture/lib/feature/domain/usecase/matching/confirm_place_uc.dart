import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class ConfirmPlaceUC extends UseCase<void, ConfirmPlaceParams> {
  final MatchingRepository repos;

  ConfirmPlaceUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(ConfirmPlaceParams params) async {
    return await repos.confirmPlaceToDating(params);
  }
}

class ConfirmPlaceParams {
  final String notificationId;
  final bool isAccepted;

  ConfirmPlaceParams(
      {@required this.notificationId, @required this.isAccepted});
}
