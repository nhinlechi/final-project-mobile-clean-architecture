import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class ConfirmMatchingUC extends UseCase<void, ConfirmMatchingParams> {
  final MatchingRepository repos;

  ConfirmMatchingUC({@required this.repos});

  @override
  Future<Either<Failure, void>> call(ConfirmMatchingParams params) async {
    return await repos.confirmMatching(params);
  }
}

class ConfirmMatchingParams {
  final String notificationId;
  final bool isAccepted;

  ConfirmMatchingParams({@required this.notificationId, @required this.isAccepted});
}
