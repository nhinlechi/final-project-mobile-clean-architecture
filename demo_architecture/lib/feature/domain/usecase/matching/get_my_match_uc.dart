import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../../data/models/chat/match_room_model.dart';
import '../../repositories/matching/matching_repos.dart';

class GetMyMatchUC extends UseCase<List<MatchRoomModel>, NoParams> {
  final MatchingRepository repository;

  GetMyMatchUC({@required this.repository});

  @override
  Future<Either<Failure, List<MatchRoomModel>>> call(NoParams params) async {
    return await repository.getMyMatch(params);
  }
}
