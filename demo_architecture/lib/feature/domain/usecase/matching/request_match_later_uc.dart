import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/failures.dart';
import 'package:demo_architecture/core/usecases/usecase.dart';
import 'package:demo_architecture/feature/domain/repositories/matching/matching_repos.dart';
import 'package:demo_architecture/feature/domain/usecase/matching/request_match_uc.dart';
import 'package:flutter/cupertino.dart';

class RequestMatchLaterUC extends UseCase<bool, RequestMatchParams> {
  final MatchingRepository repository;

  RequestMatchLaterUC({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(RequestMatchParams params) async {
    return await repository.requestMatchLater(params);
  }

}