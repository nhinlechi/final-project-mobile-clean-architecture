import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../repositories/matching/matching_repos.dart';

class UpdateFirebaseTokenUC extends UseCase<bool, String> {
  final MatchingRepository repos;

  UpdateFirebaseTokenUC({@required this.repos});

  @override
  Future<Either<Failure, bool>> call(String firebaseToken) async {
    return await repos.updateFirebaseToken(firebaseToken);
  }
}
