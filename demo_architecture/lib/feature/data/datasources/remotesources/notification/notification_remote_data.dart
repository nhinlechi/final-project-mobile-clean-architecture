import 'dart:developer';

import 'package:demo_architecture/core/error/exceptions.dart';

import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/notification/notification_model.dart';
import '../../../../domain/usecase/notifications/get_notification_uc.dart';
import 'package:flutter/cupertino.dart';

abstract class NotificationRemoteData {
  Future<List<NotificationModel>> getNotifications(
      GetNotificationParams params);
}

class NotificationRemoteDataImpl extends NotificationRemoteData {
  final HttpClient client;

  NotificationRemoteDataImpl({@required this.client});

  @override
  Future<List<NotificationModel>> getNotifications(
      GetNotificationParams params) async {
    try {
      // setup
      final apiPath = APIPathHelper.getValue(APIPath.get_notifications);
      final Map<String, dynamic> queryParams = {
        'page': params.page.toString(),
        'rowsperpage': params.rowPerPage.toString(),
      };
      if (params.code != null) queryParams.addAll({'code': params.code});
      if (params.serviceId != null)
        queryParams.addAll({'serviceid': params.serviceId});
      // call api
      final jsonData = await this.client.fetchData(
            apiPath: apiPath,
            queryParameters: queryParams,
          );
      // return result
      return NotificationModel.listFromJson(jsonData);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }
}
