import 'dart:developer';

import 'package:flutter/foundation.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_inform.dart';

abstract class UpdateRelaUserRemoteData {
  Future<UserInformModel> updateUserInform(int rela);
}

class UpdateRelaInformRemoteDataSourceImpl implements UpdateRelaUserRemoteData {
  final HttpClient client;

  UpdateRelaInformRemoteDataSourceImpl({@required this.client});

  @override
  Future<UserInformModel> updateUserInform(int rela) => _updateUserInform(
        APIPathHelper.getValue(APIPath.update_user),
        rela,
      );

  Future<UserInformModel> _updateUserInform(String apiPath, int rela) async {
    log("apiPath: " + apiPath);
    try {
      final response = await client.updateData(
        apiPath: apiPath,
        body: {
          'relationship': rela.toString(),
        },
      );
      return UserInformModel.fromJson(response);
    } on APIException catch (e) {
      throw e;
    }
  }
}
