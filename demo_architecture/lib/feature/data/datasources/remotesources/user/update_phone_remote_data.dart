import 'dart:developer';

import 'package:demo_architecture/core/error/exceptions.dart';
import 'package:flutter/foundation.dart';

import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_inform.dart';

abstract class UpdatePhoneUserRemoteData {
  Future<UserInformModel> updateUserInform(String phone);
}

class UpdatePhoneInformRemoteDataSourceImpl
    implements UpdatePhoneUserRemoteData {
  final HttpClient client;

  UpdatePhoneInformRemoteDataSourceImpl({@required this.client});

  @override
  Future<UserInformModel> updateUserInform(String phone) => _getUserInformUrl(
        APIPathHelper.getValue(APIPath.update_user),
        phone,
      );

  Future<UserInformModel> _getUserInformUrl(
      String apiPath, String phone) async {
    log("apiPath: " + apiPath);

    try {
      final response = await client.updateData(
        apiPath: apiPath,
        body: {
          'phone': phone,
        },
      );
      return UserInformModel.fromJson(response);
    } on APIException catch (e) {
      throw e;
    }
  }
}
