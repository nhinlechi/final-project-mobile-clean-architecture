import 'dart:developer';
import 'dart:io';

import '../../../../domain/usecase/user/register_social_usecase.dart';
import '../../../../domain/usecase/user/update_user_avatar_uc.dart';
import 'package:flutter/foundation.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_inform.dart';

abstract class UpdateBasicUserRemoteData {
  Future<UserInformModel> updateUserInform(
      String birthDay, String lastname, String firstName, String gender);

  Future<UserInformModel> updateRegisterInform(
      String birthDay, String gender, String relationship);

  Future<void> addFavorite(List<String> idFavoriteList);

  Future<UserInformModel> updateUserInformForSocialRegister(
      ParamsRegisterSocial paramsRegisterSocial);

  Future<bool> updateUserAvatar(UpdateUserAvatarParams params);
}

class UpdateBasicInformRemoteDataSourceImpl
    implements UpdateBasicUserRemoteData {
  final HttpClient client;

  UpdateBasicInformRemoteDataSourceImpl({@required this.client});

  @override
  Future<UserInformModel> updateUserInform(
    String birthDay,
    String lastname,
    String firstName,
    String gender,
  ) =>
      _updateUserInform(
        APIPathHelper.getValue(APIPath.update_user),
        birthDay,
        lastname,
        firstName,
        gender,
      );

  Future<UserInformModel> _updateUserInform(
    String apiPath,
    String birthDay,
    String lastname,
    String firstName,
    String gender,
  ) async {
    log("apiPath: " + apiPath);
    try {
      final response = await client.updateData(
        apiPath: apiPath,
        body: {
          'firstname': firstName,
          'lastname': lastname,
          'dateofbirth': birthDay,
          'gender': gender,
        },
      );
      return UserInformModel.fromJson(response);
    } catch (e) {
      throw e;
    }
  }

  @override
  Future<UserInformModel> updateRegisterInform(
          String birthDay, String gender, String relationship) =>
      _updateRegisterInformUrl(
        APIPathHelper.getValue(APIPath.update_user),
        birthDay,
        gender,
        relationship,
      );

  Future<UserInformModel> _updateRegisterInformUrl(String apiPath,
      String birthDay, String gender, String relationship) async {
    log("apiPath: " + apiPath);
    try {
      final response = await client.updateData(
        apiPath: apiPath,
        body: {
          'dateofbirth': birthDay,
          'gender': gender,
          'relationship': relationship
        },
      );
      return UserInformModel.fromJson(response);
    } catch (e) {
      // TODO: refactor return message (status code + message)
      throw e;
    }
  }

  @override
  Future<void> addFavorite(List<String> idFavoriteList) => _addFavorite(
      APIPathHelper.getValue(APIPath.add_information_extension),
      idFavoriteList);

  Future<void> _addFavorite(String apiPath, List<String> idFavoriteList) async {
    try {
      await client.updateData(apiPath: apiPath, body: {
        "favorites": idFavoriteList,
      });
      return;
    } catch (e) {
      log('add favorite' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<UserInformModel> updateUserInformForSocialRegister(
          ParamsRegisterSocial paramsRegisterSocial) =>
      _updateUserInformForSocialRegister(
          APIPathHelper.getValue(APIPath.update_user), paramsRegisterSocial);

  Future<UserInformModel> _updateUserInformForSocialRegister(
      String apiPath, ParamsRegisterSocial paramsRegisterSocial) async {
    log("apiPath: " + apiPath);
    try {
      final response = await client.updateData(
        apiPath: apiPath,
        body: {
          'firstname': paramsRegisterSocial.firstName,
          'lastname': paramsRegisterSocial.lastName,
          'dateofbirth': paramsRegisterSocial.birthDay,
          'gender': paramsRegisterSocial.gender,
          'relationship': paramsRegisterSocial.relationship
        },
      );
      return UserInformModel.fromJson(response);
    } catch (e) {
      log('register social' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<bool> updateUserAvatar(UpdateUserAvatarParams params) async {
    try {
      final List<String> accessUrlList = [];

      // Old avatars
      params.oldAvatarUrls.forEach((avatarUrl) {
        accessUrlList.add(avatarUrl);
      });

      // New Avatars
      for (int i = 0; i < params.imageFiles.length; i++) {
        accessUrlList.add(await client.uploadImage(params.imageFiles[i]));
      }

      final apiPath = APIPathHelper.getValue(APIPath.update_user);
      await client.updateData(
        apiPath: apiPath,
        body: {
          'avatar': accessUrlList,
        },
      );
      return true;
    } on APIException catch (e) {
      log("update user information" + e.toString());
      throw e;
    }
  }
}
