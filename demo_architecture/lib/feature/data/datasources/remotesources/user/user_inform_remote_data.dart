import 'dart:developer';

import '../../../../domain/usecase/user/get_user_inform.dart';
import '../../../../domain/usecase/user/get_user_inform_by_id.dart';
import '../../../../domain/usecase/user/update_user_inform_uc.dart';
import 'package:flutter/foundation.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_inform.dart';

abstract class UserInformRemoteData {
  Future<UserInformModel> getUserInform(GetUserInformParams params);
  Future<UserInformModel> getUserInformById(
      GetUserInformByIdParams getUserInformByIdParams);
  Future<void> updateUserInform(UpdateUserInformParams updateUserInformParams);
}

class UserInformRemoteDataSourceImpl implements UserInformRemoteData {
  final HttpClient client;

  UserInformRemoteDataSourceImpl({@required this.client});

  @override
  Future<UserInformModel> getUserInform(GetUserInformParams params) =>
      _getUserInformUrl(APIPathHelper.getValue(APIPath.fetch_user), params);

  Future<UserInformModel> _getUserInformUrl(
      String apiPath, GetUserInformParams getUserInformParams) async {
    log("url: " + apiPath);
    final Map<String, String> params = {};
    if (getUserInformParams.select != null) {
      final select = getUserInformParams.select;
      for (int i = 0; i < select.length; i++) {
        params.addAll({'select[$i]': select[i]});
      }
    }
    try {
      final response =
          await client.fetchData(apiPath: apiPath, queryParameters: params);
      return UserInformModel.fromJson(response);
    } on APIException catch (e) {
      throw e;
    }
  }

  @override
  Future<UserInformModel> getUserInformById(
          GetUserInformByIdParams getUserInformByIdParams) =>
      _getUserInformById(APIPathHelper.getValue(APIPath.get_user_by_id),
          getUserInformByIdParams);

  Future<UserInformModel> _getUserInformById(
      String value, GetUserInformByIdParams getUserInformByIdParams) async {
    final Map<String, String> params = {};
    if (getUserInformByIdParams.select != null) {
      final select = getUserInformByIdParams.select;
      for (int i = 0; i < select.length; i++) {
        params.addAll({'select[$i]': select[i]});
      }
    }
    try {
      final response = await client.fetchData(
          apiPath: value + '/${getUserInformByIdParams.id}',
          queryParameters: params);
      return UserInformModel.fromJson(response);
    } on APIException catch (e) {
      log(e.toString(), name: 'user inform by id', error: e);
      throw e;
    }
  }

  @override
  Future<void> updateUserInform(
          UpdateUserInformParams updateUserInformParams) =>
      _updateUserInform(
          APIPathHelper.getValue(APIPath.update_user), updateUserInformParams);

  Future<void> _updateUserInform(
      String value, UpdateUserInformParams updateUserInformParams) async {
    final Map<String, dynamic> params = {};
    if (updateUserInformParams.dateofbirth != null) {
      params.addAll({
        "dateofbirth": updateUserInformParams.dateofbirth.toString(),
      });
    }

    if (updateUserInformParams.firstname != null) {
      params.addAll({
        "firstname": updateUserInformParams.firstname,
      });
    }

    if (updateUserInformParams.lastname != null) {
      params.addAll({
        "lastname": updateUserInformParams.lastname,
      });
    }

    if (updateUserInformParams.relationship != null) {
      params.addAll({
        "relationship": updateUserInformParams.relationship.toString(),
      });
    }

    if (updateUserInformParams.gender != null) {
      params.addAll({
        "gender": updateUserInformParams.gender.toString(),
      });
    }

    if (updateUserInformParams.phone != null) {
      params.addAll({
        "phone": updateUserInformParams.phone,
      });
    }

    if (updateUserInformParams.avatar != null) {
      final List<String> accessUrl = [];
      for (int i = 0; i < updateUserInformParams.avatar.length; i++) {
        accessUrl
            .add(await client.uploadImage(updateUserInformParams.avatar[i]));
      }
      params.addAll({
        "avatar": accessUrl,
      });
    }

    if (updateUserInformParams.favorites != null) {
      params.addAll({
        "favorites": updateUserInformParams.favorites,
      });
    }

    try {
      await client.updateData(apiPath: value, body: params);
      return;
    } on APIException catch (e) {
      log("register " + e.toString());
      throw e;
    }
  }
}
