import 'dart:developer';

import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/group/group_model.dart';
import '../../../models/group/list_group_model.dart';
import '../../../models/group/list_tag_model.dart';
import '../../../../domain/usecase/group/add_new_group.dart';
import '../../../../domain/usecase/group/get_group_by_id.dart';
import '../../../../domain/usecase/group/get_list_group.dart';
import '../../../../domain/usecase/group/leave_group.dart';
import '../../../../domain/usecase/group/request_join_group_uc.dart';
import '../../../../domain/usecase/group/update_group.dart';

abstract class GroupRemoteData {
  Future<ListGroupModel> getListGroupModel(
      GetListGroupParams getListGroupParams);

  Future<void> addNewGroup(AddNewGroupParams addNewGroupParams);

  Future<ListTagModel> getListTagOfGroup();

  Future<ListGroupModel> getListGroupOfUser();

  Future<void> requestJoinGroup(RequestJoinGroupParams requestJoinGroupParams);

  Future<void> leaveGroup(LeaveGroupParams leaveGroupParams);

  Future<void> updateGroup(UpdateGroupParams updateGroupParams);

  Future<GroupModel> getGroupById(GetGroupByIdParams getGroupByIdParams);

  Future<bool> createNewGroupEvent(CreateNewGroupEventParams params);

  Future<ListGroupEventModel> getListGroupEvent(GetListGroupEventParams params);

  Future<void> confirmRequestJoinGroup(ConfirmRequestJoinGroupParams params);

  Future<void> joinGroupEvent(JoinGroupEventParams params);

  Future<void> updateGroupEvent(UpdateGroupEventParams params);

  Future<List<MessageModel>> getGroupEventMessages(
      GetGroupEventMessagesParams params);
}

class GroupRemoteDataImpl implements GroupRemoteData {
  final HttpClient client;

  GroupRemoteDataImpl(this.client);

  @override
  Future<ListGroupModel> getListGroupModel(
          GetListGroupParams getListGroupParams) =>
      _getListGroupModel(
          APIPathHelper.getValue(APIPath.get_list_group), getListGroupParams);

  Future<ListGroupModel> _getListGroupModel(
      String apiPath, GetListGroupParams getListGroupParams) async {
    Map<String, String> params = {
      "page": getListGroupParams.page.toString(),
      "rowsperpage": getListGroupParams.rowsperpage.toString()
    };
    if (getListGroupParams.isPrivate != null)
      params.addAll({
        "isprivate": getListGroupParams.isPrivate.toString(),
      });

    if (getListGroupParams.sort != null) {
      params.addAll({
        "sort": getListGroupParams.sort.toString(),
      });
    }

    if (getListGroupParams.tagId != null) {
      params.addAll({
        "tag": getListGroupParams.tagId,
      });
    }

    if (getListGroupParams.textSearch != null) {
      params.addAll({
        "textsearch": getListGroupParams.textSearch,
      });
    }
    try {
      final response = await client.fetchData(
        apiPath: apiPath,
        queryParameters: params,
      );
      return ListGroupModel.fromJson(response);
    } catch (e) {
      log(e, name: 'get list group model', error: e);
      throw e;
    }
  }

  @override
  Future<void> addNewGroup(AddNewGroupParams addNewGroupParams) => _addNewGroup(
      APIPathHelper.getValue(APIPath.get_list_group), addNewGroupParams);

  Future<void> _addNewGroup(
      String value, AddNewGroupParams addNewGroupParams) async {
    try {
      final avatarGroupUrl =
          await client.uploadImage(addNewGroupParams.avatarFile);
      final response = await client.postData(apiPath: value, body: {
        "name": addNewGroupParams.name,
        "description": addNewGroupParams.description,
        "isprivate": addNewGroupParams.isPrivate.toString(),
        "avatar": avatarGroupUrl,
        "tag": addNewGroupParams.tags
      });
      return null;
    } catch (e) {
      log("add new group " + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<ListTagModel> getListTagOfGroup() =>
      _getListTagOfGroup(APIPathHelper.getValue(APIPath.get_tag));

  Future<ListTagModel> _getListTagOfGroup(String apiPath) async {
    try {
      final response = await client.fetchData(apiPath: apiPath);

      return ListTagModel.fromJson(response, 'data');
    } catch (e) {
      log("get list tag of group " + e.toString(), name: "_getListTagOfGroup");
      throw e;
    }
  }

  @override
  Future<ListGroupModel> getListGroupOfUser() =>
      _getListGroupOfUser(APIPathHelper.getValue(APIPath.get_group_of_user));

  Future<ListGroupModel> _getListGroupOfUser(String apiPath) async {
    try {
      final response = await client.fetchData(
        apiPath: apiPath,
      );
      return ListGroupModel.fromJson(response);
    } on APIException catch (e) {
      throw e;
    }
  }

  @override
  Future<void> requestJoinGroup(
          RequestJoinGroupParams requestJoinGroupParams) =>
      _requestJoinGroup(APIPathHelper.getValue(APIPath.request_join_group),
          requestJoinGroupParams);

  Future<void> _requestJoinGroup(
      String apiPath, RequestJoinGroupParams requestJoinGroupParams) async {
    try {
      await client.postData(apiPath: apiPath, body: {
        "groupid": requestJoinGroupParams.idGroup,
        "description": requestJoinGroupParams.description,
      });
      return;
    } on APIException catch (e) {
      log("request join group: " + e.toString());
      throw e;
    }
  }

  @override
  Future<void> leaveGroup(LeaveGroupParams leaveGroupParams) => _leaveGroup(
      APIPathHelper.getValue(APIPath.leave_group), leaveGroupParams);

  Future<void> _leaveGroup(
      String value, LeaveGroupParams leaveGroupParams) async {
    try {
      await client
          .delete(apiPath: value, body: {"groupid": leaveGroupParams.groupId});
      return;
    } on APIException catch (e) {
      log("request leave group " + e.toString());
      throw e;
    }
  }

  @override
  Future<void> updateGroup(UpdateGroupParams updateGroupParams) => _updateGroup(
      APIPathHelper.getValue(APIPath.get_list_group), updateGroupParams);

  Future<void> _updateGroup(
      String value, UpdateGroupParams updateGroupParams) async {
    Map<String, dynamic> body = {
      "groupid": updateGroupParams.groupId,
    };
    if (updateGroupParams.name != null) {
      body.addAll({
        "name": updateGroupParams.name,
      });
    }

    if (updateGroupParams.description != null) {
      body.addAll({
        "description": updateGroupParams.description,
      });
    }

    if (updateGroupParams.tags != null) {
      body.addAll({
        "tag": updateGroupParams.tags,
      });
    }

    if (updateGroupParams.avatarFile != null) {
      final avtUrl = await client.uploadImage(updateGroupParams.avatarFile);
      body.addAll({
        "avatar": avtUrl,
      });
    }

    try {
      await client.updateData(apiPath: value, body: body);
    } on APIException catch (e) {
      log("update group " + e.toString());
      throw e;
    }
  }

  @override
  Future<GroupModel> getGroupById(GetGroupByIdParams getGroupByIdParams) =>
      _getGroupById(
          APIPathHelper.getValue(APIPath.get_group_by_id), getGroupByIdParams);

  Future<GroupModel> _getGroupById(
      String apiPath, GetGroupByIdParams getGroupByIdParams) async {
    try {
      final response = await client.fetchData(
        apiPath: apiPath + '/${getGroupByIdParams.groudId}',
      );
      return GroupModel.fromJson(response['data']);
    } on APIException catch (e) {
      log("get group by id" + e.toString());
      throw e;
    }
  }

  @override
  Future<bool> createNewGroupEvent(CreateNewGroupEventParams params) async {
    try {
      final apiPath = GroupAPIPathHelper.getValue(
        APIPath.create_new_group_event,
        params.groupId,
      );
      await client.postData(
        apiPath: apiPath,
        body: {
          'date': params.date,
          'name': params.title,
          'description': params.description,
          'placeid': params.placeId,
        },
      );
      return true;
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<ListGroupEventModel> getListGroupEvent(
      GetListGroupEventParams params) async {
    try {
      final apiPath = GroupAPIPathHelper.getValue(
        APIPath.get_list_group_event,
        params.groupId,
      );
      final Map<String, String> queryParameters = {
        'page': params.page.toString(),
        'rowsperpage': params.rowsperpage.toString(),
      };
      final responseJson = await client.fetchData(
        apiPath: apiPath,
        queryParameters: queryParameters,
      );
      return ListGroupEventModel.fromJson(responseJson);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> confirmRequestJoinGroup(
      ConfirmRequestJoinGroupParams params) async {
    try {
      final apiPath =
          APIPathHelper.getValue(APIPath.confirm_request_join_group);
      await client.postData(
        apiPath: apiPath,
        body: {
          'notificationid': params.notificationId,
          'status': params.status,
        },
      );
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> joinGroupEvent(JoinGroupEventParams params) async {
    try {
      final apiPath =
          GroupAPIPathHelper.getValue(APIPath.join_group_event, params.groupId);
      await client.postData(
        apiPath: apiPath,
        body: {
          'eventid': params.eventId,
          'status': params.status,
        },
      );
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> updateGroupEvent(UpdateGroupEventParams params) async {
    try {
      final apiPath = GroupAPIPathHelper.getValue(
          APIPath.update_group_event, params.groupId);
      await client.updateData(
        apiPath: apiPath,
        body: {
          'eventid': params.eventId,
          'date': params.date,
          'name': params.title,
          'description': params.description,
          'placeid': params.placeId,
        },
      );
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<List<MessageModel>> getGroupEventMessages(
      GetGroupEventMessagesParams params) async {
    try {
      final apiPath = GroupAPIPathHelper.getValue(
        APIPath.get_group_event_messages,
        params.groupId,
      );
      final queryParameters = {
        'eventid': params.eventId,
      };
      if (params.lastMessageIndex != null) {
        queryParameters.addAll({
          'lastMessage': params.lastMessageIndex.toString(),
        });
      }
      final jsonData = await client.fetchData(
        apiPath: apiPath,
        queryParameters: queryParameters,
      );
      return MessageModel.listFromApiJson(jsonData);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }
}
