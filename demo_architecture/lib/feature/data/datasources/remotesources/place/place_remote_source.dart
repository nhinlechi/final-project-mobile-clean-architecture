import 'dart:developer';

import '../../../../domain/usecase/places/get_places_uc.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../../domain/usecase/places/comment_place_uc.dart';
import '../../../models/place/list_comment_place_model.dart';
import '../../../models/place/list_place_model.dart';

abstract class PlaceRemoteDataSource {
  Future<ListPlaceModel> getPlaces(GetPlacesParams params);

  Future<ListCommentPlaceModel> getCommentPlace(String id);

  Future<bool> commentPlace(CommentPlaceParams params);
}

class PlaceRemoteDataSourceImpl implements PlaceRemoteDataSource {
  final HttpClient client;

  PlaceRemoteDataSourceImpl(this.client);

  @override
  Future<ListPlaceModel> getPlaces(GetPlacesParams params) =>
      _getPlaces(APIPathHelper.getValue(APIPath.get_places), params);

  @override
  Future<ListCommentPlaceModel> getCommentPlace(String id) => _getCommentPlace(
      APIPathHelper.getValue(APIPath.get_comment_place) + "/" + id);

  @override
  Future<bool> commentPlace(CommentPlaceParams params) =>
      _commentPlace(APIPathHelper.getValue(APIPath.comment_place), params);

  Future<ListPlaceModel> _getPlaces(String url, GetPlacesParams params) async {
    try {
      final apiParams = {
        "page": params.page.toString(),
        "rowsperpage": params.rowsPerPage.toString(),
      };
      if (params.type != null) apiParams.addAll({'type': params.type});
      if (params.sort != null && params.sortBy != null)
        apiParams.addAll(
          {
            'sort': params.sort.toString(),
            'sortby': params.sortBy.toString(),
          },
        );
      if (params.searchText != null && params.searchText.isNotEmpty)
        apiParams.addAll({'textsearch': params.searchText});
      final response =
          await client.fetchData(apiPath: url, queryParameters: apiParams);
      return ListPlaceModel.fromJson(response);
    } on APIException catch (e) {
      log(e.toString(), name: 'get places', error: e);
      throw e;
    }
  }

  Future<ListCommentPlaceModel> _getCommentPlace(String path) async {
    try {
      final response = await client.fetchData(
        apiPath: path,
      );
      return ListCommentPlaceModel.fromJson(response);
    } on APIException catch (e) {
      log(e.toString(), name: 'get comment place', error: e);
      throw e;
    }
  }

  Future<bool> _commentPlace(String value, CommentPlaceParams params) async {
    try {
      final body = {
        "id": params.id,
        "rating": params.rating.toString(),
      };
      if (params.comment != null) body.addAll({'comment': params.comment});
      await client.updateData(apiPath: value, body: body);
      return true;
    } on APIException catch (e) {
      log(e.toString(), name: 'comment place', error: e);
      throw e;
    }
  }
}
