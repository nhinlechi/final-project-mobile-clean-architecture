import 'dart:developer';

import 'package:demo_architecture/core/error/exceptions.dart';

import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/favorite/favorite_model.dart';

abstract class FavoriteRemoteData {
  Future<List<FavoriteModel>> getListFavorite();
  Future<List<FavoriteModel>> getMyListFavorite();
}

class FavoriteRemoteDataImpl implements FavoriteRemoteData {
  final HttpClient client;

  FavoriteRemoteDataImpl(this.client);

  @override
  Future<List<FavoriteModel>> getListFavorite() =>
      _getListFavorite(APIPathHelper.getValue(APIPath.get_list_favorite));

  Future<List<FavoriteModel>> _getListFavorite(String apiPath) async {
    try {
      final response = await client.fetchDataWithNoToken(
        apiPath: apiPath,
      );
      final listJson = response['data'];
      final List<FavoriteModel> listFavorite = [];
      listJson.forEach(
          (element) => listFavorite.add(FavoriteModel.fromJson(element)));
      return listFavorite;
    } catch (e) {
      log("get list favorite" + e.toString(), error: e);
      throw APIException();
    }
  }

  @override
  Future<List<FavoriteModel>> getMyListFavorite() =>
      _getMyListFavorite(APIPathHelper.getValue(APIPath.fetch_user));

  Future<List<FavoriteModel>> _getMyListFavorite(String apiPath) async {
    try {
      final response = await client.fetchData(
          apiPath: apiPath, queryParameters: {"select[0]": "favorites"});
      final listJson = response['data']['favorites'];
      final List<FavoriteModel> listFavorite = [];
      listJson.forEach(
          (element) => listFavorite.add(FavoriteModel.fromJson(element)));
      return listFavorite;
    } catch (e) {
      log("get my favorite list " + e.toString(), error: e);
      throw APIException();
    }
  }
}
