import 'dart:developer';

import 'package:demo_architecture/core/error/exceptions.dart';
import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/data/models/place/list_place_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/list_place_entity.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../../domain/usecase/chats/get_chat_messages_uc.dart';
import '../../../models/chat/message_model.dart';

abstract class ChatRemoteData {
  Future<List<MessageModel>> getChat(GetChatParam params);

  Future<String> uploadMessageImage(UploadMessageImageParams params);

  Future<List<PlaceEntity>> getRecommendPlace(GetRecommendPlaceParams params);

  Future<List<DateModel>> getDates(GetDatesParams params);
}

class ChatRemoteDataImpl extends ChatRemoteData {
  final HttpClient client;

  ChatRemoteDataImpl({@required this.client});

  @override
  Future<List<MessageModel>> getChat(GetChatParam params) async {
    try {
      final Map<String, dynamic> queryParams = {
        'matchId': params.matchId,
      };
      if (params.lastMessageIndex != null) {
        queryParams.addAll(
          {
            'lastMessage': params.lastMessageIndex.toString(),
          },
        );
      }

      final jsonData = await client.fetchData(
        apiPath: APIPathHelper.getValue(APIPath.get_chat),
        queryParameters: queryParams,
      );
      return MessageModel.listFromApiJson(jsonData);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<String> uploadMessageImage(UploadMessageImageParams params) async {
    try {
      // New Image URL
      final imageUrl = await client.uploadImage(params.pickedImage);
      return imageUrl;
    } on APIException catch (e) {
      log(e.toString(), name: 'update message image');
      throw e;
    }
  }

  @override
  Future<List<PlaceEntity>> getRecommendPlace(
      GetRecommendPlaceParams params) async {
    try {
      final jsonData = await client.fetchData(
        apiPath: APIPathHelper.getValueWithParam(
          APIPath.get_recommend_place,
          params.matchId,
        ),
      );
      return ListPlaceEntity.fromModel(
        ListPlaceModel.fromRecommendPlaceJson(jsonData),
      ).places;
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<List<DateModel>> getDates(GetDatesParams params) async {
    try {
      final jsonMap = await client.fetchData(
        apiPath: APIPathHelper.getValue(APIPath.match_meeting_history),
      );
      return DateModel.listDatesFromJson(jsonMap);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }
}
