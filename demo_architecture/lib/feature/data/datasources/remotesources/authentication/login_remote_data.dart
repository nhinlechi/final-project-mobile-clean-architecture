import 'package:flutter/foundation.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_login.dart';

abstract class LoginRemoteData {
  Future<UserLogin> login(String username, String password);

  Future<UserLogin> loginWithGoogle();

  Future<UserLogin> loginWithFacebook();

  Future<UserLogin> register(
    String email,
    String password,
    String firstName,
    String lastName,
  );

  Future<void> logout();
}

class LoginRemoteDataImpl implements LoginRemoteData {
  final HttpClient client;

  LoginRemoteDataImpl({
    @required this.client,
  });

  final _googleLogin = GoogleSignIn(scopes: ['email']);

  @override
  Future<UserLogin> login(String username, String password) =>
      _login(APIPathHelper.getValue(APIPath.login), username, password);

  Future<UserLogin> _login(
    String apiPath,
    String username,
    String password,
  ) async {
    final response = await client.postDataNoHeader(
      apiPath: apiPath,
      body: {
        'username': username,
        'password': password,
      },
    );
    return UserLogin.fromJson(response);
  }

  //final _facebookLogin = FacebookLogin()

  @override
  Future<UserLogin> loginWithGoogle() =>
      _loginWithGoogle(APIPathHelper.getValue(APIPath.login_google));

  Future<UserLogin> _loginWithGoogle(String apiPath) async {
    final googleAccount = await _googleLogin.signIn();
    final authenticate = await googleAccount.authentication;
    final response = await client.postDataNoHeader(
      apiPath: apiPath,
      body: {
        'googleToken': authenticate.idToken,
      },
    );
    return UserLogin.fromJson(response);
  }

  @override
  Future<UserLogin> loginWithFacebook() =>
      _loginWithFacebook(APIPathHelper.getValue(APIPath.login_facebook));

  Future<UserLogin> _loginWithFacebook(String apiPath) async {
    final LoginResult result = await FacebookAuth.i.login(permissions: []);
    switch (result.status) {
      case LoginStatus.success:
        try {
          final response =
              await client.postDataNoHeader(apiPath: apiPath, body: {
            "accesstoken": result.accessToken.token,
          });
          return UserLogin.fromJson(response);
        } catch (e) {
          throw e;
        }
        break;
      default:
        throw APIException();
    }
  }

  @override
  Future<UserLogin> register(
          String email, String password, String firstName, String lastName) =>
      _register(APIPathHelper.getValue(APIPath.register), email, password,
          firstName, lastName);

  Future<UserLogin> _register(String value, String email, String password,
      String firstName, String lastName) async {
    final response = await client.postDataNoHeader(apiPath: value, body: {
      "username": email,
      "password": password,
      "confirmpassword": password,
      "firstname": firstName,
      "lastname": lastName
    });
    return UserLogin.fromJson(response);
  }

  @override
  Future<void> logout() async {
    await FacebookAuth.i.logOut();
    await _googleLogin.signOut();
  }
}
