import 'package:flutter/foundation.dart';

import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../models/user/user_login.dart';

abstract class AuthenticateRemoteData {
  Future<UserLogin> login(String username, String password);
}

class AuthenticateRemoteDataImpl implements AuthenticateRemoteData {
  final HttpClient client;

  AuthenticateRemoteDataImpl({@required this.client});

  @override
  Future<UserLogin> login(String username, String password) async {
    final String apiPath = APIPathHelper.getValue(APIPath.login);

    try {
      final response = await client.postData(
        apiPath: apiPath,
        body: {
          "username": username,
          "password": password,
        },
      );
      return UserLogin.fromJson(response);
    } catch (e) {
      throw e;
    }
  }
}
