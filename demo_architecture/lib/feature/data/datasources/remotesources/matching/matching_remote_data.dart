import 'dart:developer';

import 'package:flutter/cupertino.dart';

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/platform/HttpClient.dart';
import '../../../../../core/platform/api_path.dart';
import '../../../../../core/share_ref/stranger_filter_share_ref.dart';
import '../../../../../injection_container.dart';
import '../../../../domain/usecase/matching/confirm_matching_uc.dart';
import '../../../../domain/usecase/matching/confirm_place_uc.dart';
import '../../../../domain/usecase/matching/request_match_uc.dart';
import '../../../../domain/usecase/matching/send_place_uc.dart';
import '../../../models/chat/match_room_model.dart';

enum MatchMode {
  Now,
  Later,
}

abstract class MatchingRemoteData {
  Future<bool> requestMatchNow(RequestMatchParams params);

  Future<bool> requestMatchLater(RequestMatchParams params);

  Future<void> confirmMatching(ConfirmMatchingParams params);

  Future<void> cancelRequestMatch();

  Future<bool> updateFirebaseToken(String firebaseToken);

  Future<void> sendPlace(SendPlaceParams params);

  Future<List<MatchRoomModel>> getMyMatch();

  Future<void> confirmPlaceToDating(ConfirmPlaceParams params);
}

class MatchingRemoteDataImpl extends MatchingRemoteData {
  final HttpClient client;

  MatchingRemoteDataImpl({@required this.client});

  @override
  Future<bool> requestMatchNow(RequestMatchParams params) async {
    try {
      return await _startRequestMatching(params, MatchMode.Now);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> confirmMatching(ConfirmMatchingParams params) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.confirm_matching);
      await client.updateData(
        apiPath: apiPath,
        body: {
          'id': params.notificationId,
          'status': params.isAccepted,
        },
      );
    } on APIException catch (e) {
      log('Confirm matching error: ' + e.toString(), error: e);
      throw e;
    }
  }

  Future<bool> _startRequestMatching(RequestMatchParams params, MatchMode mode) async {
    try {
      final apiPath = APIPathHelper.getValue(
        mode == MatchMode.Now ? APIPath.match_now : APIPath.match_later,
      );
      final filter = sl<StrangerFilterShareRef>();
      final body = {
        'lat': params.lat,
        'long': params.long,
        'distance': filter.maxDistance,
        'maxage': filter.maxAge,
        'minage': filter.minAge,
      };
      if (filter.gender != 0) body.addAll({'gender': filter.gender});
      if (filter.relationship != 0) body.addAll({'relationship': filter.relationship});

      final res = await client.postData(apiPath: apiPath, body: body);
      log('${res.toString()}', name: 'start request matching');
      return true;
    } on APIException catch (e) {
      log('start request matching error: ' + e.toString(), error: e);
      throw e;
    }
  }

  Future<void> _sendFirebaseToken(String firebaseToken) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.send_firebase_token);
      await client.postData(
        apiPath: apiPath,
        body: {
          'tokenid': firebaseToken,
        },
      );
    } on APIException catch (e) {
      log('send firebase token error: ' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> cancelRequestMatch() async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.cancel_request_match);
      await client.postData(
        apiPath: apiPath,
        body: {},
      );
    } on APIException catch (e) {
      log('can\'t cancel request match error: ' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<bool> updateFirebaseToken(String firebaseToken) async {
    try {
      await _sendFirebaseToken(firebaseToken);
      return true;
    } on APIException catch (e) {
      log('can\'t cancel request match error: ' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> sendPlace(SendPlaceParams params) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.send_place);
      await client.postData(apiPath: apiPath, body: {
        'matchid': params.matchId,
        'placeid': params.placeId,
        'date': params.datingTime.toString(),
      });
    } on APIException catch (e) {
      log('can\'t send place error - ' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<List<MatchRoomModel>> getMyMatch() async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.get_my_match);
      final jsonMap = await client.fetchData(apiPath: apiPath);
      return MatchRoomModel.listFromJson(jsonMap);
    } on APIException catch (e) {
      log('can\'t get match - ' + e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<void> confirmPlaceToDating(ConfirmPlaceParams params) async {
    try {
      final apiPath = APIPathHelper.getValue(APIPath.confirm_place);
      await client.postData(
        apiPath: apiPath,
        body: {
          'notificationid': params.notificationId,
          'status': params.isAccepted,
        },
      );
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }

  @override
  Future<bool> requestMatchLater(RequestMatchParams params) async {
    try {
      return await _startRequestMatching(params, MatchMode.Later);
    } on APIException catch (e) {
      log(e.toString(), error: e);
      throw e;
    }
  }
}
