import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exceptions.dart';
import '../../models/user/user_login.dart';

abstract class AuthentcateLocalData {
  Future<UserLogin> getUser();

  Future<void> cacheUserLoginInfor(UserLogin remoteData);
}

const CACHED_USER_LOGIN = 'CACHED_USER_LOGIN';

class AuthentcateLocalDataImpl implements AuthentcateLocalData {
  final SharedPreferences sharedPreferences;

  AuthentcateLocalDataImpl({@required this.sharedPreferences});
  @override
  Future<void> cacheUserLoginInfor(UserLogin remoteData) {
    return sharedPreferences.setString(
      CACHED_USER_LOGIN,
      json.encode(remoteData.toJson()),
    );
  }

  @override
  Future<UserLogin> getUser() {
    final jsonString = sharedPreferences.getString(CACHED_USER_LOGIN);
    log(jsonString);

    if (jsonString != null) {
      return Future.value(UserLogin.fromJson(json.decode(jsonString)));
    } else {
      log("getUser Null");
      throw CacheException();
    }
  }
}
