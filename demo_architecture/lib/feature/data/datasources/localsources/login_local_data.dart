import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exceptions.dart';
import '../../models/user/user_login.dart';

abstract class LoginLocalData {
  Future<UserLogin> login();

  Future<void> cacheLoginLocal(UserLogin userLoginRemote);

  Future<void> logout();
}

const CACHED_USER_LOGIN = 'CACHED_USER_LOGIN';

class LoginLocalDataImpl implements LoginLocalData {
  final SharedPreferences sharedPreferences;

  LoginLocalDataImpl(this.sharedPreferences);

  @override
  Future<void> cacheLoginLocal(UserLogin user) {
    return sharedPreferences.setString(
        CACHED_USER_LOGIN, json.encode(user.toJson()));
  }

  @override
  Future<UserLogin> login() {
    final jsonString = sharedPreferences.getString(CACHED_USER_LOGIN);
    if (jsonString != null) {
      return Future.value(UserLogin.fromJson(json.decode(jsonString)));
    } else
      throw CacheException();
  }

  @override
  Future<void> logout() async {
    await sharedPreferences.setString(CACHED_USER_LOGIN, null);
  }
}
