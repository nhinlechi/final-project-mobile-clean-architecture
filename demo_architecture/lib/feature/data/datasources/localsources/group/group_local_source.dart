import 'dart:convert';
import 'dart:developer';

import '../../../../../core/error/exceptions.dart';
import '../../../models/group/list_tag_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class GroupLocalSource {
  Future<ListTagModel> getListTagModelLocal();
  Future<void> cacheListTagModel(ListTagModel listTagModel);
}

const LIST_TAG_MODEL_CACHE = "list_tag_model_cache";

class GroupLocalSourceImpl implements GroupLocalSource {
  final SharedPreferences sharedPreferences;

  GroupLocalSourceImpl({this.sharedPreferences});

  @override
  Future<void> cacheListTagModel(ListTagModel listTagModel) {
    return sharedPreferences.setString(
        LIST_TAG_MODEL_CACHE, json.encode(listTagModel.toJson()));
  }

  @override
  Future<ListTagModel> getListTagModelLocal() {
    final jsonString = sharedPreferences.getString(LIST_TAG_MODEL_CACHE);

    if (jsonString != null) {
      return Future.value(
          ListTagModel.fromJson(json.decode(jsonString), "data"));
    } else {
      log("getListTagModelLocal Null");
      throw CacheException();
    }
  }
}
