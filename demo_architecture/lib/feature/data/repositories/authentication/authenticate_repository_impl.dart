import 'dart:async';
import 'dart:developer';

import '../../../../core/error/exceptions.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/entities/user/auth_token.dart';
import '../../../domain/repositories/user/authen_repo.dart';
import '../../datasources/localsources/autheticate_local_data.dart';
import '../../datasources/remotesources/authentication/authenticate_remote_data.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

class AuthenRepoImpl implements AuthenRepository {
  final AuthentcateLocalData authentcateLocalData;
  final AuthenticateRemoteData authenticateRemoteData;
  final NetworkInfo networkInfo;
  final _controller = StreamController<AuthenticationStatus>();

  AuthenRepoImpl(
      this.authentcateLocalData, this.authenticateRemoteData, this.networkInfo);

  Stream<AuthenticationStatus> get status async* {
    await Future<void>.delayed(const Duration(seconds: 1));
    yield AuthenticationStatus.unauthenticated;
    yield* _controller.stream;
  }

  @override
  Future<AuthenticateToken> authenticate() async {
    try {
      final localData = await authentcateLocalData.getUser();
      log("authenticate ");
      log(localData.token);
      return AuthenticateToken(
        firstName: localData.firstName,
        token: localData.token,
        userId: localData.userId,
        isNewUser: localData.isNewUser,
      );
    } on CacheException {
      _controller.add(AuthenticationStatus.unauthenticated);
      log("authenticate Null");
      return null;
    }
  }

  @override
  Future<AuthenticateToken> logIn(String userName, String passWord) async {
    try {
      final remoteData = await authenticateRemoteData.login(userName, passWord);
      authentcateLocalData.cacheUserLoginInfor(remoteData);
      _controller.add(AuthenticationStatus.authenticated);
      log("logIn ");
      log(remoteData.token);

      return AuthenticateToken(
        firstName: remoteData.firstName,
        token: remoteData.token,
        userId: remoteData.userId,
        isNewUser: remoteData.isNewUser,
      );
    } on APIException catch (e) {
      _controller.add(AuthenticationStatus.unauthenticated);
      log("authenticate Null");
      return null;
    }
  }

  void logOut() {
    _controller.add(AuthenticationStatus.unauthenticated);
  }

  void dispose() => _controller.close();
}
