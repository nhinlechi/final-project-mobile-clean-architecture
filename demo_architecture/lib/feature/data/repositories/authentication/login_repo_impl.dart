import 'dart:developer';

import 'package:dartz/dartz.dart';
import '../../datasources/remotesources/user/user_inform_remote_data.dart';
import '../../../domain/usecase/user/register_uc.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/entities/user/auth_token.dart';
import '../../../domain/repositories/user/login_repo.dart';
import '../../datasources/localsources/login_local_data.dart';
import '../../datasources/remotesources/authentication/login_remote_data.dart';

class LoginRepositoryImpl implements LoginRepository {
  final LoginRemoteData loginRemoteData;
  final LoginLocalData loginLocalData;
  final NetworkInfo networkInfo;

  //final UpdateBasicUserRemoteData updateRegister;
  final UserInformRemoteData userInformRemoteData;

  LoginRepositoryImpl(this.loginRemoteData, this.loginLocalData,
      this.networkInfo, this.userInformRemoteData);

  @override
  Future<Either<Failure, AuthenticateToken>> login(
      String username, String password) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await loginRemoteData.login(username, password);
        log("Login Response" + remoteData.toString());
        loginLocalData.cacheLoginLocal(remoteData);
        return right(AuthenticateToken(
            firstName: remoteData.firstName,
            token: remoteData.token,
            userId: remoteData.userId,
            isNewUser: remoteData.isNewUser));
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      try {
        final cacheData = await loginLocalData.login();
        return right(AuthenticateToken(
            firstName: cacheData.firstName,
            token: cacheData.token,
            isNewUser: cacheData.isNewUser,
            userId: cacheData.userId));
      } on CacheException {
        return left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, AuthenticateToken>> loginGG() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await loginRemoteData.loginWithGoogle();
        log("Login Response" + remoteData.toString());
        loginLocalData.cacheLoginLocal(remoteData);
        return right(AuthenticateToken(
            firstName: remoteData.firstName,
            token: remoteData.token,
            userId: remoteData.userId,
            isNewUser: remoteData.isNewUser));
      } on APIException catch (e) {
        return left(APIFailure(message: e.toString()));
      } catch (e) {
        return left(APIFailure(message: e.toString()));
      }
    } else {
      try {
        final cacheData = await loginLocalData.login();
        return right(AuthenticateToken(
            firstName: cacheData.firstName,
            token: cacheData.token,
            isNewUser: cacheData.isNewUser,
            userId: cacheData.userId));
      } on CacheException {
        return left(CacheFailure());
      } catch (e) {
        return left(APIFailure(message: e));
      }
    }
  }

  @override
  Future<Either<Failure, AuthenticateToken>> loginFB() async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await loginRemoteData.loginWithFacebook();
        log("Login Response" + remoteData.toString());
        loginLocalData.cacheLoginLocal(remoteData);
        return Right(AuthenticateToken(
            firstName: remoteData.firstName,
            token: remoteData.token,
            userId: remoteData.userId,
            isNewUser: remoteData.isNewUser));
      } on APIException catch (e) {
        return left(APIFailure(message: e.toString()));
      } catch (e) {
        return left(APIFailure(message: e));
      }
    } else {
      try {
        final cacheData = await loginLocalData.login();
        return right(AuthenticateToken(
            firstName: cacheData.firstName,
            token: cacheData.token,
            isNewUser: cacheData.isNewUser,
            userId: cacheData.userId));
      } on CacheException {
        return left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> register(ParamsRegister paramsRegister) async {
    if (await networkInfo.isConnected) {
      try {
        final paramUpdate = paramsRegister.updateUserInformParams;
        final remoteData = await loginRemoteData.register(
            paramsRegister.email,
            paramsRegister.password,
            paramUpdate.firstname,
            paramUpdate.lastname);
        loginLocalData.cacheLoginLocal(remoteData);
        await userInformRemoteData.updateUserInform(paramUpdate);
        return right(true);
      } on APIException catch (e) {
        return left(APIFailure(message: e.toString()));
      }
    }
  }

  @override
  Future<Either<Failure, void>> logout() async {
    try {
      await loginRemoteData.logout();
      return right(await loginLocalData.logout());
    } catch (e) {
      return left(APIFailure(message: e.toString()));
    }
  }

  @override
  Future<Either<Failure, AuthenticateToken>> autoLogin() async {
    try {
      final user = await loginLocalData.login();
      return right(AuthenticateToken(
        firstName: user.firstName,
        token: user.token,
        userId: user.userId,
        isNewUser: user.isNewUser,
      ));
    } catch (e) {
      return left(CacheFailure());
    }
  }
}
