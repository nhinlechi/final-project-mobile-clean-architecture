import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/exceptions.dart';
import 'package:demo_architecture/feature/data/models/chat/date_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_dates_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/get_recommend_place_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/chats/upload_message_image_uc.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/repositories/chat/get_chat_repos.dart';
import '../../../domain/usecase/chats/get_chat_messages_uc.dart';
import '../../datasources/remotesources/chat/chat_remote_data.dart';
import '../../models/chat/message_model.dart';

class ChatRepositoryImpl extends ChatRepository {
  final NetworkInfo networkInfo;
  final ChatRemoteData remoteData;

  ChatRepositoryImpl({@required this.networkInfo, @required this.remoteData});

  @override
  Future<Either<Failure, List<MessageModel>>> getChat(
      GetChatParam params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.getChat(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, String>> uploadMessageImage(
      UploadMessageImageParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.uploadMessageImage(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<PlaceEntity>>> getRecommendPlace(
      GetRecommendPlaceParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.getRecommendPlace(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<DateModel>>> getAllDates(
      GetDatesParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.getDates(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }
}
