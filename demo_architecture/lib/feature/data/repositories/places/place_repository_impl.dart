import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/exceptions.dart';
import '../../../domain/usecase/places/get_places_uc.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/entities/place/list_place_entity.dart';
import '../../../domain/repositories/place/place_repository.dart';
import '../../../domain/usecase/places/comment_place_uc.dart';
import '../../datasources/remotesources/place/place_remote_source.dart';
import '../../models/place/list_comment_place_model.dart';

class PlaceRepositoryImpl implements PlaceRepository {
  final PlaceRemoteDataSource remote;
  final NetworkInfo networkInfo;

  PlaceRepositoryImpl(this.remote, this.networkInfo);
  @override
  Future<Either<Failure, ListPlaceEntity>> getPlaces(
      GetPlacesParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remote.getPlaces(params);
        return Right(ListPlaceEntity.fromModel(result));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ListCommentPlaceModel>> getCommentPlace(
      String id) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remote.getCommentPlace(id);
        return Right(result);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> commentPlace(CommentPlaceParams params) async {
    if (await networkInfo.isConnected) {
      try {
        await remote.commentPlace(params);
        return Right(true);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }
}
