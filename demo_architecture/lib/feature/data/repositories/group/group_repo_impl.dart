import 'package:dartz/dartz.dart';
import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/domain/usecase/group/confirm_request_join_group_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/create_new_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_group_event_messages_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/get_list_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/join_group_event_uc.dart';
import 'package:demo_architecture/feature/domain/usecase/group/update_group_event_uc.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../datasources/localsources/group/group_local_source.dart';
import '../../datasources/remotesources/group/group_remote_data.dart';
import '../../models/group/list_tag_model.dart';
import '../../../domain/entities/group/group_entity.dart';
import '../../../domain/entities/group/list_group_entity.dart';
import '../../../domain/repositories/group/group_repository.dart';
import '../../../domain/usecase/group/add_new_group.dart';
import '../../../domain/usecase/group/get_group_by_id.dart';
import '../../../domain/usecase/group/get_list_group.dart';
import '../../../domain/usecase/group/leave_group.dart';
import '../../../domain/usecase/group/request_join_group_uc.dart';
import '../../../domain/usecase/group/update_group.dart';

class GroupRepositoryImplement implements GroupRepository {
  final GroupRemoteData remoteData;
  final NetworkInfo networkInfo;
  final GroupLocalSource localData;

  GroupRepositoryImplement(this.remoteData, this.networkInfo, this.localData);

  @override
  Future<Either<Failure, ListGroupEntity>> getListGroup(
      GetListGroupParams getListGroupParams) async {
    if (await networkInfo.isConnected) {
      try {
        final listGroupModel =
            await remoteData.getListGroupModel(getListGroupParams);
        return Right(ListGroupEntity.fromModel(listGroupModel));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, void>> addNewGroup(
      AddNewGroupParams addNewGroupParams) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteData.addNewGroup(addNewGroupParams);
        return Right(null);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ListTagModel>> getListTagOfGroup() async {
    if (await networkInfo.isConnected) {
      try {
        final listTagModel = await remoteData.getListTagOfGroup();
        localData.cacheListTagModel(listTagModel);
        return Right(listTagModel);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final listTagModel = await localData.getListTagModelLocal();
        return Right(listTagModel);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, ListGroupEntity>> getListGroupOfUser() async {
    try {
      final result = await remoteData.getListGroupOfUser();
      return Right(ListGroupEntity.fromModel(result));
    } on APIException catch (e) {
      return Left(APIFailure());
    }
  }

  @override
  Future<Either<Failure, void>> requestJoinGroup(
      RequestJoinGroupParams requestJoinGroupParams) async {
    try {
      await remoteData.requestJoinGroup(requestJoinGroupParams);
      return Right(null);
    } on APIException catch (e) {
      return Left(APIFailure());
    }
  }

  @override
  Future<Either<Failure, void>> leaveGroup(
      LeaveGroupParams leaveGroupParams) async {
    try {
      await remoteData.leaveGroup(leaveGroupParams);
      return Right(null);
    } on APIException catch (e) {
      return Left(APIFailure());
    }
  }

  @override
  Future<Either<Failure, void>> updateGroup(
      UpdateGroupParams updateGroupParams) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteData.updateGroup(updateGroupParams);
        return Right(null);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, GroupEntity>> getGroupById(
      GetGroupByIdParams getGroupByIdParams) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteData.getGroupById(getGroupByIdParams);
        return Right(GroupEntity.fromModel(result));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> createNewGroupEvent(
      CreateNewGroupEventParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.createNewGroupEvent(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, ListGroupEventModel>> getListGroupEvent(
      GetListGroupEventParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.getListGroupEvent(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> confirmRequestJoinGroup(
      ConfirmRequestJoinGroupParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.confirmRequestJoinGroup(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> joinGroupEvent(
      JoinGroupEventParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.joinGroupEvent(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> updateGroupEvent(
      UpdateGroupEventParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.updateGroupEvent(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<MessageModel>>> getGroupEventMessages(
      GetGroupEventMessagesParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.getGroupEventMessages(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }
}
