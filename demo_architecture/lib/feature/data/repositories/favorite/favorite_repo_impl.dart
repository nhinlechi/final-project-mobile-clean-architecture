import 'package:dartz/dartz.dart';
import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../datasources/remotesources/favorites/favorite_remote_data.dart';
import '../../models/favorite/favorite_model.dart';
import '../../../domain/repositories/favorite/favorite_repository.dart';

class FavoriteReposiotyImpl implements FavoriteRepository {
  final NetworkInfo networkInfo;
  final FavoriteRemoteData remoteData;

  FavoriteReposiotyImpl(this.networkInfo, this.remoteData);
  @override
  Future<Either<Failure, List<FavoriteModel>>> getListFavorite() async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteData.getListFavorite();
        return Right(result);
      } on APIException catch(e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, List<FavoriteModel>>> getMyListFavorite() async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteData.getMyListFavorite();
        return Right(result);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }
}
