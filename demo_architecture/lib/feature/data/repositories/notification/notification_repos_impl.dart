import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/exceptions.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/repositories/notification/notification_repos.dart';
import '../../../domain/usecase/notifications/get_notification_uc.dart';
import '../../datasources/remotesources/notification/notification_remote_data.dart';
import '../../models/notification/notification_model.dart';

class NotificationReposImpl extends NotificationRepos {
  final NotificationRemoteData remoteData;
  final NetworkInfo networkInfo;

  NotificationReposImpl({
    @required this.networkInfo,
    @required this.remoteData,
  });

  @override
  Future<Either<Failure, List<NotificationModel>>> getNotifications(
      GetNotificationParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.getNotifications(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }
}
