import 'package:dartz/dartz.dart';
import 'package:demo_architecture/core/error/exceptions.dart';
import '../../../../core/usecases/usecase.dart';
import '../../models/chat/match_room_model.dart';
import '../../../domain/usecase/matching/confirm_place_uc.dart';
import '../../../domain/usecase/matching/send_place_uc.dart';
import 'package:flutter/foundation.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../domain/repositories/matching/matching_repos.dart';
import '../../../domain/usecase/matching/confirm_matching_uc.dart';
import '../../../domain/usecase/matching/request_match_uc.dart';
import '../../datasources/remotesources/matching/matching_remote_data.dart';

class MatchingRepositoryImpl extends MatchingRepository {
  final MatchingRemoteData remoteData;
  final NetworkInfo networkInfo;

  MatchingRepositoryImpl(
      {@required this.remoteData, @required this.networkInfo});

  @override
  Future<Either<Failure, void>> confirmMatching(
      ConfirmMatchingParams params) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteData.confirmMatching(params);
        return right(null);
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else
      return left(NetworkFailure());
  }

  @override
  Future<Either<Failure, bool>> requestMatchNow(
      RequestMatchParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.requestMatchNow(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> cancelRequestMatch() async {
    if (await networkInfo.isConnected) {
      try {
        await remoteData.cancelRequestMatch();
        return right(null);
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> updateFirebaseToken(
      String firebaseToken) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.updateFirebaseToken(firebaseToken));
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> sendPlace(SendPlaceParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.sendPlace(params));
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, List<MatchRoomModel>>> getMyMatch(
      NoParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.getMyMatch());
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, void>> confirmPlaceToDating(
      ConfirmPlaceParams params) async {
    if (await networkInfo.isConnected) {
      try {
        return right(await remoteData.confirmPlaceToDating(params));
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> requestMatchLater(RequestMatchParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await remoteData.requestMatchLater(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure(message: e.message));
      }
    } else {
      return left(NetworkFailure());
    }
  }
}
