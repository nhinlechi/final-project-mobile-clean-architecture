import 'dart:developer';

import 'package:dartz/dartz.dart';
import '../../../domain/usecase/user/get_user_inform.dart';
import '../../../domain/usecase/user/get_user_inform_by_id.dart';
import '../../../domain/usecase/user/update_user_inform_uc.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../../core/utils/convertToUserInform.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/repositories/user/user_inform_repository.dart';
import '../../datasources/localsources/user_inform_local_sources.dart';
import '../../datasources/remotesources/user/user_inform_remote_data.dart';

class UserInformRepoImpl implements UserInformRepository {
  final UserInformLocalData userInformLocalData;
  final UserInformRemoteData userInformRemoteData;
  final NetworkInfo networkInfo;

  UserInformRepoImpl(
      {@required this.userInformLocalData,
      @required this.userInformRemoteData,
      @required this.networkInfo});

  @override
  Future<Either<Failure, UserInform>> getUserInform(
      GetUserInformParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await userInformRemoteData.getUserInform(params);
        userInformLocalData.cacheUserInform(remoteData);
        log(remoteData.toString());
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final localData = await userInformLocalData.getUserInform();
        return Right(ConvertUserInform.convert(localData));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserInform>> getUserInformById(
      GetUserInformByIdParams getUserInformByIdParams) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await userInformRemoteData
            .getUserInformById(getUserInformByIdParams);
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }

  @override
  Future<Either<Failure, void>> updateUserInform(
      UpdateUserInformParams params) async {
    if (await networkInfo.isConnected) {
      try {
        await userInformRemoteData.updateUserInform(params);
        return Right(null);
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    }
  }
}
