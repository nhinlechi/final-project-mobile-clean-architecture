import 'dart:developer';

import 'package:dartz/dartz.dart';
import '../../../domain/usecase/user/register_social_usecase.dart';
import '../../../domain/usecase/user/update_user_avatar_uc.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../../core/utils/convertToUserInform.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/repositories/user/update_basic_user_inform_repo.dart';
import '../../datasources/localsources/user_inform_local_sources.dart';
import '../../datasources/remotesources/user/update_basic_remote_data.dart';

class UpdateBasicRepoImpl implements UpdateBasicUserInformRepo {
  final UpdateBasicUserRemoteData updateBasicUserRemoteData;
  final UserInformLocalData userInformLocalData;
  final NetworkInfo networkInfo;

  UpdateBasicRepoImpl(this.updateBasicUserRemoteData, this.userInformLocalData,
      this.networkInfo);

  @override
  Future<Either<Failure, UserInform>> updateBasicUserInform(
      String birthDay, String lastname, String firstName, String gender) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await updateBasicUserRemoteData.updateUserInform(
            birthDay, lastname, firstName, gender);
        userInformLocalData.cacheUserInform(remoteData);
        log(remoteData.toString());
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final localData = await userInformLocalData.getUserInform();
        return Right(ConvertUserInform.convert(localData));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, UserInform>> updateUserInformForSocialRegister(
      ParamsRegisterSocial paramsRegisterSocial) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await updateBasicUserRemoteData
            .updateUserInformForSocialRegister(paramsRegisterSocial);
        userInformLocalData.cacheUserInform(remoteData);
        log(remoteData.toString());
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch(e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final localData = await userInformLocalData.getUserInform();
        return Right(ConvertUserInform.convert(localData));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }

  @override
  Future<Either<Failure, bool>> updateUserAvatar(
      UpdateUserAvatarParams params) async {
    if (await networkInfo.isConnected) {
      try {
        final rs = await updateBasicUserRemoteData.updateUserAvatar(params);
        return right(rs);
      } on APIException catch (e) {
        return left(APIFailure());
      }
    } else {
      return left(NetworkFailure());
    }
  }
}
