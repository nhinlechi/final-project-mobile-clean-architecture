import 'dart:developer';

import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../../core/utils/convertToUserInform.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/repositories/user/update_phone_user_inform_repo.dart';
import '../../datasources/localsources/user_inform_local_sources.dart';
import '../../datasources/remotesources/user/update_phone_remote_data.dart';

class UpdatePhoneRepoImpl implements UpdatePhoneUserInformRepo {
  final UpdatePhoneUserRemoteData updatePhoneUserRemoteData;
  final UserInformLocalData userInformLocalData;
  final NetworkInfo networkInfo;

  UpdatePhoneRepoImpl(this.updatePhoneUserRemoteData, this.userInformLocalData,
      this.networkInfo);

  @override
  Future<Either<Failure, UserInform>> updatePhoneUserInform(
      String phone) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData =
            await updatePhoneUserRemoteData.updateUserInform(phone);
        userInformLocalData.cacheUserInform(remoteData);
        log(remoteData.toString());
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final localData = await userInformLocalData.getUserInform();
        return Right(ConvertUserInform.convert(localData));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
