import 'dart:developer';

import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/platform/network_infor.dart';
import '../../../../core/utils/convertToUserInform.dart';
import '../../../domain/entities/user/user_inform.dart';
import '../../../domain/repositories/user/update_rela_user_inform_repo.dart';
import '../../datasources/localsources/user_inform_local_sources.dart';
import '../../datasources/remotesources/user/update_relationship_remote_data.dart';

class UpdateRelaRepoImpl implements UpdateRelaUserInformRepo {
  final UpdateRelaUserRemoteData updateRelaUserRemoteData;
  final UserInformLocalData userInformLocalData;
  final NetworkInfo networkInfo;

  UpdateRelaRepoImpl(this.updateRelaUserRemoteData, this.userInformLocalData,
      this.networkInfo);

  @override
  Future<Either<Failure, UserInform>> updateRelaUserInform(int rela) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData =
            await updateRelaUserRemoteData.updateUserInform(rela);
        userInformLocalData.cacheUserInform(remoteData);
        log(remoteData.toString());
        return Right(ConvertUserInform.convert(remoteData));
      } on APIException catch (e) {
        return Left(APIFailure());
      }
    } else {
      try {
        final localData = await userInformLocalData.getUserInform();
        return Right(ConvertUserInform.convert(localData));
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
