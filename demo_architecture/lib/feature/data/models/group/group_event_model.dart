import 'package:demo_architecture/feature/data/models/place/place_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ParticipantModel extends Equatable {
  final String avatar;
  final String id;
  final String firstName;

  ParticipantModel({
    @required this.avatar,
    @required this.id,
    @required this.firstName,
  });

  factory ParticipantModel.fromJson(Map<String, dynamic> jsonMap) {
    return ParticipantModel(
      avatar: jsonMap['avatar'][0],
      id: jsonMap['_id'],
      firstName: jsonMap['firstname'],
    );
  }

  static getListParticipantsFromJson(List<dynamic> participantsJsonData) {
    // Get all participants
    final List<ParticipantModel> participants = [];
    participantsJsonData.forEach((p) {
      participants.add(ParticipantModel.fromJson(p));
    });
    return participants;
  }

  @override
  List<Object> get props => [avatar, id, firstName];
}

class GroupEventModel extends Equatable {
  final String id;
  final List<ParticipantModel> participants;
  final int eventTime;
  final int status;
  final String title;
  final String description;
  final PlaceEntity place;
  final String groupId;

  GroupEventModel({
    @required this.id,
    @required this.participants,
    @required this.eventTime,
    @required this.status,
    @required this.title,
    @required this.description,
    @required this.place,
    @required this.groupId,
  });

  factory GroupEventModel.fromJson(Map<String, dynamic> jsonMap) {
    // Get all participants
    final List<ParticipantModel> participants = [];
    final participantsJsonData = jsonMap['participants'] as List;
    participantsJsonData.forEach((p) {
      participants.add(ParticipantModel.fromJson(p));
    });
    // Return Group Event Model
    return GroupEventModel(
      participants: participants,
      id: jsonMap['_id'],
      eventTime: jsonMap['date'],
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['placeid']),
      ),
      title: jsonMap['name'],
      description: jsonMap['description'],
      groupId: jsonMap['groupid'],
      status: jsonMap['status'],
    );
  }

  @override
  List<Object> get props =>
      [id, participants, place, groupId, title, description, eventTime, status];
}

class ListGroupEventModel extends Equatable {
  final List<GroupEventModel> groupEvents;
  final int total;

  ListGroupEventModel({
    @required this.groupEvents,
    @required this.total,
  });

  factory ListGroupEventModel.fromJson(Map<String, dynamic> jsonMap) {
    final List<GroupEventModel> groupEvents = [];
    final groupEventsJsonData = jsonMap['data']['events'] as List;
    groupEventsJsonData.forEach((eventJsonMap) {
      groupEvents.add(GroupEventModel.fromJson(eventJsonMap));
    });
    return ListGroupEventModel(
      groupEvents: groupEvents,
      total: jsonMap['data']['total'],
    );
  }

  bool get isHaveUpcomingEvent =>
      groupEvents.indexWhere((event) => event.status == 0) >= 0;

  GroupEventModel get upcomingEvent =>
      groupEvents.firstWhere((event) => event.status == 0);

  @override
  List<Object> get props => [];
}
