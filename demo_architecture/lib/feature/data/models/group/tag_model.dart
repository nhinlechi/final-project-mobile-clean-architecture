import 'package:equatable/equatable.dart';

class TagModel extends Equatable{
  final String tagId;
  final String tagName;

  TagModel({this.tagId, this.tagName});

  @override
  // TODO: implement props
  List<Object> get props => [tagId, tagName];

  factory TagModel.fromJson(Map<String, dynamic> jsonMap){
    final result = TagModel(tagId: jsonMap['_id'] as String, tagName:  jsonMap['name'] as String);
    return result;
  }

  Map<String, dynamic> toJson(){
    return {
      '_id': tagId,
      'name': tagName,
    };
  }

}