import 'group_model.dart';
import 'package:equatable/equatable.dart';

class ListGroupModel extends Equatable {
  final int total;
  final List<GroupModel> listGroup;

  ListGroupModel(this.total, this.listGroup);

  @override
  List<Object> get props => [total, listGroup];

  factory ListGroupModel.fromJson(Map<String, dynamic> jsonMap) {
    final listGroupJson = jsonMap['data']['groups'];
    List<GroupModel> listGroupModel = [];
    listGroupJson
        .forEach((element) => listGroupModel.add(GroupModel.fromJson(element)));
    final result =
        ListGroupModel(jsonMap['data']['total'] as int, listGroupModel);
    return result;
  }
}
