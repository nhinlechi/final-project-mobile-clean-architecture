import 'tag_model.dart';
import 'package:equatable/equatable.dart';

class ListTagModel extends Equatable {
  final List<TagModel> listTagModel;

  ListTagModel({this.listTagModel});

  @override
  // TODO: implement props
  List<Object> get props => [listTagModel];

  factory ListTagModel.fromJson(Map<String, dynamic> jsonMap, String jsonName) {
    final listJson = jsonMap[jsonName];
    final List<TagModel> result = [];
    listJson.forEach((element) => result.add(TagModel.fromJson(element)));

    return ListTagModel(listTagModel: result);
  }

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> listTagModelJson = [];
    this.listTagModel.forEach((element) {
      listTagModelJson.add(element.toJson());
    });
    return {'data': listTagModelJson};
  }
}
