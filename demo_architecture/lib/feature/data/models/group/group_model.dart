import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/foundation.dart';

import 'group_event_model.dart';
import 'list_tag_model.dart';

class GroupModel extends Equatable {
  final String description;
  final List<ParticipantModel> participant;
  final bool isprivate;
  final int star;
  final String id;
  final String host;
  final String name;
  final int createAt;
  final ListTagModel tag;
  final String avatar;

  GroupModel({
    this.description,
    this.participant,
    this.isprivate,
    this.star,
    this.id,
    this.host,
    this.name,
    this.createAt,
    this.tag,
    this.avatar,
  });

  @override
  List<Object> get props =>
      [description, participant, isprivate, star, id, host, name, createAt];

  factory GroupModel.fromJson(Map<String, dynamic> jsonMap) {
    List<ParticipantModel> participants = [];
    (jsonMap['participant'] as List).forEach((e) {
      participants.add(ParticipantModel.fromJson(e));
    });
    //
    ListTagModel tags = ListTagModel.fromJson(jsonMap, 'tag');
    //
    final host = jsonMap['host'] != null ? jsonMap['host']['_id'] ?? '' : null;

    GroupModel result = GroupModel(
      description: jsonMap['description'] as String,
      participant: participants,
      isprivate: jsonMap['isprivate'] as bool,
      star: jsonMap['star'] as int,
      id: jsonMap['_id'] as String,
      host: host,
      name: jsonMap['name'] as String,
      createAt: jsonMap['createAt'] as int,
      tag: tags,
      avatar: jsonMap['avatar'] as String,

    );
    return result;
  }

  Map<String, dynamic> toJson() {
    return {
      "description": description,
      "isprivate": isprivate,
      "participant": participant,
      "star": star,
      "_id": id,
      "host": host,
      "name": name,
      "createAt": createAt,
    };
  }
}
