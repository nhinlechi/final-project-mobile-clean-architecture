import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

class FilterModel<T> {
  final IconData iconData;
  final T value;
  final String shortName;
  final String longName;
  final String typeOnAPI;

  FilterModel({
    this.iconData,
    @required this.value,
    @required this.shortName,
    this.longName,
    this.typeOnAPI,
  });
}
