class SignedUrlModel {
  final String uploadUrl;
  final String accessUrl;

  SignedUrlModel(this.uploadUrl, this.accessUrl);

  factory SignedUrlModel.fromJson(Map<String, dynamic> json) {
    return SignedUrlModel(
      json['data']['uploadurl'],
      json['data']['accessurl'],
    );
  }
}
