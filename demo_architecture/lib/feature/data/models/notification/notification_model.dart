import 'package:demo_architecture/core/utils/convertToUserInform.dart';
import 'package:demo_architecture/feature/data/models/user/user_inform.dart';
import 'package:demo_architecture/feature/domain/entities/user/user_inform.dart';

import '../place/place_model.dart';
import '../../../domain/entities/place/place_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class NotificationModel extends Equatable {
  static const int ALL = -1;
  static const int REQUEST_MATCH_NOW = 1;
  static const int REQUEST_MATCH_LATER = 2;
  static const int CONFIRM_MATCH = 3;
  static const int REQUEST_JOIN_GROUP = 4;
  static const int JOIN_GROUP_SUCCESS = 5;
  static const int COMING_MEETING = 6;
  static const int REQUEST_PLACE = 8;
  //static const int SOME_ONE_CALL = 9;

  final String id;
  final int type;
  final int createAt;
  final NotificationData data;

  NotificationModel({
    @required this.type,
    @required this.id,
    @required this.data,
    @required this.createAt,
  });

  @override
  List<Object> get props => [type, id, data];

  factory NotificationModel.fromJson(jsonMap) {
    NotificationData data;
    final type = jsonMap['code'];
    switch (type) {
      case REQUEST_JOIN_GROUP:
        data = RequestJoinGroupData.fromJson(jsonMap);
        break;
      case REQUEST_PLACE:
        data = RequestPlaceData.fromJson(jsonMap);
        break;
      case REQUEST_MATCH_NOW:
      case REQUEST_MATCH_LATER:
        data = RequestMatchData.fromJson(jsonMap);
    }
    return NotificationModel(
      type: type,
      id: jsonMap['_id'],
      createAt: jsonMap['created'],
      data: data,
    );
  }

  factory NotificationModel.fromNotifyData(
      String notifyCode, Map<String, dynamic> jsonMap) {
    NotificationData data;
    final type = int.tryParse(notifyCode);
    switch (type) {
      case REQUEST_PLACE:
        data = RequestPlaceData.fromNotifyData(jsonMap);
        break;
      case REQUEST_MATCH_NOW:
      case REQUEST_MATCH_LATER:
        data = RequestMatchData.fromNotifyData(jsonMap);
        break;
    }
    return NotificationModel(
      type: type,
      createAt: jsonMap['created'],
      id: jsonMap['notificationid'],
      data: data,
    );
  }

  static List<NotificationModel> listFromJson(jsonData) {
    List<NotificationModel> rs = [];
    final notifications = jsonData['data']['notification'] as List;
    for (int i = 0; i < notifications.length; i++) {
      rs.add(NotificationModel.fromJson(notifications[i]));
    }
    return rs;
  }
}

abstract class NotificationData extends Equatable {}

class RequestMatchData extends NotificationData {
  final List<UserInform> users;
  final int timeout;

  RequestMatchData({
    @required this.users,
    @required this.timeout,
  });

  factory RequestMatchData.fromJson(Map<String, dynamic> jsonMap) {
    return RequestMatchData.fromNotifyData(jsonMap['data']);
  }

  factory RequestMatchData.fromNotifyData(Map<String, dynamic> jsonMap) {
    final user1 = jsonMap['user1'];
    final user2 = jsonMap['user2'];

    final user1Info = ConvertUserInform.convert(
      UserInformModel.fromNotifyData(user1),
    );
    final user2Info = ConvertUserInform.convert(
      UserInformModel.fromNotifyData(user2),
    );

    return RequestMatchData(
      users: [user1Info, user2Info],
      timeout: int.tryParse(jsonMap['timeout']),
    );
  }

  @override
  List<Object> get props => [users, timeout];
}

class RequestJoinGroupData extends NotificationData {
  final String userId;
  final String description;
  final String groupId;

  RequestJoinGroupData({
    @required this.userId,
    @required this.description,
    @required this.groupId,
  });

  @override
  List<Object> get props => [userId, description, groupId];

  factory RequestJoinGroupData.fromJson(jsonData) {
    return RequestJoinGroupData(
      userId: jsonData['data']['userid'],
      description: jsonData['data']['description'],
      groupId: jsonData['serviceid'],
    );
  }
}

class RequestPlaceData extends NotificationData {
  final String senderId;
  final String matchId;
  final PlaceEntity place;
  final int datingTime;

  RequestPlaceData({
    @required this.senderId,
    @required this.matchId,
    @required this.place,
    @required this.datingTime,
  });

  factory RequestPlaceData.fromJson(Map<String, dynamic> jsonMap) {
    return RequestPlaceData(
      senderId: jsonMap['sender'],
      matchId: jsonMap['serviceid'],
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['data']['place']),
      ),
      datingTime: int.tryParse(jsonMap['data']['date'].toString()),
    );
  }

  factory RequestPlaceData.fromNotifyData(Map<String, dynamic> jsonMap) {
    return RequestPlaceData(
      matchId: jsonMap['serviceid'],
      senderId: jsonMap['sender'],
      place: PlaceEntity.fromModel(PlaceModel.fromJson(jsonMap['place'])),
      datingTime: int.tryParse(jsonMap['date']),
    );
  }

  @override
  List<Object> get props => [matchId, place];
}
