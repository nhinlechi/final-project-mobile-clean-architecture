import 'package:equatable/equatable.dart';

class FavoriteModel extends Equatable{
  final String id;
  final String name;

  FavoriteModel(this.id, this.name);

  @override
  // TODO: implement props
  List<Object> get props => [id, name];

  factory FavoriteModel.fromJson(Map<String, dynamic> jsonMap){
    return FavoriteModel(jsonMap['_id'] as String, jsonMap['name'] as String);
  }

}