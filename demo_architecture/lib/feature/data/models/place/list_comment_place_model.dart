import 'package:equatable/equatable.dart';

import 'comment_place_model.dart';

class ListCommentPlaceModel extends Equatable {
  final double avgRating;
  final int ratingTotal;
  final List<CommentPlaceModel> listCommentPlace;

  ListCommentPlaceModel({
    this.listCommentPlace,
    this.avgRating,
    this.ratingTotal,
  });

  @override
  // TODO: implement props
  List<Object> get props => [listCommentPlace];

  factory ListCommentPlaceModel.fromJson(Map<String, dynamic> jsonMap) {
    final List<CommentPlaceModel> result = [];
    final list = jsonMap['data']['comment'];
    list.forEach((element) {
      result.add(CommentPlaceModel.fromJson(element));
    });

    return ListCommentPlaceModel(
      listCommentPlace: result,
      avgRating: double.parse(double.parse(jsonMap['data']['rating'].toString())
          .toStringAsFixed(1)),
      ratingTotal: jsonMap['data']['user_ratings_total'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'data': {'comments': listCommentPlace}
    };
  }
}
