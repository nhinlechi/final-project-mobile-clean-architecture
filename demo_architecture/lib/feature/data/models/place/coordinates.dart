import 'package:equatable/equatable.dart';

class CoordinatesPlace extends Equatable{
  final double lat;
  final double long;

  CoordinatesPlace(this.lat, this.long);

  @override
  // TODO: implement props
  List<Object> get props => [lat,long];

}