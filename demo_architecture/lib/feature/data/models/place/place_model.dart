import 'package:equatable/equatable.dart';

import 'coordinates.dart';

class PlaceModel extends Equatable {
  final String address;
  final List<String> photo;
  final double rating;
  final String id;
  final CoordinatesPlace location;
  final String icon;
  final String name;
  final String type;
  final int user_ratings_total;

  PlaceModel(this.address, this.photo, this.rating, this.id, this.location,
      this.icon, this.name, this.type, this.user_ratings_total);

  @override
  // TODO: implement props
  List<Object> get props =>
      [address, photo, rating, id, location, icon, name, type];

  factory PlaceModel.fromJson(Map<String, dynamic> jsonMap) {
    List<String> stringList =
        (jsonMap['photos'] as List<dynamic>).cast<String>();

    final place = PlaceModel(
        jsonMap["address"] as String,
        stringList,
        double.parse(jsonMap["rating"].toStringAsFixed(1)),
        jsonMap["_id"] as String,
        CoordinatesPlace(jsonMap["location"]["lat"] * 1.0,
            jsonMap["location"]["lng"] * 1.0),
        jsonMap["icon"] as String,
        jsonMap["name"] as String,
        jsonMap["type"] as String,
        jsonMap["user_ratings_total"] as int);
    return place;
  }

  Map<String, dynamic> toJson() {
    return {
      "address": address,
      "photo": photo,
      "rating": rating,
      "_id": id,
      "location": {"lat": location.lat, "lng": location.long},
      "icon": icon,
      "name": name,
      "type": type
    };
  }
}
