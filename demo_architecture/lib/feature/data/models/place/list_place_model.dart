import 'package:equatable/equatable.dart';

import 'place_model.dart';

class ListPlaceModel extends Equatable {
  final List<PlaceModel> listPlaceModel;
  int total;

  ListPlaceModel(this.listPlaceModel, this.total);

  @override
  // TODO: implement props
  List<Object> get props => [listPlaceModel, total];

  factory ListPlaceModel.fromJson(Map<String, dynamic> jsonMap) {
    final list = jsonMap["data"]["places"];
    List<PlaceModel> listPlaces = [];
    list.forEach((element) => listPlaces.add(PlaceModel.fromJson(element)));
    return ListPlaceModel(listPlaces, jsonMap["data"]["total"] as int);
  }

  factory ListPlaceModel.fromRecommendPlaceJson(Map<String, dynamic> jsonMap) {
    final list = jsonMap["data"];
    List<PlaceModel> listPlaces = [];
    list.forEach((element) => listPlaces.add(PlaceModel.fromJson(element)));
    return ListPlaceModel(listPlaces, 0);
  }

  Map<String, dynamic> toJson() {
    List<Map<String, dynamic>> listPlace = [];
    listPlaceModel.forEach((element) {
      listPlace.add(element.toJson());
    });
    return {
      "data": {
        "places": listPlaceModel,
        "total": total,
      }
    };
  }
}
