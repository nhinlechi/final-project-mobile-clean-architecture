import 'package:equatable/equatable.dart';

class CommentPlaceModel extends Equatable {
  final String userId;
  final String firstName;
  final String lastName;
  final String avatar;
  final String comment;
  final int rating;
  final String createdMoment;

  CommentPlaceModel(this.userId, this.comment, this.rating,
      this.createdMoment, this.firstName, this.lastName, this.avatar);

  @override
  List<Object> get props => [userId, comment, rating, createdMoment];

  factory CommentPlaceModel.fromJson(Map<String, dynamic> jsonMap) {
    final result = CommentPlaceModel(
      jsonMap['user']['userId'] as String,
      jsonMap['comment'] as String,
      jsonMap['rating'],
      jsonMap['created_comment'].toString(),
      jsonMap['user']['firstname'],
      jsonMap['user']['lastname'],
      jsonMap['user']['avatar'][0],
    );
    return result;
  }

  Map<String, dynamic> toJson() {
    return {
      'userId': userId,
      'comment': comment,
      'rating': rating,
      'created_comment': createdMoment
    };
  }
}
