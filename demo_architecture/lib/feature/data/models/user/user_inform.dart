import 'dart:convert';
import 'dart:developer';

import '../favorite/favorite_model.dart';
import 'package:equatable/equatable.dart';

class UserInformModel extends Equatable {
  final String username;
  final String email;
  final String id;
  final String phone;
  final String firstname;
  final String lastname;
  final int registerdate;
  final int dateofbirth;
  final int gender;
  final int relationship;
  final double lat;
  final double long;
  final List<String> avatar;
  final List<FavoriteModel> favorites;

  UserInformModel(
      this.id,
      this.phone,
      this.firstname,
      this.lastname,
      this.registerdate,
      this.dateofbirth,
      this.gender,
      this.relationship,
      this.lat,
      this.long,
      this.username,
      this.email,
      this.avatar,
      this.favorites);

  @override
  List<Object> get props => [
        id,
        phone,
        firstname,
        lastname,
        registerdate,
        dateofbirth,
        gender,
        relationship,
        lat,
        long,
        username,
        email,
        avatar,
        favorites
      ];

  factory UserInformModel.fromJson(Map<String, dynamic> jsonMap) {
    //log(jsonMap.toString() + "UserInformFromJson");
    final listJson = jsonMap['data']['favorites'];
    final List<FavoriteModel> listFavorite = [];
    listJson?.forEach((element) =>
        {if (element != null) listFavorite.add(FavoriteModel.fromJson(element))});

    final listAvatar = jsonMap["data"]["avatar"] as List<dynamic>;
    final List<String> avatars = [];
    listAvatar.forEach((e) {
      avatars.add(e as String);
    });
    final lat = jsonMap["data"]["lat"] ?? 0;
    final long = jsonMap["data"]["long"] ?? 0;

    return UserInformModel(
      jsonMap["data"]["_id"] as String,
      jsonMap["data"]["phone"] as String,
      jsonMap["data"]["firstname"] as String,
      jsonMap["data"]["lastname"] as String,
      jsonMap["data"]["registerdate"] as int,
      jsonMap["data"]["dateofbirth"] as int,
      jsonMap["data"]["gender"] as int,
      jsonMap["data"]["relationship"] as int,
      lat * 1.0,
      long * 1.0,
      jsonMap["data"]["username"] as String,
      jsonMap["data"]["email"] as String,
      avatars,
      listFavorite,
    );
  }

  factory UserInformModel.fromNotifyData(Map<String, dynamic> jsonMap) {
    final listJson = jsonMap['favorites'] as List<dynamic>;
    final List<FavoriteModel> listFavorite = [];
    /*listJson.forEach((element) =>
    {if (element != null) listFavorite.add(FavoriteModel.fromJson(element))});*/
    final listAvatar = jsonMap["avatar"] as List<dynamic>;
    final List<String> avatars = [];
    final lat = jsonMap["lat"] ?? 0;
    final long = jsonMap["long"] ?? 0;
    listAvatar.forEach((e) {
      avatars.add(e as String);
    });

    return UserInformModel(
      jsonMap["_id"] as String,
      jsonMap["phone"] as String,
      jsonMap["firstname"] as String,
      jsonMap["lastname"] as String,
      jsonMap["registerdate"] as int,
      jsonMap["dateofbirth"] as int,
      jsonMap["gender"] as int,
      jsonMap["relationship"] as int,
      lat * 1.0,
      long * 1.0,
      jsonMap["username"] as String,
      jsonMap["email"] as String,
      avatars,
      listFavorite,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "data": {
        'username': username,
        'email': email,
        "_id": id,
        "phone": phone,
        "firstname": firstname,
        "lastname": lastname,
        "registerdate": registerdate,
        "dateofbirth": dateofbirth,
        "gender": gender,
        "relationship": relationship,
        "avatar": avatar
      }
    };
  }
}
