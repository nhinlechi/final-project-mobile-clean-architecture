import 'dart:developer';

import 'package:equatable/equatable.dart';

class UserLogin extends Equatable {
  final String token;
  final String firstName;
  final String userId;
  final bool isNewUser;

  UserLogin(this.token, this.firstName, this.userId, this.isNewUser);

  @override
  List<Object> get props => [token, firstName, userId, isNewUser];

  factory UserLogin.fromJson(Map<String, dynamic> jsonMap) {
    //log(jsonMap.toString() + " UserLoginFromJson");
    final user = UserLogin(
        jsonMap["data"]["token"] as String,
        jsonMap["data"]["first_name"] as String,
        jsonMap['data']['userid'] as String,
        jsonMap['data']['isNewUser'] as bool);
    //log("[data][token] UserLoginFromJson");
    //log(jsonMap["data"]["token"] as String);
    return user;
  }

  Map<String, dynamic> toJson() {
    log("UserLogin ToJson");
    return {
      "data": {
        'token': token,
        'first_name': firstName,
        'userid' : userId,
      }
    };
  }
}
