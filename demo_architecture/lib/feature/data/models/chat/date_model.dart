import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/data/models/place/place_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:flutter/cupertino.dart';

class DateModel {
  final String id;
  final String matchId;
  final List<ParticipantModel> participants;
  final int datingTime;
  final int status;
  final PlaceEntity place;

  DateModel({
    @required this.id,
    @required this.matchId,
    @required this.participants,
    @required this.datingTime,
    @required this.status,
    @required this.place,
  });

  factory DateModel.fromJson(Map<String, dynamic> jsonMap) {
    return DateModel(
      id: jsonMap['_id'],
      matchId: jsonMap['matchid'],
      participants: ParticipantModel.getListParticipantsFromJson(
        jsonMap['participants'],
      ),
      datingTime: jsonMap['datetime'],
      status: jsonMap['status'],
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['placeid']), //?????????????????????????????
      ),
    );
  }

  static List<DateModel> listDatesFromJson(Map<String, dynamic> jsonMap) {
    final List<DateModel> rs = [];
    (jsonMap['data'] as List).forEach((e) {
      final date = DateModel.fromJson(e);
      if (date.status != -1) rs.add(date);
    });
    return rs;
  }
}
