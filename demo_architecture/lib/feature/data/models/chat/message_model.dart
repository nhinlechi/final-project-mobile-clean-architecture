import 'package:flutter/foundation.dart';

class MessageData {
  static const String TEXT = 'text';
  static const String IMAGE = 'image';
  static const String TYPING = 'typing';

  final String message;
  final String type;

  MessageData({@required this.message, @required this.type});

  factory MessageData.fromJson(jsonData) {
    return MessageData(message: jsonData['message'], type: jsonData['type']);
  }
}

class MessageModel {
  final List<MessageData> messages;
  final String userId;
  final String userName;
  final String avatar;
  final int createdAt;
  final int index;

  MessageModel({
    @required this.messages,
    @required this.userId,
    this.userName,
    @required this.avatar,
    this.createdAt = 0,
    this.index,
  });

  factory MessageModel.initial() {
    return MessageModel(
      messages: [
        MessageData(
          message: 'Say Hello',
          type: MessageData.TEXT,
        )
      ],
      userId: null,
      avatar: null,
    );
  }

  factory MessageModel.fromJson(jsonData) {
    return MessageModel(
      messages: [MessageData.fromJson(jsonData['data'])],
      userId: jsonData['id'],
      userName: jsonData['firstname'] + jsonData['lastname'],
      avatar: jsonData['avatar'][0],
      createdAt: jsonData['created'],
    );
  }

  factory MessageModel.fromTypingEventJson(jsonData) {
    return MessageModel(
      messages: null,
      userId: jsonData['id'],
      userName: jsonData['firstname'] + jsonData['lastname'],
      avatar: jsonData['avatar'][0],
    );
  }

  factory MessageModel.fromApiJson(jsonData) {
    final List rawMessages = jsonData['data'];
    List<MessageData> messages = [];
    for (var mess in rawMessages) {
      messages.add(MessageData.fromJson(mess));
    }

    return MessageModel(
      messages: messages,
      userId: jsonData['sender'],
      userName: /*jsonData['firstname'] + jsonData['lastname']*/ null,
      avatar: jsonData['avatar'] != null ? jsonData['avatar'][0] : null,
      index: jsonData['index'],
      createdAt: jsonData['created'],
    );
  }

  static List<MessageModel> listFromApiJson(jsonData) {
    List<MessageModel> rs = [];
    final messages = jsonData['data'] as List;
    for (int i = 0; i < messages.length; i++) {
      rs.add(MessageModel.fromApiJson(messages[i]));
    }
    return rs;
  }
}
