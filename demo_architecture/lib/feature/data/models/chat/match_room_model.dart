import 'package:demo_architecture/feature/data/models/chat/message_model.dart';
import '../../../../core/utils/convertToUserInform.dart';
import '../user/user_inform.dart';
import '../../../domain/entities/user/user_inform.dart';
import 'package:flutter/cupertino.dart';

class MatchRoomModel {
  final List<UserInform> participants;
  final String matchId;
  final DateTime createTime;
  final MessageModel lastMessage;

  MatchRoomModel({
    @required this.participants,
    @required this.matchId,
    this.createTime,
    this.lastMessage,
  });

  factory MatchRoomModel.fromJson(Map<String, dynamic> jsonMap) {
    List<UserInform> participants = [];
    participants = (jsonMap['participants'] as List)
        .map((e) =>
            ConvertUserInform.convert(UserInformModel.fromJson({'data': e})))
        .toList();
    return MatchRoomModel(
      participants: participants,
      matchId: jsonMap['_id'],
      createTime: DateTime.fromMicrosecondsSinceEpoch(jsonMap['created']),
      lastMessage: jsonMap['messages'] != null && jsonMap['messages'].length > 0
          ? MessageModel.fromApiJson(jsonMap['messages'][0])
          : MessageModel.initial(),
    );
  }

  static List<MatchRoomModel> listFromJson(Map<String, dynamic> jsonMap) {
    final List<MatchRoomModel> rs = [];
    jsonMap['data'].forEach((e) {
      rs.add(MatchRoomModel.fromJson(e));
    });
    return rs;
  }
}
