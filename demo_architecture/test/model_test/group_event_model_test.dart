import 'package:demo_architecture/feature/data/models/group/group_event_model.dart';
import 'package:demo_architecture/feature/data/models/place/place_model.dart';
import 'package:demo_architecture/feature/domain/entities/place/place_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../mock_json/group_event_json.dart';

void main() {
  test('test group event model from json', () {
    final jsonMap = mockGroupEventJson['data'];
    final groupEvent = GroupEventModel.fromJson(jsonMap['events'][0]);
    final expectGroupEvent = GroupEventModel(
      participants: [
        ParticipantModel(
          avatar:
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/38e1011f-b9cc-4ecb-aa21-78feaa12cc47",
          id: "60ae6345aa91cc2fb0a815a8",
          firstName: "Ngoc",
        ),
      ],
      id: "60c01cee549cc2001708b1e7",
      status: 0,
      title: "testing group event",
      description: "ok it's work",
      eventTime: 1623195547434,
      groupId: "60b7937adfb4ee0017fb133f",
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['events'][0]['placeid']),
      ),
    );
    expect(groupEvent, expectGroupEvent);
  });

  test('test list group event model from json', () {
    // GIVE
    final jsonMap = mockGroupEventJson;
    final expectGroupEvent = GroupEventModel(
      participants: [
        ParticipantModel(
          avatar:
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/38e1011f-b9cc-4ecb-aa21-78feaa12cc47",
          id: "60ae6345aa91cc2fb0a815a8",
          firstName: "Ngoc",
        ),
      ],
      id: "60c01cee549cc2001708b1e7",
      status: 0,
      title: "testing group event",
      description: "ok it's work",
      eventTime: 1623195547434,
      groupId: "60b7937adfb4ee0017fb133f",
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['data']['events'][0]['placeid']),
      ),
    );
    final expectListGroupEvent = ListGroupEventModel(
      groupEvents: [expectGroupEvent],
      total: 1,
    );

    // WHEN
    final groupEvents = ListGroupEventModel.fromJson(jsonMap);

    // THEN
    expect(groupEvents, expectListGroupEvent);
  });

  test('test list group event model is have upcoming event', () {
    // GIVE
    final jsonMap = mockGroupEventJson;

    // WHEN
    final groupEvents = ListGroupEventModel.fromJson(jsonMap);

    // THEN
    expect(groupEvents.isHaveUpcomingEvent, true);
  });

  test('test get upcoming event in list', () {
    // GIVE
    final jsonMap = mockGroupEventJson;
    final expectGroupEvent = GroupEventModel(
      participants: [
        ParticipantModel(
          avatar:
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/38e1011f-b9cc-4ecb-aa21-78feaa12cc47",
          id: "60ae6345aa91cc2fb0a815a8",
          firstName: "Ngoc",
        ),
      ],
      id: "60c01cee549cc2001708b1e7",
      status: 0,
      title: "testing group event",
      description: "ok it's work",
      eventTime: 1623195547434,
      groupId: "60b7937adfb4ee0017fb133f",
      place: PlaceEntity.fromModel(
        PlaceModel.fromJson(jsonMap['data']['events'][0]['placeid']),
      ),
    );

    // WHEN
    final groupEvents = ListGroupEventModel.fromJson(jsonMap);

    // THEN
    expect(groupEvents.upcomingEvent, expectGroupEvent);
  });
}
