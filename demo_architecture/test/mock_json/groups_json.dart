final Map<String, dynamic> mockGroupJson = {
  "avatar":
      "https://storage.googleapis.com/final-project-togethers-app.appspot.com/4628fefb-442c-4bee-bb50-eae5effc1768",
  "description": "aaaa",
  "tag": [
    {"_id": "60902a1c8ae10c00177c1a4c", "name": "Demo tag2"},
    {"_id": "60902a1c8ae10c00177c1a4d", "name": "Demo tag3"}
  ],
  "participant": [],
  "isprivate": true,
  "star": 0,
  "_id": "60b7937adfb4ee0017fb133f",
  "message": [],
  "host": "60ae6345aa91cc2fb0a815a8",
  "name": "Nhom 1",
  "createdAt": 1622643578907,
  "__v": 0
};
