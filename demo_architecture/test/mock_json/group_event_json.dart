final Map<String, dynamic> mockGroupEventJson = {
  "result": true,
  "message": "Query data success",
  "data": {
    "total": 1,
    "events": [
      {
        "participants": [
          {
            "avatar": [
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/38e1011f-b9cc-4ecb-aa21-78feaa12cc47",
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/7f5c2f97-d5cd-4b96-87b4-ae0d2b163dab",
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/07774cb5-c865-445f-bfaf-00bcbe8a2322",
              "https://storage.googleapis.com/final-project-togethers-app.appspot.com/d79e1ddd-66da-4eda-a61b-5a988944417b"
            ],
            "_id": "60ae6345aa91cc2fb0a815a8",
            "firstname": "Ngoc"
          }
        ],
        "date": 1623195547434,
        "status": 0,
        "_id": "60c01cee549cc2001708b1e7",
        "name": "testing group event",
        "description": "ok it's work",
        "placeid": {
          "address":
              "Số 6 Đường 84 Cao Lỗ, Phường 4, Quận 8, Thành phố Hồ Chí Minh, Việt Nam",
          "photos": [
            "https://lh3.googleusercontent.com/p/AF1QipMrQF9804zxF2HkRMAixIvjYnZ8OPzKa6UpKBhq=s1600-w2448-h3264"
          ],
          "rating": 3.3333333333333335,
          "user_ratings_total": 9,
          "_id": "604c89b7fa976f49008e0914",
          "location": {"lat": 10.7411245, "lng": 106.675795},
          "icon":
              "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/restaurant-71.png",
          "name": "Nhà Hàng Chú Tám",
          "type": "restaurant",
          "__v": 0
        },
        "groupid": "60b7937adfb4ee0017fb133f",
        "__v": 0
      }
    ]
  },
  "err": null
};
